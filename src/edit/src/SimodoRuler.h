#ifndef SIMODORULER_H
#define SIMODORULER_H

#include <QMap>
#include <QVector>
#include <QDir>

#include "ListReporter.h"
#include "codeeditor/RuleData.h"

#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/GrammarManagement.h"

class SimodoRuler
{
    QString                         _data_dir;
    QString                         _work_dir;
    ListReporter                    _reporter;
    simodo::dsl::GrammarManagement  _grammar_manager;
    QMap<QString,RuleData>          _rulename_to_ruledata_map;

    QVector<QPair<QString,QString>> _filemask_to_rulename_map;
    /// \todo Replace QPair with std:tuple, add the 'uint64 statistics' counter,
    /// save it in the settings and sort it in descending order after loading to optimize the search.

public:
    SimodoRuler() = delete;

    SimodoRuler(QString data_dir, QString work_dir, BufferedListWidget * messageList);

    bool loadRulesFromPath(QDir dir);

    simodo::dsl::GrammarManagement & grammar_manager() { return _grammar_manager; }

    RuleData *findRuledataByFilename(QString filename);
    QString findRulenameByFilename(QString filename);

protected:
    void ErrorReport(QString text);

private:
    bool loadRule(const QFileInfo & file_info);
};

#endif // SIMODORULER_H
