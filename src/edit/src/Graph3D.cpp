#include "Graph3D.h"

#include <QList>

#include <algorithm>
#include <vector>
//#include <map>

using namespace QtDataVisualization;

Graph3D::Graph3D(QDockWidget * dock_widget, CommonConfig & config)
    : _dock_widget(dock_widget)
    , _config(config)
{
//    axisX()->setTitle("X");
//    axisY()->setTitle("Y");
//    axisZ()->setTitle("Z");
    axisZ()->setReversed(true);

    activeTheme()->setType(Q3DTheme::Theme(_config.graph3d_theme));

    scene()->activeCamera()->setCameraPosition(_config.graph3d_X, _config.graph3d_Y, _config.graph3d_zoom);

    _theme_action_group = new QActionGroup(this);

    setReflection(false);
    setShadowQuality(QAbstract3DGraph::ShadowQualityNone);
}

void Graph3D::createThemesActions(QMenu *theme_menu)
{
    static const std::vector<std::pair<int,QString>> thems {
        {Q3DTheme::Theme::ThemeQt,              "&Qt"},
        {Q3DTheme::Theme::ThemePrimaryColors,   "&Primary Colors"},
        {Q3DTheme::Theme::ThemeDigia,           "&Digia"},
        {Q3DTheme::Theme::ThemeStoneMoss,       "&Stone Moss"},
        {Q3DTheme::Theme::ThemeArmyBlue,        "&Army Blue"},
        {Q3DTheme::Theme::ThemeRetro,           "&Retro"},
        {Q3DTheme::Theme::ThemeEbony,           "&Ebony"},
        {Q3DTheme::Theme::ThemeIsabelle,        "&Isabelle"},
    };

    for(auto [i,name] : thems)
    {
        QAction *a = new QAction();

        a->setText(name);
        a->setData(i);
        a->setCheckable(true);
        a->setChecked(_config.graph3d_theme == i);
        connect(a, &QAction::triggered, this,
                [this](bool )
                {
                    const QAction *action = qobject_cast<const QAction *>(sender());
                    if (action == nullptr)
                        return;

                    int theme = action->data().toInt();

                    _config.graph3d_theme = theme;

                    activeTheme()->setType(Q3DTheme::Theme(theme));
                });
        _theme_action_group->addAction(a);
        theme_menu->addAction(a);
    }

    theme_menu->setStatusTip(tr("Select graph3d color theme"));
}

void Graph3D::handleInitialization(QString title)
{
    for(auto s : seriesList())
    {
        removeSeries(s);

        delete s;
    }

    _series_point_counts.clear();

    setTitle(title);

    _dock_widget->setVisible(true);
}

void Graph3D::handleAddSeries(QString series_name, int mesh)
{
    QScatter3DSeries * series = new QScatter3DSeries();

    series->setName(series_name);
    series->setMesh(static_cast<QAbstract3DSeries::Mesh>(mesh));

    addSeries(series);

    if (auto it=_series_point_counts.find(series_name); it == _series_point_counts.end())
    {
        _series_point_counts[series_name] = 1000;
    }
}

void Graph3D::handleAddPoint(Graph3DPointVector points)
{
    for (auto & point : points)
    {
        auto series_list = seriesList();
        auto point_scatter_comparator = [&point](const QScatter3DSeries * s) -> bool { return point.first == s->name(); };
        auto s_it = std::find_if(series_list.begin(), series_list.end(), point_scatter_comparator);

        if (s_it == series_list.end())
        {
            handleAddSeries(point.first, QAbstract3DSeries::Mesh::MeshPoint);
            series_list = seriesList();
            s_it = std::find_if(series_list.begin(), series_list.end(), point_scatter_comparator);
        }

        auto * s = *s_it;

        s->dataProxy()->addItem(point.second);

        int size = s->dataProxy()->array()->size();
        s->setSelectedItem(size - 1);

        if (auto it = _series_point_counts.find(point.first); it != _series_point_counts.end())
        {
            if (it->second != 0 && size > it->second)
            {
                s->dataProxy()->removeItems(0, size - it->second);
            }
        }
    }
}

void Graph3D::handleSetPointsCount(QString series_name, int count)
{
    _series_point_counts[series_name] = count;
}
