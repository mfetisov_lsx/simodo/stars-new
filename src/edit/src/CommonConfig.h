#ifndef COMMONCONFIG_H
#define COMMONCONFIG_H

#include <QString>
#include <QStringList>
#include <QVector>

#include "codeeditor/CodeEditorColorTheme.h"

#include "SimodoRuler.h"

struct DecorTheme
{
    QString group;
    QString name;
    QString displayname;
    QString copyright;
    QString license;
    QString description;
    QString sourceurl;
    QString help;
    QString icon;
    QString stylesheet;
    QString iconstheme;
    QString highlight;
    bool    active = false;
};

enum class AppChangeResult
{
    NoActionRequired,
    EditorUpdateRequired,
    ReloadRequired,
    AnErrorHasOccurred
};

class MainWindow;
class QListWidget;

struct CommonConfig
{
    QListWidget * error_list= nullptr;

    // Loading from Settings:

    QString data_dir        = ".";
    QString working_dir     = ".";
    QString decor_theme_name= "default";
    int     tab_size        = 4;
    int     base_font_size  = 10;
    int     curr_font_size  = 10;
    bool    text_wrapping   = true;
    bool    statusbar_viewing=true;
    bool    scope_view      = true;
    bool    completer       = true;

    bool    text_tab_replacement = true;
    bool    text_auto_indent     = true;

    bool    spell_english   = true;
    bool    spell_national  = true;
    bool    spell_ids       = true;
    /// @note Параметр подсветки семантики я специально не стал добавлять в сохранение,
    /// чтобы стимулировать пользователей сообщать об ошибках в семантическом анализаторе.
    /// (Михаил Фетисов)
    bool    highlight_semantics = true;

    int     chart_theme         = 0;
    bool    chart_antialiasing  = true;
    int     chart_legent        = 0;

    int     graph3d_theme   = 0;
    float   graph3d_X       = 0.0;
    float   graph3d_Y       = 0.0;
    float   graph3d_zoom    = 100.0;

    // Loading from common.json:

    QString editor_font_name        = "";
    QString help_main_page_url      = "";

    QString terminal                = "xterm-256color";

    QString spell_path              = "";
    QString spell_english_dict_name = "en_US";
    QString spell_national_dict_name= "ru_RU";
    QString spell_encoding          = "KOI8-R";

    QMap<int,QString>       terminal_color_schemes;

    QStringList             hidden_names;

    QStringList             decor_themes_groups;
    QVector<DecorTheme>     decor_themes;

    QStringList             default_icons_paths;

    CodeEditorColorTheme    editor_color_theme;

    CodeEditorColorTheme setupDefaultHighlightTheme() const;

    void setErrorList(QListWidget * el) { error_list = el; }


    AppChangeResult setDecorTheme(QString theme_name);
    void readCommonJson();
    bool readHighlightTheme(QString highlight_theme_name, CodeEditorColorTheme & ct);
    QTextCharFormat::UnderlineStyle getUnderlineStyle(QString underline_style_name);
    QTextCharFormat getTextCharFormat(QJsonObject obj);
    int getFontWeight(QString sw);

protected:
    void ErrorReport(QString text);
};

#endif // COMMONCONFIG_H
