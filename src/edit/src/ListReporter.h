#ifndef LISTREPORTER_H
#define LISTREPORTER_H

#include <QObject>

#include "BufferedListWidget.h"
#include "simodo/dsl/AReporter.h"
#include "codeeditor/ErrorInfo.h"

class MdiChild;
class QListWidgetItem;

class ListReporter : public QObject, public simodo::dsl::AReporter
{
    Q_OBJECT

    MdiChild *  _editor;
    bool        _need_icons;

public:
    ListReporter(QObject * parent, MdiChild * editor, bool need_icons);
    ListReporter(MdiChild * editor, bool need_icons);

    virtual void report(const simodo::dsl::SeverityLevel level,
                        simodo::dsl::TokenLocation loc,
                        const std::u16string & briefly,
                        const std::u16string & atlarge) override;

signals:
    void addItem(QListWidgetItem * item);
};

#endif // LISTREPORTER_H
