#ifndef MDICHILD_H
#define MDICHILD_H

#include "codeeditor/CodeEditor.h"

#include <QMutex>

class MainWindow;

class MdiChild : public CodeEditor
{
    friend class WorkObject;

    Q_OBJECT

public:
    MdiChild(CodeEditorColorTheme ct, SpellChecker &checker, MainWindow * parent);
    ~MdiChild();

    void newFile(QString filePath="");
    bool loadFile(const QString &fileName);
    bool save();
    bool saveAs();
    QString saveFile(const QString &fileName);
    bool saveFileAs(const QString &fileName);
    QString userFriendlyCurrentFile();
    QString currentFile();
    QMultiMap<int,ErrorInfo> & error_info();

    void resetSemantics(std::vector<simodo::dsl::SemanticName> name_set,
                        std::vector<simodo::dsl::SemanticScope> scope_set);

    bool runnable();
    void execute(bool needExecute = true);

    void resetCurrentFileName(QString filePath);

    void setClose(bool flag = true)
    { _closeFlag = flag; }

signals:
    void onClose();

protected:
    virtual void timerEvent(QTimerEvent *event) override;
    virtual void closeEvent(QCloseEvent *event) override;
    virtual void focusInEvent(QFocusEvent * event) override;
    virtual void focusOutEvent(QFocusEvent * event) override;
    virtual void wheelEvent(QWheelEvent *e) override;

    virtual const QString currentFileName() const override { return current_file.fileName(); }

    virtual std::vector<simodo::dsl::SemanticName> getNameSet() override;
    virtual std::vector<simodo::dsl::SemanticScope> getScopeSet() override;

private slots:
    void documentWasModified();

private:
    static constexpr char TEMP_FILE_POSTFIX[] = "~";

    bool _closeFlag = true;

    void saveTempFile(const QString & file_name);
    void saveTempFile();
    void removeTempFile(const QString & file_name);
    void removeTempFile();
    bool isTempFileExists(const QString & file_name);
    bool isTempFileExists();
    bool loadTempFile();
    bool loadTempFile(const QString & file_name);

    QString tempFile();
    QString tempFile(const QString & file_name);
    QString _loadFile(const QString & file_name);
    bool maybeSave();
    void setCurrentFile(const QString &fileName);
    QString strippedName(const QString &fullFileName);
    void startThisTimer(int interval);
    void killThisTimer();

    std::vector<simodo::dsl::SemanticName>   _name_set;
    std::vector<simodo::dsl::SemanticScope>  _scope_set;
    QMutex       _semantic_mutex;
    MainWindow * main_window;
    QFile        current_file;
    QMultiMap<int,ErrorInfo> _error_info;
    int          timerID;
    bool         isUntitled;
};

#endif // MDICHILD_H
