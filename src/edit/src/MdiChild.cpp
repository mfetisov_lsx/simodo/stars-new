#include "MdiChild.h"
#include "MainWindow.h"
#include "ListReporter.h"
#include "src/codeeditor/LineNumberArea.h"

#include <QApplication>
#include <QMutexLocker>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QMessageBox>

MdiChild::MdiChild(CodeEditorColorTheme ct, SpellChecker &checker, MainWindow * parent)
    : CodeEditor(ct,
                 {
                    parent->config().tab_size,
                    parent->config().scope_view,
                    parent->config().completer,
                    parent->config().text_tab_replacement,
                    parent->config().text_auto_indent,
                    parent->config().spell_english,
                    parent->config().spell_national,
                    parent->config().spell_ids,
                    parent->config().highlight_semantics
                 },
                 checker,
                 _error_info)
    , main_window(parent)
    , timerID(0)
{
    QFont font;

    if (main_window->config().editor_font_name.isEmpty())
        font = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    else
        font.setFamily(main_window->config().editor_font_name);

    font.setFixedPitch(true);
    font.setPointSize(main_window->config().curr_font_size);
    setFont(font);

    setAttribute(Qt::WA_DeleteOnClose);
    isUntitled = true;

    setTabStopDistance(QFontMetricsF(font).horizontalAdvance(' ') * main_window->config().tab_size);

    setWordWrapMode(main_window->config().text_wrapping ? QTextOption::WordWrap : QTextOption::NoWrap);

    connect(this, SIGNAL(cursorPositionChanged()), parent, SLOT(showLineNumber()));
}

MdiChild::~MdiChild()
{
    killThisTimer();
}

void MdiChild::newFile(QString file_path)
{
    if (file_path.isEmpty())
    {
        static int sequenceNumber = 1;

        isUntitled = true;
        current_file.setFileName(tr("document%1.txt").arg(sequenceNumber++));
        setWindowTitle(currentFile() + "[*]");
    }
    else
    {
        setCurrentFile(file_path);
    }

    connect(document(), &QTextDocument::contentsChanged, this, &MdiChild::documentWasModified);
}

QString MdiChild::_loadFile(const QString & file_name)
{
    QFile file(file_name);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return file.errorString();
    }

    //    setupLexicalParameters(fileName);

    QTextStream in(&file);
    in.setCodec("UTF-8");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    setPlainText(in.readAll());
    QApplication::restoreOverrideCursor();

    return {};
}

static int loadBackupMessageBox()
{
    QMessageBox msg_box;
    msg_box.setWindowTitle(QObject::tr("SIMODO"));
    msg_box.setText(QObject::tr("The document has backup."));
    msg_box.setInformativeText(QObject::tr("Do you want to load backup?"));
    msg_box.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msg_box.setDefaultButton(QMessageBox::Yes);

    return msg_box.exec();
}

static int loadBackupMessageBox2()
{
    QMessageBox msg_box;
    msg_box.setWindowTitle("SIMODO");
    msg_box.setText(QObject::tr("Discarded backup can't be recovered. Confirm to discard backup."));
    msg_box.setInformativeText(QObject::tr("Are you sure do discard backup?"));
    msg_box.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
    msg_box.setDefaultButton(QMessageBox::No);

    return msg_box.exec();
}

bool MdiChild::loadTempFile()
{
    return loadTempFile(currentFile());
}

bool MdiChild::loadTempFile(const QString & file_name)
{
    if (isTempFileExists(file_name)
        && (loadBackupMessageBox() == QMessageBox::Yes || loadBackupMessageBox2() == QMessageBox::No)
        )
    {
        if (QString error_string = _loadFile(tempFile(file_name)); !error_string.isEmpty())
        {
            QMessageBox::warning(this
                                , tr("SIMODO")
                                , tr("Cannot read temp file %1:\n%2. Original file will be loaded.")
                                    .arg(tempFile(file_name))
                                    .arg(error_string)
                                );
        } else {
            return true;
        }
    }

    return false;
}

bool MdiChild::loadFile(const QString &file_name)
{
    if (file_name.endsWith(TEMP_FILE_POSTFIX))
    {
        QMessageBox::critical(this
                                , tr("SIMODO")
                                , tr("It is not allowed to open backup files.")
                                );
        return false;
    }

    bool temp_file_loaded = false;
    if (temp_file_loaded = loadTempFile(file_name); !temp_file_loaded)
    {
        if (QString error_string = _loadFile(file_name); !error_string.isEmpty())
        {
            QMessageBox::warning(this
                                , tr("SIMODO")
                                , tr("Cannot read file %1:\n%2.")
                                .arg(file_name)
                                .arg(error_string));
            return false;
        }
    }

    if (QFile file(file_name); !file.exists())
    {
        if (!file.open(QFile::WriteOnly))
        {
            return false;
        } else {
            file.close();
        }
        file.close();
    }

    setCurrentFile(file_name);
    document()->setModified(temp_file_loaded);

    updateTextStructure();

    connect(document(), &QTextDocument::contentsChanged, this, &MdiChild::documentWasModified);

    killThisTimer();

    if (rule_data() != nullptr && !rule_data()->grammar_name().isEmpty())
    {
        startThisTimer(0);
    }

    return true;
}

void MdiChild::saveTempFile()
{
    saveTempFile(currentFile());
}

void MdiChild::saveTempFile(const QString & file_name)
{
    saveFile(file_name + TEMP_FILE_POSTFIX);
}

void MdiChild::removeTempFile()
{
    removeTempFile(currentFile());
}

void MdiChild::removeTempFile(const QString & file_name)
{
    QFile temp_file(file_name + TEMP_FILE_POSTFIX);
    if (temp_file.exists())
    {
        temp_file.remove();
    }
}

bool MdiChild::isTempFileExists()
{
    return isTempFileExists(currentFile());
}

bool MdiChild::isTempFileExists(const QString & file_name)
{
    return QFile(tempFile(file_name)).exists();
}

QString MdiChild::tempFile()
{
    return tempFile(currentFile());
}

QString MdiChild::tempFile(const QString & file_name)
{
    return file_name + TEMP_FILE_POSTFIX;
}

bool MdiChild::save()
{
    if (isUntitled)
        return saveAs();
    else
        return saveFileAs(currentFile());
}

bool MdiChild::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), currentFile());
    if (fileName.isEmpty())
        return false;

    return saveFileAs(fileName);
}

QString MdiChild::saveFile(const QString &file_name)
{
    QFile file(file_name);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return file.errorString();
    }

    QTextStream out(&file);
    out.setCodec("UTF-8");
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << toPlainText();
    QApplication::restoreOverrideCursor();

    return {};
}

bool MdiChild::saveFileAs(const QString &file_name)
{
    performTabReplacement();
    if (QString error_string = saveFile(file_name); !error_string.isEmpty())
    {
        QMessageBox::warning(this
                            , tr("SIMODO")
                            , tr("Cannot write file %1:\n%2.").arg(QDir::toNativeSeparators(file_name)
                                                                    , error_string
                                                                    )
                            );
        return false;
    }
    
    // removeTempFile();
    setCurrentFile(file_name);
    return true;
}

QString MdiChild::userFriendlyCurrentFile()
{
    return strippedName(currentFile());
}

QString MdiChild::currentFile()
{
    return current_file.fileName();
}

QMultiMap<int,ErrorInfo> & MdiChild::error_info()
{
    return _error_info;
}

void MdiChild::resetSemantics(std::vector<simodo::dsl::SemanticName> name_set,
                              std::vector<simodo::dsl::SemanticScope> scope_set)
{
    QMutexLocker locker(&_semantic_mutex);

    _name_set = name_set;
    _scope_set = scope_set;
}

std::vector<simodo::dsl::SemanticName> MdiChild::getNameSet()
{
    QMutexLocker locker(&_semantic_mutex);

    return _name_set;
}

std::vector<simodo::dsl::SemanticScope> MdiChild::getScopeSet()
{
    QMutexLocker locker(&_semantic_mutex);

    return _scope_set;
}

bool MdiChild::runnable()
{
    return rule_data() != nullptr && !rule_data()->interpreter_handler_name().isEmpty();
}

void MdiChild::execute(bool needExecute)
{
    if (rule_data() == nullptr || (needExecute && rule_data()->interpreter_handler_name().isEmpty()))
        return;

    main_window->setRunActionEnabled(false);
    main_window->getEditorMsgList()->clear();
    if (needExecute)
    {
        main_window->getExecuteMsgList()->clear();
    }
//    main_window->getExecuteMsgWindow()->setVisible(true);

    emit main_window->startExecution(this, needExecute);
}

void MdiChild::resetCurrentFileName(QString file_path)
{
    bool temp_file_existed = isTempFileExists();
    if (temp_file_existed)
    {
        removeTempFile();
    }

    current_file.setFileName(QFileInfo(file_path).canonicalFilePath());
    isUntitled = false;
    document()->setModified(false);
    setWindowTitle(userFriendlyCurrentFile() + "[*]");
    setRuleData(main_window->simodo_ruler().findRuledataByFilename(QFileInfo(file_path).fileName()));
    rehighlight();
    update();

    if (temp_file_existed)
    {
        saveTempFile();
    }
}

void MdiChild::timerEvent(QTimerEvent * event)
{
    if (event->timerId() != timerID)
    {
        // Странно удалять таймер, который не был зарегистрирован этим объектом
        // killTimer(event->timerId());
        CodeEditor::timerEvent(event);
        return;
    }

    qDebug() << "Timer ID:" << event->timerId() << ", starting working thread...";

    killThisTimer();

    if (document()->isModified())
    {
        saveTempFile();
    }

    if (rule_data() != nullptr && !rule_data()->grammar_name().isEmpty())
    {
        /// @todo Не очень корректно управлять подсветкой синтаксиса исскуственных языков параметром,
        /// определяющим проверку правописания естественных языков :-).
//        if (parameters().id_spell_checking_requared)
//        { main_window->run(false); }
//        else
//        {
//            updateSemanticInfo();
//            rehighlight();
//            update();
//        }

        main_window->run(false);
        updateSemanticInfo();
        rehighlight();
        update();
    }
}

void MdiChild::closeEvent(QCloseEvent * event)
{
    if (_closeFlag)
    {
        if (maybeSave())
        {
            removeTempFile();
        }
        event->accept();
        return;
    }

    emit onClose();
    event->ignore();
}

void MdiChild::focusInEvent(QFocusEvent * event)
{
    if(event->gotFocus())
    {
        connect(document(), &QTextDocument::undoAvailable, main_window->edit_undo_action(), &QAction::setEnabled);
        connect(document(), &QTextDocument::redoAvailable, main_window->edit_redo_action(), &QAction::setEnabled);

        connect(main_window->edit_undo_action(), &QAction::triggered, this, &QPlainTextEdit::undo);
        connect(main_window->edit_redo_action(), &QAction::triggered, this, &QPlainTextEdit::redo);

//        emit cursorPositionChanged();
        main_window->showLineNumber(this);
        main_window->showRulename(rule_data() == nullptr ? "-" : rule_data()->name());

        main_window->setDirectoryPosition(currentFile());
    }
    CodeEditor::focusInEvent(event);
}

void MdiChild::focusOutEvent(QFocusEvent * event)
{
    if (event->lostFocus())
    {
        disconnect(document(), &QTextDocument::undoAvailable, main_window->edit_undo_action(), &QAction::setEnabled);
        disconnect(document(), &QTextDocument::redoAvailable, main_window->edit_redo_action(), &QAction::setEnabled);

        disconnect(main_window->edit_undo_action(), &QAction::triggered, this, &QPlainTextEdit::undo);
        disconnect(main_window->edit_redo_action(), &QAction::triggered, this, &QPlainTextEdit::redo);
    }
    CodeEditor::focusOutEvent(event);
}

void MdiChild::wheelEvent(QWheelEvent * event)
{
    if (event->modifiers() == Qt::ControlModifier)
    {
        if (event->angleDelta().y() > 0)
        {
            zoomIn(1);
        } else {
            zoomOut(1);
        }

        main_window->setTextZooming(font().pointSize());
    }

    CodeEditor::wheelEvent(event);
}

void MdiChild::documentWasModified()
{
    setWindowModified(document()->isModified());

    if (!isRehighlightActive())
    {
        killThisTimer();
        // if (rule_data() != nullptr && !rule_data()->grammar_name().isEmpty())
        // {
            startThisTimer(1500);
            // main_window->getGlobalMsgList()->addItem("timer started id = " + QString::number(timerID));
        // }
    }
}

bool MdiChild::maybeSave()
{
    if (!document()->isModified())
        return true;

    const QMessageBox::StandardButton ret
            = QMessageBox::warning(this
                                    , tr("SIMODO")
                                    , tr("'%1' has been modified.\n"
                                         "Do you want to save your changes?"
                                         ).arg(userFriendlyCurrentFile())
                                    , QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel
                                    );
    switch (ret)
    {
    case QMessageBox::Save:
        return save();
    case QMessageBox::Cancel:
        return false;
    default:
        break;
    }
    return true;
}

void MdiChild::setCurrentFile(const QString &file_name)
{
    resetCurrentFileName(file_name);
    setWindowModified(false);
}

QString MdiChild::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}

void MdiChild::startThisTimer(int interval)
{
    killThisTimer();

    timerID = CodeEditor::startTimer(interval);
}

void MdiChild::killThisTimer()
{
    if (timerID != 0)
    {
        CodeEditor::killTimer(timerID);
        timerID = 0;
    }
}
