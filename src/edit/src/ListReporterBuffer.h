#ifndef LISTREPORTERBUFFER_H
#define LISTREPORTERBUFFER_H

#include <QObject>
#include <QTimerEvent>
#include <QListWidgetItem>
#include <deque>

class ListReporterBuffer :  public QObject
{
    Q_OBJECT

    bool _skip_equal;

    std::deque<QListWidgetItem *> _buffer;
    int  _timer_id;
    long _timer_start_time;

    void _killTimer();

public:
    // 1 / 60Hz = 0.01(6)s
    static constexpr int TIMER_INTERVAL = 10 * 16;
    static constexpr int BUFFER_SIZE = 2000;

    ListReporterBuffer(QObject * parent = nullptr, bool skip_equal = false);
    ~ListReporterBuffer();

    bool empty() { return _buffer.empty(); }
    size_t size() { return _buffer.size(); }

signals:
    void addBufferedItems(std::vector<QListWidgetItem *> items);

public slots:
    void addItem(QListWidgetItem * item);
    void startBufferTimer();

protected:
    void timerEvent(QTimerEvent * event) override;
};

#endif // LISTREPORTERBUFFER_H
