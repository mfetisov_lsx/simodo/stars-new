#include "MainWindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QTranslator>
#include <QLibraryInfo>
#include <QItemSelection>
#include <QListWidgetItem>
#include <QPoint>
#include <QString>
#include <QVector3D>

#include <vector>
#include <string>

Q_DECLARE_METATYPE(std::string);
Q_DECLARE_METATYPE(std::vector<QListWidgetItem *>);

using ChartPoint = std::pair<QString, QPointF>;
Q_DECLARE_METATYPE(ChartPoint);
using ChartPointVector = std::vector<ChartPoint>;
Q_DECLARE_METATYPE(ChartPointVector);

using Graph3DPoint = std::pair<QString, QVector3D>;
Q_DECLARE_METATYPE(Graph3DPoint);
using Graph3DPointVector = std::vector<Graph3DPoint>;
Q_DECLARE_METATYPE(Graph3DPointVector);

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(simgui);

    QApplication app(argc, argv);

    qRegisterMetaType<std::string>();
    qRegisterMetaType<std::vector<QListWidgetItem *>>();
    qRegisterMetaType<QItemSelection>();
    qRegisterMetaType<ChartPoint>("ChartPoint");
    qRegisterMetaType<ChartPointVector>("ChartPointVector");
    qRegisterMetaType<Graph3DPoint>("Graph3DPoint");
    qRegisterMetaType<Graph3DPointVector>("Graph3DPointVector");

    // qt translation for default dialogs (QFileDialog) and so on
    QTranslator qtTranslator2;
    qtTranslator2.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator2);

    QCoreApplication::setApplicationName("edit");
    QCoreApplication::setOrganizationName("SIMODO");
    QCoreApplication::setApplicationVersion("0.0.11");

    QCommandLineParser parser;
    parser.setApplicationDescription("SIMODO edit");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open.");
    parser.process(app);

    QStringList files;
    foreach (const QString &fileName, parser.positionalArguments())
        files.push_back(fileName);

    bool reload = true;
    int ret = 0;

    while(reload == true)
    {
        MainWindow mainWin;
        mainWin.show();

        for(auto s : files)
            mainWin.loadFile(s);

        ret = app.exec();
        reload = mainWin.reload();

        if (reload)
            files = mainWin.files();
    }

    return ret;
}
