#ifndef COCKPIT3D_H
#define COCKPIT3D_H

#include <Qt3DCore/QEntity>
#include <Qt3DExtras/Qt3DWindow>
#include <Qt3DRender/QCamera>
#include <QtCore/QObject>
#include <QtGui/QVector3D>

class Cockpit3D : public Qt3DExtras::Qt3DWindow
{
    Q_OBJECT

    static const QVector3D SUN_LIGHT_DIRECTION;
    static const QVector3D FIGHTER_INITIAL_POSITION;
    static const QVector3D CAMERA_POSITION_RELATIVE_TO_FIGHTER;

    Qt3DCore::QEntity * _root_entity;
    Qt3DRender::QCamera * _camera;
    Qt3DCore::QTransform * _fighter_transform;

    QVector3D _camera_position;
    QVector3D _fighter_position;
    QVector3D _fighter_rotation;

    void _init();
    Qt3DCore::QEntity * _createScene();
    void _updateCamera();

public:
    Cockpit3D();

    void showEvent(QShowEvent * event) override;
    void hideEvent(QHideEvent * event) override;

public slots:
    void setPosition(qreal x, qreal height, qreal z);

    // Вид из кабины вперёд
    // Тангаж -90..+90, > 0 подъём носа вверх
    // Курс 0..360, > 0 поворот вправо
    // Крен -180..+180, > 0 крен на правое крыло
    void setAngles(qreal pitch, qreal course, qreal roll);
};

#endif // COCKPIT3D_H
