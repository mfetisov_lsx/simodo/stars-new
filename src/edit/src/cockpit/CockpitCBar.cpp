#include "CockpitCBar.h"

#include <QtGui/QPen>
#include <QtGui/QBrush>
#include <QtGui/QPainter>
#include <QtGui/QVector2D>

#include <cmath>
#include <qnamespace.h>

const QString CockpitCBar::TEXT_FONT_FAMILY = "monospace";

CockpitCBar::CockpitCBar(ValueRisingDirection value_rising_direction)
    : CockpitBar(CockpitBar::ClampPolicy::ClampAbs180)
    , _value_rising_direction(value_rising_direction)
{}

void CockpitCBar::paintEvent(QPaintEvent * event)
{
    CockpitBar::paintEvent(event);

    QPen black_pen(Qt::black);
    QPen dark_blue_pen(Qt::darkBlue);
    black_pen.setWidth(2);
    dark_blue_pen.setWidth(2);
    QBrush cyan_brush(Qt::cyan);

    QPainter painter(this);
    painter.setPen(black_pen);

    QPointF center = {width() / 2.0, height() / 2.0};
    qreal side_size = std::min(width(), height()) * (1 - 2 * SIDE_PADDING_MULTIPLIER);
    QRectF circule_rect = {center.x() - side_size / 2, center.y() - side_size / 2, side_size, side_size};

    int directed_value = value() * (_value_rising_direction == ValueRisingDirection::Clockwise ? -1 : 1);
    int angle_degrees = directed_value + 180;
    qreal angle_radians = M_PI / 180.0 * (angle_degrees);
    QPointF arrow_point = {circule_rect.x() + circule_rect.width() * (std::cos(angle_radians) + 1) / 2
                            , circule_rect.y() + circule_rect.height() * (1 - (std::sin(angle_radians) + 1) / 2)};
    // QPointF mirror_point = {circule_rect.x() + circule_rect.width() * (1 - (std::cos(angle_radians) + 1) / 2)
                            // , circule_rect.y() + circule_rect.height() * (std::sin(angle_radians) + 1) / 2};
    qreal padding = (std::min(width(), height()) - side_size) / 2;
    QRectF arrow_circule = {arrow_point.x() - padding, arrow_point.y() - padding, padding * 2, padding * 2};
    // QRectF mirror_circule = {mirror_point.x() - padding, mirror_point.y() - padding, padding * 2, padding * 2};

    QRect text_rect = {int(center.x() - side_size / 4), int(center.y() - side_size / 4), int(side_size / 2), int(side_size / 2)};

    painter.drawEllipse(circule_rect);
    
    painter.setBrush(cyan_brush);
    painter.drawPie(circule_rect, 180 * 16, directed_value * 16);
    painter.drawPie(circule_rect, 0, directed_value * 16);

    painter.drawEllipse(arrow_circule);
    // painter.drawEllipse(mirror_circule);

    painter.setFont(CockpitBarUtils::fitTextToRect(QString::number(value()), QFont(TEXT_FONT_FAMILY), text_rect));
    painter.setPen(dark_blue_pen);
    painter.drawText(text_rect, Qt::AlignCenter, QString::number(value()));
}
