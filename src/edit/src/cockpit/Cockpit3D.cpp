#include "Cockpit3D.h"

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>
#include <Qt3DExtras/QDiffuseSpecularMaterial>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QDirectionalLight>
#include <Qt3DRender/QMesh>
#include <Qt3DRender/QTexture>
#include <Qt3DRender/QTextureImage>
#include <QtCore/QVariant>
#include <QtGui/QColor>

#include <cmath>

const QVector3D Cockpit3D::SUN_LIGHT_DIRECTION = {-0.5, -1.0, 0.5};
const QVector3D Cockpit3D::CAMERA_POSITION_RELATIVE_TO_FIGHTER = {0.0, 2.0, 0.0};
const QVector3D Cockpit3D::FIGHTER_INITIAL_POSITION = {0.0, 10.0, 0.0};

Cockpit3D::Cockpit3D()
    : _root_entity(nullptr)
    , _camera(nullptr)
    , _fighter_transform(nullptr)
    , _camera_position(FIGHTER_INITIAL_POSITION + CAMERA_POSITION_RELATIVE_TO_FIGHTER)
    , _fighter_position(FIGHTER_INITIAL_POSITION)
    , _fighter_rotation{0, 0, 0}
{
    _init();
}

void Cockpit3D::_init()
{
    _root_entity = _createScene();
    _camera = camera();
    _camera->lens()->setPerspectiveProjection(60.f, float(width()) / height(), 0.1f, 1000.f);
    setPosition(_fighter_position.x(), _fighter_position.y(), -_fighter_position.z());
    setAngles(_fighter_rotation.x(), _fighter_rotation.y(), _fighter_rotation.z());

    setRootEntity(_root_entity);
}

void Cockpit3D::showEvent(QShowEvent * event)
{
    if (_root_entity != nullptr)
    {
        setRootEntity(nullptr);
        _root_entity->deleteLater();
        _root_entity = nullptr;
    }

    _init();

    Qt3DExtras::Qt3DWindow::showEvent(event);
}

void Cockpit3D::hideEvent(QHideEvent * event)
{
    if (_root_entity != nullptr)
    {
        setRootEntity(nullptr);
        _root_entity->deleteLater();
        _root_entity = nullptr;
    }

    Qt3DExtras::Qt3DWindow::hideEvent(event);
}

Qt3DCore::QEntity * Cockpit3D::_createScene()
{
    auto * result_entity = new Qt3DCore::QEntity;

    auto * fighter_entity = new Qt3DCore::QEntity(result_entity);
    fighter_entity->setObjectName("fighter");
    auto * fighter_mesh = new Qt3DRender::QMesh(fighter_entity);
    fighter_mesh->setSource(QUrl("qrc:/models3d/fighter.obj"));

    auto * fighter_material = new Qt3DExtras::QPhongMaterial(fighter_entity);
    _fighter_transform = new Qt3DCore::QTransform(fighter_entity);

    fighter_entity->addComponent(fighter_mesh);
    fighter_entity->addComponent(_fighter_transform);
    fighter_entity->addComponent(fighter_material);

    auto * city_entity = new Qt3DCore::QEntity(result_entity);
    auto * city_mesh = new Qt3DRender::QMesh(city_entity);
    city_mesh->setSource(QUrl("qrc:/models3d/city.obj"));

    auto * city_material = new Qt3DExtras::QDiffuseSpecularMaterial(city_entity);
    auto * city_texture = new Qt3DRender::QTexture2D(city_material);
    auto * city_texture_image = new Qt3DRender::QTextureImage(city_texture);
    city_texture_image->setSource(QUrl::fromLocalFile("://models3d/city-image.png"));
    city_texture->addTextureImage(city_texture_image);

    city_material->setDiffuse(QVariant::fromValue(city_texture));
    city_material->setAmbient(QColorConstants::White); 
    city_material->setSpecular(QVariant::fromValue(QColorConstants::Black));
    city_material->setShininess(0);

    auto * city_transform = new Qt3DCore::QTransform(city_entity);

    city_entity->addComponent(city_mesh);
    city_entity->addComponent(city_transform);
    city_entity->addComponent(city_material);

    Qt3DCore::QEntity * light_entity = new Qt3DCore::QEntity(result_entity);
    Qt3DRender::QDirectionalLight * directional_light = new Qt3DRender::QDirectionalLight(light_entity);
    directional_light->setWorldDirection(SUN_LIGHT_DIRECTION);
    Qt3DCore::QTransform * light_transform = new Qt3DCore::QTransform(light_entity);

    light_entity->addComponent(directional_light);
    light_entity->addComponent(light_transform);

    return result_entity;
}

void Cockpit3D::_updateCamera()
{
    _camera_position = _fighter_position + CAMERA_POSITION_RELATIVE_TO_FIGHTER;

    Qt3DCore::QTransform camera_transform;

    QVector3D summary_camera_translation = {0, 0, 0};
    QVector3D last_camera_translation = {0, 0, 0};
    QVector3D rotate_point = -CAMERA_POSITION_RELATIVE_TO_FIGHTER;

    auto m = Qt3DCore::QTransform::rotateAround(rotate_point
                                                , _fighter_rotation.x(), {1.0, 0.0, 0.0});
    camera_transform.setMatrix(m);
    last_camera_translation = camera_transform.translation();
    rotate_point -= last_camera_translation;
    summary_camera_translation += last_camera_translation;

    m = Qt3DCore::QTransform::rotateAround(rotate_point
                                            , _fighter_rotation.y(), {0.0, 1.0, 0.0});
    camera_transform.setMatrix(m);
    last_camera_translation = camera_transform.translation();
    rotate_point -= last_camera_translation;
    summary_camera_translation += last_camera_translation;

    auto yaw = M_PI * (180 + _fighter_rotation.y()) / 180.0;
    auto pitch = M_PI * _fighter_rotation.x() / 180.0;
    QVector3D rotation_axis = {float(std::cos(pitch) * std::sin(yaw))
                    , float(std::sin(pitch))
                    , float(std::cos(pitch) * std::cos(yaw))
                    };
    m = Qt3DCore::QTransform::rotateAround(rotate_point
                                            , -_fighter_rotation.z(), rotation_axis);
    camera_transform.setMatrix(m);
    last_camera_translation = camera_transform.translation();
    summary_camera_translation += last_camera_translation;

    _camera_position = summary_camera_translation + _camera_position;
}

void Cockpit3D::setPosition(qreal x, qreal height, qreal z)
{
    auto camera_position_shift = _camera_position - _fighter_position;

    _fighter_position = {float(z), float(height), float(-x)};
    _camera_position = _fighter_position + camera_position_shift;

    if (_root_entity == nullptr)
    {
        return;
    }

    _fighter_transform->setTranslation(_fighter_position);
    _camera->setPosition(_camera_position);

    requestUpdate();
}

void Cockpit3D::setAngles(qreal pitch, qreal course, qreal roll)
{
    _fighter_rotation = {float(pitch), float(-course), float(-roll)};
    _updateCamera();

    if (_root_entity == nullptr)
    {
        return;
    }

    _fighter_transform->setRotationX(_fighter_rotation.x());
    _fighter_transform->setRotationY(_fighter_rotation.y());
    _fighter_transform->setRotationZ(_fighter_rotation.z());

    _camera->setPosition(_camera_position);
    _camera->transform()->setRotation(_fighter_transform->rotation());

    requestUpdate();
}
