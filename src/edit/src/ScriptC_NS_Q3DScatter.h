#ifndef SCRIPTC_NS_Q3DSCATTER_H
#define SCRIPTC_NS_Q3DSCATTER_H

#include <vector>

#include <QObject>

#include "Graph3D.h"

#include "simodo/dsl/SemanticBase.h"

using Graph3DPoint = std::pair<QString, QVector3D>;

class ScriptC_NS_Q3DScatter : public QObject, public simodo::dsl::IScriptC_Namespace
{
    Q_OBJECT

    QDockWidget * _graph_dock;
    Graph3D *     _scatter_graph;

public:
    ScriptC_NS_Q3DScatter(QDockWidget * graph_dock=nullptr, Graph3D * scatter_graph=nullptr);
    virtual ~ScriptC_NS_Q3DScatter();

    QDockWidget * graph_dock() { return _graph_dock; }
    QtDataVisualization::Q3DScatter * scatter_graph() { return _scatter_graph; }

signals:
    void doInitialize(QString title);
    void doAddSeries(QString series_name, int mesh);
    void doAddPoint(Graph3DPoint point);
    void doSetPointsCounts(QString series_name, int count);

public:
    void init(std::u16string title);
    void addSeries(std::u16string serias_name, int mesh);
    void addPoint(std::u16string serias_name, double x, double y, double z);
    void setPointsCount(std::u16string serias_name, int count);

public:
    virtual simodo::dsl::SCI_Namespace_t getNamespace() override;

};

#endif // SCRIPTC_NS_Q3DSCATTER_H
