#ifndef GRAPH3DBUFFER_H
#define GRAPH3DBUFFER_H

#include <QObject>
#include <QTimerEvent>
#include <QListWidgetItem>
#include <deque>
#include <QVector3D>

using Graph3DPoint = std::pair<QString, QVector3D>;
using Graph3DPointVector = std::vector<Graph3DPoint>;

class Graph3DBuffer :  public QObject
{
    Q_OBJECT

    std::deque<Graph3DPoint> _buffer;
    int  _timer_id;
    long _timer_start_time;

    void _killTimer();

public:
    // 1 / 60Hz = 0.01(6)s
    static constexpr int TIMER_INTERVAL = 16;

    Graph3DBuffer(QObject * parent = nullptr);
    ~Graph3DBuffer();

    bool empty() { return _buffer.empty(); }
    size_t size() { return _buffer.size(); }

signals:
    void addBufferedItems(Graph3DPointVector items);

public slots:
    void addItem(Graph3DPoint item);
    void startBufferTimer();

protected:
    void timerEvent(QTimerEvent * event) override;
};

#endif // GRAPH3DBUFFER_H
