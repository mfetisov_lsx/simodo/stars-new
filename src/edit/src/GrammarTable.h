#ifndef GRAMMARTABLE_H
#define GRAMMARTABLE_H

#include <QTreeWidget>

#include "simodo/dsl/Grammar.h"

class MainWindow;

class GrammarTable : public QTreeWidget
{
    Q_OBJECT

    MainWindow *    _main_window;

    QAction *       _act_open_grammar_file;
    QAction *       _act_reload_grammar;
    QAction *       _act_reload_SLR;
    QAction *       _act_reload_LR1;

public:
    explicit GrammarTable(MainWindow *parent = nullptr);

    void refill();

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *e) override;

protected:
    void openGrammarFile(const QTreeWidgetItem *item) const;
    void reloadGrammar(const QTreeWidgetItem *item, simodo::dsl::TableBuildMethod builder);
};

#endif // GRAMMARTABLE_H
