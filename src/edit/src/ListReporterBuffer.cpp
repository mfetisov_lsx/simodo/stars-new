#include "ListReporterBuffer.h"

#include <chrono>
#include <algorithm>
#include <vector>

static long current_millis()
{
    auto time = std::chrono::system_clock::now(); // get the current time
    auto since_epoch = time.time_since_epoch(); // get the duration since epoch
    // I don't know what system_clock returns
    // I think it's uint64_t nanoseconds since epoch
    // Either way this duration_cast will do the right thing
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
    return millis.count(); // just like java (new Date()).getTime();
}

ListReporterBuffer::ListReporterBuffer(QObject * parent, bool skip_equal)
    : QObject(parent)
    , _skip_equal(skip_equal)
    , _timer_id(0)
{}

ListReporterBuffer::~ListReporterBuffer()
{
    _killTimer();

    for (auto p : _buffer)
    {
        delete p;
    }
}

void ListReporterBuffer::_killTimer()
{
    if (_timer_id != 0)
    {
        killTimer(_timer_id);
        _timer_id = 0;
    }
}

void ListReporterBuffer::startBufferTimer()
{
    _killTimer();

    _timer_id = startTimer(TIMER_INTERVAL);
    _timer_start_time = current_millis();
}

void ListReporterBuffer::addItem(QListWidgetItem * item)
{
    if (item != nullptr)
    {
        bool skip = _skip_equal
                    && !_buffer.empty()
                    && item->text() == _buffer.back()->text();
        if (skip)
        {
            delete item;
        }
        else
        {
            _buffer.push_back(item);

            if (_buffer.size() > BUFFER_SIZE)
            {
                delete _buffer.front();
                _buffer.pop_front();
            }
        }
    }

    if (current_millis() - _timer_start_time >= TIMER_INTERVAL)
    {
        QTimerEvent event(_timer_id);
        timerEvent(&event);
    }
}

void ListReporterBuffer::timerEvent(QTimerEvent * event)
{
    if (event->timerId() != _timer_id)
    {
        QObject::timerEvent(event);
        return;
    }

    _timer_start_time = current_millis();

    if (_buffer.empty())
    {
        return;
    }

    std::vector<QListWidgetItem *> send_buffer(_buffer.begin(), _buffer.end());
    _buffer.clear();

    emit addBufferedItems(std::move(send_buffer));
}
