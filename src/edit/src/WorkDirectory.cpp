#include "WorkDirectory.h"
#include "MainWindow.h"
#include "WorkDirectoryGetName.h"
#include "MdiChild.h"

#include <QFileSystemModel>
#include <QMenu>
#include <QToolTip>
#include <QDesktopServices>
#include <QDateTime>
#include <QMessageBox>

/// \todo reflect file system change in opening editors (may be another function?)

WorkDirectory::WorkDirectory(MainWindow *parent)
    : _main_window(parent)
{
    _fs_model = new QFileSystemModel();
    _sort_proxy = new WorkDirectorySortProxy(_main_window->hidden_names());

    _sort_proxy->setSourceModel(_fs_model);
    setModel(_sort_proxy);

    setColumnHidden(1,true);
    setColumnHidden(2,true);
    setColumnHidden(3,true);
    setAnimated(true);
    setHeaderHidden(true);
    setUniformRowHeights(true);

    setDragEnabled(true);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
    setDragDropMode(QAbstractItemView::DragDrop);
    setDragDropOverwriteMode(false);

//    setAlternatingRowColors(true);

    _fs_model->setFilter(QDir::AllEntries | QDir::Hidden | QDir::NoDotAndDotDot);
    _fs_model->setReadOnly(false);

//    QFileIconProvider * icon_provider = _fs_model->iconProvider();
//    if (icon_provider != nullptr)
//        icon_provider->setOptions(QFileIconProvider::DontUseCustomDirectoryIcons);

    connect(_fs_model, &QFileSystemModel::fileRenamed, this, &WorkDirectory::onFileRenamed);

    createActions();
}

WorkDirectory::~WorkDirectory()
{
    setModel(nullptr);

    delete _sort_proxy;
    delete _fs_model;
}

void WorkDirectory::setWorkDirectory(QString dir)
{
    QDir::setCurrent(dir);
    _fs_model->setRootPath(dir);
    setRootIndex(_sort_proxy->mapFromSource(_fs_model->index(QDir::currentPath())));
    _main_window->setWindowTitle("SIMODO - [" + dir + "]");
}

QFileInfo WorkDirectory::getFileInfo(const QModelIndex &index) const
{
    return _fs_model->fileInfo(_sort_proxy->mapToSource(index));
}

QIcon WorkDirectory::getFileIcon(QString file_path) const
{
    const QModelIndex &index = _fs_model->index(file_path);
    return _fs_model->fileIcon(index);
}

bool WorkDirectory::setSelectorPosition(QString path)
{
    if (!selectedIndexes().empty())
    {
        const QModelIndex view_index = selectedIndexes().back();

        if (view_index.isValid())
            selectionModel()->select(view_index,QItemSelectionModel::Clear|QItemSelectionModel::Deselect);
    }

    QModelIndex new_index = _sort_proxy->mapFromSource(_fs_model->index(path));

    if (!new_index.isValid())
        return false;

    selectionModel()->select(new_index,QItemSelectionModel::SelectCurrent);
    scrollTo(new_index);

    return true;
}

void WorkDirectory::createActions()
{
    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/document-open"));
    _act_open_file = new QAction(openIcon, tr("Open"), this);
    _act_open_file->setShortcuts(QKeySequence::Open);
    _act_open_file->setStatusTip(tr("Open selected file"));
    connect(_act_open_file, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
            _main_window->openEditWindow(selectedIndexes().back());
    });
    addAction(_act_open_file);

    _act_open_file_in_associated_program = new QAction(tr("Open in associated program"), this);
    _act_open_file_in_associated_program->setStatusTip(tr("Open selected file in associated program"));
    connect(_act_open_file_in_associated_program, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
        {
            QFileInfo fi = getFileInfo(selectedIndexes().back());

            if (fi.isFile())
                QDesktopServices::openUrl(QUrl("file:///"+fi.filePath(), QUrl::TolerantMode));
        }
    });
    addAction(_act_open_file);

    const QIcon docnewIcon = QIcon::fromTheme("document-new", QIcon(":/images/document-new"));
    _act_new_file = new QAction(docnewIcon, tr("Create a file in selected directory"), this);
    connect(_act_new_file, &QAction::triggered, this, &WorkDirectory::onAddFileToDirectory);
    addAction(_act_new_file);

    const QIcon foldernewIcon = QIcon::fromTheme("folder-new", QIcon(":/images/folder-new"));
    _act_new_directory = new QAction(foldernewIcon, tr("Create a directory in selected directory"), this);
    connect(_act_new_directory, &QAction::triggered, this, &WorkDirectory::onAddDirectoryToDirectory);
    addAction(_act_new_directory);

    const QIcon deleteIcon = QIcon::fromTheme("edit-delete", QIcon(":/images/edit-delete"));
    _act_delete_file = new QAction(deleteIcon, tr("Delete"), this);
    _act_delete_file->setShortcut(QKeySequence::Delete);
    connect(_act_delete_file, &QAction::triggered, this, &WorkDirectory::onDelete);
    addAction(_act_delete_file);

    const QIcon copyIcon = QIcon::fromTheme("edit-copy", QIcon(":/images/edit-copy"));
    _act_duplicate_file = new QAction(copyIcon, tr("Duplicate file"), this);
    _act_duplicate_file->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_F2));
    connect(_act_duplicate_file, &QAction::triggered, this, &WorkDirectory::onDuplicateFile);
    addAction(_act_duplicate_file);

    _act_rename_file = new QAction(tr("Rename"), this);
    _act_rename_file->setShortcut(QKeySequence(Qt::Key_F2));
    connect(_act_rename_file, &QAction::triggered, this,
            [this]()
    {
        if (!selectedIndexes().empty())
            edit(selectedIndexes().back());
    });
    addAction(_act_rename_file);

    _act_collapse_all = new QAction(tr("Collapce all"), this);
    connect(_act_collapse_all, &QAction::triggered, this, [this](){ collapseAll(); });
    addAction(_act_collapse_all);

    _act_expand_all = new QAction(tr("Expand all"), this);
    connect(_act_expand_all, &QAction::triggered, this, [this](){ expandAll(); });
    addAction(_act_expand_all);

//    const QIcon root_docnew_icon = QIcon::fromTheme("document-new", QIcon(":/images/document-new"));
    _act_root_new_file = new QAction(/*root_docnew_icon,*/ tr("Create a file in WORK directory"), this);
    connect(_act_root_new_file, &QAction::triggered, this, [this](){onAddFileToDirectory(true);});
    addAction(_act_root_new_file);

//    const QIcon root_foldernew_icon = QIcon::fromTheme("folder-new", QIcon(":/images/folder-new"));
    _act_root_new_directory = new QAction(/*root_foldernew_icon,*/ tr("Create a directory in WORK directory"), this);
    connect(_act_root_new_directory, &QAction::triggered, this, [this](){onAddDirectoryToDirectory(true);});
    addAction(_act_root_new_directory);

}

void WorkDirectory::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    if (!selectedIndexes().empty())
    {
        QFileInfo fi = getFileInfo(selectedIndexes().back());

        if (fi.isDir())
        {
            menu.addAction(_act_new_file);
            menu.addAction(_act_new_directory);
            menu.addSeparator();
        }
        else
        {
            menu.addAction(_act_open_file);
            menu.addAction(_act_open_file_in_associated_program);
            menu.addSeparator();
            menu.addAction(_act_duplicate_file);
        }

        menu.addAction(_act_rename_file);
        menu.addAction(_act_delete_file);
        menu.addSeparator();
    }
    menu.addAction(_act_collapse_all);
    menu.addAction(_act_expand_all);
    menu.addSeparator();
    menu.addAction(_act_root_new_file);
    menu.addAction(_act_root_new_directory);

    menu.exec(event->globalPos());
}

void WorkDirectory::mouseDoubleClickEvent(QMouseEvent *e)
{
    const QModelIndex view_index = indexAt(e->pos());

    if (!view_index.isValid())
        return;

    QFileInfo fi = getFileInfo(view_index);

    if (fi.isDir())
    {
        if (isExpanded(view_index))
            collapse(view_index);
        else
            expand(view_index);
    }
    else
        _main_window->openEditWindow(view_index);

    e->ignore();
}

bool WorkDirectory::event(QEvent *event)
{
    if (event->type() == QEvent::ToolTip)
    {
        QHelpEvent * e = static_cast<QHelpEvent *>(event);

        const QModelIndex view_index = indexAt(e->pos());

        if (!view_index.isValid())
            return true;

        QModelIndex fs_index = _sort_proxy->mapToSource(view_index);

        qint64 size = _fs_model->size(fs_index);
        QString size_str;

        if (size > 100000000)
            size_str = QString(tr("%1 MB")).arg(size/1000000);
        else if (size > 100000)
            size_str = QString(tr("%1 KB")).arg(size/1000);
        else if (size == 0)
            size_str = "";
        else
            size_str = QString(tr("%1 B")).arg(size);

        QString text =  "<i>file:</i> <b>" + _fs_model->fileName(fs_index) + "</b><br>" +
                        tr("<i>type:</i> ") + _fs_model->type(fs_index) + "<br>" +
                        ((size == 0) ? "" : (tr("<i>size:</i> ") + size_str + "<br>")) +
                        tr("<i>modified:</i> ") + _fs_model->lastModified(fs_index).toString("yyyy-MM-dd ddd HH:mm t");

        if (!text.isEmpty())
        {
            QToolTip::showText(e->globalPos(), text);
        }
        else
        {
            QToolTip::hideText();
            event->ignore();
        }

        return true;
    }
    return QTreeView::event(event);
}

void WorkDirectory::onFileRenamed(const QString &path, const QString &oldName, const QString &newName)
{
    if (MdiChild *existing = _main_window->findMdiChildCanonicalPath(path + "/" + oldName);
        existing != nullptr)
    {
        existing->resetCurrentFileName(path + "/" +newName);
    }
}

void WorkDirectory::onAddFileToDirectory(bool root)
{
    if (selectedIndexes().empty() && !root)
        return;

    QString path;

    if (root)
    {
        path = _main_window->config().working_dir;
    }
    else
    {
        const QModelIndex view_index = selectedIndexes().back();
        if (!view_index.isValid())
            return;

        const QModelIndex fs_index = _sort_proxy->mapToSource(view_index);
        if (!fs_index.isValid())
            return;

        QFileInfo fi = _fs_model->fileInfo(fs_index);
        if (fi.fileName().isEmpty() || !fi.isDir())
            return;

        path = fi.filePath();
    }
//    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), fi.filePath());
//    if (fileName.isEmpty())
//        return;

    WorkDirectoryGetName getter(this);

    getter.setIntro("Input file name:");

    int ret = getter.exec();

    if (ret == 1)
    {
        QString name = path + "/" + getter.getText();
        if (name.isEmpty())
            return;

        MdiChild *child = _main_window->createMdiChild(QIcon());

        if (child->saveFileAs(name))
        {
            child->newFile(name);
            child->show();
        }
        else
        {
            child->close();
        }
    }
}

void WorkDirectory::onAddDirectoryToDirectory(bool root)
{
    if (selectedIndexes().empty() && !root)
        return;

    QModelIndex view_index;
    QModelIndex fs_index;

    if (root)
    {
        fs_index = _fs_model->index(_main_window->config().working_dir);
        if (!fs_index.isValid())
            return;

        view_index = _sort_proxy->mapFromSource(fs_index);
        if (!view_index.isValid())
            return;
    }
    else
    {
        view_index = selectedIndexes().back();
        if (!view_index.isValid())
            return;

        fs_index = _sort_proxy->mapToSource(view_index);
        if (!fs_index.isValid())
            return;
    }

    QFileInfo fi = _fs_model->fileInfo(fs_index);
    if (fi.fileName().isEmpty() || !fi.isDir())
        return;

    WorkDirectoryGetName getter(this);

    getter.setIntro("Input file name:");

    int ret = getter.exec();

    if (ret == 1)
    {
        QString name = getter.getText();
        if (name.isEmpty())
            return;

        QModelIndex fs_new_dir_index = _fs_model->mkdir(fs_index,name);

        if (fs_new_dir_index.isValid())
        {
            QModelIndex view_new_dir_index = _sort_proxy->mapFromSource(fs_new_dir_index);
            if (view_new_dir_index.isValid())
            {
                expand(view_index);
                selectionModel()->select(view_index,QItemSelectionModel::Clear|QItemSelectionModel::Deselect);
                selectionModel()->select(view_new_dir_index,QItemSelectionModel::SelectCurrent);
            }
        }
    }
}

void WorkDirectory::onDuplicateFile()
{
    if (selectedIndexes().empty())
        return;

    const QModelIndex view_index = selectedIndexes().back();

    if (!view_index.isValid())
        return;

    const QModelIndex fs_index = _sort_proxy->mapToSource(view_index);

    if (!fs_index.isValid())
        return;

    QFileInfo fi = _fs_model->fileInfo(fs_index);

    if (fi.fileName().isEmpty() || !fi.isFile())
        return;

    int copy_count = 1;
    const int copy_limit = 100;
    QString fileName;

    for(; copy_count < copy_limit; ++copy_count)
    {
        fileName = fi.canonicalPath() + "/" +
                   fi.completeBaseName() + tr("~%1").arg(copy_count) + "." +
                   fi.suffix();

        if(!QFile::exists(fileName))
            break;
    }

    if (copy_count < copy_limit)
    {
        if (!QFile::copy(fi.filePath(),fileName))
        {
            QMessageBox::critical(this, tr("SIMODO"),
                                  tr("Failed to copy file '%1'").arg(fileName),
                                  QMessageBox::Ok);
        }
    }
}

void WorkDirectory::onDelete()
{
    if (selectedIndexes().empty())
        return;

    const QModelIndex view_index = selectedIndexes().back();

    if (!view_index.isValid())
        return;

    const QModelIndex fs_index = _sort_proxy->mapToSource(view_index);

    if (!fs_index.isValid())
        return;

    QFileInfo fi = _fs_model->fileInfo(fs_index);

    if (fi.fileName().isEmpty())
        return;

    QString   item_type = fi.isDir() ? "Directory" : "File";
    QString   item_name = fi.fileName();

    if (QMessageBox::Yes == QMessageBox::warning(this, tr("SIMODO"),
                                       tr("%1 '%2' will be completely deleted.\nAre you sure?").arg(item_type).arg(item_name),
                                       QMessageBox::Yes | QMessageBox::No))
    {
        bool ok = false;

        if (fi.isDir())
            ok = _fs_model->rmdir(fs_index);
        else if (fi.isFile())
            ok = _fs_model->remove(fs_index);

        if (!ok)
        {
            QMessageBox::critical(this, tr("SIMODO"),
                                  tr("Failed to delete '%1'").arg(item_name),
                                  QMessageBox::Ok);
            return;
        }

        if (fi.isFile())
        {
            if (MdiChild *existing = _main_window->findMdiChildCanonicalPath(fi.filePath()); existing != nullptr)
            {
                emit existing->document()->contentsChanged();
            }
        }
    }
}

/*
** WorkDirectorySortProxy
*/

WorkDirectorySortProxy::WorkDirectorySortProxy(QStringList hidden_names)
    : QSortFilterProxyModel(nullptr)
    , _hidden_names(hidden_names)
{
}

bool WorkDirectorySortProxy::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QFileSystemModel *fsm = qobject_cast<QFileSystemModel*>(sourceModel());
//    bool asc = sortOrder() == Qt::AscendingOrder ? true : false;

    QFileInfo lfi = fsm->fileInfo(left);
    QFileInfo rfi = fsm->fileInfo(right);

    if (lfi.isDir() != rfi.isDir())
        return lfi.isDir();

    return lfi.fileName() < rfi.fileName();
}

bool WorkDirectorySortProxy::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);

    for(const auto & hidden : _hidden_names)
        if (sourceModel()->data(index0).toString() == hidden)
            return false;

    return QSortFilterProxyModel::filterAcceptsRow(sourceRow,sourceParent);
}
