#ifndef WORKDIRECTORY_H
#define WORKDIRECTORY_H

#include <QTreeView>
#include <QFileInfo>
#include <QSortFilterProxyModel>

QT_BEGIN_NAMESPACE
class QFileSystemModel;
QT_END_NAMESPACE

class WorkDirectorySortProxy : public QSortFilterProxyModel
{
    Q_OBJECT

    QStringList _hidden_names;

public:
    WorkDirectorySortProxy(QStringList hidden_names);

    virtual bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
};

class MainWindow;

class WorkDirectory : public QTreeView
{
    Q_OBJECT

    MainWindow *    _main_window;

    QFileSystemModel * _fs_model;
    WorkDirectorySortProxy * _sort_proxy;

    QAction *       _act_open_file;
    QAction *       _act_open_file_in_associated_program;
    QAction *       _act_new_file;
    QAction *       _act_new_directory;
    QAction *       _act_delete_file;
    QAction *       _act_duplicate_file;
    QAction *       _act_rename_file;

    QAction *       _act_collapse_all;
    QAction *       _act_expand_all;
    QAction *       _act_root_new_file;
    QAction *       _act_root_new_directory;

public:
    WorkDirectory(MainWindow *parent);
    virtual ~WorkDirectory() override;

    void        setWorkDirectory(QString dir);
    QFileInfo   getFileInfo(const QModelIndex & index) const;
    QIcon       getFileIcon(QString file_path) const;
    bool        setSelectorPosition(QString path);

protected:
    void        createActions();

    virtual void contextMenuEvent(QContextMenuEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *e) override;
    virtual bool event(QEvent *event) override;

private slots:
    void onFileRenamed(const QString &path, const QString &oldName, const QString &newName);
    void onAddFileToDirectory(bool root=false);
    void onAddDirectoryToDirectory(bool root=false);
    void onDuplicateFile();
    void onDelete();
};

#endif // WORKDIRECTORY_H
