#ifndef BUFFEREDLISTWIDGET_H
#define BUFFEREDLISTWIDGET_H

#include <QListWidget>
#include <vector>
#include <deque>
#include <QTimerEvent>
#include <QListWidgetItem>
#include <array>

/*
 * @attention Для удаления элементов использовать ТОЛЬКО
 * метод BufferedListWidget::clear (затирает метод базового класса), потому что
 * этот метод очистит виджет, не нарушая целостность внутреннего буфера
 */
class BufferedListWidget : public QListWidget
{
    static constexpr int BUFFER_SIZE = 2000;

private:
    bool _autoscroll;
    int _show_timer_id;
    int _erase_timer_id;

    std::array<QListWidgetItem, BUFFER_SIZE> _items;

    std::deque<QListWidgetItem *> _input_buffer;
    std::deque<QListWidgetItem *> _to_delete_buffer;

    void _eraseTimerEvent();
    void _showTimerEvent();
public:
    // 1 / 60Hz = 0.01(6)s
    static constexpr int HZ_60_INTERVAL = 16;
    static constexpr int SEC_1_INTERVAL = 1000;
    static constexpr int N_ITEMS_TO_SHOW = 200;

    BufferedListWidget(QWidget * parent = nullptr, bool autoscroll = false);
    ~BufferedListWidget();

    void timerEvent(QTimerEvent * event) override;

    void clear();

public slots:
    void addBufferedItem(QListWidgetItem * item);
    void addBufferedItems(std::vector<QListWidgetItem *> items);
};

#endif // BUFFEREDLISTWIDGET_H
