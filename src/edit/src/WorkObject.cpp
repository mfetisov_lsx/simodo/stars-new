#include "WorkObject.h"
#include "MdiChild.h"
#include "MainWindow.h"
#include "ScriptC_NS_Chart.h"
#include "ScriptC_NS_Q3DScatter.h"
#include "ScriptC_NS_Cockpit.h"

#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/Parser.h"
#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_NS_TableFunction.h"
#include "simodo/dsl/ScriptC_Semantics.h"
#include "simodo/dsl/FuzeParser.h"
#include "simodo/dsl/GrammarBuilder_SLR.h"
#include "simodo/dsl/GrammarBuilder_LR1.h"
#include "simodo/dsl/ModulesManagement.h"

#include "ListReporterBuffer.h"
#include "ChartBuffer.h"
#include "Graph3DBuffer.h"
#include <QThread>

WorkObject::WorkObject(QObject * parent)
    : QObject(parent)
    , _active_machine(nullptr)
{
}

void WorkObject::doWork(MdiChild * editor, bool needExecute)
{
    std::vector<simodo::dsl::SemanticName>   name_set;
    std::vector<simodo::dsl::SemanticScope>  scope_set;

    QString grammar_name = editor->rule_data() == nullptr ? "NULL" : editor->rule_data()->grammar_name();
    QString semantic_name = editor->rule_data() == nullptr ? "NULL" : editor->rule_data()->semantic_handler_name();
    QString machine_name = editor->rule_data() == nullptr ? "NULL" : editor->rule_data()->interpreter_handler_name();

    if (_active_machine != nullptr)
    {
        qDebug() << "Couldn't run on active machine '" << machine_name << "' (from thread)";
        emit workIsOver(editor, false);
        return;
    }

    editor->error_info().clear();

    if(semantic_name == "fuze")
    {
        QThread                     work_object_event_loop_thread;
        ListReporter                parser_msg(editor, true);
        auto *                      parser_msg_buffer = new ListReporterBuffer(nullptr, true);
        parser_msg_buffer->moveToThread(&work_object_event_loop_thread);

        connect(&work_object_event_loop_thread, &QThread::started,  parser_msg_buffer, &ListReporterBuffer::startBufferTimer);
        connect(&work_object_event_loop_thread, &QThread::finished, parser_msg_buffer, &ListReporterBuffer::deleteLater);
        connect(&parser_msg, &ListReporter::addItem, parser_msg_buffer, &ListReporterBuffer::addItem);
        connect(parser_msg_buffer, &ListReporterBuffer::addBufferedItems, editor->main_window->getEditorMsgList(), &BufferedListWidget::addBufferedItems);

        work_object_event_loop_thread.start();

        std::vector<simodo::dsl::GrammarRuleTokens>  rules;
        simodo::dsl::Token          main_rule;
        std::set<std::string>       dummy_import;
        QFileInfo                   fi(editor->currentFile());
        QString                     grammar_dir;
        
        if (fi.absoluteDir() == QDir(editor->main_window->config().data_dir + "/grammar"))
        {
            grammar_dir = QDir(editor->main_window->config().data_dir + "/grammar").absolutePath();
        } else {
            grammar_dir = fi.absoluteDir().absolutePath();
        }
        
        // QDir                        data_grammar_dir(editor->main_window->config().data_dir + "/grammar");
        // std::string                 grammar_path = fi.path().toStdString();
        QString                     grammar_name = fi.baseName();
        simodo::dsl::FuzeParser     fuze(parser_msg, grammar_dir.toStdString(), grammar_name.toStdString(), dummy_import);
        std::u16string              buffer = editor->toPlainText().toStdU16String();
        simodo::dsl::BufferStream   buffer_stream(buffer.data());
        simodo::dsl::Grammar        g;

        bool ok = fuze.parse(buffer_stream, rules, main_rule, g.handlers, g.lexical);

        if (ok)
        {
            simodo::dsl::TableBuildMethod method = editor->main_window->grammar_manager().getGrammar(grammar_name.toStdString()).build_method;

            std::unique_ptr<simodo::dsl::GrammarBuilder>  builder;

            if (method == simodo::dsl::TableBuildMethod::SLR)
                builder = std::make_unique<simodo::dsl::GrammarBuilder_SLR>(parser_msg,
                                                                            grammar_dir.toStdString(),
                                                                            grammar_name.toStdString(),
                                                                            g);
            else
                builder = std::make_unique<simodo::dsl::GrammarBuilder_LR1>(parser_msg,
                                                                            grammar_dir.toStdString(),
                                                                            grammar_name.toStdString(),
                                                                            g);

            ok = builder->build(rules, main_rule);

            name_set.emplace_back(simodo::dsl::TokenLocation(u""),
                                  u"ID",
                                  simodo::dsl::SemanticNameQualification::Function,
                                  simodo::dsl::SemanticNameType::Undefined,
                                  simodo::dsl::SemanticName::SemanticNameContext::Grammar);
            name_set.emplace_back(simodo::dsl::TokenLocation(u""),
                                  u"NUMBER",
                                  simodo::dsl::SemanticNameQualification::Function,
                                  simodo::dsl::SemanticNameType::Undefined,
                                  simodo::dsl::SemanticName::SemanticNameContext::Grammar);
            name_set.emplace_back(simodo::dsl::TokenLocation(u""),
                                  u"ANNOTATION",
                                  simodo::dsl::SemanticNameQualification::Function,
                                  simodo::dsl::SemanticNameType::Undefined,
                                  simodo::dsl::SemanticName::SemanticNameContext::Grammar);

            for(const simodo::dsl::GrammarRuleTokens & r : rules)
            {
                name_set.emplace_back(simodo::dsl::Token(r.production.getLocation().file_name).getLocation(),
                                      r.production.getToken(),
                                      simodo::dsl::SemanticNameQualification::None,
                                      simodo::dsl::SemanticNameType::Undefined,
                                      simodo::dsl::SemanticName::SemanticNameContext::Grammar);

                for(const simodo::dsl::Token & p : r.pattern)
                    if (p.getType() == simodo::dsl::LexemeType::Punctuation
                     && p.getQualification() == simodo::dsl::TokenQualification::None)
                            name_set.emplace_back(simodo::dsl::Token(p.getLocation().file_name).getLocation(),
                                                  p.getLexeme(),
                                                  simodo::dsl::SemanticNameQualification::Scalar,
                                                  simodo::dsl::SemanticNameType::Undefined,
                                                  simodo::dsl::SemanticName::SemanticNameContext::Grammar);
            }

            if (ok)
                ok = builder->checkGrammarAstBlocks(name_set, scope_set);

            editor->resetSemantics(name_set, scope_set);
        }

        while (!parser_msg_buffer->empty())
        {
            QThread::msleep(ListReporterBuffer::TIMER_INTERVAL);
        }
        work_object_event_loop_thread.quit();
        work_object_event_loop_thread.wait();

        emit workIsOver(editor, ok);
        return;
    }

    simodo::dsl::GrammarManagement & gm = editor->main_window->grammar_manager();

    if (!gm.loadGrammar(false, grammar_name.toStdString()))
    {
        qDebug() << "Couldn't load grammar '" << grammar_name << "' (from thread)";
        emit workIsOver(editor, false);
        return;
    }

    const simodo::dsl::Grammar & g = gm.getGrammar(grammar_name.toStdString());

    QThread                     work_object_event_loop_thread;
    ListReporter                parser_msg(editor, true);
    auto *                      parser_msg_buffer = new ListReporterBuffer(nullptr, true);
    parser_msg_buffer->moveToThread(&work_object_event_loop_thread);

    connect(&work_object_event_loop_thread, &QThread::started,   parser_msg_buffer, &ListReporterBuffer::startBufferTimer);
    connect(&work_object_event_loop_thread, &QThread::finished,  parser_msg_buffer, &ListReporterBuffer::deleteLater);
    connect(&parser_msg, &ListReporter::addItem, parser_msg_buffer, &ListReporterBuffer::addItem);
    connect(parser_msg_buffer, &ListReporterBuffer::addBufferedItems, editor->main_window->getEditorMsgList(), &BufferedListWidget::addBufferedItems);

    ListReporter                executor_msg(nullptr, false);
    auto *                      executor_msg_buffer = new ListReporterBuffer;
    executor_msg_buffer->moveToThread(&work_object_event_loop_thread);

    connect(&work_object_event_loop_thread, &QThread::started,  executor_msg_buffer, &ListReporterBuffer::startBufferTimer);
    connect(&work_object_event_loop_thread, &QThread::finished, executor_msg_buffer, &ListReporterBuffer::deleteLater);
    connect(&executor_msg, &ListReporter::addItem, executor_msg_buffer, &ListReporterBuffer::addItem);
    connect(executor_msg_buffer, &ListReporterBuffer::addBufferedItems, editor->main_window->getExecuteMsgList(), &BufferedListWidget::addBufferedItems);

    auto *                      chart_buffer = new ChartBuffer;
    chart_buffer->moveToThread(&work_object_event_loop_thread);
    connect(&work_object_event_loop_thread, &QThread::started, chart_buffer, &ChartBuffer::startBufferTimer);
    connect(&work_object_event_loop_thread, &QThread::finished, chart_buffer, &ChartBuffer::deleteLater);

    auto *                      graph3d_buffer = new Graph3DBuffer;
    graph3d_buffer->moveToThread(&work_object_event_loop_thread);
    connect(&work_object_event_loop_thread, &QThread::started, graph3d_buffer, &Graph3DBuffer::startBufferTimer);
    connect(&work_object_event_loop_thread, &QThread::finished, graph3d_buffer, &Graph3DBuffer::deleteLater);

    work_object_event_loop_thread.start();

    simodo::dsl::Parser        p(editor->currentFile().toStdString(),parser_msg,g);
    simodo::dsl::AstNode       ast(editor->currentFile().toStdU16String());
    simodo::dsl::AstBuilder    builder(parser_msg, ast, g.handlers);
    std::u16string             buffer = editor->toPlainText().toStdU16String();
    simodo::dsl::BufferStream  buffer_stream(buffer.data());

    if (!p.parse(buffer_stream,builder))
    {
        while (!parser_msg_buffer->empty())
        {
            QThread::msleep(ListReporterBuffer::TIMER_INTERVAL);
        }
        work_object_event_loop_thread.quit();
        work_object_event_loop_thread.wait();

        emit workIsOver(editor, false);
        return;
    }

    if (semantic_name.isEmpty())
    {
        while (!parser_msg_buffer->empty())
        {
            QThread::msleep(ListReporterBuffer::TIMER_INTERVAL);
        }
        work_object_event_loop_thread.quit();
        work_object_event_loop_thread.wait();

        emit workIsOver(editor, true);
        return;
    }

    simodo::dsl::ScriptC_NS_Sys   sys(executor_msg);
    simodo::dsl::ScriptC_NS_Math  math;
    simodo::dsl::ScriptC_NS_TableFunction tf;

    ScriptC_NS_Chart              chart(editor->main_window->chart_dock(), editor->main_window->chart_view());
    ScriptC_NS_Q3DScatter         graph3d(editor->main_window->graph3d_dock(), editor->main_window->scatter_graph());
    ScriptC_NS_Cockpit            cockpit(editor->main_window->cockpit_view(), editor->main_window->cockpit3d());

    std::vector<std::pair<std::u16string,simodo::dsl::IScriptC_Namespace*>> additional_namespaces {
        {u"chart", &chart},
        {u"cockpit", &cockpit},
        {u"graph3d", &graph3d}
    };
    simodo::dsl::ModulesManagement mm(parser_msg, executor_msg, &gm, "", nullptr, additional_namespaces);

    if (!needExecute && !semantic_name.isEmpty())
    {
        bool res = true;

        if (editor->main_window->isSemanticHighlight())
        {
            simodo::dsl::ScriptC_Semantics checker(parser_msg, mm, name_set, scope_set);

            checker.importNamespace(u"sys", sys.getNamespace());
            checker.importNamespace(u"math", math.getNamespace());
            checker.importNamespace(u"tf", tf.getNamespace());
            checker.importNamespace(u"scene", simodo::dsl::ScriptC_NS_Scene().getNamespace());
            checker.importNamespace(u"chart", chart.getNamespace());
            checker.importNamespace(u"graph3d", graph3d.getNamespace());
            checker.importNamespace(u"cockpit", cockpit.getNamespace());

            res = checker.check(ast);
        }

        editor->resetSemantics(name_set, scope_set);

        while (!parser_msg_buffer->empty() || !executor_msg_buffer->empty())
        {
            QThread::msleep(ListReporterBuffer::TIMER_INTERVAL);
        }
        work_object_event_loop_thread.quit();
        work_object_event_loop_thread.wait();

        emit workIsOver(editor, res);
        return;
    }

    simodo::dsl::ScriptC_Interpreter machine(parser_msg, mm);
    
    simodo::dsl::ScriptC_NS_Scene scene(&machine);

    connect(&chart, &ScriptC_NS_Chart::doAddPoint, chart_buffer, &ChartBuffer::addItem);
    connect(chart_buffer, &ChartBuffer::addBufferedItems, editor->main_window->chart_view(), &ChartView::handleAddPoint);
    connect(&graph3d, &ScriptC_NS_Q3DScatter::doAddPoint, graph3d_buffer, &Graph3DBuffer::addItem);
    connect(graph3d_buffer, &Graph3DBuffer::addBufferedItems, editor->main_window->scatter_graph(), &Graph3D::handleAddPoint);

    machine.importNamespace(u"sys", sys.getNamespace());
    machine.importNamespace(u"math", math.getNamespace());
    machine.importNamespace(u"tf", tf.getNamespace());
    machine.importNamespace(u"scene", scene.getNamespace());
    machine.importNamespace(u"chart", chart.getNamespace());
    machine.importNamespace(u"graph3d", graph3d.getNamespace());
    machine.importNamespace(u"cockpit", cockpit.getNamespace());

    _active_machine = &machine;

    simodo::dsl::SCI_RunnerCondition res = machine.catchAst(ast);

    _active_machine = nullptr;

    while (!parser_msg_buffer->empty() || !executor_msg_buffer->empty()
            || !chart_buffer->empty() || !graph3d_buffer->empty())
    {
        QThread::msleep(2 * ListReporterBuffer::TIMER_INTERVAL);
    }
    QThread::msleep(2 * ListReporterBuffer::TIMER_INTERVAL);
    work_object_event_loop_thread.quit();
    work_object_event_loop_thread.wait();

    emit workIsOver(editor, res != simodo::dsl::SCI_RunnerCondition::Regular);
}

void WorkObject::stopExecution()
{
    if (_active_machine != nullptr)
        _active_machine->stop();
}
