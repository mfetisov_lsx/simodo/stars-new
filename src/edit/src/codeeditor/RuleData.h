#ifndef RULEDATA_H
#define RULEDATA_H

#include <QStringList>
#include <QVector>
#include <QPair>

#include "simodo/dsl/Tokenizer.h"

class RuleData
{
    QString     _name;
    QStringList _file_masks;
    QVector<QPair<QString,QString>> _run_command_set;
    int         _collapsible_starts_block_number;
    simodo::dsl::LexicalParameters _lexis;
    std::vector<std::u16string> _error_format_words;
    std::vector<std::u16string> _keyword_format_words;
    std::vector<std::u16string> _module_format_words;
    std::vector<std::u16string> _parameter_format_words;
    std::vector<std::u16string> _type_format_words;
    std::vector<std::u16string> _tuple_format_words;
    std::vector<std::u16string> _function_format_words;
    QString     _grammar_name;
    QString     _semantic_handler_name;
    QString     _interpreter_handler_name;
    bool        _prevent_text_tab_replacement;

public:
    RuleData() = delete;

//    RuleData & operator = (const RuleData &) = delete;

    RuleData(QString name
                , QStringList file_masks
                , QVector<QPair<QString,QString>> run_command_set
                , int collapsible_starts_block_number
                , simodo::dsl::LexicalParameters lexis
                , std::vector<std::u16string> error_format_words
                , std::vector<std::u16string> keyword_format_words
                , std::vector<std::u16string> module_format_words
                , std::vector<std::u16string> parameter_format_words
                , std::vector<std::u16string> type_format_words
                , std::vector<std::u16string> tuple_format_words
                , std::vector<std::u16string> function_format_words
                , QString grammar_name
                , QString semantic_handler_name
                , QString interpreter_handler_name
                , bool prevent_text_tab_replacement
                );

    QString name() const { return _name; }
    const QStringList & file_masks() const { return _file_masks; }
    const QVector<QPair<QString,QString>> & run_command_set() const { return _run_command_set; }
    int collapsible_starts_block_number() const { return _collapsible_starts_block_number; }
    const simodo::dsl::LexicalParameters & lexis() const { return _lexis; }
    const std::vector<std::u16string> & error_format_words() const { return _error_format_words; }
    const std::vector<std::u16string> & keyword_format_words() const { return _keyword_format_words; }
    const std::vector<std::u16string> & module_format_words() const { return _module_format_words; }
    const std::vector<std::u16string> & parameter_format_words() const { return _parameter_format_words; }
    const std::vector<std::u16string> & type_format_words() const { return _type_format_words; }
    const std::vector<std::u16string> & tuple_format_words() const { return _tuple_format_words; }
    const std::vector<std::u16string> & function_format_words() const { return _function_format_words; }
    QString grammar_name() const { return _grammar_name; }
    QString semantic_handler_name() const { return _semantic_handler_name; }
    QString interpreter_handler_name() const { return _interpreter_handler_name; }
    bool prevent_text_tab_replacement() const { return _prevent_text_tab_replacement; }
};

#endif // RULEDATA_H
