#include "TextBlockData.h"

using namespace std;
using namespace simodo::dsl;

TextBlockData::TextBlockData(vector<Token> tokens, std::vector<Token> spell_errors)
    : _tokens(tokens)
    , _spell_errors(spell_errors)
{
}

bool TextBlockData::isContextStart() const
{
    if (_tokens.empty())
        return false;

    if (_tokens.back().getLocation().context != NO_TOKEN_CONTEXT_INDEX)
        if (_tokens.size() > 1 || _tokens.front().getToken().size() != _tokens.front().getLexeme().size())
            return true;

    return false;
}

bool TextBlockData::isContextInside(QTextBlock block) const
{
    if (!block.previous().isValid())
        return false;

    if (block.previous().userState() == -1)
        return false;

    return !isContextEnd();
}

bool TextBlockData::isContextEnd() const
{
    if (_tokens.empty())
        return false;

    return (_tokens.size() > 1 || _tokens.back().getLocation().context == NO_TOKEN_CONTEXT_INDEX);
}

bool TextBlockData::isCollapsed(QTextBlock block) const
{
    if (isContextStart())
        return (block.next().isValid() && !block.next().isVisible());

    return (block.isValid() && block.isVisible());
}

uint32_t TextBlockData::getContext() const
{
    if (_tokens.empty())
        return NO_TOKEN_CONTEXT_INDEX;

    return _tokens.back().getLocation().context;
}
