#include "CodeEditor.h"
#include "TextBlockData.h"

#include "simodo/dsl/SemanticNameTable.h"

#include <QtCore/QDebug>

#include <QtWidgets/QWidget>
#include <QtWidgets/QToolTip>

#include <QtGui/QTextBlock>
#include <QtGui/QPainter>
#include <QtGui/QIcon>

#include <cmath>
#include <algorithm>
#include <functional>

LineNumberArea::LineNumberArea(CodeEditor *editor)
    : QWidget(editor)
    , _editor(editor)
{
    setMouseTracking(true);
}

int LineNumberArea::getAreaWidth() const
{
    return getMessageAreaWidth() + getNumberAreaWidth() + getHidingAreaWidth();
}

int LineNumberArea::getMessageAreaWidth() const
{
    return _editor->fontMetrics().height() + 2;
}

int LineNumberArea::getNumberAreaWidth() const
{
    int digits = 1;
    int max = std::max(1, _editor->blockCount());
    while (max >= 10)
    {
        max /= 10;
        ++digits;
    }

    int space = _editor->fontMetrics().horizontalAdvance(QLatin1Char('9')) * digits + 2;

    return space;
}

int LineNumberArea::getHidingAreaWidth() const
{
    if (_editor->parameters().scope_viewing)
    {
        return _editor->fontMetrics().horizontalAdvance(QLatin1Char('9')) + 2;
    }

    return 5;
}

QSize LineNumberArea::sizeHint() const
{
    return QSize(_editor->lineNumberAreaWidth(), 0);
}

void LineNumberArea::paintEvent(QPaintEvent *event)
{
    const QMultiMap<int, ErrorInfo> & error_info = _editor->_error_info;

    QPainter painter(this);
    painter.setFont(_editor->font());
    painter.fillRect(event->rect(), _editor->_color_theme.line_number_area_color);

    int message_width = getMessageAreaWidth();
    int number_width  = getNumberAreaWidth();
    int hiding_width  = getHidingAreaWidth();

    QTextCursor cursor = _editor->textCursor();

    int current_line = cursor.block().blockNumber();

    QTextBlock block = _editor->firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = int(_editor->blockBoundingGeometry(block).translated(_editor->contentOffset()).top());
    int bottom = top + int(_editor->blockBoundingRect(block).height());

    while (block.isValid() && top <= event->rect().bottom())
    {
        if (block.isVisible() && bottom >= event->rect().top())
        {
            simodo::dsl::SeverityLevel level = simodo::dsl::SeverityLevel::Information;
            auto                       it    = error_info.find(blockNumber);
            bool                       error = false;

            for( ; it != error_info.end() && it.key() == blockNumber; ++it)
                if (it.value().loc.file_name == _editor->currentFileName())
                {
                    level = std::max(level,it.value().level);
                    error = true;
                }

            if (error)
            {
                painter.fillRect(event->rect().x()
                                , top, message_width + number_width
                                , _editor->fontMetrics().height()
                                , _editor->_color_theme.line_number_area_color_error
                                );
                painter.setPen(_editor->_color_theme.line_number_color_error);

                QString level_str;

                switch(level)
                {
                case simodo::dsl::SeverityLevel::Information:
                    level_str = "information";
                    break;
                case simodo::dsl::SeverityLevel::Warning:
                    level_str = "warning";
                    break;
                default:
                    level_str = "error";
                    break;
                }

                const QIcon icon = QIcon::fromTheme("dialog-"+level_str, QIcon(":/images/dialog-"+level_str));
                QPixmap pixmap = icon.pixmap(QSize(_editor->fontMetrics().height(), _editor->fontMetrics().height()));
                painter.drawPixmap(0, top, _editor->fontMetrics().height(), _editor->fontMetrics().height(), pixmap);
            }
            else
            {
                if (current_line == blockNumber)
                    painter.setPen(_editor->_color_theme.line_number_current_line_color);
                else
                    painter.setPen(_editor->_color_theme.line_number_color);
            }

            QString number = QString::number(blockNumber + 1);
            painter.drawText(message_width, top, number_width, _editor->fontMetrics().height(), Qt::AlignRight, number);

            if (_editor->parameters().scope_viewing)
            {
                QString block_hider = " ";

                TextBlockData * data = static_cast<TextBlockData *>(block.userData());
                if (data != nullptr)
                {
                    if (data->isContextStart())
                    {
                        if (data->isCollapsed(block))
                        {
                            block_hider = "→";
                            painter.fillRect(message_width+number_width, top, hiding_width, _editor->fontMetrics().height(),
                                             _editor->_color_theme.line_number_collapsed_background_color);
                            painter.setPen(_editor->_color_theme.line_number_collapsed_foreground_color);
                        } else {
                            block_hider = "↓";
                        }
                    } else if (data->isContextInside(block))
                    {
                        block_hider = "·";
                    }
                }

                if (block_hider == " ")
                {
                    size_t index_candidate = simodo::dsl::UNDEFINED_INDEX;
                    for(size_t i=0; i < _editor->_scope_set.size(); ++i)
                    {
                        const simodo::dsl::SemanticScope & s = _editor->_scope_set[i];

                        if (s.first.file_name != _editor->currentFileName())
                            continue;

                        if (s.first.begin != 0
                            && size_t(blockNumber) + 1 >= s.first.line
                            && size_t(blockNumber) + 1 <= s.second.line)
                        {
                            if (index_candidate == simodo::dsl::UNDEFINED_INDEX)
                            {
                                index_candidate = i;
                            } else {
                                const simodo::dsl::SemanticScope & s_candidate = _editor->_scope_set[index_candidate];

                                if (s.first.line > s_candidate.first.line || s.second.line < s_candidate.second.line)
                                {
                                    index_candidate = i;
                                }
                            }
                        }
                    }

                    if (index_candidate != simodo::dsl::UNDEFINED_INDEX)
                    {
                        if (static_cast<size_t>(blockNumber)+1 == _editor->_scope_set[index_candidate].first.line)
                        {
                            QTextBlock next_block = block.next();
                            if (next_block.isValid() && !next_block.isVisible())
                            {
                                block_hider = "→";
                                painter.fillRect(message_width+number_width, top, hiding_width, _editor->fontMetrics().height(),
                                                 _editor->_color_theme.line_number_collapsed_background_color);
                                painter.setPen(_editor->_color_theme.line_number_collapsed_foreground_color);
                            } else {
                                block_hider = "↓";
                            }
                        } else {
                            block_hider = "·";
                        }
                    }
                }

                painter.drawText(message_width+number_width, top, hiding_width, _editor->fontMetrics().height(), Qt::AlignCenter, block_hider);
            }
        }

        block = block.next();
        top = bottom;
        bottom = top + static_cast<int>(_editor->blockBoundingRect(block).height());
        ++blockNumber;
    }
}

void LineNumberArea::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget::mouseReleaseEvent(event);

    if (!_editor->parameters().scope_viewing || event->button() != Qt::LeftButton)
        return;

    auto mouse_position = event->pos();

    if (mouse_position.x() <= (getMessageAreaWidth() + getNumberAreaWidth())
        || mouse_position.x() >= width()
        || mouse_position.y() < _editor->contentOffset().y()
        )
    {
        return;
    }
    
    QTextBlock block = _editor->firstVisibleBlock();

    while(block.isValid()
            && (!block.isVisible()
                || mouse_position.y() >= (_editor->blockBoundingGeometry(block).bottom() + _editor->contentOffset().y())
                )
            )
    {
        block = block.next();
    }

    if (!block.isValid()
        || !block.isVisible()
        || mouse_position.y() >= _editor->blockBoundingGeometry(block).bottom() + _editor->contentOffset().y()
        )
    {
        return;
    }

    auto change_visibility = [this, &block](std::function<bool(const QTextBlock &)> break_predicate) -> void
    {
        _editor->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        block = block.next();
        bool visible = block.isVisible();

        while(block.isValid())
        {
            if (break_predicate(block))
            {
                break;
            }

            block.setVisible(!visible);
            block = block.next();
        }

        _editor->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        _editor->viewport()->update();
        update();
    };

    const TextBlockData * data = static_cast<TextBlockData *>(block.userData());
    if (data != nullptr && data->isContextStart())
    {
        change_visibility([](const QTextBlock & block) -> bool
                            {
                                auto * data = static_cast<TextBlockData *>(block.userData());
                                return data == nullptr || data->isContextEnd();
                            });
        return;
    }
    
    size_t start_line = size_t(block.blockNumber()) + 1;
    const auto & current_scope_iterator = std::find_if(_editor->_scope_set.begin()
                                , _editor->_scope_set.end()
                                , [start_line](const simodo::dsl::SemanticScope & scope) -> bool
                                {
                                    return scope.first.begin != 0 && start_line == scope.first.line;
                                });
    if (current_scope_iterator != _editor->_scope_set.end())
    {
        change_visibility([&current_scope_iterator](const QTextBlock & block) -> bool
                            {
                                return size_t(block.blockNumber()) + 1 > current_scope_iterator->second.line;
                            });
    }
}

void LineNumberArea::mouseMoveEvent(QMouseEvent *e)
{
    QWidget::mouseMoveEvent(e);

    /// \todo To highlight hiddable block when mouse moves over it
}

bool LineNumberArea::event(QEvent *event)
{
    if (event->type() == QEvent::ToolTip)
    {
        QHelpEvent * e = static_cast<QHelpEvent *>(event);

        int message_width = getMessageAreaWidth();
        int number_width  = getNumberAreaWidth();

        QTextBlock block = _editor->firstVisibleBlock();
        int top = static_cast<int>(_editor->blockBoundingGeometry(block).translated(_editor->contentOffset()).top());
        int height = static_cast<int>(_editor->blockBoundingRect(block).height());

        QPoint p = e->pos();

        if (p.y() < top)
            return true;

        int mouse_line   = (p.y()-top) / height;
        int line_count   = _editor->document()->lineCount();
        int current_line = block.lineCount();

        if (mouse_line <= line_count - current_line)
        {
            if (p.x() >= 0 && p.x() < message_width+number_width)
            {
                while(mouse_line > 0)
                {
                    if (!block.isValid())
                        return true;
                    block = block.next();
                    if (block.isVisible())
                        mouse_line --;
                }

                QString text = _editor->getErrorText(block.blockNumber());

                if (!text.isEmpty())
                {
                    QToolTip::showText(e->globalPos(), text);
                }
                else
                {
                    QToolTip::hideText();
                    event->ignore();
                }
            }
        }

        return true;
    }
    return QWidget::event(event);
}

