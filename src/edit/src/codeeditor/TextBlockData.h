#ifndef TEXTBLOCKDATA_H
#define TEXTBLOCKDATA_H

#include <QTextBlockUserData>

#include "simodo/dsl/Token.h"

/*!
 * \brief "Пользовательский блок данных", прикреплённый к блокe текста (QTextBlock)
 *
 * Содержит контейнер всех токенов, которые были получены при анализе блока
 * текста лексическим анализатором при подсветке текста (см. Highlighter::highlightBlock)
 */

class TextBlockData : public QTextBlockUserData
{
    std::vector<simodo::dsl::Token> _tokens;
    std::vector<simodo::dsl::Token> _spell_errors;

public:
    TextBlockData() = delete;
    TextBlockData(std::vector<simodo::dsl::Token> tokens, std::vector<simodo::dsl::Token> spell_errors);

    bool isContextStart() const;
    bool isContextInside(QTextBlock block) const;
    bool isContextEnd() const;
    bool isCollapsed(QTextBlock block) const;

    uint32_t getContext() const;

public:
    const std::vector<simodo::dsl::Token> & tokens() const { return _tokens; }
    const std::vector<simodo::dsl::Token> & spell_errors() const { return _spell_errors; }
};

#endif // TEXTBLOCKDATA_H
