#include "RuleData.h"

#include <QFileInfo>

    RuleData::RuleData(QString name
                , QStringList file_masks
                , QVector<QPair<QString,QString>> run_command_set
                , int collapsible_starts_block_number
                , simodo::dsl::LexicalParameters lexis
                , std::vector<std::u16string> error_format_words
                , std::vector<std::u16string> keyword_format_words
                , std::vector<std::u16string> module_format_words
                , std::vector<std::u16string> parameter_format_words
                , std::vector<std::u16string> type_format_words
                , std::vector<std::u16string> tuple_format_words
                , std::vector<std::u16string> function_format_words
                , QString grammar_name
                , QString semantic_handler_name
                , QString interpreter_handler_name
                , bool prevent_text_tab_replacement
                )
    : _name(name)
    , _file_masks(file_masks)
    , _run_command_set(run_command_set)
    , _collapsible_starts_block_number(collapsible_starts_block_number)
    , _lexis(lexis)
    , _error_format_words(error_format_words)
    , _keyword_format_words(keyword_format_words)
    , _module_format_words(module_format_words)
    , _parameter_format_words(parameter_format_words)
    , _type_format_words(type_format_words)
    , _tuple_format_words(tuple_format_words)
    , _function_format_words(function_format_words)
    , _grammar_name(grammar_name)
    , _semantic_handler_name(semantic_handler_name)
    , _interpreter_handler_name(interpreter_handler_name)
    , _prevent_text_tab_replacement(prevent_text_tab_replacement)
{
}
