/*
MIT License

Copyright (c) 2019 BMSTU IU6, Michael Fetisov

https://bmstu.codes/lsx/simodo/stars
*/

#include "Highlighter.h"
#include "TextBlockData.h"
#include "ErrorInfo.h"
#include "CodeEditor.h"

#include "simodo/dsl/Tokenizer.h"
#include "simodo/convert.h"

/// @note Поддержка дополнений к выделению ключевых слов при форматировании текста
/// необходима для выделения примитивных типов, а также при подсветке семантики новых
/// языков, у которых ещё нет семантического анализатора (например, для статей)
#define SIMODO_EDIT_FORMAT_SUBSTITUTION_SUPPORT

using namespace simodo::dsl;

Highlighter::Highlighter(CodeEditor * editor, LexicalParameters lexis)
    : QSyntaxHighlighter(editor->document())
    , _editor(editor)
    , _lexical_parameters(lexis)
{
}

void Highlighter::highlightBlock(const QString &text)
{
    int     block_number     = currentBlock().blockNumber();
    int     block_position   = currentBlock().position();
    auto &  color_theme     = _editor->_color_theme;

    auto u16text = text.toStdU16String();
    BufferStream stream(u16text.c_str());
    Tokenizer    tokenizer(u""
                            , stream
                            , _lexical_parameters
                            , _editor->parameters().tab_size
                            , (previousBlockState() == -1
                                ? NO_TOKEN_CONTEXT_INDEX
                                : uint32_t(previousBlockState()))
                            );

    std::vector<Token> tokens(std::vector<Token>::size_type(10));
    std::vector<Token> spell_errors;

    for (Token token = tokenizer.getAnyToken(); token.getType() != LexemeType::Empty; token = tokenizer.getAnyToken())
    {
        switch(token.getType())
        {
        case LexemeType::Punctuation:
            if (token.getQualification() == TokenQualification::Keyword)
            {
                setTokenFormat(token, color_theme.keyword_format);
            } else {
                setTokenFormat(token, color_theme.punctuation_format);
            }
            break;
        case LexemeType::Id:
            if (_editor->parameters().id_spell_checking_requared) {
                decltype(spell_errors) temp = checkSpell(token, setIdFormat(token, block_position, color_theme.word_format));
                std::copy(temp.begin(), temp.end(), std::back_inserter(spell_errors));
            }
            else
                setIdFormat(token, block_position, color_theme.word_format);
            break;
        case LexemeType::Annotation:
        {
            std::vector<Token> annotation_spell_errors = produceSpellChecking(token, LexemeType::Annotation);

            QTextCharFormat annotation_format = color_theme.annotation_format;
            setTokenFormat(token, annotation_format);
            annotation_format.setUnderlineColor(_editor->_color_theme.spell_underline_color);
            annotation_format.setUnderlineStyle(_editor->_color_theme.spell_underline_style);

            for (const auto & error : annotation_spell_errors)
            {
                setTokenFormat(error, annotation_format);
                spell_errors.push_back(error);
            }
        }
            break;
        case LexemeType::Number:
            setTokenFormat(token
                            , (token.getQualification() == TokenQualification::NotANumber
                                ? color_theme.error_format
                                : color_theme.number_format)
                            );
            break;
        case LexemeType::Comment:
        {
            std::vector<Token> annotation_spell_errors = produceSpellChecking(token, LexemeType::Comment);

            QTextCharFormat comment_format = _editor->_color_theme.comment_format;
            setTokenFormat(token, comment_format);
            comment_format.setUnderlineColor(_editor->_color_theme.spell_underline_color);
            comment_format.setUnderlineStyle(_editor->_color_theme.spell_underline_style);

            for (const Token & error : annotation_spell_errors)
            {
                setTokenFormat(error, comment_format);
                spell_errors.push_back(error);
            }
        }
            break;
        case LexemeType::Error:
            setTokenFormat(token, color_theme.error_format);
            break;
        default:
            break;
        }

        tokens.push_back(token);
    }

    const TokenLocation & loc = tokenizer.getLocation();

    int state = (loc.context == NO_TOKEN_CONTEXT_INDEX ? -1 : int(loc.context));

    setCurrentBlockState(state);
    setCurrentBlockUserData(new TextBlockData(tokens,spell_errors));

    // Подсвечиваем ошибки

    QTextCharFormat format;

    for (auto it = _editor->_error_info.find(block_number);
        it != _editor->_error_info.end() && it.key() == block_number;
        ++it)
    {
        auto & error_info = it.value();
        auto & error_location = error_info.loc;

        if (error_location.file_name == _editor->currentFileName())
        {
            switch(error_info.level)
            {
            case simodo::dsl::SeverityLevel::Information:
                format.setUnderlineColor(color_theme.information_underline_color);
                format.setUnderlineStyle(color_theme.information_underline_style);
                break;
            case simodo::dsl::SeverityLevel::Warning:
                format.setUnderlineColor(color_theme.warning_underline_color);
                format.setUnderlineStyle(color_theme.warning_underline_style);
                break;
            default:
                format.setUnderlineColor(color_theme.error_underline_color);
                format.setUnderlineStyle(color_theme.error_underline_style);
                break;
            }
            
            setFormat(int(error_location.begin) - block_position
                        , int(error_location.end - error_location.begin)
                        , format
                        );
        }
    }
}

void Highlighter::setTokenFormat(const simodo::dsl::Token & token
                                , const QTextCharFormat & format
                                )
{
    setFormat(token.getLocation().begin, token.getToken().size(), format);
}


QTextCharFormat Highlighter::setIdFormat(const Token & token
                                        , int position
                                        , const QTextCharFormat & default_format
                                        )
{
    CodeEditorColorTheme &  color_theme = _editor->_color_theme;
    QTextCharFormat         format      = default_format; //color_theme.word_format;

    /// @note Поддержка дополнений к выделению ключевых слов при форматировании текста
    /// необходима для выделения примитивных типов, а также при подсветке семантики новых
    /// языков, у которых ещё нет семантического анализатора (например, для статей)
#ifdef SIMODO_EDIT_FORMAT_SUBSTITUTION_SUPPORT
    if (_editor->_rule_data != nullptr)
    {
        auto rule_data = _editor->_rule_data;
        const auto & errors = rule_data->error_format_words();
        const auto & keywords = rule_data->keyword_format_words();
        const auto & parameters = rule_data->parameter_format_words();
        const auto & types = rule_data->type_format_words();
        /// @todo Желательно сделать. Но не обязательно.
        // const auto & modules = rule_data->module_format_words();
        // const auto & tuples = rule_data->tuple_format_words();
        // const auto & functions = rule_data->function_format_words();
        // const auto & inputs = rule_data->input_format_words();
        // const auto & outputs = rule_data->output_format_words();

        auto contains_token = [&token](const std::vector<std::u16string> & tokens) -> bool
        {
            return tokens.end() != std::find(tokens.begin(), tokens.end(), token.getToken());
        };

        format = contains_token(errors)
            ? color_theme.error_format
            : contains_token(keywords)
            ? color_theme.keyword_format
            // : contains_token(modules)
            // ? color_theme.module_format
            : contains_token(parameters)
            ? color_theme.parameter_format
            : contains_token(types)
            ? color_theme.type_format
            // : contains_token(tuples)
            // ? color_theme.tuple_format
            // : contains_token(functions)
            // ? color_theme.function_format
            : default_format
            ;
    }
    if (format == default_format)
    {
#endif
        size_t candidate_index = _editor->findValiableIndexByName(QString::fromStdU16String(token.getLexeme()),
                                                                  position,
                                                                  /// @todo Такая сокращённая передача параметров может
                                                                  /// повлиять на поиск владельца. Нужно последить за отличиями в поведении
                                                                  /// отображения информации в тул-типе и подсветкой.
                                                                  /// А если разницы нет, то оптимизировать алгоритм поиска.
                                                                  0, {token});

        if (candidate_index != UNDEFINED_INDEX)
        {
            const auto & candidate = _editor->_name_set[candidate_index];

            if (candidate.semantic_flags & SemanticFlag_Input)
                format = color_theme.input_format;
            else if (candidate.semantic_flags & SemanticFlag_Output)
                format = color_theme.output_format;
            else if (candidate.semantic_flags & SemanticFlag_Method)
                format = color_theme.method_format;
            else if (candidate.qualification == SemanticNameQualification::Type)
                format = color_theme.type_format;
            else if (candidate.qualification == SemanticNameQualification::Function)
                format = color_theme.function_format;
            else if (candidate.name.getLocation().file_name.empty()
                        && candidate.owner == UNDEFINED_INDEX
                        )
                format = color_theme.module_format;
            else if (candidate.owner != UNDEFINED_INDEX
                        && candidate.owner < _editor->_name_set.size()
                        && _editor->_name_set[candidate.owner].qualification == SemanticNameQualification::Function
                    )
                format = color_theme.parameter_format;
            else if (candidate.qualification == SemanticNameQualification::Tuple)
                format = color_theme.tuple_format;
        }
#ifdef SIMODO_EDIT_FORMAT_SUBSTITUTION_SUPPORT
    }
#endif

    setTokenFormat(token, format);

    return format;
}

std::vector<Token> Highlighter::checkSpell(const Token & token
                                                        , const QTextCharFormat & default_format
                                                        )
{
    if (!_editor->parameters().id_spell_checking_requared)
    {
        return {};
    }

    std::vector<Token> spell_errors;

    QTextCharFormat format = default_format;
    std::vector<Token> id_spell_errors = produceSpellChecking(token, LexemeType::Id);

    if (format.underlineStyle() == QTextCharFormat::NoUnderline)
    {
        format.setUnderlineColor(_editor->_color_theme.id_spell_underline_color);
        format.setUnderlineStyle(_editor->_color_theme.id_spell_underline_style);

        for(const Token & error : id_spell_errors)
        {
            setTokenFormat(error, format);
        }
    }

    for(const Token & error : id_spell_errors)
    {
        spell_errors.push_back(error);
    }

    return spell_errors;
}

std::vector<Token> Highlighter::produceSpellChecking(const Token & token, LexemeType type )
{
    if (!_editor->_parameters.spell_checking_english_required
        && !_editor->_parameters.spell_checking_national_required)
    {
        return {};
    }

    if (token.getToken().empty())
    {
        return {};
    }

    std::vector<Token> spell_errors;

    const std::u16string &  alphabet        = _editor->_checker.alphabet();
    const std::u16string &  latin_alphabet  = _editor->_checker.latin_alphabet();
    const std::u16string    IGNORES         = u"0123456789";


    std::u16string  text                = token.getToken();
    bool            is_word_scanning    = (std::u16string::npos != alphabet.find(text[0]));
    size_t          element_begin       = 0;
    std::u16string  element;

    for(size_t text_pos=0; text_pos < text.size(); ++text_pos)
    {
        char16_t    ch          = text[text_pos];
        auto        it_current  = alphabet.find(ch);
        bool        is_letter   = (std::u16string::npos != it_current);
        bool        is_bounding = (is_letter != is_word_scanning);
        size_t      latin_alpha_2 = latin_alphabet.size() / 2;


        if (!is_bounding && type == LexemeType::Id && is_word_scanning && text_pos > 0)
        {
            // camel-style id?

            auto    it_left     = latin_alphabet.find(text[text_pos - 1]);
            auto    it_right    = latin_alphabet.find(ch);

            if (it_left != std::u16string::npos && it_right != std::u16string::npos)
            {
                if (it_right >= latin_alpha_2 && it_left < latin_alpha_2)
                {
                    is_bounding = true;
                }
                else if (text_pos <= text.size() - 1
                        && it_right >= latin_alpha_2 && it_left >= latin_alpha_2
                        && latin_alphabet.find(text[text_pos + 1]) < latin_alpha_2)
                {
                    is_bounding = true;
                }
            }
        }

        if (is_bounding || (text_pos == text.size()-1 && is_letter))
        {
            if (text_pos == text.size()-1 && is_letter)
            {
                if (type != LexemeType::Id)
                {
                    element += ch;
                } else if(text_pos > 0)
                {
                    size_t id_left = latin_alphabet.find(text[text_pos-1]);
                    size_t id_right = latin_alphabet.find(ch);

                    if (id_left / latin_alpha_2 == id_right / latin_alpha_2)
                    {
                        element += ch;
                    }
                }
            }

            bool ok = true;

            if (is_word_scanning)
            {
                bool checkable       = true;
                int  last_char_index = text_pos - element.size() - 1;

                if (type != LexemeType::Id
                    && last_char_index >= 0
                    && size_t(last_char_index) < text_pos && text_pos < text.size())
                {
                    if (std::u16string::npos != IGNORES.find(text[last_char_index])
                        || std::u16string::npos != IGNORES.find(text[text_pos]))
                    {
                        checkable = false;
                    }
                }

                if (checkable)
                {
                    if (std::u16string::npos != latin_alphabet.find(element[0]))
                    {
                        ok = _editor->_parameters.spell_checking_english_required
                            ? _editor->_checker.checkLatin(element)
                            : true;
                    } else {
                        ok = _editor->_parameters.spell_checking_national_required
                            ? _editor->_checker.checkNational(element)
                            : true;
                    }
                }
            }

            if (!ok)
            {
                TokenLocation loc = token.getLocation();
                loc.begin = token.getLocation().begin + element_begin;
                loc.end = token.getLocation().begin + element_begin + element.size();
                spell_errors.emplace_back(type,element,loc);
            }

            element.clear();
            element_begin = text_pos;
            is_word_scanning = is_letter;
        }

        if (text_pos != text.size() - 1 || !is_letter)
        {
            element += ch;
        }
    }

    return spell_errors;
}
