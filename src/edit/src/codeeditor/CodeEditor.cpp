#include "CodeEditor.h"

#include "Highlighter.h"
#include "TextBlockData.h"

#include "simodo/convert.h"

#include <QtCore/QStringListModel>

#include <QtWidgets/QMenu>
#include <QtWidgets/QAction>
#include <QtWidgets/QAbstractItemView>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QToolTip>

using namespace simodo::dsl;

namespace
{
    static QString eow("~!@#$%^&*()+{}|:\"<>?,./;'[]\\-= \t\n\r"); // end of word

    static QChar charAt(QTextCursor & c, int pos = -1)
    {
        if (pos == -1)
        {
            pos = c.position();
        }
        return c.document()->characterAt(pos);
    }

    static QTextCursor selectWord(QTextCursor tc)
    {
        tc.clearSelection();

        if (tc.atBlockStart() && tc.atBlockEnd())
        {
            return tc;
        }

        while (!tc.atBlockStart()
                && tc.movePosition(QTextCursor::Left)
                && !eow.contains(charAt(tc))
                );

        while (tc.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor)
                && !tc.atBlockEnd()
                && !eow.contains(charAt(tc))
                );

        if (tc.position() - tc.anchor() < 1)
        {
            return tc;
        }

        auto left = tc.anchor();
        auto right = tc.position();

        if (eow.contains(charAt(tc, tc.anchor())))
        {
            ++left;
        }

        tc.setPosition(left);
        tc.setPosition(right, QTextCursor::KeepAnchor);

        return tc;
    }
}

CodeEditor::CodeEditor(CodeEditorColorTheme ct,
                       CodeEditorParameters params,
                       SpellChecker &checker,
                       const QMultiMap<int, ErrorInfo> &error_info)
    : _line_number_area(nullptr)
    , _parameters(params)
    , _rule_data(nullptr)
    , _error_info(error_info)
    , _highlighter(nullptr)
    , _c_model(nullptr)
    , _c(nullptr)
    , _context(CompletionContext::NoContext)
    , _checker(checker)
    , _is_rehighlight_active(false)
{
    setColorTheme(ct);

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    _line_number_area = new LineNumberArea(this);

    _c_model = new QStringListModel();

    _c = new QCompleter(this);
    _c->setModel(params.completion? _c_model : nullptr);
    _c->setModelSorting(QCompleter::UnsortedModel);
    _c->setCaseSensitivity(Qt::CaseInsensitive);
    _c->setFilterMode(Qt::MatchContains);
    _c->setMaxVisibleItems(10);
    _c->setWrapAround(false);
    _c->setWidget(this);
    _c->setCompletionMode(QCompleter::PopupCompletion);

    connect(_c, SIGNAL(activated(QString)), this, SLOT(insertCompletion(QString)));

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
//    connect(this, SIGNAL(textChanged()), this, SLOT(updateTextStructure()));

    updateLineNumberAreaWidth(0);
    highlightCurrentLine();
}

CodeEditor::~CodeEditor()
{
    if (_c != nullptr)
    {
        delete _c;
    }

    if (_c_model != nullptr)
    {
        delete _c_model;
    }

    if (_highlighter != nullptr)
    {
        delete _highlighter;
    }

    if (_line_number_area != nullptr)
    {
        delete _line_number_area;
    }
}

void CodeEditor::setRuleData(RuleData *rule_data)
{
    _rule_data = rule_data;

    if (rule_data == nullptr)
    {
        if (_highlighter != nullptr)
        {
            delete _highlighter;
        }

        _highlighter = nullptr;

        if (_c != nullptr)
        {
            _c->setModel(nullptr);
        }

        return;
    }

    if (_highlighter == nullptr)
    {
        _highlighter = new Highlighter(this, _rule_data->lexis());
    }
}

void CodeEditor::updateTextStructure()
{
    if (parameters().scope_viewing && rule_data() != nullptr && rule_data()->collapsible_starts_block_number() >= 0)
    {
        int collapsible_starts_block_number = rule_data()->collapsible_starts_block_number();

        /// \todo remove code duplication! (ref: LineNumberArea::mouseReleaseEvent)

        QTextBlock  block = document()->firstBlock();

        const TextBlockData * data = static_cast<TextBlockData *>(block.userData());
        if (data == nullptr || data->getContext() != static_cast<uint32_t>(collapsible_starts_block_number))
            return;

        block = block.next();
        while(block.isValid())
        {
            data = static_cast<TextBlockData *>(block.userData());
            if (data == nullptr || data->isContextEnd())
                break;
            block.setVisible(!block.isVisible());
            block = block.next();
        }

        update();
    }
}

void CodeEditor::setColorTheme(CodeEditorColorTheme ct)
{
    setStyleSheet("QPlainTextEdit {"
                      " color : "+ct.text_foreground_color.name()+"; "
                      " background-color : "+ct.text_background_color.name()+"; "
                      " alternate-background-color : "+ct.alternate_text_background_color.name()+"; "
                      " selection-color : "+ct.selection_fg_color.name()+"; "
                      " selection-background-color : "+ct.selection_bg_color.name()+"; "
                "}");

    _color_theme = ct;

    rehighlight();
    highlightCurrentLine();
}

void CodeEditor::updateSemanticInfo()
{
    _name_set = getNameSet();
    _scope_set = getScopeSet();

//    for(size_t i=0; i < _name_set.size(); ++i)
//        _name_map.insert(_name_set[i].name.getLexeme(),i);
}

void CodeEditor::rehighlight()
{
    if (_highlighter != nullptr)
    {
        setRehighlightActive(true);
        _highlighter->rehighlight();
        setRehighlightActive(false);
    }
}

void CodeEditor::setParameters(CodeEditorParameters params)
{
    if (parameters().scope_viewing != params.scope_viewing)
    {
        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        for(QTextBlock block=document()->firstBlock(); block.isValid(); block = block.next())
            block.setVisible(true);

        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        update();
    }

    if (parameters().tab_size != params.tab_size)
    {
        setTabStopDistance(QFontMetricsF(font()).horizontalAdvance(' ') * params.tab_size);
        update();
    }

    _c->setModel(params.completion ? _c_model : nullptr);

    _parameters = params;
    rehighlight();
}

QString CodeEditor::getErrorText(int lineNumber, int position)
{
    QString text;

    for(auto it = _error_info.find(lineNumber);
         it != _error_info.end() && it.key() == lineNumber;
         ++it)
    {
        if (it.value().loc.file_name == currentFileName())
        {
            if (position == -1
                || (it.value().loc.begin <= size_t(position)
                    && it.value().loc.end >= size_t(position)
                    )
                )
            {
                if (!text.isEmpty())
                {
                    text += "<hr>";
                }

                QString severity_level_name = QString::fromStdU16String(simodo::dsl::getSeverityLevelName(it.value().level));
                if (!severity_level_name.isEmpty())
                {
                    text += "<u>" + severity_level_name + "</u>";
                }
                
                text += "<b>" + it.value().briefly + "</b>";
                if (!it.value().atlarge.isEmpty())
                {
                    QString str = it.value().atlarge;
                    text += "<br>" + str.replace("\n","<br>");
                }
            }
        }
    }

    return text;
}

size_t CodeEditor::findValiableIndexByName(const QString &name, int position, size_t token_index, const std::vector<Token> &tokens)
{
    if (!_parameters.highlight_semantics)
        return UNDEFINED_INDEX;

    if (name.isEmpty())
        return UNDEFINED_INDEX;

    size_t i = _name_set.size()-1;

    for(; i < _name_set.size(); --i)
        if (name == QString::fromStdU16String(_name_set[i].name.getLexeme()))
        {
            if (_name_set[i].name.getLocation().file_name.empty())
                break;

            if ((_name_set[i].name.getLocation().file_name == currentFileName().toStdU16String() ||
                 _name_set[i].name.getLocation().begin == 0 ||
                 _name_set[i].name.getLocation().begin <= static_cast<size_t>(position))
             && (_name_set[i].lower_scope.end == 0 ||
                 _name_set[i].lower_scope.end >= static_cast<size_t>(position)))
                break;

            size_t owner = _name_set[i].owner;
            size_t index = token_index;
            size_t root_owner = UNDEFINED_INDEX;

            while(owner != UNDEFINED_INDEX)
            {
                const std::u16string & owner_name = _name_set[owner].name.getLexeme();

                index --;

                while(index < tokens.size() && tokens[index].getType() != LexemeType::Id)
                    index --;

                if (index > tokens.size() || owner_name != tokens[index].getLexeme())
                    break;

                if (_name_set[owner].owner == UNDEFINED_INDEX)
                {
                    root_owner = owner;
                    break;
                }
                owner = _name_set[owner].owner;
            }

            if (root_owner != UNDEFINED_INDEX)
                break;
        }

    return i;
}

//QString CodeEditor::getTokenInfo(const QTextBlock block, int pos)
//{
//    QString text;

//    if (!block.isValid())
//        return text;

//    TextBlockData * data = static_cast<TextBlockData *>(block.userData());

//    if (data == nullptr)
//        return text;

//    for(const Token & t : data->tokens())
//        if (t.getLocation().begin <= static_cast<size_t>(pos)
//         && t.getLocation().end >= static_cast<size_t>(pos))
//        {
//            switch(t.getType())
//            {
//            case LexemeType::Empty:
//            case LexemeType::Comment:
//            case LexemeType::Annotation:
//                break;
//            case LexemeType::Punctuation:
//                if (t.getQualification() != TokenQualification::Keyword)
//                    break;
//                [[fallthrough]];
//            default:
//                text = tr("<i>Lexeme type:</i> ") + QString::fromStdU16String(getLexemeTypeName(t.getType()))
//                     + ((t.getQualification() == TokenQualification::None)
//                        ? ""
//                        : tr("<br><i>Token qualification:</i> ")+QString::fromStdU16String(getQualificationName(t.getQualification())));
//                break;
//            }
//            break;
//        }

//    return text;
//}

void CodeEditor::performBlockShiftRigth()
{
    performBlockShift(ShiftDirection::Right);
}

void CodeEditor::performBlockShiftLeft()
{
    performBlockShift(ShiftDirection::Left);
}

void CodeEditor::contextMenuEvent(QContextMenuEvent *e)
{
    QPoint      pos      = e->pos();
    QTextCursor tc       = cursorForPosition(pos);
    int         position = tc.position();
    QTextBlock  block    = tc.block();
    int         block_pos= block.position();

    Token       error_token(u"");
    QStringList sugg;

    if (block.isValid() && block.userData() != nullptr)
    {
        int line_pos = position - block_pos;
        TextBlockData * data = static_cast<TextBlockData *>(block.userData());

        for(const Token & t : data->spell_errors())
            if (t.getLocation().begin <= static_cast<size_t>(line_pos)
             && t.getLocation().end >= static_cast<size_t>(line_pos))
            {
                error_token = t;
                sugg =_checker.suggest(t.getToken());
                break;
            }
    }

    QMenu * menu = createStandardContextMenu();
    QString word = QString::fromStdU16String(error_token.getToken());

    if (!word.isEmpty())
    {
        menu->addSeparator();

        QAction *spell_action = new QAction("'" + word + "'");
        spell_action->setEnabled(false);
        menu->addAction(spell_action);

        if (!sugg.isEmpty())
        {
            QMenu * suggestion_menu = menu->addMenu(tr("Suggestion"));

            for(auto s : sugg)
            {
                QAction *a = new QAction();

                a->setText(s);
                a->setVisible(true);
                connect(a, &QAction::triggered, this, [this,error_token,block_pos](bool)
                {
                    if (const QAction *action = qobject_cast<const QAction *>(sender());
                        action != nullptr)
                    {
                        QTextCursor cursor = textCursor();
                        cursor.setPosition(block_pos+error_token.getLocation().begin);
                        cursor.setPosition(block_pos+error_token.getLocation().end, QTextCursor::KeepAnchor);
                        cursor.insertText(action->text());
                        setTextCursor(cursor);
                    }

                });
                suggestion_menu->addAction(a);
            }
        }

        QAction *add_action = new QAction(tr("Ignore"));
        connect(add_action, &QAction::triggered, this, [this,word](bool)
        {
            _checker.add(word.toStdU16String());
            rehighlight();
        });
        menu->addAction(add_action);
    }

    menu->exec(e->globalPos());
    delete menu;
}

void CodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        _line_number_area->scroll(0, dy);
    else
        _line_number_area->update(0, rect.y(), _line_number_area->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void CodeEditor::insertCompletion(const QString &completion)
{
    if (_c->widget() != this)
        return;

    QTextCursor tc = selectWord(textCursor());
    tc.removeSelectedText();
    tc.insertText(completion);
    setTextCursor(tc);
}

void CodeEditor::fillCompletionModel()
{
    CompletionContext context = _context;
    if (_rule_data == nullptr || _c_model == nullptr)
    {
        return;
    }

    QStringList list;

    QTextCursor tc = textCursor();
    size_t position = size_t(tc.position());

    if (context == CompletionContext::Tuple)
    {
        /// \todo ...

        QTextBlock      block = tc.block();
        TextBlockData * data  = static_cast<TextBlockData *>(block.userData());
        size_t          pos   = position - size_t(block.position());
        std::u16string  tuple = u"";

        if (data != nullptr)
        {
            const std::vector<Token> & tokens = data->tokens();
            size_t i = tokens.size() - 1;

            for(; i < tokens.size(); --i)
            {
                const Token & t = tokens[i];

                if (t.getLocation().end < pos
                    && t.getType() == LexemeType::Id)
                    break;
            }

            if (i < tokens.size())
                tuple = tokens[i].getLexeme();
        }

        if (!tuple.empty())
        {
            for(size_t i=0; i < _name_set.size(); ++i)
                if (_name_set[i].owner != UNDEFINED_INDEX)
                {
                    size_t owner = _name_set[i].owner;

                    if (owner < _name_set.size()
                     && _name_set[owner].name.getLexeme() == tuple
                     && _name_set[owner].qualification == SemanticNameQualification::Tuple)
                        list << simodo::convertToU8(_name_set[i].name.getLexeme()).c_str();
                }

            list.removeDuplicates();
            _c_model->setStringList(list);
            return;
        }
    }

    for(size_t i=0; i < _name_set.size(); ++i)
        if (_name_set[i].name.getLexeme().empty())
        {}
        else if (_name_set[i].name.getLocation().file_name.empty()
              && _name_set[i].owner == UNDEFINED_INDEX)
            list << simodo::convertToU8(_name_set[i].name.getLexeme()).c_str();
        else if (_name_set[i].name.getLocation().end < position
              && _name_set[i].lower_scope.end >= position)
        {
            size_t owner = _name_set[i].owner;

            if (owner == UNDEFINED_INDEX)
                list << simodo::convertToU8(_name_set[i].name.getLexeme()).c_str();
            else if (owner < _name_set.size()
             && _name_set[owner].owner == UNDEFINED_INDEX
             && _name_set[owner].qualification == SemanticNameQualification::Function
             && _name_set[i].name.getLexeme() != u"")
                list << simodo::convertToU8(_name_set[i].name.getLexeme()).c_str();
        }

    const simodo::dsl::LexicalParameters & _lexical_parameters = _rule_data->lexis();

    for(std::u16string s : _lexical_parameters.punctuation_words)
        if (_lexical_parameters.latin_alphabet.find(s[0]) != std::string::npos)
            list << simodo::convertToU8(s).c_str();

    list.removeDuplicates();
    _c_model->setStringList(list);
    return;
}

QString CodeEditor::textUnderCursor() const
{
    return selectWord(textCursor()).selectedText();
}

void CodeEditor::showCompliterPopup()
{
    QRect completer_rect = cursorRect();
    completer_rect.setX(completer_rect.x() + lineNumberAreaWidth());
    completer_rect.setWidth(_c->popup()->sizeHintForColumn(0) + _c->popup()->verticalScrollBar()->sizeHint().width());
    _c->complete(completer_rect); // popup it up!
}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    _line_number_area->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::keyPressEvent(QKeyEvent *event)
{
    if (!_c->completionPrefix().contains(textUnderCursor()))
    {
        _c->popup()->hide();
    }

    if (_c != nullptr && _c->popup()->isVisible()) {
        // The following keys are forwarded by the completer to the widget
        switch (event->key())
        {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Escape:
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            event->ignore();
            return; // let the completer do default behavior
        default:
            break;
        }
    }

    if (event->key() == Qt::Key_Tab && event->modifiers() == Qt::NoModifier)
    {
        if (textCursor().selectionStart() == textCursor().selectionEnd())
        {
            performTab();
        } else {
            performBlockShiftRigth();
        }

        event->ignore();
        return;
    }

    if ((event->key() == Qt::Key_Return /*|| e->key() == Qt::Key_Enter*/)
        && event->modifiers() == Qt::NoModifier
        && _parameters.is_autoindenting_necessary)
    {
        performAutoindent();
        event->ignore();
        return;
    }

    if ((event->modifiers() & Qt::ControlModifier) && event->key() == Qt::Key_Space)
    {
        auto tc = selectWord(textCursor());
        tc.setPosition(tc.selectionStart());
        if (tc.movePosition(QTextCursor::Left)
            && charAt(tc) == '.')
        {
            _context = CompletionContext::Tuple;
        } else {
            _context = CompletionContext::NoContext;
        }
        fillCompletionModel();
        showCompliterPopup();
        event->ignore();
        return;
    }

    QPlainTextEdit::keyPressEvent(event);

    if (_highlighter == nullptr || _c == nullptr)
    {
        return;
    }

    const bool ctrl_or_shift = event->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
    if (ctrl_or_shift && event->text().isEmpty())
    {
        return;
    }

    QString completion_prefix = textUnderCursor();

    if (event->key() == Qt::Key_Period)
    {
        _context = CompletionContext::Tuple;
    } else {
        auto tc = selectWord(textCursor());
        tc.setPosition(tc.selectionStart());
        if (tc.movePosition(QTextCursor::Left)
            && charAt(tc) == '.')
        {
            _context = CompletionContext::Tuple;
        } else {
            _context = CompletionContext::NoContext;
        }
    }

    if ((( event->modifiers() != Qt::NoModifier
               && !ctrl_or_shift)
            || event->text().isEmpty()
            || completion_prefix.length() < 2 // prefix_limit_chars_count
            || eow.contains(event->text().right(1)))
        && _context != CompletionContext::Tuple)
    {
        _c->popup()->hide();
        return;
    }

    fillCompletionModel();
    if (completion_prefix != _c->completionPrefix())
    {
        _c->setCompletionPrefix(completion_prefix);
        _c->popup()->setCurrentIndex(_c->completionModel()->index(0, 0));
    }

    showCompliterPopup();
}

void CodeEditor::focusInEvent(QFocusEvent *e)
{
    if (_c != nullptr)
        _c->setWidget(this);

    QPlainTextEdit::focusInEvent(e);
}

bool CodeEditor::event(QEvent *event)
{
    if (event->type() == QEvent::ToolTip)
    {
        QHelpEvent *    e                       = static_cast<QHelpEvent *>(event);
        QPoint          pos                     = e->pos();
        int             line_number_area_width  = lineNumberAreaWidth();

        pos.setX(pos.x() - line_number_area_width);

        QTextCursor     tc          = cursorForPosition(pos);
        QTextBlock      block       = tc.block();
        int             position    = tc.position();
        TextBlockData * data        = static_cast<TextBlockData *>(block.userData());
        size_t          token_index = 0;
        QString         word;

        if (data != nullptr)
        {
            int pos = position-block.position();
            for(; token_index < data->tokens().size(); ++token_index)
            {
                const Token & t = data->tokens()[token_index];

                if (t.getLocation().begin <= static_cast<size_t>(pos)
                 && t.getLocation().end >= static_cast<size_t>(pos))
                {
                    word = QString::fromStdU16String(t.getLexeme());
                    break;
                }
            }
        }
        else
        {
            tc.select(QTextCursor::WordUnderCursor);
            word = tc.selectedText();
        }

        /// @note Выпилил лексическую информацию - не актуально.
        /// Однако можно пока оставить в закомментированном виде.
        QString text;// = getTokenInfo(block,position-block.position());
        size_t i = findValiableIndexByName(word, position, token_index, data->tokens());

        if (!word.isEmpty() && i < _name_set.size())
        {
            QString name_annotation;

            if (_name_set[i].context == simodo::dsl::SemanticName::SemanticNameContext::Grammar)
            {
                name_annotation = "<i>Grammar element type:</i> ";

                if (_name_set[i].qualification == SemanticNameQualification::None)
                    name_annotation += "compound";
                else if (_name_set[i].qualification == SemanticNameQualification::Function)
                    name_annotation += "metatype";
                else  if (_name_set[i].qualification == SemanticNameQualification::Scalar)
                    name_annotation += "terminal";
                else
                    name_annotation = "";
            }
            else
            {
                QString type_name;
                QString parameters;

                if (_name_set[i].qualification == SemanticNameQualification::Function
                 || _name_set[i].qualification == SemanticNameQualification::Tuple
                 || _name_set[i].qualification == SemanticNameQualification::Type)
                {
                    for (const auto & n : _name_set)
                    {
                        if (n.owner == i)
                        {
                            if (n.name.getLexeme() == u"" && _name_set[i].qualification == SemanticNameQualification::Function)
                                type_name = QString::fromUtf16(getSemanticNameTypeName(n.type).c_str()) + " ";
                            else
                            {
                                if (!parameters.isEmpty())
                                    parameters += ", ";
                                /// \todo function return type!
                                if (_name_set[i].qualification == SemanticNameQualification::Function)
                                    parameters += QString::fromUtf16(getSemanticNameTypeName(n.type).c_str()) + " ";
                                else
                                    parameters += QString::fromUtf16(getSemanticNameQualificationName(n.qualification).c_str()) + " ";

                                parameters += "<i>" + QString::fromUtf16(n.name.getLexeme().c_str()) + "</i>";
                            }
                        }
                    }

                    if (_name_set[i].qualification == SemanticNameQualification::Function)
                        parameters = " (" + parameters + ")";
                    else
                        parameters = " {" + parameters + "}";
                }
                else
                {
                    type_name = QString::fromUtf16(getSemanticNameTypeName(_name_set[i].type).c_str()) + " ";
                }

                name_annotation = type_name + "<b>" + QString::fromUtf16(_name_set[i].name.getLexeme().c_str()) + "</b>" + parameters + "<br>"
                        + QString::fromUtf16(getSemanticNameQualificationName(_name_set[i].qualification).c_str())
                        ;

                if (_name_set[i].semantic_flags != 0) {
                    if ((_name_set[i].semantic_flags & SemanticFlag_Input))
                        name_annotation += "<br>Input parameter";
                    if ((_name_set[i].semantic_flags & SemanticFlag_Output))
                        name_annotation += "<br>Output parameter";
                    /// @note Для ScroptC0 отмечание именно метода не имеет смысла, т.к. синтаксически закрыта возможность
                    /// создавать локальные лямбды или функции.
                    // if ((_name_set[i].semantic_flags & SemanticFlag_Method))
                    //     name_annotation += "<br>Method of the type";
                }

                size_t owner = _name_set[i].owner;

                if (owner != UNDEFINED_INDEX)
                {
                    name_annotation += "<br>part of: " +
                            QString::fromUtf16(getSemanticNameQualificationName(_name_set[owner].qualification).c_str()) + " <b>" +
                            QString::fromUtf16(_name_set[owner].name.getLexeme().c_str()) + "</b>";
                }
            }

            if (!text.isEmpty())
                text += "<hr>";

            text += name_annotation;
        }

        QString error_text = getErrorText(block.blockNumber(), position);
        if (!error_text.isEmpty())
        {
            if (!text.isEmpty())
                text += "<hr>";

            text += error_text;
        }

        if (data != nullptr && !data->spell_errors().empty())
        {
            int line_pos = position - block.position();

            for(const Token & t : data->spell_errors())
                if (t.getLocation().begin <= static_cast<size_t>(line_pos)
                 && t.getLocation().end >= static_cast<size_t>(line_pos))
                {
                    if (!text.isEmpty())
                        text += "<hr>";

                    text += tr("<b>Spelling mistake!</b>");
                    break;
                }
        }

        if (!text.isEmpty())
        {
            QToolTip::showText(e->globalPos(), text);
        }
        else
        {
            QToolTip::hideText();
            event->ignore();
        }

        return true;
    }
    return QPlainTextEdit::event(event);
}

int CodeEditor::shiftRight(QTextCursor &cursor, int end_selection)
{
    if (isTabReplacementNecessary())
    {
        cursor.insertText(QString(_parameters.tab_size, ' '));
        end_selection += _parameters.tab_size;
    }
    else
    {
        cursor.insertText("\t");
        end_selection ++;
    }

    return end_selection;
}

int CodeEditor::shiftLeft(QTextCursor &cursor, int end_selection)
{
    QString text = cursor.block().text();
    int     i    = 0;

    for(; i < text.size(); ++i)
        if (i == _parameters.tab_size)
            break;
        else if (text[i] != ' ')
        {
            if (text[i] == '\t')
                i++;
            break;
        }

    if (i > 0)
    {
        cursor.setPosition(cursor.position()+i, QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
        end_selection -= i;
    }

    return end_selection;
}

void CodeEditor::performBlockShift(ShiftDirection direction)
{
    QTextCursor cursor          = textCursor();
    int         start_sel       = cursor.selectionStart();
    int         end_sel         = cursor.selectionEnd();
    int         init_position   = cursor.position();
    int         init_anchor     = cursor.anchor();

    if (start_sel >= end_sel)
        return;

    cursor.beginEditBlock();
    cursor.setPosition(start_sel);
    cursor.movePosition(QTextCursor::StartOfLine);

    do
    {
        end_sel = (direction == ShiftDirection::Right) ? shiftRight(cursor, end_sel) : shiftLeft(cursor, end_sel);

        if (!cursor.movePosition(QTextCursor::QTextCursor::NextBlock))
            break;
    }
    while(cursor.position() < end_sel);

    end_sel = std::min(end_sel,cursor.position());

    cursor.setPosition((init_position > init_anchor) ? init_anchor : end_sel);
    cursor.setPosition((init_position > init_anchor) ? end_sel : init_position, QTextCursor::KeepAnchor);
    cursor.endEditBlock();
    setTextCursor(cursor);
}

void CodeEditor::performAutoindent()
{
    QTextCursor cursor      = textCursor();
    QTextBlock  block       = cursor.block();
    QString     block_str   = block.text();
    int         pos_inline  = cursor.position() - block.position();
    QString     indent_str;

    if (pos_inline < block_str.size())
        for(int i=0; i < pos_inline; ++i)
        {
            auto ch = block_str[i];

            if (ch != ' ' && ch != '\t')
                break;

            indent_str += ch;
        }
    else
        while(true)
        {
            int i = 0;
            for(; i < pos_inline; ++i)
            {
                auto ch = block_str[i];

                if (ch != ' ' && ch != '\t')
                    break;

                indent_str += ch;
            }

            if (i < pos_inline)
                break;

            indent_str.clear();

            block = block.previous();

            if (!block.isValid())
                break;

            block_str   = block.text();
            pos_inline  = block_str.size();
        }

    insertPlainText("\n" + indent_str);
}

void CodeEditor::handleCommenting()
{
    if (_rule_data == nullptr)
    {
        return;
    }

    QString     comment_line_start;
    QString     comment_multiline_start;
    QString     comment_multiline_end;

    const LexicalParameters & lexis = _rule_data->lexis();

    for(const MarkupSymbol & ms : lexis.markups)
        if (ms.type == LexemeType::Comment)
        {
            if (ms.end.empty())
                comment_line_start = QString::fromStdU16String(ms.start);
            else
            {
                comment_multiline_start = QString::fromStdU16String(ms.start);
                comment_multiline_end = QString::fromStdU16String(ms.end);
            }
        }

    if (comment_line_start.isEmpty() && comment_multiline_start.isEmpty())
        return;

    QTextCursor cursor          = textCursor();
    int         start_sel       = cursor.selectionStart();
    int         end_sel         = cursor.selectionEnd();
    int         init_position   = cursor.position();
    int         init_anchor     = cursor.anchor();

    cursor.setPosition(start_sel);

    if (!comment_multiline_start.isEmpty() && init_position != init_anchor)
    {
        int ind = cursor.block().text().indexOf(comment_multiline_start, start_sel - cursor.block().position());

        if (ind == start_sel - cursor.block().position())
        {
            //TODO: crush if end_sel-comment_multiline_end.size() < 0, but unreal.
            int pos_left_comment = end_sel - comment_multiline_end.size();

            cursor.setPosition(pos_left_comment);

            int ind = cursor.block().text().indexOf(comment_multiline_end, pos_left_comment - cursor.block().position());

            if (ind == pos_left_comment - cursor.block().position())
            {
                // match!
                cursor.beginEditBlock();
                cursor.setPosition(start_sel);
                cursor.setPosition(start_sel+comment_multiline_start.size(),  QTextCursor::KeepAnchor);
                cursor.removeSelectedText();
                end_sel-= comment_multiline_start.size();
                cursor.setPosition(end_sel-comment_multiline_end.size());
                cursor.setPosition(end_sel,  QTextCursor::KeepAnchor);
                cursor.removeSelectedText();
                end_sel-= comment_multiline_end.size();

                cursor.setPosition((init_position > init_anchor) ? init_anchor : end_sel);
                cursor.setPosition((init_position > init_anchor) ? end_sel : init_position, QTextCursor::KeepAnchor);
                cursor.endEditBlock();
                setTextCursor(cursor);

                return;
            }
        }
    }

    cursor.setPosition(end_sel);

    int end_block_position = cursor.block().position();

    cursor.setPosition(start_sel);

    int start_block_position = cursor.block().position();

    if (!comment_line_start.isEmpty()
     && (start_sel == end_sel || (start_sel == start_block_position && end_sel == end_block_position)))
    {
        int delta = 0;

        cursor.beginEditBlock();
        cursor.movePosition(QTextCursor::StartOfLine);

        do
        {
            if (!cursor.block().text().isEmpty())
            {
                if (delta == 0)
                    delta = (cursor.block().text().startsWith(comment_line_start))
                            ? -comment_line_start.size()
                            : comment_line_start.size();

                if (delta < 0)
                {
                    if (cursor.block().text().startsWith(comment_line_start))
                    {
                        cursor.setPosition(cursor.block().position()-delta, QTextCursor::KeepAnchor);
                        cursor.removeSelectedText();
                        end_sel += delta;
                    }
                }
                else
                {
                    cursor.insertText(comment_line_start);
                    end_sel += delta;
                }
            }

            if (!cursor.movePosition(QTextCursor::QTextCursor::NextBlock))
                break;
        }
        while(cursor.position() < end_sel);

        if (init_position == init_anchor)
            cursor.setPosition(end_sel);
        else
        {
            cursor.setPosition((init_position > init_anchor) ? init_anchor : end_sel);
            cursor.setPosition((init_position > init_anchor) ? end_sel : init_position, QTextCursor::KeepAnchor);
        }
        cursor.endEditBlock();
        setTextCursor(cursor);

        return;
    }

    if (!comment_multiline_start.isEmpty() && init_position != init_anchor)
    {
        cursor.beginEditBlock();
        cursor.setPosition(start_sel);
        cursor.insertText(comment_multiline_start);
        end_sel += comment_multiline_start.size();
        cursor.setPosition(end_sel);
        cursor.insertText(comment_multiline_end);
        end_sel += comment_multiline_end.size();

        cursor.setPosition((init_position > init_anchor) ? init_anchor : end_sel);
        cursor.setPosition((init_position > init_anchor) ? end_sel : init_position, QTextCursor::KeepAnchor);
        cursor.endEditBlock();
        setTextCursor(cursor);
    }
}

void CodeEditor::performTabReplacement()
{
    if (!isTabReplacementNecessary())
    {
        return;
    }

    QTextCursor cursor          = textCursor();
    int         last_position   = cursor.position();
    int         last_block_number=cursor.blockNumber();
    int         selection_start = cursor.selectionStart();
    int         selection_end   = cursor.selectionEnd();

    cursor.setPosition(selection_start);
    int         start_block_number   = cursor.blockNumber();
    int         start_block_position = cursor.positionInBlock();

    cursor.setPosition(selection_end);
    int         end_block_number   = cursor.blockNumber();
    int         end_block_position = cursor.positionInBlock();

    int         new_selection_start = 0;
    int         new_selection_end = 0;

    bool        keep_selection = true;

    QTextBlock  block  = document()->firstBlock();

    cursor.beginEditBlock();

    while(block.isValid())
    {
        cursor.setPosition(block.position());

        int changed = performTabReplacement(cursor);

        if (block.blockNumber() == start_block_number)
        {
            if (changed != 0)
                keep_selection = false;
            else
                new_selection_start = block.position() + std::min(block.text().size(),start_block_position);
        }

        if (block.blockNumber() == end_block_number)
        {
            if (changed != 0)
                keep_selection = false;
            else
                new_selection_end = block.position() + std::min(block.text().size(),end_block_position);
        }

        if (last_block_number == block.blockNumber())
        {
            if (changed != 0)
                last_position = std::min(last_position, block.position() + block.text().size());
        }
        else if (block.blockNumber() < last_block_number)
            last_position += changed;

        block = block.next();
    }

    cursor.endEditBlock();

    if (selection_start == selection_end || !keep_selection)
    {
        cursor.setPosition(last_position);
    }
    else if (last_position == selection_start)
    {
        cursor.setPosition(new_selection_end);
        cursor.setPosition(new_selection_start, QTextCursor::KeepAnchor);
    }
    else
    {
        cursor.setPosition(new_selection_start);
        cursor.setPosition(new_selection_end, QTextCursor::KeepAnchor);
    }

    setTextCursor(cursor);
}

void CodeEditor::performTab()
{
    QTextCursor cursor = textCursor();

    cursor.beginEditBlock();

    if (isTabReplacementNecessary())
    {
        performTabReplacement(cursor);
    }

    int cursor_last_position = cursor.position();
    int line_position        = cursor.block().position();
    int shift                = _parameters.tab_size - (cursor_last_position - line_position) % _parameters.tab_size;

    cursor.insertText(QString(shift, ' '));

    cursor.endEditBlock();
    setTextCursor(cursor);
}

int CodeEditor::performTabReplacement(QTextCursor &cursor)
{
    int cursor_last_position = cursor.position();
    int line_position        = cursor.block().position();
    int position             = line_position;
    int changed              = 0;

    // replace tab to spaces in whole line

    while(true)
    {
        int     i    = position - line_position;
        QString text = cursor.block().text();

        if (i >= text.size())
            break;

        if (text[i] == '\t')
        {
            int shift = _parameters.tab_size - i % _parameters.tab_size;

            cursor.setPosition(position);
            cursor.setPosition(position + 1, QTextCursor::KeepAnchor);
            cursor.insertText(QString(shift, ' '));
            if (position < cursor_last_position)
                cursor_last_position += shift-1;
            position += shift;
            changed += shift-1;
        }
        else
            position ++;
    }

    // remove last spaces

    int     space_count = 0;
    QString text        = cursor.block().text();

    position = line_position + text.size();

    for(; space_count < text.size(); ++space_count)
    {
        if (position-space_count <= cursor_last_position)
            break;

        if (text[text.size()-space_count-1] != ' ')
            break;
    }

    if (space_count > 0)
    {
        cursor.setPosition(position);
        cursor.setPosition(position - space_count, QTextCursor::KeepAnchor);
        cursor.removeSelectedText();
        changed -= space_count;
    }

    cursor.setPosition(cursor_last_position);

    return changed;
}

bool CodeEditor::isTabReplacementNecessary()
{
    return _parameters.is_tab_replacement_necessary
        && (_rule_data == nullptr
            || !_rule_data->prevent_text_tab_replacement()
            )
        ;
}

void CodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly())
    {
        QTextEdit::ExtraSelection selection;

        selection.format.setBackground(_color_theme.current_line_color);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

