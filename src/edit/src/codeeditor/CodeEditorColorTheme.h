#ifndef CODEEDITORCOLORTHEME_H
#define CODEEDITORCOLORTHEME_H

#include <QTextCharFormat>

struct CodeEditorColorTheme
{
    QColor  text_background_color;
    QColor  alternate_text_background_color;
    QColor  text_foreground_color;
    QColor  selection_bg_color;
    QColor  selection_fg_color;

    QColor  current_line_color;
    QColor  line_number_area_color;
    QColor  line_number_color;
    QColor  line_number_current_line_color;
    QColor  line_number_area_color_error;
    QColor  line_number_color_error;
    QColor  line_number_collapsed_foreground_color;
    QColor  line_number_collapsed_background_color;

    QColor                          information_underline_color;
    QTextCharFormat::UnderlineStyle information_underline_style;
    QColor                          warning_underline_color;
    QTextCharFormat::UnderlineStyle warning_underline_style;
    QColor                          error_underline_color;
    QTextCharFormat::UnderlineStyle error_underline_style;
    QColor                          spell_underline_color;
    QTextCharFormat::UnderlineStyle spell_underline_style;
    QColor                          id_spell_underline_color;
    QTextCharFormat::UnderlineStyle id_spell_underline_style;

    QTextCharFormat punctuation_format;
    QTextCharFormat keyword_format;
    QTextCharFormat word_format;
    QTextCharFormat comment_format;
    QTextCharFormat annotation_format;
    QTextCharFormat number_format;
    QTextCharFormat error_format;

    QTextCharFormat module_format;
    QTextCharFormat parameter_format;
    QTextCharFormat type_format;
    QTextCharFormat function_format;
    QTextCharFormat tuple_format;
    QTextCharFormat input_format;
    QTextCharFormat output_format;
    QTextCharFormat method_format;

};

#endif // CODEEDITORCOLORTHEME_H
