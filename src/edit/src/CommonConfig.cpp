#include "CommonConfig.h"
#include "MainWindow.h"

#include <QApplication>
#include <QListWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

void loadRule(QString filename);

CodeEditorColorTheme CommonConfig::setupDefaultHighlightTheme() const
{
    CodeEditorColorTheme ct;

    ct.current_line_color = "#e0e0e0";
    ct.line_number_area_color = Qt::lightGray;
    ct.line_number_color = Qt::black;
    ct.line_number_current_line_color = Qt::black;
    ct.line_number_area_color_error = "#e0c0c0";
    ct.line_number_color_error = Qt::black;
    ct.line_number_collapsed_foreground_color = Qt::black;
    ct.line_number_collapsed_background_color = Qt::white;
    ct.text_background_color = Qt::white;
    ct.alternate_text_background_color = Qt::gray;
    ct.text_foreground_color = Qt::black;
    ct.selection_bg_color = Qt::gray;
    ct.selection_fg_color = Qt::white;

    ct.punctuation_format.setForeground(Qt::black);
    ct.punctuation_format.setFontWeight(QFont::Normal);

    ct.keyword_format.setForeground(Qt::darkRed);
    ct.keyword_format.setFontWeight(QFont::Bold);

    ct.word_format.setForeground(Qt::darkBlue);
    ct.word_format.setFontWeight(QFont::Normal);

    ct.comment_format.setForeground(Qt::darkGray);
    ct.comment_format.setFontWeight(QFont::Normal);

    ct.annotation_format.setForeground(Qt::darkCyan);
    ct.annotation_format.setFontWeight(QFont::Bold);

    ct.number_format.setForeground(Qt::magenta);
    ct.number_format.setFontWeight(QFont::Bold);

    ct.error_format.setForeground(Qt::gray);
    ct.error_format.setFontWeight(QFont::Normal);
    ct.error_format.setFontItalic(true);
    ct.error_format.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);
    ct.error_format.setUnderlineColor(Qt::red);

    ct.module_format.setForeground(Qt::red);
    ct.module_format.setFontWeight(QFont::Bold);

    ct.parameter_format.setForeground(Qt::darkGreen);
    ct.parameter_format.setFontWeight(QFont::Normal);
    ct.parameter_format.setFontItalic(true);

    ct.type_format.setForeground(Qt::darkMagenta);
    ct.type_format.setFontWeight(QFont::Normal);

    ct.function_format.setForeground(Qt::darkGreen);
    ct.function_format.setFontWeight(QFont::Normal);

    ct.tuple_format.setForeground(Qt::darkBlue);
    ct.tuple_format.setFontWeight(QFont::Normal);

    ct.input_format.setForeground(Qt::darkBlue);
    ct.input_format.setFontWeight(QFont::Normal);

    ct.output_format.setForeground(Qt::darkBlue);
    ct.output_format.setFontWeight(QFont::Normal);

    ct.method_format.setForeground(Qt::darkBlue);
    ct.method_format.setFontWeight(QFont::Normal);

    ct.information_underline_color = Qt::darkCyan;
    ct.information_underline_style = QTextCharFormat::DotLine;
    ct.warning_underline_color     = Qt::darkYellow;
    ct.warning_underline_style     = QTextCharFormat::DashUnderline;
    ct.error_underline_color       = Qt::red;
    ct.error_underline_style       = QTextCharFormat::SingleUnderline;

    ct.spell_underline_color       = Qt::red;
    ct.spell_underline_style       = QTextCharFormat::SpellCheckUnderline;
    ct.id_spell_underline_color    = Qt::darkCyan;
    ct.id_spell_underline_style    = QTextCharFormat::SpellCheckUnderline;

    return ct;
}

AppChangeResult CommonConfig::setDecorTheme(QString theme_name)
{
    AppChangeResult res = AppChangeResult::NoActionRequired;

    QString previous_stylesheet;
    QString previous_iconstheme;
    QString previous_highlight;

    for(const auto & dt : decor_themes)
        if (dt.name == decor_theme_name)
        {
            previous_stylesheet = dt.stylesheet;
            previous_iconstheme = dt.iconstheme;
            previous_highlight = dt.highlight;
            break;
        }

    for(const auto & dt : decor_themes)
        if (dt.name == theme_name)
        {
            if (dt.iconstheme != previous_iconstheme
             && (dt.iconstheme.isEmpty() || previous_iconstheme.isEmpty()))
                res = AppChangeResult::ReloadRequired;
            else if (dt.highlight != previous_highlight)
                res = AppChangeResult::EditorUpdateRequired;
            else if (dt.stylesheet != previous_stylesheet)
                res = AppChangeResult::NoActionRequired;

            // Setup stylesheet

            if (!dt.stylesheet.isEmpty())
            {
                QString image_path = data_dir + "/ide/styles/" + dt.stylesheet;
                QFile file(image_path + ".qss");
                bool ok = file.open(QFile::ReadOnly);
                if (ok)
                {
                    QString style_sheet = QString::fromUtf8(file.readAll());
                    style_sheet.replace("url(","url("+image_path+"/");
                    qApp->setStyleSheet(style_sheet);
                }
            }

            // Setup icons

            if (default_icons_paths.empty())
                default_icons_paths = QIcon::themeSearchPaths();

            if (!dt.iconstheme.isEmpty())
            {
                QStringList paths;
                paths.push_back(data_dir + "/ide/icons");
                for(const auto & p : default_icons_paths)
                    paths.push_back(p);
                QIcon::setThemeSearchPaths(paths);
                QIcon::setThemeName(dt.iconstheme);
            }
            else
                QIcon::setThemeSearchPaths(default_icons_paths);

            QString path = ":/images";
            if (!QIcon::fallbackSearchPaths().contains(path))
            {
                QIcon::fallbackSearchPaths().push_back(path);
            }

            // Setup highlightes by default

            editor_color_theme = setupDefaultHighlightTheme();

            // Setup highlightes

            if (!dt.highlight.isEmpty())
                readHighlightTheme(dt.highlight, editor_color_theme);
        }

    return res;
}

void CommonConfig::readCommonJson()
{
    QString filename(data_dir + "/ide/common.json");

    QFile ruleFile(filename);

    if (!ruleFile.open(QIODevice::ReadOnly))
    {
        ErrorReport("Couldn't open common file '" + filename + "'");
        return;
    }

    QByteArray data = ruleFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(data));

    QJsonObject root = loadDoc.object();

    if (root.find("help_main_page_url") != root.end())
        help_main_page_url = root.value("help_main_page_url").toString();

    if (root.find("editor_font_name") != root.end())
        editor_font_name = root.value("editor_font_name").toString();

    if (root.find("hide") != root.end())
    {
        QJsonArray files = root.value("hide").toArray();

        for(auto w : files)
            hidden_names.push_back(w.toString());
    }

    if (root.find("terminal") != root.end())
        terminal = root.value("terminal").toString();

    if (root.find("spell_path") != root.end())
        spell_path = root.value("spell_path").toString();
    if (root.find("spell_english_dict_name") != root.end())
        spell_english_dict_name = root.value("spell_english_dict_name").toString();
    if (root.find("spell_national_dict_name") != root.end())
        spell_national_dict_name = root.value("spell_national_dict_name").toString();
    if (root.find("spell_encoding") != root.end())
        spell_encoding = root.value("spell_encoding").toString();

    if (root.find("theme-groups") != root.end())
    {
        QJsonArray files = root.value("theme-groups").toArray();

        for(auto w : files)
            decor_themes_groups.push_back(w.toString());
    }
    else
    {
        ErrorReport("Couldn't load common file '" + filename + "' (required parameters not found)");
        return;
    }

    if (root.find("themes") != root.end())
    {
        QJsonArray themes = root.value("themes").toArray();

        for(auto theme : themes)
        {
            QJsonObject m = theme.toObject();

            DecorTheme  decor;

            if (m.find("group") != m.end())
                decor.group = m.value("group").toString();
            if (m.find("name") != m.end())
                decor.name = m.value("name").toString();
            if (m.find("displayname") != m.end())
                decor.displayname = m.value("displayname").toString();
            if (m.find("copyright") != m.end())
                decor.copyright = m.value("copyright").toString();
            if (m.find("license") != m.end())
                decor.license = m.value("license").toString();
            if (m.find("description") != m.end())
                decor.description = m.value("description").toString();
            if (m.find("sourceurl") != m.end())
                decor.sourceurl = m.value("sourceurl").toString();
            if (m.find("help") != m.end())
                decor.help = m.value("help").toString();
            if (m.find("icon") != m.end())
                decor.icon = m.value("icon").toString();
            if (m.find("stylesheet") != m.end())
                decor.stylesheet = m.value("stylesheet").toString();
            if (m.find("iconstheme") != m.end())
                decor.iconstheme = m.value("iconstheme").toString();
            if (m.find("highlight") != m.end())
                decor.highlight = m.value("highlight").toString();
            if (m.find("active") != m.end())
                decor.active = m.value("active").toBool();

            decor_themes.push_back(decor);
        }
    }
    else
    {
        ErrorReport("Couldn't load common file '" + filename + "' (required parameters not found)");
        return;
    }
}

bool CommonConfig::readHighlightTheme(QString highlight_theme_name, CodeEditorColorTheme &ct)
{
    QString filename(data_dir + "/ide/highlights/"+highlight_theme_name+".json");

    QFile ruleFile(filename);

    if (!ruleFile.open(QIODevice::ReadOnly))
    {
        ErrorReport("Couldn't open highlight theme file '" + filename + "'");
        return false;
    }

    QByteArray data = ruleFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(data));

    QJsonObject root = loadDoc.object();

    if (root.find("text_background_color") != root.end())
        ct.text_background_color.setNamedColor(root.value("text_background_color").toString());
    if (root.find("alternate_text_background_color") != root.end())
        ct.alternate_text_background_color.setNamedColor(root.value("alternate_text_background_color").toString());
    if (root.find("text_foreground_color") != root.end())
        ct.text_foreground_color.setNamedColor(root.value("text_foreground_color").toString());
    if (root.find("selection_bg_color") != root.end())
        ct.selection_bg_color.setNamedColor(root.value("selection_bg_color").toString());
    if (root.find("selection_fg_color") != root.end())
        ct.selection_fg_color.setNamedColor(root.value("selection_fg_color").toString());

    if (root.find("current_line_color") != root.end())
        ct.current_line_color.setNamedColor(root.value("current_line_color").toString());
    if (root.find("line_number_area_color") != root.end())
        ct.line_number_area_color.setNamedColor(root.value("line_number_area_color").toString());
    if (root.find("line_number_color") != root.end())
        ct.line_number_color.setNamedColor(root.value("line_number_color").toString());
    if (root.find("line_number_current_line_color") != root.end())
        ct.line_number_current_line_color.setNamedColor(root.value("line_number_current_line_color").toString());
    if (root.find("line_number_area_color_error") != root.end())
        ct.line_number_area_color_error.setNamedColor(root.value("line_number_area_color_error").toString());
    if (root.find("line_number_color_error") != root.end())
        ct.line_number_color_error.setNamedColor(root.value("line_number_color_error").toString());
    if (root.find("line_number_collapsed_foreground_color") != root.end())
        ct.line_number_collapsed_foreground_color.setNamedColor(root.value("line_number_collapsed_foreground_color").toString());
    if (root.find("line_number_collapsed_background_color") != root.end())
        ct.line_number_collapsed_background_color.setNamedColor(root.value("line_number_collapsed_background_color").toString());

    if (root.find("information_underline_color") != root.end())
        ct.information_underline_color.setNamedColor(root.value("information_underline_color").toString());
    if (root.find("information_underline_style") != root.end())
        ct.information_underline_style = getUnderlineStyle(root.value("information_underline_style").toString());
    if (root.find("warning_underline_color") != root.end())
        ct.warning_underline_color.setNamedColor(root.value("warning_underline_color").toString());
    if (root.find("warning_underline_style") != root.end())
        ct.warning_underline_style = getUnderlineStyle(root.value("warning_underline_style").toString());
    if (root.find("error_underline_color") != root.end())
        ct.error_underline_color.setNamedColor(root.value("error_underline_color").toString());
    if (root.find("error_underline_style") != root.end())
        ct.error_underline_style = getUnderlineStyle(root.value("error_underline_style").toString());

    if (root.find("spell_underline_color") != root.end())
        ct.spell_underline_color.setNamedColor(root.value("spell_underline_color").toString());
    if (root.find("spell_underline_style") != root.end())
        ct.spell_underline_style = getUnderlineStyle(root.value("spell_underline_style").toString());
    if (root.find("id_spell_underline_color") != root.end())
        ct.id_spell_underline_color.setNamedColor(root.value("id_spell_underline_color").toString());
    if (root.find("id_spell_underline_style") != root.end())
        ct.id_spell_underline_style = getUnderlineStyle(root.value("id_spell_underline_style").toString());

    if (root.find("punctuation_format") != root.end())
        ct.punctuation_format = getTextCharFormat(root.value("punctuation_format").toObject());
    if (root.find("keyword_format") != root.end())
        ct.keyword_format = getTextCharFormat(root.value("keyword_format").toObject());
    if (root.find("word_format") != root.end())
        ct.word_format = getTextCharFormat(root.value("word_format").toObject());
    if (root.find("comment_format") != root.end())
        ct.comment_format = getTextCharFormat(root.value("comment_format").toObject());
    if (root.find("annotation_format") != root.end())
        ct.annotation_format = getTextCharFormat(root.value("annotation_format").toObject());
    if (root.find("number_format") != root.end())
        ct.number_format = getTextCharFormat(root.value("number_format").toObject());
    if (root.find("error_format") != root.end())
        ct.error_format = getTextCharFormat(root.value("error_format").toObject());

    if (root.find("module_format") != root.end())
        ct.module_format = getTextCharFormat(root.value("module_format").toObject());
    if (root.find("parameter_format") != root.end())
        ct.parameter_format = getTextCharFormat(root.value("parameter_format").toObject());
    if (root.find("type_format") != root.end())
        ct.type_format = getTextCharFormat(root.value("type_format").toObject());
    if (root.find("function_format") != root.end())
        ct.function_format = getTextCharFormat(root.value("function_format").toObject());
    if (root.find("tuple_format") != root.end())
        ct.tuple_format = getTextCharFormat(root.value("tuple_format").toObject());
    if (root.find("input_format") != root.end())
        ct.input_format = getTextCharFormat(root.value("input_format").toObject());
    if (root.find("output_format") != root.end())
        ct.output_format = getTextCharFormat(root.value("output_format").toObject());
    if (root.find("method_format") != root.end())
        ct.method_format = getTextCharFormat(root.value("method_format").toObject());

    return true;
}

QTextCharFormat::UnderlineStyle CommonConfig::getUnderlineStyle(QString underline_style_name)
{
    QTextCharFormat::UnderlineStyle us = QTextCharFormat::NoUnderline;

    if (underline_style_name == "SingleUnderline")
        us = QTextCharFormat::SingleUnderline;
    else if (underline_style_name == "DashUnderline")
        us = QTextCharFormat::DashUnderline;
    else if (underline_style_name == "DotLine")
        us = QTextCharFormat::DotLine;
    else if (underline_style_name == "DashDotLine")
        us = QTextCharFormat::DashDotLine;
    else if (underline_style_name == "DashDotDotLine")
        us = QTextCharFormat::DashDotDotLine;
    else if (underline_style_name == "WaveUnderline")
        us = QTextCharFormat::WaveUnderline;
    else if (underline_style_name == "SpellCheckUnderline")
        us = QTextCharFormat::SpellCheckUnderline;

    return us;
}

QTextCharFormat CommonConfig::getTextCharFormat(QJsonObject obj)
{
    QTextCharFormat format;

    if (obj.find("background") != obj.end())
    {
        QColor c;
        c.setNamedColor(obj.value("background").toString());
        format.setBackground(c);
    }
    if (obj.find("foreground") != obj.end())
    {
        QColor c;
        c.setNamedColor(obj.value("foreground").toString());
        format.setForeground(c);
    }
    if (obj.find("strikeout") != obj.end())
        format.setFontStrikeOut(obj.value("strikeout").toBool());
    if (obj.find("italic") != obj.end())
        format.setFontItalic(obj.value("italic").toBool());
    if (obj.find("underline_color") != obj.end())
    {
        QColor c;
        c.setNamedColor(obj.value("underline_color").toString());
        format.setUnderlineColor(c);
    }
    if (obj.find("underline_style") != obj.end())
        format.setUnderlineStyle(getUnderlineStyle(obj.value("underline_style").toString()));
    if (obj.find("weight") != obj.end())
        format.setFontWeight(getFontWeight(obj.value("weight").toString()));

    return format;
}

int CommonConfig::getFontWeight(QString sw)
{
    int weight = QFont::Normal;

    if (sw == "Thin")
        weight = QFont::Thin;
    else if (sw == "ExtraLight")
        weight = QFont::ExtraLight;
    else if (sw == "Light")
        weight = QFont::Light;
    else if (sw == "Normal")
        weight = QFont::Normal;
    else if (sw == "Medium")
        weight = QFont::Medium;
    else if (sw == "DemiBold")
        weight = QFont::DemiBold;
    else if (sw == "Bold")
        weight = QFont::Bold;
    else if (sw == "ExtraBold")
        weight = QFont::ExtraBold;
    else if (sw == "Black")
        weight = QFont::Black;

    return weight;
}

void CommonConfig::ErrorReport(QString text)
{
    if (error_list == nullptr)
        return;

    error_list->addItem("Error: " + text);
}
