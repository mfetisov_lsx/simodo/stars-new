#include "FindReplace.h"
#include "ui_FindReplace.h"

#include "MainWindow.h"
#include "MdiChild.h"

#include <QMessageBox>

FindReplace::FindReplace(bool replace, MainWindow *parent)
    : QDialog(parent)
    , _ui(new Ui::FindReplace)
    , _main_window(parent)
    , _is_replace(replace)
{
    _ui->setupUi(this);
    setFindReplaceEnabled(false);

    _ui->lbReplace->setVisible(replace);
    _ui->leReplace->setVisible(replace);
    _ui->cbReplaceAll->setVisible(replace);
    _ui->bnReplace->setVisible(replace);

    connect(_ui->bnFind, &QPushButton::pressed, this, &FindReplace::find);
    connect(_ui->leFind, &QLineEdit::returnPressed, this, &FindReplace::find);

    if (replace)
    {
        connect(_ui->bnReplace, &QPushButton::pressed, this, &FindReplace::replace);
        connect(_ui->leReplace, &QLineEdit::returnPressed, this, &FindReplace::replace);

        setMaximumHeight(230);
    }
    else
        setMaximumHeight(140);

    adjustSize();
}

FindReplace::~FindReplace()
{
    delete _ui;
}

void FindReplace::setFocus(bool /*checked*/)
{
    if (_is_replace)
        _main_window->_replace_dock->setVisible(!_main_window->_replace_dock->isVisible());
    else
        _main_window->_find_dock->setVisible(!_main_window->_find_dock->isVisible());

    if (isVisible())
    {
        if (_is_replace)
            _main_window->_find_dock->setVisible(false);
        else
            _main_window->_replace_dock->setVisible(false);

        _ui->leFind->setFocus();
    }
}

bool FindReplace::isReplace() const
{
    return _is_replace;
}

void FindReplace::setFindReplaceEnabled(bool enable)
{
    _ui->bnFind->setEnabled(enable);
    _ui->bnReplace->setEnabled(enable);
}

void FindReplace::find()
{
    MdiChild * editor = _main_window->activeMdiChild();
    if (editor != nullptr && !_ui->leFind->text().isEmpty())
    {
        QTextDocument::FindFlags options;

        if (_ui->cbWholeWord->isChecked())
            options |= QTextDocument::FindWholeWords;

        if (_ui->cbCaseSensitive->isChecked())
            options |= QTextDocument::FindCaseSensitively;

        while(true)
        {
            bool ok = editor->find(_ui->leFind->text(), options);

            if (ok)
            {
                editor->setFocus();
                break;
            }
            else if (!letSearchFromBeginning(editor))
                break;
        }
    }
}

void FindReplace::replace()
{
    MdiChild * editor = _main_window->activeMdiChild();
    if (editor != nullptr && !_ui->leFind->text().isEmpty())
    {
        QTextDocument::FindFlags options;

        if (_ui->cbWholeWord->isChecked())
            options |= QTextDocument::FindWholeWords;

        if (_ui->cbCaseSensitive->isChecked())
            options |= QTextDocument::FindCaseSensitively;

        bool replace_all = _ui->cbReplaceAll->isChecked();
        bool ok          = true;

        if (editor->textCursor().hasSelection())
        {
            // thes need for replase selected text, if match
            QTextCursor cursor = editor->textCursor();
            cursor.setPosition(cursor.selectionStart());
            editor->setTextCursor(cursor);
        }

        while(true)
        {
            do
            {
                ok = editor->find(_ui->leFind->text(), options);
                if (!ok)
                    break;

                QTextCursor selection = editor->textCursor();
                selection.insertText(_ui->leReplace->text());
            }
            while(replace_all);

            if (ok)
            {
                editor->setFocus();
                break;
            }
            else if (!letSearchFromBeginning(editor))
                break;
        }
    }
}

void FindReplace::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        e->ignore();
}

bool FindReplace::letSearchFromBeginning(MdiChild *editor)
{
    const QMessageBox::StandardButton ret
            = QMessageBox::warning(this, tr("SIMODO"),
                                   tr("Sample '%1' not found.\n"
                                      "Search from the beginning of the document?")
                                   .arg(_ui->leFind->text()),
                                   QMessageBox::Yes | QMessageBox::No);
    if (ret == QMessageBox::Yes)
    {
        QTextCursor cur = editor->textCursor();
        cur.setPosition(0);
        editor->setTextCursor(cur);
    }

    return (ret == QMessageBox::Yes);
}

