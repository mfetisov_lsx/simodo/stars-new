#ifndef GRAPH3D_H
#define GRAPH3D_H

#include <QDockWidget>
#include <QMenu>
#include <Q3DScatter>

#include "CommonConfig.h"

using Graph3DPoint = std::pair<QString, QVector3D>;
using Graph3DPointVector = std::vector<Graph3DPoint>;

class Graph3D : public QtDataVisualization::Q3DScatter
{
    Q_OBJECT

    QDockWidget * _dock_widget;
    CommonConfig & _config;
    QActionGroup * _theme_action_group;

    std::map<QString,int> _series_point_counts;

public:
    Graph3D(QDockWidget *dock_widget, CommonConfig & config);

    void createThemesActions(QMenu *theme_menu);

    float X() const { return scene()->activeCamera()->xRotation(); }
    float Y() const { return scene()->activeCamera()->yRotation(); }
    float zoom() const { return scene()->activeCamera()->zoomLevel(); }

public slots:
    void handleInitialization(QString title);
    void handleAddSeries(QString series_name, int mesh);
    void handleAddPoint(Graph3DPointVector points);
    void handleSetPointsCount(QString series_name, int count);
};

#endif // GRAPH3D_H
