#ifndef WORKDIRECTORYGETNAME_H
#define WORKDIRECTORYGETNAME_H

#include <QDialog>

namespace Ui {
class WorkDirectoryGetName;
}

class WorkDirectoryGetName : public QDialog
{
    Q_OBJECT

public:
    explicit WorkDirectoryGetName(QWidget *parent = nullptr);
    ~WorkDirectoryGetName();

    void setIntro(QString intro);
    QString getText() const;

private:
    Ui::WorkDirectoryGetName *ui;
};

#endif // WORKDIRECTORYGETNAME_H
