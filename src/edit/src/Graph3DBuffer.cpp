#include "Graph3DBuffer.h"

#include <chrono>
#include <algorithm>
#include <vector>

static long current_millis()
{
    auto time = std::chrono::system_clock::now(); // get the current time
    auto since_epoch = time.time_since_epoch(); // get the duration since epoch
    // I don't know what system_clock returns
    // I think it's uint64_t nanoseconds since epoch
    // Either way this duration_cast will do the right thing
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);
    return millis.count(); // just like java (new Date()).getTime();
}

Graph3DBuffer::Graph3DBuffer(QObject * parent)
    : QObject(parent)
    , _timer_id(0)
{}

Graph3DBuffer::~Graph3DBuffer()
{
    _killTimer();
}

void Graph3DBuffer::_killTimer()
{
    if (_timer_id != 0)
    {
        killTimer(_timer_id);
        _timer_id = 0;
    }
}

void Graph3DBuffer::startBufferTimer()
{
    _killTimer();

    _timer_id = startTimer(TIMER_INTERVAL);
    _timer_start_time = current_millis();
}

void Graph3DBuffer::addItem(Graph3DPoint item)
{
    _buffer.push_back(item);

    if (_buffer.size() > 2000)
    {
        _buffer.pop_front();
    }

    if (current_millis() - _timer_start_time >= TIMER_INTERVAL)
    {
        QTimerEvent event(_timer_id);
        timerEvent(&event);
    }
}

void Graph3DBuffer::timerEvent(QTimerEvent * event)
{
    if (event->timerId() != _timer_id)
    {
        QObject::timerEvent(event);
        return;
    }

    _timer_start_time = current_millis();

    if (_buffer.empty())
    {
        return;
    }

    int send_count = _buffer.size() < 200 ? _buffer.size() : 200;
    Graph3DPointVector send_buffer(_buffer.begin(), _buffer.begin() + send_count);
    _buffer.erase(_buffer.begin(), _buffer.begin() + send_count);

    emit addBufferedItems(std::move(send_buffer));
}
