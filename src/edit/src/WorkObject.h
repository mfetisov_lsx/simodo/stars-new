#ifndef WORKOBJECT_H
#define WORKOBJECT_H

#include <QObject>

#include "simodo/dsl/ScriptC_Interpreter.h"

class MdiChild;

class WorkObject : public QObject
{
    Q_OBJECT

public:
    explicit WorkObject(QObject * parent = nullptr);

    simodo::dsl::ScriptC_Interpreter * getActiveMachine() const { return _active_machine; }

    void stopExecution();

public slots:
    void doWork(MdiChild * editor, bool needExecute);

signals:
    void workIsOver(MdiChild * editor, bool success);

private:
    simodo::dsl::ScriptC_Interpreter * _active_machine;
};

#endif // WORKOBJECT_H
