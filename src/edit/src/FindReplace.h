#ifndef FINDREPLACE_H
#define FINDREPLACE_H

#include <QDialog>

namespace Ui {
class FindReplace;
}

class MainWindow;
class MdiChild;

class FindReplace : public QDialog
{
    Q_OBJECT

public:
    explicit FindReplace(bool replace, MainWindow *parent = nullptr);
    ~FindReplace();

    bool isReplace() const;
    void setFindReplaceEnabled(bool enable);

public slots:
    void setFocus(bool = false);
    void find();
    void replace();

protected:
    virtual void keyPressEvent(QKeyEvent *e) override;

private:
    bool letSearchFromBeginning(MdiChild * editor);

private:
    Ui::FindReplace * _ui;
    MainWindow *      _main_window;
    bool              _is_replace;
};

#endif // FINDREPLACE_H
