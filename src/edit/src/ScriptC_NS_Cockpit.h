#ifndef SCRIPTC_NS_COCKPIT_H
#define SCRIPTC_NS_COCKPIT_H

#include "cockpit/CockpitView.h"
#include "cockpit/Cockpit3D.h"
#include "simodo/dsl/SemanticBase.h"

#include <QObject>

#include <vector>
#include <unordered_map>
#include <string>
#include <atomic>

class ScriptC_NS_Cockpit : public QObject, public simodo::dsl::IScriptC_Namespace
{
    Q_OBJECT

    CockpitView *   _cockpit_view;
    Cockpit3D *     _cockpit3d;

    std::unordered_map<std::string, std::atomic_bool> _key_states;

public:
    ScriptC_NS_Cockpit(CockpitView * cockpit_view = nullptr, Cockpit3D * cockpit3d = nullptr);
    virtual ~ScriptC_NS_Cockpit();

    void setPosition(double x, double height, double z);

    // Вид из кабины вперёд
    // Тангаж -90..+90, > 0 подъём носа вверх
    // Курс 0..360, > 0 поворот вправо
    // Крен -180..+180, > 0 крен на правое крыло
    void setAngles(double pitch, double course, double roll);

    void setSpeed(double speed);
    void setVerticalSpeedSlide(double vertical_speed, double slide);

    bool getKeyState(std::string key);

    virtual simodo::dsl::SCI_Namespace_t getNamespace() override;

signals:
    void changeHeight(qreal height);
    void changePosition(qreal x, qreal height, qreal z);

    // Вид из кабины вперёд
    // Тангаж -90..+90, > 0 подъём носа вверх
    // Курс 0..360, > 0 поворот вправо
    // Крен -180..+180, > 0 крен на правое крыло
    void changeAngles(qreal pitch, qreal course, qreal roll);

    void changeSpeed(qreal speed);
    void changeVerticalSpeedSlide(qreal vertical_speed, qreal slide);

public slots:
    void handlePressedKey(std::string key);
    void handleReleasedKey(std::string key);
};

#endif // SCRIPTC_NS_COCKPIT_H
