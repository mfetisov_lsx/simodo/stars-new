#include "BufferedListWidget.h"

#include <algorithm>

BufferedListWidget::BufferedListWidget(QWidget * parent, bool autoscroll)
    : QListWidget(parent)
    , _autoscroll(autoscroll)
    , _show_timer_id(startTimer(10 * HZ_60_INTERVAL))
    , _erase_timer_id(startTimer(SEC_1_INTERVAL))
{
    setWordWrap(true);
}

BufferedListWidget::~BufferedListWidget()
{
    if (_show_timer_id != 0)
    {
        killTimer(_show_timer_id);
    }

    if (_erase_timer_id != 0)
    {
        killTimer(_erase_timer_id);
    }

    clear();

    for (size_t i = 0; i < _input_buffer.size(); ++i)
    {
        delete _input_buffer.at(i);
    }

    for (size_t i = 0; i < _to_delete_buffer.size(); ++i)
    {
        delete _to_delete_buffer.at(i);
    }
}

void BufferedListWidget::timerEvent(QTimerEvent * event)
{
    auto timer_id = event->timerId();

    if (timer_id == _erase_timer_id)
    {
        _eraseTimerEvent();
        return;
    }

    if (timer_id == _show_timer_id)
    {
        _showTimerEvent();
        return;
    }

    QListWidget::timerEvent(event);
}

void BufferedListWidget::_eraseTimerEvent()
{
    int to_remove_count = _input_buffer.size() - BUFFER_SIZE;
    if (to_remove_count > 0)
    {
        for (int i = 0; i < to_remove_count; ++i)
        {
            delete _input_buffer.front();
            _input_buffer.pop_front();
        }
    }

    while(!_to_delete_buffer.empty())
    {
        delete _to_delete_buffer.front();
        _to_delete_buffer.pop_front();
    }
}

void BufferedListWidget::_showTimerEvent()
{
    if (_input_buffer.empty())
    {
        return;
    }

    int add_count = _input_buffer.size();
    if (N_ITEMS_TO_SHOW < add_count)
    {
        add_count = N_ITEMS_TO_SHOW;
    }
    for (int i = 0; i < add_count; ++i)
    {
        if (count() == BUFFER_SIZE)
        {
            auto removed_item = takeItem(0);
            (*removed_item) = *_input_buffer.front();
            addItem(removed_item);
        }
        else
        {
            _items.at(count()) = *_input_buffer.front();
            addItem(&_items.at(count()));
        }

        _to_delete_buffer.push_back(_input_buffer.front());
        _input_buffer.pop_front();
    }

    if (_autoscroll)
    {
        scrollToBottom();
    }
}

void BufferedListWidget::clear()
{
    while(count() != 0)
    {
        takeItem(0);
    }
}

void BufferedListWidget::addBufferedItem(QListWidgetItem * item)
{
    if (item == nullptr)
    {
        return;
    }

    _input_buffer.push_back(item);
}

void BufferedListWidget::addBufferedItems(std::vector<QListWidgetItem *> items)
{
    for (auto p : items)
    {
        addBufferedItem(p);
    }
}
