#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "simodo/dsl/Parser.h"

#include "SimodoRuler.h"

SimodoRuler::SimodoRuler(QString data_dir, QString work_dir, BufferedListWidget *messageList)
    : _data_dir(data_dir)
    , _work_dir(work_dir)
    , _reporter(messageList,nullptr,true)
    , _grammar_manager(_reporter, {work_dir.toStdString()+"/data/grammar", data_dir.toStdString()+"/grammar"})
{
    QDir dir_in_data_path(_data_dir + "/ide/rules");

    loadRulesFromPath(dir_in_data_path);

    QDir dir_in_work_path(_work_dir + "/data/ide/rules");

    if (dir_in_work_path.exists())
        loadRulesFromPath(dir_in_work_path);
}

bool SimodoRuler::loadRulesFromPath(QDir dir)
{
    qDebug() << "Loading rules from " << dir.path();

    dir.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot);

    QStringList filters;
    filters << "*.json";
    dir.setNameFilters(filters);

    QFileInfoList list = dir.entryInfoList();

    for (const QFileInfo & fi : list)
        loadRule(fi);

    return dir.isReadable();
}

RuleData *SimodoRuler::findRuledataByFilename(QString filename)
{
    QString rulename = findRulenameByFilename(filename);

    if (rulename.isEmpty())
        return nullptr;

    auto it = _rulename_to_ruledata_map.find(rulename);

    if (it == _rulename_to_ruledata_map.end())
        return nullptr;

    return &it.value();
}

QString SimodoRuler::findRulenameByFilename(QString filename)
{
    for(const auto & [mask,name] : _filemask_to_rulename_map)
    {
        QString pattern = QRegularExpression::wildcardToRegularExpression(mask);
        if (!pattern.contains("$"))
            pattern += "$";
        QRegularExpression re(pattern);
        QRegularExpressionMatch match = re.match(filename);
        bool hasMatch = match.hasMatch();
        if (hasMatch)
            return name;
    }

    return "";
}

void SimodoRuler::ErrorReport(QString text)
{
    _reporter.reportFatal(text.toStdU16String());
}


bool SimodoRuler::loadRule(const QFileInfo &file_info)
{
    QFile ruleFile(file_info.absoluteFilePath());

    if (!ruleFile.open(QIODevice::ReadOnly))
    {
        ErrorReport("Couldn't open rule file '" + file_info.absoluteFilePath() + "'");
        return false;
    }

    QByteArray    ruleData = ruleFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(ruleData));
    QJsonObject   root     = loadDoc.object();

    QStringList                     file_masks;
    QVector<QPair<QString,QString>> run_command_set;
    int                             collapsible_starts_block_number = -1;
    simodo::dsl::LexicalParameters  lexical;
    QString                         grammar_name;
    QString                         semantic_handler_name;
    QString                         interpreter_handler_name;
    bool                            prevent_text_tab_replacement = false;

    if (root.find("files") != root.end())   /// \todo doubling
    {
        QJsonArray files = root.value("files").toArray();

        for(auto w : files)
            file_masks.push_back(w.toString());
    }

    if (file_masks.isEmpty())
    {
        ErrorReport("Couldn't load rule file '" + file_info.absoluteFilePath() + "' (files loading error)");
        return false;
    }

    if (root.find("run_command_set") != root.end())
    {
        QJsonArray list = root.value("run_command_set").toArray();

        for(auto element : list)
            if (element.isObject())
            {
                QJsonObject obj     = element.toObject();
                QString     text    = obj.value("text").toString("");
                QString     command = obj.value("command").toString("");

                if (text.isEmpty())
                    text = command;
                if (!text.isEmpty())
                    run_command_set.push_back(QPair(text,command));
            }
    }

    if (root.find("collapsible_starts_block_number") != root.end())
        collapsible_starts_block_number = root.value("collapsible_starts_block_number").toInt();

    if (root.find("prevent_text_tab_replacement") != root.end())
        prevent_text_tab_replacement = root.value("prevent_text_tab_replacement").toBool();

    if (root.find("grammar") != root.end())
    {
        grammar_name = root.value("grammar").toString();

        if (!_grammar_manager.loadGrammar(false,grammar_name.toStdString()))
        {
            ErrorReport("Couldn't load rule file '" + file_info.absoluteFilePath() + "' (grammar loading error)");
            return false;
        }

        const simodo::dsl::Grammar & g = _grammar_manager.getGrammar(grammar_name.toStdString());

        lexical = simodo::dsl::Parser::makeLexicalParameters(g);

        if (root.find("semantic") != root.end())
            semantic_handler_name = root.value("semantic").toString();

        if (root.find("interpreter") != root.end())
            interpreter_handler_name = root.value("interpreter").toString();
    }
    else if (root.find("lexical") != root.end())
    {
        QJsonObject lo = root.value("lexical").toObject();

        if (lo.find("digits") != lo.end())
            lexical.digits = lo.value("digits").toString().toStdU16String();
        if (lo.find("latin_alphabet") != lo.end())
            lexical.latin_alphabet = lo.value("latin_alphabet").toString().toStdU16String();
        if (lo.find("national_alphabet") != lo.end())
            lexical.national_alphabet = lo.value("national_alphabet").toString().toStdU16String();
        if (lo.find("id_extra_symbols") != lo.end())
            lexical.id_extra_symbols = lo.value("id_extra_symbols").toString().toStdU16String();
        if (lo.find("punctuation_chars") != lo.end())
            lexical.punctuation_chars = lo.value("punctuation_chars").toString().toStdU16String();
        if (lo.find("is_case_sensitive") != lo.end())
            lexical.is_case_sensitive = lo.value("is_case_sensitive").toBool();
        if (lo.find("may_national_letters_use") != lo.end())
            lexical.may_national_letters_use = lo.value("may_national_letters_use").toBool();
        if (lo.find("may_national_letters_mix") != lo.end())
            lexical.may_national_letters_mix = lo.value("may_national_letters_mix").toBool();

        if (lo.find("markups") != lo.end())
        {
            QJsonArray markups = lo.value("markups").toArray();

            for(auto mvalue : markups)
            {
                QJsonObject m = mvalue.toObject();

                simodo::dsl::MarkupSymbol ms;

                if (m.find("start") != m.end())
                    ms.start = m.value("start").toString().toStdU16String();
                if (m.find("end") != m.end())
                    ms.end = m.value("end").toString().toStdU16String();
                if (m.find("ignore_sign") != m.end())
                    ms.ignore_sign = m.value("ignore_sign").toString().toStdU16String();
                if (m.find("type") != m.end())
                {
                    if (m.value("type").toString() == "Comment")
                        ms.type = simodo::dsl::LexemeType::Comment;
                    else if (m.value("type").toString() == "Annotation")
                        ms.type = simodo::dsl::LexemeType::Annotation;
                    else
                    {
                        ErrorReport("Couldn't load rule file '" + file_info.absoluteFilePath() + "' (incorrect markup type)");
                        return false;
                    }
                }

                lexical.markups.push_back(ms);
            }
        }

        if (lo.find("masks") != lo.end())
        {
            QJsonArray masks = lo.value("masks").toArray();

            lexical.masks.clear();

            for(auto mvalue : masks)
            {
                QJsonObject m = mvalue.toObject();

                simodo::dsl::NumberMask mask {u"", simodo::dsl::LexemeType::Number, 10};

                if (m.find("chars") != m.end())
                    mask.chars = m.value("chars").toString().toStdU16String();
                if (m.find("type") != m.end())
                {
                    if (m.value("type").toString() == "Number")
                        mask.type = simodo::dsl::LexemeType::Number;
                    else
                    {
                        ErrorReport("Couldn't load rule file '" + file_info.absoluteFilePath() + "' (incorrect number mask type)");
                        return false;
                    }
                }
                if (m.find("system") != m.end())
                    mask.system = m.value("system").toInt();

                if (!mask.chars.empty())
                    lexical.masks.push_back(mask);
            }
        }

        if (lo.find("punctuation_words") != lo.end())
        {
            QJsonArray words = lo.value("punctuation_words").toArray();

            for(auto w : words)
                lexical.punctuation_words.push_back(w.toString().toStdU16String());
        }
    }
    else
    {
        ErrorReport("Couldn't load rule file '" + file_info.absoluteFilePath() + "' (required parameters not found)");
        return false;
    }

    std::vector<std::u16string> error_format_words;
    if (root.find("error_format_words") != root.end())
    {
        QJsonArray list = root.value("error_format_words").toArray();

        for(auto w : list)
            error_format_words.push_back(w.toString().toStdU16String());
    }

    std::vector<std::u16string> keyword_format_words;
    if (root.find("keyword_format_words") != root.end())
    {
        QJsonArray list = root.value("keyword_format_words").toArray();

        for(auto w : list)
            keyword_format_words.push_back(w.toString().toStdU16String());
    }

    std::vector<std::u16string> module_format_words;
    if (root.find("module_format_words") != root.end())
    {
        QJsonArray list = root.value("module_format_words").toArray();

        for(auto w : list)
            module_format_words.push_back(w.toString().toStdU16String());
    }

    std::vector<std::u16string> parameter_format_words;
    if (root.find("parameter_format_words") != root.end())
    {
        QJsonArray list = root.value("parameter_format_words").toArray();

        for(auto w : list)
            parameter_format_words.push_back(w.toString().toStdU16String());
    }

    std::vector<std::u16string> type_format_words;
    if (root.find("type_format_words") != root.end())
    {
        QJsonArray list = root.value("type_format_words").toArray();

        for(auto w : list)
            type_format_words.push_back(w.toString().toStdU16String());
    }

    std::vector<std::u16string> tuple_format_words;
    if (root.find("tuple_format_words") != root.end())
    {
        QJsonArray list = root.value("tuple_format_words").toArray();

        for(auto w : list)
            tuple_format_words.push_back(w.toString().toStdU16String());
    }

    std::vector<std::u16string> function_format_words;
    if (root.find("function_format_words") != root.end())
    {
        QJsonArray list = root.value("function_format_words").toArray();

        for(auto w : list)
            function_format_words.push_back(w.toString().toStdU16String());
    }

    _rulename_to_ruledata_map.insert(file_info.baseName()
                                    , RuleData(file_info.baseName()
                                                , file_masks
                                                , run_command_set
                                              , collapsible_starts_block_number
                                              , lexical
                                              , error_format_words
                                              , keyword_format_words
                                              , module_format_words
                                              , parameter_format_words
                                              , type_format_words
                                              , tuple_format_words
                                              , function_format_words
                                              , grammar_name
                                              , semantic_handler_name
                                              , interpreter_handler_name
                                              , prevent_text_tab_replacement
                                              )
                                    );

    for(const QString & file_mask : file_masks)
        _filemask_to_rulename_map.push_back({file_mask,file_info.baseName()});

    return true;
}
