#include "ChartView.h"

#include <QMenu>
#include <QGraphicsLayout>
#include <QXYSeries>
#include <QSplineSeries>
#include <QScatterSeries>

ChartView::ChartView(MainWindow *main_window, QDockWidget *parent)
    : QChartView(parent)
    , _main_window(main_window)
    , _chart_dock(parent)
//    , _is_ready(true)
{
    setRenderHint(QPainter::Antialiasing, _main_window->_config.chart_antialiasing);

    if (chart() != nullptr)
    {
        chart()->layout()->setContentsMargins(0, 0, 0, 0);
        chart()->setBackgroundRoundness(0);
        chart()->setTheme(static_cast<QtCharts::QChart::ChartTheme>(_main_window->_config.chart_theme));
    }
}

void ChartView::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);

    QAction * action_antialiasing = new QAction("Anti-aliasing");
    action_antialiasing->setCheckable(true);
    action_antialiasing->setChecked(_main_window->_config.chart_antialiasing);
    menu.addAction(action_antialiasing);

    connect(action_antialiasing, &QAction::triggered, this, [this](bool active){
        _main_window->_config.chart_antialiasing = active;
        setRenderHint(QPainter::Antialiasing, _main_window->_config.chart_antialiasing);
    });


    QActionGroup *     color_group       = new QActionGroup(this);
    QMenu *            color_scheme_menu = menu.addMenu("Color themes");
    QtCharts::QChart::ChartTheme current_theme     = static_cast<QtCharts::QChart::ChartTheme>(_main_window->_config.chart_theme);

    const QVector<QPair<QString,QtCharts::QChart::ChartTheme>> themes {
        {"Light", QtCharts::QChart::ChartThemeLight},
        {"Blue Cerulean", QtCharts::QChart::ChartThemeBlueCerulean},
        {"Dark", QtCharts::QChart::ChartThemeDark},
        {"Brown Sand", QtCharts::QChart::ChartThemeBrownSand},
        {"Blue NCS", QtCharts::QChart::ChartThemeBlueNcs},
        {"High Contrast", QtCharts::QChart::ChartThemeHighContrast},
        {"Blue Icy", QtCharts::QChart::ChartThemeBlueIcy},
        {"Qt", QtCharts::QChart::ChartThemeQt},
    };

    for(auto [name, theme] : themes)
    {
        QAction *a = new QAction(name);
        a->setVisible(true);
        a->setCheckable(true);
        a->setChecked(current_theme == theme);
        a->setData(theme);
        connect(a, &QAction::triggered, this, &ChartView::setColorTheme);
        color_group->addAction(a);
        color_scheme_menu->addAction(a);
    }

    QActionGroup * legent_group   = new QActionGroup(this);
    QMenu *        legent_menu    = menu.addMenu("Legent aligment");
    int            current_legent = _main_window->_config.chart_legent;

    const QVector<QPair<QString,int>> legents {
        {"No Legend ", 0},
        {"Legend Top", Qt::AlignTop},
        {"Legend Bottom", Qt::AlignBottom},
        {"Legend Left", Qt::AlignLeft},
        {"Legend Right", Qt::AlignRight},
    };

    for(auto [name, legent] : legents)
    {
        QAction *a = new QAction(name);
        a->setVisible(true);
        a->setCheckable(true);
        a->setChecked(current_legent == legent);
        a->setData(legent);
        connect(a, &QAction::triggered, this, &ChartView::setLegent);
        legent_group->addAction(a);
        legent_menu->addAction(a);
    }

    menu.exec(event->globalPos());
}

void ChartView::setColorTheme()
{
    const QAction *action = qobject_cast<const QAction *>(sender());
    if (action == nullptr)
        return;

    _main_window->_config.chart_theme = action->data().toInt();

    if (chart() != nullptr)
        chart()->setTheme(static_cast<QtCharts::QChart::ChartTheme>(_main_window->_config.chart_theme));
}

void ChartView::setLegent()
{
    const QAction *action = qobject_cast<const QAction *>(sender());
    if (action == nullptr)
        return;

    _main_window->_config.chart_legent = action->data().toInt();

    if (chart() != nullptr)
    {
        if (_main_window->_config.chart_legent == 0)
            chart()->legend()->hide();
        else
        {
            chart()->legend()->setAlignment(static_cast<Qt::Alignment>(_main_window->_config.chart_legent));
            chart()->legend()->show();
        }
    }
}


void ChartView::handleInitialization(QString title)
{
    _serieses.clear();

    QtCharts::QChart * old_chart = chart();

    QtCharts::QChart * chart = new QtCharts::QChart;

    chart->layout()->setContentsMargins(0, 0, 0, 0);
    chart->setBackgroundRoundness(0);
    chart->setTheme(static_cast<QtCharts::QChart::ChartTheme>(_main_window->_config.chart_theme));
    chart->setTitle(title);

    if (_main_window->_config.chart_legent == 0)
        chart->legend()->hide();
    else
    {
        chart->legend()->setAlignment(static_cast<Qt::Alignment>(_main_window->_config.chart_legent));
        chart->legend()->show();
    }

    setChart(chart);

    if (old_chart != nullptr)
        delete old_chart;

    _chart_dock->setVisible(true);

//    _is_ready = true;
}

void ChartView::handleAddSeries(QString series_name, int series_style)
{
//    if (!_is_ready)
//        return;

//    QScopedValueRollback<bool> ready_guard(_is_ready, false);

    auto it = _serieses.find(series_name);

    if (it != _serieses.end())
        return;

    _serieses.insert({series_name,{static_cast<SeriesStype>(series_style),{}}});
}

void ChartView::handleAddPoint(ChartPointVector points)
{
//    if (!_is_ready)
//        return;

//    QScopedValueRollback<bool> ready_guard(_is_ready, false);

    for (auto & point : points)
    {
        auto it = _serieses.find(point.first);

        if (it == _serieses.end())
        {
            auto it_new = _serieses.insert({point.first,{Line,{}}});

            it = it_new.first;
        }

        it->second.second.push_back(point.second);
        if (it->second.second.size() > 2000)
        {
            it->second.second.pop_front();
        }
    }

    show();
}

void ChartView::show()
{
//    if (!_is_ready)
//        return;

//    QScopedValueRollback<bool> ready_guard(_is_ready, false);

    chart()->removeAllSeries();

    for(auto & [n,p] : _serieses)
    {
        QtCharts::QXYSeries *series;
        switch(p.first)
        {
        case Spline:
            series = new QtCharts::QSplineSeries();
            break;
        case Scatter:
            series = new QtCharts::QScatterSeries();
            break;
        default:
            series = new QtCharts::QLineSeries();
            break;
        }

        series->setName(n);
        series->replace(p.second);
        chart()->addSeries(series);
    }

    chart()->createDefaultAxes();

    QChartView::show();
}

