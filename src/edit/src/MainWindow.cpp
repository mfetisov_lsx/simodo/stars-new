#include "ChartView.h"
#include "cockpit/Cockpit3D.h"
#include "cockpit/CockpitView.h"
#include "FindReplace.h"
#include "GrammarTable.h"
#include "Graph3D.h"
#include "MainWindow.h"
#include "MdiChild.h"
#include "WorkObject.h"

#include "simodo/convert.h"
#include "simodo/dsl/Parser.h"
#include "simodo/version.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QJsonObject>
#include <QtCore/QSettings>
#include <QtGui/QDesktopServices>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMdiArea>
#include <QtWidgets/QMdiSubWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>

MainWindow::MainWindow()
    : _simodo_thread_object(nullptr)
    , _mdi_area(new QMdiArea)
    , _global_msg_dock(nullptr)
    , _global_msg_list(nullptr)
    , _editor_msg_dock(nullptr)
    , _editor_msg_list(nullptr)
    , _execute_msg_dock(nullptr)
    , _execute_msg_list(nullptr)
    , _reload(false)
{
    setWindowIcon(QIcon(":/images/logo-01-128"));

    _global_msg_dock = new QDockWidget(tr("Global Messages"), this);
    _global_msg_dock->setObjectName("Global Messages");
    _global_msg_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::BottomDockWidgetArea, _global_msg_dock);

    _global_msg_list = new BufferedListWidget(_global_msg_dock);
    _global_msg_dock->setWidget(_global_msg_list);

    connect(_global_msg_list, &QListWidget::itemDoubleClicked, this, &MainWindow::activateMessage);

    readCommonSettings();

    _config.setErrorList(_global_msg_list);
    _config.readCommonJson();
    _config.setDecorTheme(_config.decor_theme_name);

    qputenv("TERM", _config.terminal.toLatin1());

    _mdi_area->setViewMode(QMdiArea::TabbedView);
    _mdi_area->setTabsClosable(true);
    _mdi_area->setDocumentMode(true);
    _mdi_area->setTabPosition(QTabWidget::West);
    _mdi_area->setActivationOrder(QMdiArea::StackingOrder); // QMdiArea::StackingOrder
    _mdi_area->setBackground(QBrush(_config.editor_color_theme.alternate_text_background_color));
    setCentralWidget(_mdi_area);

    _rulename_label = new QLabel(this);
    _rulename_label->setTextFormat(Qt::PlainText);
    statusBar()->addPermanentWidget(_rulename_label);

    _linenumber_label = new QLabel(this);
    _linenumber_label->setTextFormat(Qt::PlainText);
    statusBar()->addPermanentWidget(_linenumber_label);

    _text_zooming_percentage_label = new QLabel(this);
    _text_zooming_percentage_label->setTextFormat(Qt::PlainText);
    statusBar()->addPermanentWidget(_text_zooming_percentage_label);

    setTextZooming(_config.curr_font_size);

    connect(_mdi_area, &QMdiArea::subWindowActivated, this, &MainWindow::updateMenus);

    createActions();
    createStatusBar();
    updateMenus();

    _simodo_ruler = new SimodoRuler(_config.data_dir, _config.working_dir, _global_msg_list);

    readRestSettings();

    _grammar_table->refill();

    setUnifiedTitleAndToolBarOnMac(true);

    _simodo_thread_object = new WorkObject; // delete?
    _simodo_thread_object->moveToThread(&_simodo_thread);
    connect(&_simodo_thread, &QThread::finished, _simodo_thread_object, &QObject::deleteLater);
    connect(this, &MainWindow::startExecution, _simodo_thread_object, &WorkObject::doWork);
    connect(_simodo_thread_object, &WorkObject::workIsOver, this, &MainWindow::handleResults);
    _simodo_thread.start();

    _global_msg_dock->setVisible(_global_msg_list->count() > 0);

    if (_config.spell_path.isEmpty())
        _config.spell_path = _config.data_dir + "/ide/hunspell";

    _spell_checker.reopenDictionaries(_config.spell_path,
                                      _config.spell_english_dict_name,
                                      _config.spell_national_dict_name,
                                      _config.spell_encoding);
}

MainWindow::~MainWindow()
{
    _simodo_thread.quit();

    if (_graph3d != nullptr)
        _graph3d->close();

//    if (_graph3d_dock != nullptr)
//        delete _graph3d_dock;

    delete _simodo_ruler;

    _simodo_thread.wait();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    _mdi_area->closeAllSubWindows();

    bool is_reject = (_mdi_area->currentSubWindow() != nullptr);

    if (is_reject)
    {
        event->ignore();
    }
    else
    {
        writeSettings();
        event->accept();
    }
}

void MainWindow::handleResults(MdiChild *editor, bool success)
{
    editor->updateSemanticInfo();
    editor->rehighlight();
    editor->update();

    if (_running_editor == nullptr
        || (_running_editor == editor
            && _simodo_thread_object->getActiveMachine() == nullptr))
    {
        _running_editor = nullptr;
        editor->setClose();

        _run_action->setEnabled(editor->runnable());
        _stop_action->setEnabled(false);

        emit doHandleResults(editor, success);
    }
}

void MainWindow::newFile()
{
    MdiChild *child = createMdiChild(QIcon());

    child->newFile();
    child->show();
}

void MainWindow::open()
{
    const QString fileName = QFileDialog::getOpenFileName(this);

    if (!fileName.isEmpty())
        openFile(fileName);
}

bool MainWindow::openFile(const QString &fileName)
{
    if (MdiChild *existing = findMdiChild(fileName); existing != nullptr)
    {
        QMdiSubWindow * subwindow = qobject_cast<QMdiSubWindow *>(existing->parent());
        _mdi_area->setActiveSubWindow(subwindow);
        return true;
    }

    const bool succeeded = loadFile(fileName);

    if (succeeded)
        statusBar()->showMessage(tr("File loaded"), 2000);

    return succeeded;
}

void MainWindow::clearLineNumber()
{
    _linenumber_label->setText("");
    _rulename_label->setText("");
}

bool MainWindow::loadFile(const QString &fileName)
{
    MdiChild * child     = createMdiChild(_fs_tree->getFileIcon(fileName));
    const bool succeeded = child->loadFile(fileName);

    if (succeeded)
    {
        child->show();
    } else {
        child->deleteLater();
    }

    MainWindow::prependToRecentFiles(fileName);

    return succeeded;
}

SimodoRuler &MainWindow::simodo_ruler()
{
    if (_simodo_ruler == nullptr)
        qt_assert("_simodo_ruler == nullptr", __FILE__, __LINE__);

    return *_simodo_ruler;
}

void MainWindow::showRulename(QString rulename)
{
    if (rulename.isEmpty())
        _rulename_label->setText("");
    else
        _rulename_label->setText("rule: " + rulename);
}

static inline QString recentDirsKey() { return QStringLiteral("recentDirList"); }
static inline QString dirKey() { return QStringLiteral("dir"); }

static QStringList readRecentDirs(QSettings &settings)
{
    QStringList result;
    const int   count = settings.beginReadArray(recentDirsKey());

    for (int i = 0; i < count; ++i)
    {
        settings.setArrayIndex(i);
        result.append(settings.value(dirKey()).toString());
    }
    settings.endArray();

    return result;
}

static void writeRecentDirs(const QStringList &dirs, QSettings &settings)
{
    const int count = dirs.size();

    settings.beginWriteArray(recentDirsKey());

    for (int i = 0; i < count; ++i)
    {
        settings.setArrayIndex(i);
        settings.setValue(dirKey(), dirs.at(i));
    }
    settings.endArray();
}

bool MainWindow::hasRecentDirs()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    const int count = settings.beginReadArray(recentDirsKey());

    settings.endArray();

    return count > 0;
}

void MainWindow::prependToRecentDirs(const QString &dirName)
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    const QStringList oldRecentDirs = readRecentDirs(settings);
    QStringList       recentDirs    = oldRecentDirs;

    recentDirs.removeAll(dirName);
    recentDirs.prepend(dirName);

    if (oldRecentDirs != recentDirs)
        writeRecentDirs(recentDirs, settings);

    setRecentDirsVisible(!recentDirs.isEmpty());
}

void MainWindow::setRecentDirsVisible(bool visible)
{
    recentDirSubMenuAct->setVisible(visible);
}

void MainWindow::updateRecentDirActions()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    const QStringList recentDirs = readRecentDirs(settings);
    const int         count       = qMin(int(MaxRecentDirs), recentDirs.size());
    int               i           = 0;

    for ( ; i < count; ++i)
    {
        const QString dirName = QFileInfo(recentDirs.at(i)).absoluteFilePath();
        recentDirActs[i]->setText(tr("&%1 %2").arg(i + 1).arg(dirName));
        recentDirActs[i]->setData(recentDirs.at(i));
        recentDirActs[i]->setVisible(true);
    }

    for ( ; i < MaxRecentDirs; ++i)
        recentDirActs[i]->setVisible(false);
}

void MainWindow::openRecentDir()
{
    if (const QAction *action = qobject_cast<const QAction *>(sender()))
    {
        _config.working_dir = action->data().toString();
        _fs_tree->setWorkDirectory(_config.working_dir);
    }
}

void MainWindow::setDataDirectory()
{
    QString dir = QFileDialog::getExistingDirectory(this,tr("Data Directory"),_config.data_dir);

    if (dir != _config.data_dir && !dir.isEmpty())
    {
        if (QMessageBox::Yes == QMessageBox::warning(this, tr("SIMODO"),
                                           tr("You must restart the application for the changes to take effect.\n"
                                              "Restart?"),
                                           QMessageBox::Yes | QMessageBox::No))
        {
            _config.data_dir = dir;
            _reload = true;
            _files = getOpenedFiles();
            close();
        }
    }
}

void MainWindow::setWorkDirectory()
{
    QString dir = QFileDialog::getExistingDirectory(this,tr("Working Directory"),_config.working_dir);

    if (dir != _config.working_dir && !dir.isEmpty())
    {
        _config.working_dir = dir;
        _fs_tree->setWorkDirectory(_config.working_dir);
        prependToRecentDirs(_config.working_dir);
    }
}

static inline QString recentFilesKey() { return QStringLiteral("recentFileList"); }
static inline QString fileKey() { return QStringLiteral("file"); }

static QStringList readRecentFiles(QSettings &settings)
{
    QStringList result;
    const int   count = settings.beginReadArray(recentFilesKey());

    for (int i = 0; i < count; ++i)
    {
        settings.setArrayIndex(i);
        result.append(settings.value(fileKey()).toString());
    }
    settings.endArray();

    return result;
}

static void writeRecentFiles(const QStringList &files, QSettings &settings)
{
    const int count = files.size();

    settings.beginWriteArray(recentFilesKey());

    for (int i = 0; i < count; ++i)
    {
        settings.setArrayIndex(i);
        settings.setValue(fileKey(), files.at(i));
    }
    settings.endArray();
}

bool MainWindow::hasRecentFiles()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    const int count = settings.beginReadArray(recentFilesKey());

    settings.endArray();

    return count > 0;
}

void MainWindow::prependToRecentFiles(const QString &fileName)
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    const QStringList oldRecentFiles = readRecentFiles(settings);
    QStringList       recentFiles    = oldRecentFiles;

    recentFiles.removeAll(fileName);
    recentFiles.prepend(fileName);

    if (oldRecentFiles != recentFiles)
        writeRecentFiles(recentFiles, settings);

    setRecentFilesVisible(!recentFiles.isEmpty());
}

void MainWindow::setRecentFilesVisible(bool visible)
{
    recentFileSubMenuAct->setVisible(visible);
}

void MainWindow::updateRecentFileActions()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    const QStringList recentFiles = readRecentFiles(settings);
    const int         count       = qMin(int(MaxRecentFiles), recentFiles.size());
    int               i           = 0;

    for ( ; i < count; ++i)
    {
        const QString fileName = QFileInfo(recentFiles.at(i)).fileName();
        recentFileActs[i]->setText(tr("&%1 %2").arg(i + 1).arg(fileName));
        recentFileActs[i]->setData(recentFiles.at(i));
        recentFileActs[i]->setVisible(true);
    }
    for ( ; i < MaxRecentFiles; ++i)
        recentFileActs[i]->setVisible(false);
}

void MainWindow::openRecentFile()
{
    if (const QAction *action = qobject_cast<const QAction *>(sender()))
        openFile(action->data().toString());
}

void MainWindow::save()
{
    if (activeMdiChild() && activeMdiChild()->save())
        statusBar()->showMessage(tr("File saved"), 2000);
}

void MainWindow::saveAs()
{
    MdiChild *child = activeMdiChild();
    if (child && child->saveAs()) {
        statusBar()->showMessage(tr("File saved"), 2000);
        MainWindow::prependToRecentFiles(child->currentFile());
    }
}

void MainWindow::saveAll()
{
    bool has_saved = false;

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());

        if (mdiChild->save())
            has_saved = true;
    }

    if (has_saved)
        statusBar()->showMessage(tr("All documents saved"), 2000);
}

#ifndef QT_NO_CLIPBOARD
void MainWindow::cut()
{
    if (activeMdiChild())
        activeMdiChild()->cut();
}

void MainWindow::copy()
{
    if (activeMdiChild())
        activeMdiChild()->copy();
}

void MainWindow::paste()
{
    if (activeMdiChild())
        activeMdiChild()->paste();
}
#endif

void MainWindow::tabulation()
{
    if (const QAction *action = qobject_cast<const QAction *>(sender());
        action != nullptr)
    {
        _config.tab_size = action->text().toInt();

        foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
        {
            MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
            CodeEditorParameters params = mdiChild->parameters();
            params.tab_size = _config.tab_size;
            mdiChild->setParameters(params);
        }
    }
}

void MainWindow::textWrapping()
{
    _config.text_wrapping = _wrap_action->isChecked();

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        mdiChild->setWordWrapMode(_config.text_wrapping ? QTextOption::WordWrap : QTextOption::NoWrap);
        mdiChild->update();
    }
}

void MainWindow::statusBarViewing()
{
    _config.statusbar_viewing = _statusbar_action->isChecked();
    statusBar()->setVisible(_config.statusbar_viewing);
}

void MainWindow::scopeViewing()
{
    _config.scope_view = _scope_action->isChecked();

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        CodeEditorParameters params = mdiChild->parameters();
        params.scope_viewing = _config.scope_view;
        mdiChild->setParameters(params);
    }
}

void MainWindow::complViewing()
{
    _config.completer = _compl_action->isChecked();

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        CodeEditorParameters params = mdiChild->parameters();
        params.completion = _config.completer;
        mdiChild->setParameters(params);
    }
}

void MainWindow::onRunCommand()
{
}

void MainWindow::run(bool needExecute)
{
    MdiChild * editor = activeMdiChild();

    if (editor != nullptr && _running_editor == nullptr)
    {
        if (editor->rule_data() == nullptr || (needExecute && editor->rule_data()->interpreter_handler_name().isEmpty()))
            return;

        _running_editor = editor;
        editor->setClose(false);
        _stop_action->setEnabled(true);

        editor->execute(needExecute);
    }
}

void MainWindow::stop()
{
    _simodo_thread_object->stopExecution();

    _run_action->setEnabled(true);
    _stop_action->setEnabled(false);
}

void MainWindow::setColorTheme()
{
    const QAction *action = qobject_cast<const QAction *>(sender());
    if (action == nullptr)
        return;

    QString theme_name = action->data().toString();

    if (theme_name.isEmpty())
        return;

    AppChangeResult res = _config.setDecorTheme(theme_name);

    if (res == AppChangeResult::AnErrorHasOccurred)
        return;

    _config.decor_theme_name = theme_name;
    _mdi_area->setBackground(QBrush(_config.editor_color_theme.alternate_text_background_color));

    for(auto a : _color_action_group->actions())
        a->setChecked(a == action);

    if (res == AppChangeResult::NoActionRequired)
        return;

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        mdiChild->setColorTheme(getCurrentTheme());
        mdiChild->update();
    }

    if (res == AppChangeResult::ReloadRequired)
    {
        if (QMessageBox::Yes == QMessageBox::warning(this, tr("SIMODO"),
                                           tr("You must restart the application for the changes to take effect.\n"
                                              "Restart?"),
                                           QMessageBox::Yes | QMessageBox::No))
        {
            _reload = true;
            _files = getOpenedFiles();
            close();
        }
    }
}

void MainWindow::activateMessage(QListWidgetItem *item)
{
    QJsonObject obj = item->data(Qt::UserRole).toJsonObject();

    QString file_name;
    int     pos = -1;

    if (obj.find("file") != obj.end())
        file_name = obj.value("file").toString();

    if (file_name.isEmpty())
        return;

    if (obj.find("begin") != obj.end())
        pos = obj.value("begin").toInt();

    if (pos == -1)
        return;

    MdiChild * editor = findMdiChild(file_name);

    if (editor == nullptr)
    {
        const bool ok = loadFile(file_name);

        if (ok)
            statusBar()->showMessage(tr("File loaded"), 2000);
        else
            return;

        editor = findMdiChild(file_name);

        if (editor == nullptr)
            return;
    }
    else
        _mdi_area->setActiveSubWindow(qobject_cast<QMdiSubWindow *>(editor));

    QTextCursor cur = editor->textCursor();

    cur.setPosition(pos);
    editor->setTextCursor(cur);
    editor->setFocus();
}

void MainWindow::about()
{
   QMessageBox::about(this
                        , tr("About SIMODO IDE")
                        , tr("The integrated development environment for dynamic systems. Project <b>SIMODO stars</b>.")
                            + tr("<br><a href=\"https://bmstu.codes/lsx/simodo/stars\">For more information click here</a>")
                            + tr("<br>SIMODO version: ")
                            + QString::number(simodo::version_major) + "."
                            + QString::number(simodo::version_middle) + "."
                            + QString::number(simodo::version_minor) + " build "
                            + QString::fromStdString(simodo::version_build)
                        );
}

void MainWindow::readMe()
{
   QMessageBox::about(this
                        , tr("SIMODO IDE READ ME")
                        , QString("<ol>")
                            + "<li>" + tr("<b>File -> Set data directory...</b> установить в <b>") + tr("<Install dir>").toHtmlEscaped() + tr("/data</b>, где по-умолчанию <b>") + tr("<Install dir>").toHtmlEscaped() + tr(" ::= /home/") + tr("<username>").toHtmlEscaped() + tr("/Simodo/SimodoEdit | C:\\Users\\") + tr("<username>").toHtmlEscaped() + tr("\\Simodo\\SimodoEdit</b>") + "</li>"
                            + "<li>" + tr("Во вкладке <b>View -> Docked Windows</b> как правило следует включить окна <b>Editor Messages</b>/<b>Execute Messages</b>") + "</li>"
                            + "<li>" + tr("В <b>") + tr("<Install dir>").toHtmlEscaped() + tr("/examples</b> можно найти примеры работы со средой") + "</li>"
                            + "</ol>"
                        );
}

void MainWindow::updateMenus()
{
    MdiChild * child = activeMdiChild();
    bool hasMdiChild = (child != nullptr);

    _save_action->setEnabled(hasMdiChild);
    _save_as_action->setEnabled(hasMdiChild);
    _save_all_action->setEnabled(hasMdiChild);
#ifndef QT_NO_CLIPBOARD
    _paste_action->setEnabled(hasMdiChild);
#endif
    _close_action->setEnabled(hasMdiChild);
    _close_all_action->setEnabled(hasMdiChild);
    _tile_action->setEnabled(hasMdiChild);
    _cascade_action->setEnabled(hasMdiChild);
    _next_action->setEnabled(hasMdiChild);
    _previous_action->setEnabled(hasMdiChild);
    _window_menu_separator_action->setVisible(hasMdiChild);

    for(QAction * a : _run_actions)
        a->setVisible(false);
    _run_separator_action->setVisible(false);

    if (hasMdiChild && child->rule_data() != nullptr)
    {
        const QVector<QPair<QString,QString>> & command_set = child->rule_data()->run_command_set();
        int i=0;
        for(const auto & [text,pattern] : command_set)
        {
            if (i == 10)
                break;

            _run_actions[i]->setText(text);
            _run_actions[i]->setData(pattern);
            _run_actions[i]->setStatusTip(makeCommand(pattern));
            _run_actions[i]->setVisible(true);
            _run_separator_action->setVisible(true);
            ++i;
        }
    }

    if (_running_editor != nullptr)
    {
        _run_action->setEnabled(false);
        _stop_action->setEnabled(true);
    }
    else
    {
        bool runActEnable = false;
        if (hasMdiChild && child->runnable())
            runActEnable = true;
        _run_action->setEnabled(runActEnable);
        _stop_action->setEnabled(false);
    }

    _edit_undo_action->setEnabled(hasMdiChild && child->document()->isUndoAvailable());
    _edit_redo_action->setEnabled(hasMdiChild && child->document()->isRedoAvailable());

    bool hasSelection = hasTextSelection();
#ifndef QT_NO_CLIPBOARD
    _cut_action->setEnabled(hasSelection);
    _copy_action->setEnabled(hasSelection);
#endif
    _text_block_shift_right_action->setEnabled(hasSelection);
    _text_block_shift_left_action->setEnabled(hasSelection);

    bool has_comment_lexeme = false;
    if (child != nullptr && child->rule_data() != nullptr)
    {
        for(auto m : child->rule_data()->lexis().markups)
            if (m.type == simodo::dsl::LexemeType::Comment)
            {
                has_comment_lexeme = true;
                break;
            }
    }
    _text_handle_comment_action->setEnabled(has_comment_lexeme);

    _editor_zoom_in_action->setEnabled(hasMdiChild);
    _editor_zoom_out_action->setEnabled(hasMdiChild);
    _editor_unzoom_action->setEnabled(hasMdiChild);

    _find_form->setFindReplaceEnabled(hasMdiChild);
    _replace_form->setFindReplaceEnabled(hasMdiChild);

    if (!hasMdiChild)
        clearLineNumber();
}

void MainWindow::updateWindowMenu()
{
    _window_menu->clear();
    _window_menu->addAction(_close_action);
    _window_menu->addAction(_close_all_action);
    _window_menu->addSeparator();
    _window_menu->addAction(_tile_action);
    _window_menu->addAction(_cascade_action);
    _window_menu->addSeparator();
    _window_menu->addAction(_next_action);
    _window_menu->addAction(_previous_action);
    _window_menu->addAction(_window_menu_separator_action);

    QList<QMdiSubWindow *> windows = _mdi_area->subWindowList();

    _window_menu_separator_action->setVisible(!windows.isEmpty());

    for (int i = 0; i < windows.size(); ++i)
    {
        QMdiSubWindow * mdiSubWindow = windows.at(i);
        MdiChild *      child        = qobject_cast<MdiChild *>(mdiSubWindow->widget());
        QString         text;

        if (i < 9)
            text = tr("&%1 %2").arg(i+1).arg(child->userFriendlyCurrentFile());
        else
            text = tr("%1 %2").arg(i+1).arg(child->userFriendlyCurrentFile());

        QAction *action = _window_menu->addAction(text, mdiSubWindow, [this, mdiSubWindow]()
        {
            _mdi_area->setActiveSubWindow(mdiSubWindow);
        });

        action->setCheckable(true);
        action->setChecked(child == activeMdiChild());
    }
}

MdiChild *MainWindow::createMdiChild(QIcon icon)
{
    MdiChild *child = new MdiChild(getCurrentTheme(), _spell_checker, this);
    QMdiSubWindow * sub = _mdi_area->addSubWindow(child);
    connect(child, &QObject::destroyed, [sub]() -> void { sub->deleteLater(); });
    connect(child, &MdiChild::onClose, [this, child]()
    {
        connect(this, &MainWindow::doHandleResults, child, &MdiChild::close);

        if (_running_editor != child)
        {
            emit doHandleResults(nullptr, false);
        }

        stop();
    });

    sub->setWindowIcon(icon);

#ifndef QT_NO_CLIPBOARD
    connect(child, &QPlainTextEdit::copyAvailable, _cut_action, &QAction::setEnabled);
    connect(child, &QPlainTextEdit::copyAvailable, _copy_action, &QAction::setEnabled);
#endif
    connect(child, &QPlainTextEdit::copyAvailable, _text_block_shift_right_action, &QAction::setEnabled);
    connect(child, &QPlainTextEdit::copyAvailable, _text_block_shift_left_action, &QAction::setEnabled);

    return child;
}

void MainWindow::openEditWindow(const QModelIndex & index)
{
    QFileInfo fi = _fs_tree->getFileInfo(index);

    if (fi.isFile())
        openFile(fi.filePath());
}

void MainWindow::createActions()
{
    // Toolbars

    _file_toolbar = new QToolBar(tr("File"), this);
    _file_toolbar->setObjectName("File");
    addToolBar(Qt::LeftToolBarArea, _file_toolbar);

    _edit_toolbar = new QToolBar(tr("Edit"), this);
    _edit_toolbar->setObjectName("Edit");
    addToolBar(Qt::LeftToolBarArea, _edit_toolbar);

    _run_toolbar = new QToolBar(tr("Run"), this);
    _run_toolbar->setObjectName("Run");
    addToolBar(Qt::LeftToolBarArea, _run_toolbar);

    // Menus

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));

    QMenu *recentDirMenu = fileMenu->addMenu(tr("Recent working directory..."));
    connect(recentDirMenu, &QMenu::aboutToShow, this, &MainWindow::updateRecentDirActions);
    recentDirSubMenuAct = recentDirMenu->menuAction();
    recentDirSubMenuAct->setStatusTip(tr("Set recent working directory..."));

    for (int i = 0; i < MaxRecentFiles; ++i)
    {
        recentDirActs[i] = recentDirMenu->addAction(QString(), this, &MainWindow::openRecentDir);
        recentDirActs[i]->setVisible(false);
    }

    setRecentDirsVisible(MainWindow::hasRecentDirs());

    const QIcon dirIcon = QIcon::fromTheme("go-home", QIcon(":/images/user-home"));
    _set_work_dir_action = new QAction(dirIcon, tr("Set working &directory..."), this);
    _set_work_dir_action->setShortcut(Qt::CTRL + Qt::Key_D);
    _set_work_dir_action->setStatusTip(tr("Set working directory"));
    connect(_set_work_dir_action, &QAction::triggered, this, &MainWindow::setWorkDirectory);
    fileMenu->addAction(_set_work_dir_action);
    _file_toolbar->addAction(_set_work_dir_action);

    fileMenu->addSeparator();

    const QIcon recentIcon = QIcon::fromTheme("document-open-recent", QIcon(":/images/document-open-recent"));
    QMenu *recentFileMenu = fileMenu->addMenu(recentIcon, tr("Recent file..."));
    connect(recentFileMenu, &QMenu::aboutToShow, this, &MainWindow::updateRecentFileActions);
    recentFileSubMenuAct = recentFileMenu->menuAction();
    recentFileSubMenuAct->setStatusTip(tr("Open recent file..."));

    for (int i = 0; i < MaxRecentFiles; ++i)
    {
        recentFileActs[i] = recentFileMenu->addAction(QString(), this, &MainWindow::openRecentFile);
        recentFileActs[i]->setVisible(false);
    }

    setRecentFilesVisible(MainWindow::hasRecentFiles());

    const QIcon newIcon = QIcon::fromTheme("document-new", QIcon(":/images/document-new"));
    _new_action = new QAction(newIcon, tr("&New"), this);
    _new_action->setShortcuts(QKeySequence::New);
    _new_action->setStatusTip(tr("Create a new file"));
    connect(_new_action, &QAction::triggered, this, &MainWindow::newFile);
    fileMenu->addAction(_new_action);
    _file_toolbar->addAction(_new_action);

    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/document-open"));
    QAction *openAct = new QAction(openIcon, tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::open);
    fileMenu->addAction(openAct);
    _file_toolbar->addAction(openAct);

    const QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/document-save"));
    _save_action = new QAction(saveIcon, tr("&Save"), this);
    _save_action->setShortcuts(QKeySequence::Save);
    _save_action->setStatusTip(tr("Save the document to disk"));
    connect(_save_action, &QAction::triggered, this, &MainWindow::save);
    fileMenu->addAction(_save_action);
    _file_toolbar->addAction(_save_action);

    const QIcon saveAsIcon = QIcon::fromTheme("document-save-as", QIcon(":/images/document-save-as"));
    _save_as_action = new QAction(saveAsIcon, tr("Save &As..."), this);
//    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    _save_as_action->setStatusTip(tr("Save the document under a new name"));
    connect(_save_as_action, &QAction::triggered, this, &MainWindow::saveAs);
    fileMenu->addAction(_save_as_action);
    _file_toolbar->addAction(_save_as_action);

    const QIcon save_all_icon = QIcon::fromTheme("document-save-all", QIcon(":/images/document-save-all"));
    _save_all_action = new QAction(save_all_icon, tr("Save All"), this);
    _save_all_action->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_S);
    _save_all_action->setStatusTip(tr("Save all documents"));
    connect(_save_all_action, &QAction::triggered, this, &MainWindow::saveAll);
    fileMenu->addAction(_save_all_action);
    _file_toolbar->addAction(_save_all_action);

    fileMenu->addSeparator();

    _set_data_dir_action = new QAction(tr("Set data directory..."), this);
    _set_data_dir_action->setStatusTip(tr("Set data directory"));
    connect(_set_data_dir_action, &QAction::triggered, this, &MainWindow::setDataDirectory);
    fileMenu->addAction(_set_data_dir_action);

    fileMenu->addSeparator();

    QAction * reload_action = new QAction(tr("&Reload editor"), this);
    reload_action->setStatusTip(tr("Reload editor"));
    connect(reload_action, &QAction::triggered, this,
            [this](){
                _reload = true;
                _files = getOpenedFiles();
                close();
            });
    fileMenu->addAction(reload_action);


    const QIcon exitIcon = QIcon::fromTheme("application-exit", QIcon(":/images/application-exit"));
    QAction *exitAct = fileMenu->addAction(exitIcon, tr("E&xit"), qApp, &QApplication::closeAllWindows);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    fileMenu->addAction(exitAct);

    QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));

    const QIcon undo_icon = QIcon::fromTheme("edit-undo", QIcon(":/images/edit-undo"));
    _edit_undo_action = new QAction(undo_icon, tr("&Undo"), this);
    _edit_undo_action->setShortcuts(QKeySequence::Undo);
    _edit_undo_action->setStatusTip(tr("Undo last text modification"));
    editMenu->addAction(_edit_undo_action);
    _edit_toolbar->addAction(_edit_undo_action);

    const QIcon redo_icon = QIcon::fromTheme("edit-redo", QIcon(":/images/edit-redo"));
    _edit_redo_action = new QAction(redo_icon, tr("Red&o"), this);
    _edit_redo_action->setShortcuts(QKeySequence::Redo);
    _edit_redo_action->setStatusTip(tr("Redo last undo action"));
    editMenu->addAction(_edit_redo_action);
    _edit_toolbar->addAction(_edit_redo_action);

    editMenu->addSeparator();
    _edit_toolbar->addSeparator();

#ifndef QT_NO_CLIPBOARD
    const QIcon cutIcon = QIcon::fromTheme("edit-cut", QIcon(":/images/edit-cut"));
    _cut_action = new QAction(cutIcon, tr("Cu&t"), this);
    _cut_action->setShortcuts(QKeySequence::Cut);
    _cut_action->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(_cut_action, &QAction::triggered, this, &MainWindow::cut);
    editMenu->addAction(_cut_action);
    _edit_toolbar->addAction(_cut_action);

    const QIcon copyIcon = QIcon::fromTheme("edit-copy", QIcon(":/images/edit-copy"));
    _copy_action = new QAction(copyIcon, tr("&Copy"), this);
    _copy_action->setShortcuts(QKeySequence::Copy);
    _copy_action->setStatusTip(tr("Copy the current selection's contents to the clipboard"));
    connect(_copy_action, &QAction::triggered, this, &MainWindow::copy);
    editMenu->addAction(_copy_action);
    _edit_toolbar->addAction(_copy_action);

    const QIcon pasteIcon = QIcon::fromTheme("edit-paste", QIcon(":/images/edit-paste"));
    _paste_action = new QAction(pasteIcon, tr("&Paste"), this);
    _paste_action->setShortcuts(QKeySequence::Paste);
    _paste_action->setStatusTip(tr("Paste the clipboard's contents into the current selection"));
    connect(_paste_action, &QAction::triggered, this, &MainWindow::paste);
    editMenu->addAction(_paste_action);
    _edit_toolbar->addAction(_paste_action);

    editMenu->addSeparator();
    _edit_toolbar->addSeparator();
#endif

    // FindReplace
    _find_dock = new QDockWidget(tr("Find"), this);
    _find_dock->setObjectName("Find");
    _find_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    _find_dock->setVisible(false);
    _find_form = new FindReplace(false, this);
    _find_dock->setWidget(_find_form);
    addDockWidget(Qt::LeftDockWidgetArea, _find_dock);

    const QIcon findIcon = QIcon::fromTheme("edit-find", QIcon(":/images/edit-find"));
    QAction * findAct = new QAction(findIcon, tr("&Find"), this);
    findAct->setShortcut(QKeySequence::Find);
    findAct->setStatusTip(tr("Show/hide find dialog"));
    connect(findAct, &QAction::triggered, _find_form, &FindReplace::setFocus);
    editMenu->addAction(findAct);
    _edit_toolbar->addAction(findAct);

    _replace_dock = new QDockWidget(tr("Replace"), this);
    _replace_dock->setObjectName("Replace");
    _replace_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    _replace_dock->setVisible(false);
    _replace_form = new FindReplace(true, this);
    _replace_dock->setWidget(_replace_form);
    addDockWidget(Qt::LeftDockWidgetArea, _replace_dock);

    const QIcon replaceIcon = QIcon::fromTheme("edit-find-replace", QIcon(":/images/edit-find-replace"));
    QAction * replaceAct = new QAction(replaceIcon, tr("&Replace"), this);
    replaceAct->setShortcut(QKeySequence::Replace);
    replaceAct->setStatusTip(tr("Show/hide replace dialog"));
    connect(replaceAct, &QAction::triggered, _replace_form, &FindReplace::setFocus);
    editMenu->addAction(replaceAct);
    _edit_toolbar->addAction(replaceAct);

    QAction * continue_find_act = new QAction(tr("Continue searching"), this);
    continue_find_act->setShortcut(Qt::Key_F3);
    continue_find_act->setStatusTip(tr("Continue searching"));
    connect(continue_find_act, &QAction::triggered, _find_form, &FindReplace::find);
    editMenu->addAction(continue_find_act);

    QAction * continue_replace_act = new QAction(tr("Continue replacing"), this);
    continue_replace_act->setShortcut(Qt::CTRL + Qt::Key_F3);
    continue_replace_act->setStatusTip(tr("Continue replacing"));
    connect(continue_replace_act, &QAction::triggered, _replace_form, &FindReplace::find);
    editMenu->addAction(continue_replace_act);

    editMenu->addSeparator();

    QMenu *extra_edit_menu = editMenu->addMenu(tr("Additional"));

    QAction * tabulationAct_1 = new QAction("1", this);
    tabulationAct_1->setCheckable(true);
    tabulationAct_1->setStatusTip(tr("Set tab size to 1"));
    connect(tabulationAct_1, &QAction::triggered, this, &MainWindow::tabulation);

    QAction *tabulationAct_2 = new QAction("2", this);
    tabulationAct_2->setCheckable(true);
    tabulationAct_2->setStatusTip(tr("Set tab size to 2"));
    connect(tabulationAct_2, &QAction::triggered, this, &MainWindow::tabulation);

    QAction *tabulationAct_4 = new QAction("4", this);
    tabulationAct_4->setCheckable(true);
    tabulationAct_4->setStatusTip(tr("Set tab size to 4"));
    connect(tabulationAct_4, &QAction::triggered, this, &MainWindow::tabulation);

    QAction *tabulationAct_8 = new QAction("8", this);
    tabulationAct_8->setCheckable(true);
    tabulationAct_8->setStatusTip(tr("Set tab size to 8"));
    connect(tabulationAct_8, &QAction::triggered, this, &MainWindow::tabulation);

    QMenu *tabMenu = extra_edit_menu->addMenu(tr("Tab &size"));
    tabMenu->addAction(tabulationAct_1);
    tabMenu->addAction(tabulationAct_2);
    tabMenu->addAction(tabulationAct_4);
    tabMenu->addAction(tabulationAct_8);

    _tab_replacement_action = new QAction(tr("Tab replacement"), this);
    _tab_replacement_action->setCheckable(true);
    _tab_replacement_action->setStatusTip(tr("Switch tabs to spaces mode"));
    connect(_tab_replacement_action, &QAction::triggered, this,
            [this](bool checked)
            {
                _config.text_tab_replacement = checked;

                foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
                {
                    MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
                    CodeEditorParameters params = mdiChild->parameters();
                    params.is_tab_replacement_necessary = _config.text_tab_replacement;
                    mdiChild->setParameters(params);
                }
            });
    extra_edit_menu->addAction(_tab_replacement_action);

    _text_auto_indent_action = new QAction(tr("Text auto-indent"), this);
    _text_auto_indent_action->setCheckable(true);
    _text_auto_indent_action->setStatusTip(tr("Switch auto-indent mode"));
    connect(_text_auto_indent_action, &QAction::triggered, this,
            [this](bool checked)
            {
                _config.text_auto_indent = checked;

                foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
                {
                    MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
                    CodeEditorParameters params = mdiChild->parameters();
                    params.is_autoindenting_necessary = _config.text_auto_indent;
                    mdiChild->setParameters(params);
                }
            });
    extra_edit_menu->addAction(_text_auto_indent_action);

    _wrap_action = new QAction(tr("Text &wrapping"), this);
    _wrap_action->setCheckable(true);
    _wrap_action->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_W);
    _wrap_action->setStatusTip(tr("Make text wrapping"));
    connect(_wrap_action, &QAction::triggered, this, &MainWindow::textWrapping);
    extra_edit_menu->addAction(_wrap_action);

    extra_edit_menu->addSeparator();

    _text_block_shift_right_action = new QAction(tr("Tab current block to right"), this);
    _text_block_shift_right_action->setShortcut(Qt::Key_Tab);
    _text_block_shift_right_action->setStatusTip(tr("Tab current block to right"));
    connect(_text_block_shift_right_action, &QAction::triggered, this,
            [this](bool )
            {
                MdiChild * edit = activeMdiChild();
                if (edit != nullptr)
                    edit->performBlockShiftRigth();
            });
    extra_edit_menu->addAction(_text_block_shift_right_action);

    _text_block_shift_left_action = new QAction(tr("Tab current block to left"), this);
    _text_block_shift_left_action->setShortcut(Qt::Key_Backtab);
    _text_block_shift_left_action->setStatusTip(tr("Tab current block to left"));
    connect(_text_block_shift_left_action, &QAction::triggered, this,
            [this](bool )
            {
                MdiChild * edit = activeMdiChild();
                if (edit != nullptr)
                    edit->performBlockShiftLeft();
            });
    extra_edit_menu->addAction(_text_block_shift_left_action);

    _text_handle_comment_action = new QAction(tr("Comment/uncomment"), this);
    _text_handle_comment_action->setShortcut(Qt::CTRL + Qt::Key_Slash);
    _text_handle_comment_action->setStatusTip(tr("Tab current block to left"));
    connect(_text_handle_comment_action, &QAction::triggered, this,
            [this](bool )
            {
                MdiChild * edit = activeMdiChild();
                if (edit != nullptr)
                    edit->handleCommenting();
            });
    extra_edit_menu->addAction(_text_handle_comment_action);

    extra_edit_menu->addSeparator();

    _editor_zoom_in_action = new QAction(tr("Zoom in text"), this);
    _editor_zoom_in_action->setShortcut(Qt::CTRL + Qt::Key_Equal);
    _editor_zoom_in_action->setStatusTip(tr("Zoom in text"));
    connect(_editor_zoom_in_action, &QAction::triggered, this,
            [this](bool )
            {
                MdiChild * edit = activeMdiChild();
                if (edit != nullptr)
                {
                    edit->zoomIn(1);
                    setTextZooming(edit->font().pointSize());
                }
            });
    extra_edit_menu->addAction(_editor_zoom_in_action);

    _editor_zoom_out_action = new QAction(tr("Zoom out text"), this);
    _editor_zoom_out_action->setShortcut(Qt::CTRL + Qt::Key_Minus);
    _editor_zoom_out_action->setStatusTip(tr("Zoom out text"));
    connect(_editor_zoom_out_action, &QAction::triggered, this,
            [this](bool )
            {
                MdiChild * edit = activeMdiChild();
                if (edit != nullptr)
                {
                    edit->zoomOut(1);
                    setTextZooming(edit->font().pointSize());
                }
            });
    extra_edit_menu->addAction(_editor_zoom_out_action);

    _editor_unzoom_action = new QAction(tr("Reset zooming"), this);
    _editor_unzoom_action->setShortcut(Qt::CTRL + Qt::Key_0);
    _editor_unzoom_action->setStatusTip(tr("Reset zooming"));
    connect(_editor_unzoom_action, &QAction::triggered, this,
            [this](bool )
            {
                MdiChild * edit = activeMdiChild();
                if (edit != nullptr)
                {
                    QFont font = edit->font();
                    font.setPointSize(_config.base_font_size);
                    edit->setFont(font);
                    setTextZooming(_config.base_font_size);
                }
            });
    extra_edit_menu->addAction(_editor_unzoom_action);

    menuBar()->addSeparator();

    // runMenu
    QMenu *runMenu = menuBar()->addMenu(tr("&Run"));

    for(int i=0; i < 10; ++i)
    {
        QAction *a = new QAction();

        a->setText("");
        a->setData(i);
        a->setVisible(false);
//        a->setShortcut(Qt::CTRL + Qt::Key_F1 + i);
        connect(a, &QAction::triggered, this, &MainWindow::onRunCommand);
        runMenu->addAction(a);
        _run_actions.push_back(a);
    }

    _run_separator_action = runMenu->addSeparator();
    _run_separator_action->setVisible(false);

    const QIcon runIcon = QIcon::fromTheme("media-playback-start", QIcon(":/images/media-playback-start"));
    _run_action = new QAction(runIcon, tr("&Run"), this);
    _run_action->setShortcut(Qt::Key_F5);
    _run_action->setStatusTip(tr("Execute current edit text"));
    connect(_run_action, &QAction::triggered, this, [this]() { run(); });
    runMenu->addAction(_run_action);

    const QIcon stopIcon = QIcon::fromTheme("media-playback-stop", QIcon(":/images/media-playback-stop"));
    _stop_action = new QAction(stopIcon, tr("&Stop"), this);
    _stop_action->setShortcut(Qt::Key_Escape);
    _stop_action->setStatusTip(tr("Terminate current execution"));
    connect(_stop_action, &QAction::triggered, this, &MainWindow::stop);
    runMenu->addAction(_stop_action);

    _run_toolbar->addAction(_run_action);
    _run_toolbar->addAction(_stop_action);

    menuBar()->addSeparator();

    // docks
    setDockOptions(QMainWindow::AnimatedDocks |
                   QMainWindow::AllowNestedDocks |
                   QMainWindow::AllowTabbedDocks);

    // directoryWindow
    _fs_dock = new QDockWidget(tr("Working Directory"), this);
    _fs_dock->setObjectName("Working Directory");
    _fs_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    _fs_tree = new WorkDirectory(this);
    _fs_dock->setWidget(_fs_tree);
    addDockWidget(Qt::LeftDockWidgetArea, _fs_dock);
    connect(_fs_tree, SIGNAL(activated(const QModelIndex &)), this, SLOT(openEditWindow(const QModelIndex &)));

    // globalMsgWindow created in MainWindow()

    // editorMsgWindow
    _editor_msg_dock = new QDockWidget(tr("Editor Messages"), this);
    _editor_msg_dock->setObjectName("Editor Messages");
    _editor_msg_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::BottomDockWidgetArea, _editor_msg_dock);
    _editor_msg_dock->setVisible(false);

    _editor_msg_list = new BufferedListWidget(_editor_msg_dock);
    _editor_msg_dock->setWidget(_editor_msg_list);

    connect(_editor_msg_list, &QListWidget::itemDoubleClicked, this, &MainWindow::activateMessage);

    // executeMsgWindow
    _execute_msg_dock = new QDockWidget(tr("Execute Messages"), this);
    _execute_msg_dock->setObjectName("Execute Messages");
    _execute_msg_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::RightDockWidgetArea, _execute_msg_dock);
    _execute_msg_dock->setVisible(false);

    _execute_msg_list = new BufferedListWidget(_execute_msg_dock, true);
    _execute_msg_dock->setWidget(_execute_msg_list);

    connect(_execute_msg_list, &QListWidget::itemDoubleClicked, this, &MainWindow::activateMessage);

    // _chart_dock
    _chart_dock = new QDockWidget(tr("Chart"), this);
    _chart_dock->setObjectName("Chart");
    _chart_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::RightDockWidgetArea, _chart_dock);
    _chart_dock->setVisible(false);

    _chart_view = new ChartView(this, _chart_dock);
//    _chart_view->setStyleSheet();
    _chart_dock->setWidget(_chart_view);

    // _graph3d_dock
    _graph3d_dock = new QDockWidget(tr("Graph3d"), this);
    _graph3d_dock->setObjectName("Graph3d");
    _graph3d_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::RightDockWidgetArea, _graph3d_dock);
    _graph3d_dock->setVisible(false);

    _graph3d = new Graph3D(_graph3d_dock, _config);
    QWidget *container = QWidget::createWindowContainer(_graph3d, _graph3d_dock);
    _graph3d_dock->setWidget(container);

    // _cockpit_dock
    _cockpit_dock = new QDockWidget(tr("Cockpit"), this);
    _cockpit_dock->setObjectName("Cockpit");
    _cockpit_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::RightDockWidgetArea, _cockpit_dock);
    _cockpit_dock->setVisible(false);

    _cockpit_view = new CockpitView(/*this, */_cockpit_dock);
//    _chart_view->setStyleSheet();
    _cockpit_dock->setWidget(_cockpit_view);

    // _cockpit3d_dock
    _cockpit3d_dock = new QDockWidget(tr("Cockpit3D"), this);
    _cockpit3d_dock->setObjectName("Cockpit3D");
    _cockpit3d_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    addDockWidget(Qt::RightDockWidgetArea, _cockpit3d_dock);
    _cockpit3d_dock->setVisible(false);

    _cockpit3d = new Cockpit3D;
//    _chart_view->setStyleSheet();
    _cockpit3d_dock->setWidget(createWindowContainer(_cockpit3d));

    // Grammar Table
    _grammar_dock = new QDockWidget(tr("Grammars"), this);
    _grammar_dock->setObjectName("Grammars");
    _grammar_dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    _grammar_table = new GrammarTable(this);
    _grammar_dock->setWidget(_grammar_table);
    addDockWidget(Qt::LeftDockWidgetArea, _grammar_dock);
//    connect(_fs_tree, SIGNAL(activated(const QModelIndex &)), this, SLOT(openEditWindow(const QModelIndex &)));

    // viewMenu
    QMenu *viewMenu = menuBar()->addMenu(tr("&View"));

    _statusbar_action = new QAction(tr("&Status bar"), this);
    _statusbar_action->setCheckable(true);
    _statusbar_action->setStatusTip(tr("Show or hide status bar"));
    connect(_statusbar_action, &QAction::triggered, this, &MainWindow::statusBarViewing);
    viewMenu->addAction(_statusbar_action);

    QMenu *toolbarMenu = viewMenu->addMenu(tr("&Toolbars"));

    toolbarMenu->addAction(_file_toolbar->toggleViewAction());
    toolbarMenu->addAction(_edit_toolbar->toggleViewAction());
    toolbarMenu->addAction(_run_toolbar->toggleViewAction());

    QMenu *dockMenu = viewMenu->addMenu(tr("&Docked Windows"));

    QAction *grammar_dock_action = _grammar_dock->toggleViewAction();
    dockMenu->addAction(grammar_dock_action);
    grammar_dock_action->setText(tr("&Grammar Table"));
//    grammar_dock_action->setShortcut(Qt::ALT + Qt::Key_3);

    QAction *messagedockAct = _global_msg_dock->toggleViewAction();
    dockMenu->addAction(messagedockAct);
    messagedockAct->setText(tr("Global &Messages"));
//    messagedockAct->setShortcut(Qt::ALT + Qt::Key_4);

    QAction *editorMsgDockAct = _editor_msg_dock->toggleViewAction();
    dockMenu->addAction(editorMsgDockAct);
    editorMsgDockAct->setText(tr("&Editor Messages"));
    editorMsgDockAct->setShortcut(Qt::ALT + Qt::Key_5);

    QAction *executeMsgDockAct = _execute_msg_dock->toggleViewAction();
    dockMenu->addAction(executeMsgDockAct);
    executeMsgDockAct->setText(tr("&Execute Messages"));
    executeMsgDockAct->setShortcut(Qt::ALT + Qt::Key_6);

    QAction *executeChartAct = _chart_dock->toggleViewAction();
    dockMenu->addAction(executeChartAct);
    executeChartAct->setText(tr("&Chart"));
    executeChartAct->setShortcut(Qt::ALT + Qt::Key_7);

    QAction *executeGraphAct = _graph3d_dock->toggleViewAction();
    dockMenu->addAction(executeGraphAct);
    executeGraphAct->setText(tr("Graph&3d"));
    executeGraphAct->setShortcut(Qt::ALT + Qt::Key_8);

    QAction *executeCockpitAct = _cockpit_dock->toggleViewAction();
    dockMenu->addAction(executeCockpitAct);
    executeCockpitAct->setText(tr("&Cockpit"));
    // executeCockpitAct->setShortcut(Qt::ALT + Qt::Key_9);

    QAction *executeCockpit3DAct = _cockpit3d_dock->toggleViewAction();
    dockMenu->addAction(executeCockpit3DAct);
    executeCockpit3DAct->setText(tr("&Cockpit3D"));
    // executeCockpitAct->setShortcut(Qt::ALT + Qt::Key_0);

    dockMenu->addSeparator();

    QAction *dirdockAct = _fs_dock->toggleViewAction();
    dockMenu->addAction(dirdockAct);
    dirdockAct->setText(tr("&Working Directory"));
    dirdockAct->setShortcut(Qt::ALT + Qt::Key_0);

    viewMenu->addSeparator();

    QMenu *colorMenu = viewMenu->addMenu(tr("&Color theme"));
    _color_action = colorMenu->menuAction();
    _color_action->setStatusTip(tr("Select color theme"));
    createThemesActions(colorMenu);

    QMenu *graph3d_menu = viewMenu->addMenu(tr("Graph&3d theme"));

    _graph3d->createThemesActions(graph3d_menu);

    viewMenu->addSeparator();

    QMenu *spellMenu = viewMenu->addMenu(tr("Spell checking"));

    _spell_english_action = new QAction(_config.spell_english_dict_name, this);
    _spell_english_action->setCheckable(true);
    _spell_english_action->setStatusTip(tr("Produce or not english words spell checking in comments and strings"));
    connect(_spell_english_action, &QAction::triggered, this,
            [this](bool checked)
            {
                _config.spell_english = checked;

                foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
                {
                    MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
                    CodeEditorParameters params = mdiChild->parameters();
                    params.spell_checking_english_required = _config.spell_english;
                    mdiChild->setParameters(params);
                }
            });
    spellMenu->addAction(_spell_english_action);

    _spell_national_action = new QAction(_config.spell_national_dict_name, this);
    _spell_national_action->setCheckable(true);
    _spell_national_action->setStatusTip(tr("Produce or not national words spell checking in comments and strings"));
    connect(_spell_national_action, &QAction::triggered, this,
            [this](bool checked)
            {
                _config.spell_national = checked;

                foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
                {
                    MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
                    CodeEditorParameters params = mdiChild->parameters();
                    params.spell_checking_national_required = _config.spell_national;
                    mdiChild->setParameters(params);
                }
            });
    spellMenu->addAction(_spell_national_action);

    spellMenu->addSeparator();

    _spell_id_action = new QAction(tr("&ID name checking"), this);
    _spell_id_action->setCheckable(true);
    _spell_id_action->setStatusTip(tr("Produce or not identificator name spell checking"));
    connect(_spell_id_action, &QAction::triggered, this,
            [this](bool checked)
            {
                _config.spell_ids = checked;

                foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
                {
                    MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
                    CodeEditorParameters params = mdiChild->parameters();
                    params.id_spell_checking_requared = _config.spell_ids;
                    mdiChild->setParameters(params);
                }
            });

    spellMenu->addAction(_spell_id_action);

    if (_config.spell_english_dict_name.isEmpty() && _config.spell_national_dict_name.isEmpty())
        spellMenu->setEnabled(false);
    if (_config.spell_english_dict_name.isEmpty())
    {
        _spell_english_action->setEnabled(false);
        _spell_id_action->setEnabled(false);
    }
    if (_config.spell_national_dict_name.isEmpty())
        _spell_national_action->setEnabled(false);

    QMenu *semanticMenu = viewMenu->addMenu(tr("S&emantics"));

    /// @note Действие по отмене подсветки семантики я специально не стал убирать в настройки,
    /// чтобы стимулировать пользователей сообщать об ошибках в семантическом анализаторе.
    _semantic_highlight_action = new QAction(tr("&Highlight semantics"), this);
    _semantic_highlight_action->setCheckable(true);
    _semantic_highlight_action->setChecked(true);
    _semantic_highlight_action->setStatusTip(tr("Should semantic highlighting be performed or not"));
    connect(_semantic_highlight_action, &QAction::triggered, this,
            [this](bool checked){
                _config.highlight_semantics = checked;

                foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
                {
                    MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
                    CodeEditorParameters params = mdiChild->parameters();
                    params.highlight_semantics = _config.highlight_semantics;
                    mdiChild->setParameters(params);
                    mdiChild->rehighlight();
                }
            });
    semanticMenu->addAction(_semantic_highlight_action);

    _scope_action = new QAction(tr("&Namespace structure"), this);
    _scope_action->setCheckable(true);
    _scope_action->setStatusTip(tr("Show or hide the namespace structure in the line number area"));
    connect(_scope_action, &QAction::triggered, this, &MainWindow::scopeViewing);
    semanticMenu->addAction(_scope_action);

    _compl_action = new QAction(tr("&Completion"), this);
    _compl_action->setCheckable(true);
    _compl_action->setStatusTip(tr("Show or not completion list"));
    connect(_compl_action, &QAction::triggered, this, &MainWindow::complViewing);
    semanticMenu->addAction(_compl_action);

    menuBar()->addSeparator();

    _window_menu = menuBar()->addMenu(tr("&Window"));
    connect(_window_menu, &QMenu::aboutToShow, this, &MainWindow::updateWindowMenu);

    _close_action = new QAction(tr("Cl&ose"), this);
    _close_action->setStatusTip(tr("Close the active window"));
    connect(_close_action, &QAction::triggered, _mdi_area, &QMdiArea::closeActiveSubWindow);

    _close_all_action = new QAction(tr("Close &All"), this);
    _close_all_action->setStatusTip(tr("Close all the windows"));
    connect(_close_all_action, &QAction::triggered, _mdi_area, &QMdiArea::closeAllSubWindows);

    _tile_action = new QAction(tr("&Tile"), this);
    _tile_action->setStatusTip(tr("Tile the windows"));
    connect(_tile_action, &QAction::triggered, _mdi_area, &QMdiArea::tileSubWindows);

    _cascade_action = new QAction(tr("&Cascade"), this);
    _cascade_action->setStatusTip(tr("Cascade the windows"));
    connect(_cascade_action, &QAction::triggered, _mdi_area, &QMdiArea::cascadeSubWindows);

    _next_action = new QAction(tr("Ne&xt"), this);
    _next_action->setShortcuts(QKeySequence::NextChild);
    _next_action->setStatusTip(tr("Move the focus to the next window"));
    connect(_next_action, &QAction::triggered, _mdi_area, &QMdiArea::activateNextSubWindow);

    _previous_action = new QAction(tr("Pre&vious"), this);
    _previous_action->setShortcuts(QKeySequence::PreviousChild);
    _previous_action->setStatusTip(tr("Move the focus to the previous window"));
    connect(_previous_action, &QAction::triggered, _mdi_area, &QMdiArea::activatePreviousSubWindow);

    _window_menu_separator_action = new QAction(this);
    _window_menu_separator_action->setSeparator(true);

    updateWindowMenu();

    menuBar()->addSeparator();

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

    QAction *help_contents_action = helpMenu->addAction(tr("SIMODO project page"), this,
                                        [this]()
                                        {
                                            QDesktopServices::openUrl(QUrl(_config.help_main_page_url, QUrl::TolerantMode));
                                        });
    help_contents_action->setStatusTip(tr("Go to SIMODO project main page"));

    const QIcon aboutIcon = QIcon::fromTheme("help-about", QIcon(":/images/help-about"));
    QAction *aboutAct = helpMenu->addAction(aboutIcon, tr("&About SIMODO IDE"), this, &MainWindow::about);
    aboutAct->setStatusTip(tr("Show the application's About box"));

    QAction *aboutQtAct = helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));

    QAction *readMeAct = helpMenu->addAction(tr("&READ ME"), this, &MainWindow::readMe);
    readMeAct->setStatusTip(tr("Some important tips"));

    _tabulation_action_group = new QActionGroup(this);
    _tabulation_action_group->addAction(tabulationAct_1);
    _tabulation_action_group->addAction(tabulationAct_2);
    _tabulation_action_group->addAction(tabulationAct_4);
    _tabulation_action_group->addAction(tabulationAct_8);
}

void MainWindow::createThemesActions(QMenu *theme_menu)
{
    _color_action_group = new QActionGroup(this);

    for(auto g : _config.decor_themes_groups)
    {
        if (!g.isEmpty())
            theme_menu->addSeparator()->setText(g);

        for(const auto & dt : _config.decor_themes)
            if (dt.group == g)
            {
                QAction *a = new QAction();

                //TODO: icon
                a->setText(dt.displayname);
                a->setData(dt.name);
                a->setVisible(dt.active);
                a->setStatusTip(dt.help);
                a->setCheckable(true);
                if (config().decor_theme_name == a->data().toString())
                    a->setChecked(true);
                connect(a, &QAction::triggered, this, &MainWindow::setColorTheme);
                _color_action_group->addAction(a);
                theme_menu->addAction(a);
            }
    }
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::readCommonSettings()
{
    QSettings        settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    const QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();

    if (geometry.isEmpty())
    {
        const QRect availableGeometry = QApplication::desktop()->availableGeometry(this);

        resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
        move((availableGeometry.width() - width()) / 2,
             (availableGeometry.height() - height()) / 2);
    }
    else
    {
        restoreGeometry(geometry);
    }

    settings.beginGroup("config");
    _config.data_dir = settings.value("data_dir",".").toString();
    _config.working_dir = settings.value("working_dir",".").toString();
    _config.decor_theme_name = settings.value("decor_theme_name","default").toString();
    _config.tab_size = settings.value("tab_size",4).toInt();
    _config.curr_font_size = settings.value("curr_font_size",12).toInt();
    _config.text_wrapping = settings.value("text_wrapping",true).toBool();
    _config.statusbar_viewing = settings.value("statusbar_viewing",true).toBool();
    _config.scope_view = settings.value("scope_view",true).toBool();
    _config.completer = settings.value("completer",true).toBool();
    _config.text_tab_replacement = settings.value("text_tab_replacement",true).toBool();
    _config.text_auto_indent = settings.value("text_auto_indent",true).toBool();

    _config.spell_english = settings.value("spell_english",true).toBool();
    _config.spell_national = settings.value("spell_national",true).toBool();
    _config.spell_ids = settings.value("spell_ids",true).toBool();

    _config.chart_theme = settings.value("chart_theme",0).toInt();
    _config.chart_antialiasing = settings.value("chart_antialiasing",true).toBool();
    _config.chart_legent = settings.value("chart_legent",Qt::AlignTop).toInt();

    _config.graph3d_theme = settings.value("graph3d_theme",0).toInt();
    _config.graph3d_X = settings.value("graph3d_X",0.0).toFloat();
    _config.graph3d_Y = settings.value("graph3d_Y",0.0).toFloat();
    _config.graph3d_zoom = settings.value("graph3d_zoom",100.0).toFloat();

    settings.endGroup();
}

void MainWindow::readRestSettings()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    settings.beginGroup("widgets");
    _file_toolbar->setVisible(settings.value("fileToolBar",true).toBool());
    _edit_toolbar->setVisible(settings.value("editToolBar",true).toBool());
    _run_toolbar->setVisible(settings.value("runToolBar",true).toBool());
    settings.endGroup();

    _fs_tree->setWorkDirectory(_config.working_dir);

    _wrap_action->setChecked(_config.text_wrapping);
    _scope_action->setChecked(_config.scope_view);
    _compl_action->setChecked(_config.completer);
    _tab_replacement_action->setChecked(_config.text_tab_replacement);
    _text_auto_indent_action->setChecked(_config.text_auto_indent);

    _spell_english_action->setChecked(_config.spell_english);
    _spell_national_action->setChecked(_config.spell_national);
    _spell_id_action->setChecked(_config.spell_ids);

    _statusbar_action->setChecked(_config.statusbar_viewing);
    statusBar()->setVisible(_config.statusbar_viewing);

    for(auto a : _tabulation_action_group->actions())
        a->setChecked(_config.tab_size == a->text().toInt());

    settings.beginGroup("terminals");
    for(int i=0; i < MAX_TERMINALS; ++i)
        _config.terminal_color_schemes.insert(i, settings.value("terminal_color_scheme_"
                                                                  + QString::number(i),"Linux").toString());
    settings.endGroup();

    restoreState(settings.value("windowState").toByteArray());
}

void MainWindow::writeSettings()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());

    settings.beginGroup("widgets");
    settings.setValue("fileToolBar", _file_toolbar->isVisible());
    settings.setValue("editToolBar", _edit_toolbar->isVisible());
    settings.setValue("runToolBar", _run_toolbar->isVisible());
    settings.endGroup();

    settings.beginGroup("config");
    settings.setValue("data_dir", _config.data_dir);
    settings.setValue("working_dir", _config.working_dir);
    settings.setValue("decor_theme_name", _config.decor_theme_name);
    settings.setValue("tab_size", _config.tab_size);
    settings.setValue("curr_font_size", _config.curr_font_size);
    settings.setValue("text_wrapping", _config.text_wrapping);
    settings.setValue("statusbar_viewing", _config.statusbar_viewing);
    settings.setValue("scope_view", _config.scope_view);
    settings.setValue("completer", _config.completer);
    settings.setValue("text_tab_replacement", _config.text_tab_replacement);
    settings.setValue("text_auto_indent", _config.text_auto_indent);

    settings.setValue("spell_english", _config.spell_english);
    settings.setValue("spell_national", _config.spell_national);
    settings.setValue("spell_ids", _config.spell_ids);

    settings.setValue("chart_theme", _config.chart_theme);
    settings.setValue("chart_antialiasing", _config.chart_antialiasing);
    settings.setValue("chart_legent", _config.chart_legent);

    settings.setValue("graph3d_theme", _config.graph3d_theme);
    settings.setValue("graph3d_X", _graph3d->X());
    settings.setValue("graph3d_Y", _graph3d->Y());
    settings.setValue("graph3d_zoom", _graph3d->zoom());
    settings.endGroup();

    settings.beginGroup("terminals");
    for(int i=0; i < MAX_TERMINALS; ++i)
        settings.setValue("terminal_color_scheme_" + QString::number(i), _config.terminal_color_schemes[i]);
    settings.endGroup();
}

MdiChild *MainWindow::activeMdiChild() const
{
    if (QMdiSubWindow *activeSubWindow = _mdi_area->activeSubWindow(); activeSubWindow != nullptr)
        return qobject_cast<MdiChild *>(activeSubWindow->widget());

    return nullptr;
}

MdiChild *MainWindow::findMdiChild(const QString &fileName) const
{
    QString canonicalFilePath = QFileInfo(fileName).canonicalFilePath();

    return findMdiChildCanonicalPath(canonicalFilePath);
}

MdiChild *MainWindow::findMdiChildCanonicalPath(const QString &fileNameCanonical) const
{
    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        if (mdiChild->currentFile() == fileNameCanonical)
            return mdiChild;
    }
    return nullptr;
}

void MainWindow::showLineNumber()
{
    MdiChild *child = activeMdiChild();
    showLineNumber(child);
}

void MainWindow::showLineNumber(MdiChild *child)
{
    if (child && _linenumber_label) {
        int lineno = child->textCursor().blockNumber() + 1;
        int columnno = child->textCursor().positionInBlock() + 1;
        int position = child->textCursor().position();
        QString str = QString(tr("| line: %1, column: %2, position: %3")).arg(lineno).arg(columnno).arg(position);
        _linenumber_label->setText(str);
    }
}

void MainWindow::setTextZooming(int curr_font_size)
{
    _config.curr_font_size = curr_font_size;

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        mdiChild->setTabStopDistance(QFontMetricsF(mdiChild->font()).horizontalAdvance(' ') * config().tab_size);
        mdiChild->update();
    }

    QString str = "| " + QString(tr("scale: %1%")).arg(_config.curr_font_size*100/_config.base_font_size);

    _text_zooming_percentage_label->setText(str);
}

QStringList MainWindow::getOpenedFiles() const
{
    QStringList files;

    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        files.push_back(mdiChild->currentFile());
    }

    return files;
}

bool MainWindow::hasTextSelection() const
{
    return activeMdiChild() && activeMdiChild()->textCursor().hasSelection();
}

void MainWindow::updateColorTheme()
{
    foreach (QMdiSubWindow *window, _mdi_area->subWindowList())
    {
        MdiChild *mdiChild = qobject_cast<MdiChild *>(window->widget());
        mdiChild->setColorTheme(getCurrentTheme());
        mdiChild->update();
    }
}

QString MainWindow::makeCommand(QString pattern)
{
    QString     command;
    MdiChild *  child   = activeMdiChild();

    command = pattern.replace("$DATADIR", _config.data_dir);
    command = command.replace("$WORKDIR", _config.working_dir);
    command = command.replace("$CURFILE_path", (child == nullptr) ? "" : QFileInfo(child->currentFile()).path());
    command = command.replace("$CURFILE_name", (child == nullptr) ? "" : QFileInfo(child->currentFile()).fileName());
    command = command.replace("$CURFILE_base", (child == nullptr) ? "" : QFileInfo(child->currentFile()).completeBaseName());
    command = command.replace("$CURFILE_suffix", (child == nullptr) ? "" : QFileInfo(child->currentFile()).suffix());
    command = command.replace("$CURFILE", (child == nullptr) ? "" : QFileInfo(child->currentFile()).canonicalFilePath());

    return command;
}

