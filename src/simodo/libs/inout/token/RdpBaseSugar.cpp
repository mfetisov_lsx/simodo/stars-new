/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/RdpBaseSugar.h"

using namespace simodo::inout::token;

bool RdpBaseSugar::reportUnexpected(const Token &t, const std::u16string &expected) const
{
    _m.reportError(t.location,
                   u"Irrelevant symbol '" + t.lexeme + u"'" + (expected.empty() ? u"" : u", expected: " + expected));

    return false;
}
