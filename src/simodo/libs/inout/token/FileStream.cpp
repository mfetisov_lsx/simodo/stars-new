/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/token/FileStream.h"

#include <cassert>
#include <cstdint>

char16_t simodo::inout::token::FileStream::get()
{
    if (_surrogate_pair != 0)
    {
        char16_t ch = _surrogate_pair;
        _surrogate_pair = 0;
        return ch;
    }

    if (_in.eof())
        return std::char_traits<char16_t>::eof();

    int ch1 = _in.get();

    if (ch1 <= 0x7F)
        return static_cast<char16_t>(ch1);

    uint32_t code;
    int      count;

    if ((ch1 & 0b11100000) == 0b11000000)
    {
        count = 2;
        code = (ch1 & 0b00011111);
    }
    else if ((ch1 & 0b11110000) == 0b11100000)
    {
        count = 3;
        code = (ch1 & 0b00001111);
    }
    else
    {
        count = 4;
        code = (ch1 & 0b00000111);
    }

    for(int i=1; i < count; ++i)
    {
        int ch = _in.get();

        if (ch == std::char_traits<char16_t>::eof())
            break;

        code = (code << 6) + (static_cast<uint32_t>(ch) & 0b00111111);
    }

    if (code <= 0xD7FF ||
       ((code >= 0xE000) && (code <= 0xFFFF)) )
    {
        return static_cast<char16_t>(code);
    }
    else if ((code >= 0xD800) && (code <= 0xDFFF))
    {
        // unicode replacement character
        return 0xFFFD;
    }
    else
    {
        // surrogate pair
        code -= 0x010000;
        _surrogate_pair = 0xD800 + ((code >> 10) & 0x3FF);
        return 0xDC00 + (code & 0x3FF);
    }
}
