/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/convert/functions.h"

namespace simodo::inout::convert
{

void replaceAll(std::u16string &str, const std::u16string &from, const std::u16string &to)
{
    if(from.empty())
        return;

    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    }
}

std::u16string encodeSpecialChars(const std::u16string & text)
{
    std::u16string res = text;
    replaceAll(res, u"\n", u"&NewLine;");
    replaceAll(res, u"\"", u"&QUOT;");
    return res;
}

std::u16string decodeSpecialChars(const std::u16string & text)
{
    std::u16string res = text;
    replaceAll(res, u"&QUOT;", u"\"");
    replaceAll(res, u"&NewLine;", u"\n");
    return res;
}

}
