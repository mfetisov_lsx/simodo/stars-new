/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/reporter/Reporter_abstract.h"

#include "simodo/inout/convert/functions.h"

using namespace simodo::inout;
using namespace simodo::inout::reporter;


void Reporter_abstract::reportWithPosition(const SeverityLevel level, const token::Location &location, const std::u16string &briefly, const std::u16string &atlarge)
{
    report( level,
            location,
            briefly,
            atlarge.empty() ? (u"Позиция разбора: " + getLocationString(location)) : atlarge);
}

std::u16string simodo::inout::reporter::getSeverityLevelName(const SeverityLevel level)
{
    switch(level)
    {
    case SeverityLevel::Information:
        return u"";
    case SeverityLevel::Warning:
        return u"Предупреждение: ";
    case SeverityLevel::Error:
        return u"Ошибка: ";
    case SeverityLevel::Fatal:
        return u"Сбой! ";
    }
    return u"";
}

std::u16string simodo::inout::reporter::getLocationString(const token::Location &location, bool in_detail)
{
    /// \todo Нужно убрать лишнее преобразование кодировок
    std::u16string str = location.file_name
               + u":" + convert::toU16(std::to_string(static_cast<unsigned>(location.line_begin)))
               + u":" + convert::toU16(std::to_string(static_cast<unsigned>(location.column_tabulated)));

    if (in_detail)
        str += u"[" + convert::toU16(std::to_string(location.begin))
                + u"," + convert::toU16(std::to_string(location.end))
                + u"]";

    return str;
}
