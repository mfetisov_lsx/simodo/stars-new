/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/inout/reporter/ConsoleReporter.h"

#include "simodo/inout/convert/functions.h"

#include <iostream>

using namespace simodo::inout;
using namespace simodo::inout::reporter;


void ConsoleReporter::report(const SeverityLevel level, token::Location, const std::u16string &briefly, const std::u16string &atlarge)
{
    if (level >= _max_severity_level)
    {
        std::cout << convert::toU8(getSeverityLevelName(level) + briefly) << std::endl;
        if (!atlarge.empty())
            std::cout << convert::toU8(atlarge) << std::endl;
    }

    _max_report_level = std::max(_max_severity_level,level);
}
