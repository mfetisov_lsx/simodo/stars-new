#include "simodo/lsp/LanguageClient.h"

namespace simodo::lsp
{
using namespace simodo::inout;

inline const std::string content_length_const = "Content-Length: ";

LanguageClient::LanguageClient(std::string process_path, std::vector<std::string> process_args, 
                variable::Record initialize_params, RpcHandle initialize_response_handle,
                inout::log::Logger_interface & logger)
    : _log(logger)
{
    try
    {
        _language_server = std::make_unique<bp::child>(
                                    process_path, process_args, 
                                    bp::std_in < _os, bp::std_out > _is, 
                                    bp::on_exit([&](auto...) {
                                            _is.close();
                                        }));

        if (!_language_server->running()) {
            _log.critical("Unable to run language server (stop)");
            _language_server->terminate();
            _language_server.reset();
            _ok = false;
        }
    }
    catch(const std::exception& e)
    {
        _log.critical("Unable to run language server (stop)", e.what());
        _language_server.reset();
        _ok = false;
    }
    
    if (_ok) {
        _response_thread = std::make_unique<std::thread>(&LanguageClient::response_listener, this);

        int64_t id = exec("initialize", initialize_params, initialize_response_handle 
                            ? initialize_response_handle 
                            : [](variable::json::Rpc ){});

        if (id > 0) {
            if (waitResponse(id))
                exec("initialized", {});
            else {
                _log.critical("Response timeout occurred (stop)");
                _ok = false;
            }
        }
        else {
            _log.critical("Response sending error occurred (stop)");
            _ok = false;
        }
    }

    if (!_ok)
        close();
}

LanguageClient::~LanguageClient()
{
    close();
    if (_response_thread)
        _response_thread->join();
}

int64_t LanguageClient::exec(const std::string method, variable::Value params, RpcHandle handle)
{
    std::unique_ptr<variable::json::Rpc> rpc;

    if (handle)
        rpc = std::make_unique<variable::json::Rpc>(convert::toU16(method), params, ++_last_id);
    else
        rpc = std::make_unique<variable::json::Rpc>(convert::toU16(method), params);

    return exec(*rpc, handle);
}

void LanguageClient::close()
{
    if (_stopping)
        return;

    if (_language_server) {
        if (waitResponse(exec("shutdown", {}, [](variable::json::Rpc ){})))
            exec("exit", {});

        _stopping = true;

        // @todo Перейти на boost::process::v2
        // std::error_code ec;
        // _language_server->wait_for(std::chrono::milliseconds(100), ec);
        // if (ec)
        //     _log.error("Language server process stopping error", ec.message());
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        if (_language_server->running())
            _language_server->terminate();
    }
}

bool LanguageClient::registerListener(std::string method, RpcHandle listener)
{
    std::lock_guard locker(_commands_listeners_mutex);
    _commands_listeners[method] = listener;
    return true;
}

int64_t LanguageClient::exec(const variable::json::Rpc & rpc, RpcHandle handle)
{
    if (!_os.good() || !rpc.is_valid()) 
        return -1;

    int64_t id = rpc.id();

    if ((id > 0 && handle == nullptr) || (id == -1 && handle != nullptr))
        return -1;

    send(rpc);

    if (id > 0) {
        std::lock_guard locker(_commands_waiting_to_be_executed_mutex);
        _commands_waiting_to_be_executed.insert({id, handle});
    }
    
    return id;
}

void LanguageClient::send(const variable::json::Rpc & rpc)
{
    std::string json = variable::json::toString(rpc.value(), true, true);
    _log.debug("Ready send content", json);

    static std::mutex send_mutex;
    std::lock_guard   locker(send_mutex);
    _os << content_length_const << json.length() 
        << std::endl << std::endl
        << json;
    _os.flush();
}

void LanguageClient::response_listener()
{
    _log.debug("Input pipe listener starting");
    while(_ok && !_stopping) {
        std::string line;
        size_t      content_length = 0;
        while(_is.good() && std::getline(_is,line) && !line.empty()) {
            if (line.find(content_length_const) == 0)
                content_length = std::stol(line.substr(content_length_const.size()));
        }

        if (!_is.good()) {
            // Штатная ситуация
            _log.info("Wrong input pipe state (stop listening)");
            break;
        }
        _log.debug("Ready receive content length " + std::to_string(content_length));

        std::string content;
        for(size_t i=0; i < content_length && _is.good(); ++i)
            content += _is.get();

        _log.debug("Ready work with content", content);

        if (content.length() != content_length || !verifyJson(content)) {
            _log.error("Wrong JSON-RPC content (try to recover input pipe)", content);
            continue;
        }

        RpcHandle                 handle = nullptr;
        const variable::json::Rpc rpc(content);
        if (!rpc.is_valid()) {
            _log.error("JSON-RPC structure is invalid", content);
        }
        else if (!rpc.method().empty()) {
            {
                std::lock_guard locker(_commands_listeners_mutex);
                auto            it = _commands_listeners.find(convert::toU8(rpc.method()));
                if (it == _commands_listeners.end())
                    _log.warning("An command or notification was ignored", content);
                else
                    handle = it->second;
            }
            if (handle)
                handle(rpc);
        }
        else {
            {
                std::lock_guard locker(_commands_waiting_to_be_executed_mutex);
                auto            it  = _commands_waiting_to_be_executed.find(rpc.id());
                if (it != _commands_waiting_to_be_executed.end()) {
                    handle = it->second;
                    _commands_waiting_to_be_executed.erase(it);
                }
                else
                    _log.error("An unexpected response was received", content);
            }
            if (handle) {
                handle(rpc);
                _command_complete_condition.notify_one();
            }
        }
    }
    _log.debug("Input pipe listener ending");
}

bool LanguageClient::waitResponse(int64_t id, int timeout_mills)
{
    if (id <= 0)
        return false;

    std::unique_lock locker(_command_complete_condition_mutex);
    return _command_complete_condition.wait_for(locker, std::chrono::milliseconds(timeout_mills), [&] { 
            std::lock_guard locker(_commands_waiting_to_be_executed_mutex);
            return _commands_waiting_to_be_executed.find(id) == _commands_waiting_to_be_executed.end();
        });
}

bool LanguageClient::verifyJson(const std::string & str)
{
    return !str.empty() && str[0] == '{' && str[str.size()-1] == '}';
}

}