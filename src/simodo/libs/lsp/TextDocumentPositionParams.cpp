#include "simodo/lsp/TextDocumentPositionParams.h"

#include "simodo/inout/convert/functions.h"

// using namespace simodo;

namespace simodo::lsp
{
bool parseTextDocumentIdentifier(const variable::Value & textDocument_value, 
                                 TextDocumentIdentifier & textDocument)
{
    if (textDocument_value.type() != variable::ValueType::Record) 
        return false;

    const variable::Value & uri_value = textDocument_value.getRecord()->find(u"uri");
    if (uri_value.type() != variable::ValueType::String)
        return false;

    textDocument.uri = inout::convert::toU8(uri_value.getString());
    
    return !textDocument.uri.empty();
}

bool parsePosition(const variable::Value & position_value, Position & position)
{
    if (position_value.type() != variable::ValueType::Record)
        return false;

    const variable::Value & line_value      = position_value.getRecord()->find(u"line");
    const variable::Value & character_value = position_value.getRecord()->find(u"character");

    if (line_value.type() != variable::ValueType::Int
     || character_value.type() != variable::ValueType::Int)
        return false;

    position.line       = line_value.getInt();
    position.character  = character_value.getInt();

    return position.line >= 0 && position.character >= 0;
}

bool parseTextDocumentPositionParams(const variable::Value & params_value, 
                                     TextDocumentPositionParams & documentPosition)
{
    if (params_value.type() != variable::ValueType::Record) 
        return false;

    std::shared_ptr<variable::Record> params_object = params_value.getRecord();

    if (!parseTextDocumentIdentifier(params_object->find(u"textDocument"), documentPosition.textDocument))
        return false;

    if (!parsePosition(params_object->find(u"position"), documentPosition.position))
        return false;

    return true;
}

}
