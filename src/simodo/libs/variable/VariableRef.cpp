/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"

#include <cassert>

using namespace simodo::variable;

VariableRef::VariableRef(Variable & var)
{
    if (var.type() != ValueType::Ref)
        _variable_ptr = &var;
    else {
        VariableRef variant_ref = std::get<VariableRef>(var.variant());  // Копируем вариант со ссылкой (чуть дольше, но безопасно?)
        Variable &  ref         = variant_ref.origin();        // Получаем изменяемую ссылку

        assert(ref.type() != ValueType::Ref);
        _variable_ptr = &ref;
    }
}

VariableRef::VariableRef(const Variable & var_c)
{
    Variable & var = const_cast<Variable &>(var_c);

    if (var.type() != ValueType::Ref)
        _variable_ptr = &var;
    else {
        VariableRef variant_ref = std::get<VariableRef>(var.variant());  // Копируем вариант со ссылкой (чуть дольше, но безопасно?)
        Variable &  ref         = variant_ref.origin();             // Получаем изменяемую ссылку

        assert(ref.type() != ValueType::Ref);
        _variable_ptr = &ref;
    }
}

const Variable & VariableRef::origin() const 
{ 
    return *_variable_ptr; 
}

Variable & VariableRef::origin()
{ 
    return *_variable_ptr; 
}

