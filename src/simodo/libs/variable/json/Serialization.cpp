/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#include "simodo/variable/json/Serialization.h"
#include "simodo/variable/json/parser/JsonRdp.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/inout/token/FileStream.h"
#include "simodo/inout/token/BufferStream.h"
#include "simodo/variable/json/Rpc.h"

#include <fstream>
#include <sstream>

using namespace simodo::inout;
using namespace simodo::variable;
using namespace simodo::variable::json;


namespace
{
    class StringReporter: public reporter::Reporter_abstract
    {
        std::string _message;

    public:
        virtual void report(const reporter::SeverityLevel level,
                            token::Location ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge) override
        {
            _message += convert::toU8(reporter::getSeverityLevelName(level) + briefly);
            if (!atlarge.empty())
                _message += " (" + convert::toU8(atlarge) + ")\n";
        }

        std::string message() const { return _message; }
    };

    void printJson(const Value & value, std::ostream & out, int level, bool compress, bool skip_empty_parameters)
    {
        switch (value.type())
        {
        case ValueType::Bool:
            out << (value.getBool() ? "true" : "false");
            break;
        case ValueType::Int:
            out << value.getInt();
            break;
        case ValueType::Real:
            out << value.getReal(); // clearNumberFractionalPart
            break;
        case ValueType::String:
            {
                std::u16string str = value.getString();
                convert::replaceAll(str,u"\\",u"\\\\");
                convert::replaceAll(str,u"\"",u"\\\"");
                out << "\"" << convert::toU8(str) << "\"";
            }
            break;
        case ValueType::Null:
            out << "null";
            break;
        case ValueType::Record:
            {
                const std::shared_ptr<Record> object = value.getRecord();

                out << "{";

                if (!compress)
                    out << std::endl;

                int i = 0;

                for(const Variable & var : object->variables()) {
                    if (level <= 1 && skip_empty_parameters && var.type() == ValueType::Null)
                        continue;

                    if (i++ > 0) {
                        out << ",";
                        if (!compress)
                            out << std::endl;
                    }

                    if (!compress)
                        for(int i=0; i < level; ++i)
                            out << "\t";

                    if (!compress)
                        out << "\"" << convert::toU8(var.name()) << "\" : ";
                    else
                        out << "\"" << convert::toU8(var.name()) << "\":";

                    printJson(var.value(), out, level+1, compress, skip_empty_parameters);
                }

                if (!compress)
                    out << std::endl;

                if (!compress)
                    for(int i=0; i < level-1; ++i)
                        out << "\t";

                out << "}";
            }
            break;
        case ValueType::Array:
            {
                std::shared_ptr<ValueArray> array = value.getArray();

                out << "[";

                if (!compress)
                    out << std::endl;

                int i = 0;

                for(const Value & v : array->values())
                {
                    if (i++ > 0) {
                        out << ",";
                        if (!compress)
                            out << std::endl;
                    }

                    if (!compress)
                        for(int i=0; i < level; ++i)
                            out << "\t";

                    printJson(v, out, level+1, compress, skip_empty_parameters);
                }

                if (!compress)
                    out << std::endl;

                if (!compress)
                    for(int i=0; i < level-1; ++i)
                        out << "\t";

                out << "]";
            }
            break;
        default:
            break;
        }
    }
}

std::string simodo::variable::json::load(const std::string &file_name, Value &value)
{
    StringReporter  reporter;
    parser::JsonRdp parser(reporter);
    std::string     result;

    bool ok = parser.parse(file_name, value);

    if (!ok)
        result = reporter.message();

    return result;
}

std::string simodo::variable::json::load(std::istream &in, Value &value, const std::string &file_name)
{
    token::FileStream   stream(in);
    StringReporter      reporter;
    parser::JsonRdp     parser(reporter);
    std::string         result;

    bool ok = parser.parse(file_name, stream, value);

    if (!ok)
        result = reporter.message();

    return result;
}

bool simodo::variable::json::save(const Value &value, std::ostream &out, bool compress, bool skip_empty_parameters)
{
    if (out.fail())
        return false;

    printJson(value, out, 1, compress, skip_empty_parameters);

    out.flush();

    return out.good();
}

bool simodo::variable::json::save(const Value &value, const std::string &file_name, bool compress, bool skip_empty_parameters)
{
    std::ofstream   out(file_name);

    if (!out)
        return false;

    return save(value, out, compress, skip_empty_parameters);
}

bool simodo::variable::json::save(const Rpc & rpc, std::ostream & out, bool compress, bool skip_empty_parameters)
{
    return save(rpc.value(), out, compress, skip_empty_parameters);
}

Value simodo::variable::json::fromString(const std::string & json_string)
{
    std::istringstream  is(json_string);
    token::FileStream   input(is);
    Value               res;
    StringReporter      reporter;
    parser::JsonRdp     parser(reporter);

    parser.parse("", input, res);

    return res;
}

Value simodo::variable::json::fromString(const std::u16string & json_string)
{
    return fromString(convert::toU8(json_string));
}

std::string simodo::variable::json::toString(const Value & value, bool compress, bool skip_empty_parameters)
{
    std::ostringstream os;
    save(value, os, compress, skip_empty_parameters);
    return os.str();
}

