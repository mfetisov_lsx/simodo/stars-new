/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/VariableStack.h"
#include "simodo/bormental/DrBormental.h"

using namespace simodo::bormental;
using namespace simodo::variable;

VariableStack::VariableStack(size_t stack_size)
{
    _stack.reserve(stack_size);
}

void VariableStack::push(Variable v)
{
    if (_stack.size() >= _stack.capacity())
        throw DrBormental("VariableStack::push", 
                          "SBL variable stack overflow occurred (size: " + std::to_string(_stack.size()) + ")");

    _stack.push_back(v);
}

Variable VariableStack::pop() 
{
    if (_stack.size() == 0)
        throw DrBormental("VariableStack::pop", 
                          "Attempt to pop an element from the empty SBL variable stack");

    Variable v = _stack.back();

    _stack.pop_back();

    return v;
}

void VariableStack::popAmount(size_t n)
{
    if (n > _stack.size())
        throw DrBormental("VariableStack::popAmount", 
                          "Attempt to pop too many elements (" + std::to_string(n)+ ") from the SBL variable stack (size: " + std::to_string(_stack.size()) + ")");

    while(n--)
        _stack.pop_back();
}

const Variable & VariableStack::top(size_t shift_from_top) const 
{
    if (shift_from_top >= _stack.size())
        throw DrBormental("VariableStack::top", 
                          "Attempt to handle invalid offset (" + std::to_string(shift_from_top)+ ") in SBL variable stack (size: " + std::to_string(_stack.size()) + ")");

    return _stack[_stack.size()-shift_from_top-1];
}

Variable & VariableStack::top(size_t shift_from_top)
{
    if (shift_from_top >= _stack.size())
        throw DrBormental("VariableStack::top", 
                          "Attempt to handle invalid offset (" + std::to_string(shift_from_top)+ ") in SBL variable stack (size: " + std::to_string(_stack.size()) + ")");

    return _stack[_stack.size()-shift_from_top-1];
}

const Variable & VariableStack::at(size_t index) const
{
    if (index > _stack.size())
        throw DrBormental("VariableStack::at", 
                          "Attempt to handle invalid offset (" + std::to_string(index)+ ") in SBL variable stack (size: " + std::to_string(_stack.size()) + ")");

    return _stack[index];
}

Variable & VariableStack::at(size_t index)
{
    if (index > _stack.size())
        throw DrBormental("VariableStack::at", 
                          "Attempt to handle invalid offset (" + std::to_string(index)+ ") in SBL variable stack (size: " + std::to_string(_stack.size()) + ")");

    return _stack[index];
}
