/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"
#include "simodo/bormental/DrBormental.h"
#include "simodo/variable/FunctionWrapper.h"

#include <algorithm>

using namespace simodo::variable;
using namespace simodo::bormental;

Record Record::copy() const
{
    Record record;

    for(const Variable & member : _variables)
        record.variables().push_back(member.copyVariable());

    return record;
}

// Record Record::instantiate() const
// {
//     Record record;

//     if (_module)
//         return _module->instantiate();

//     return copy();
// }

bool Record::exists(const std::u16string & name) const
{
    auto it = find_if(_variables.begin(), _variables.end(), [name](const Variable & v){
        return v.name() == name;
    });

    return it != _variables.end();
}

const Value & Record::find(const std::u16string & name) const
{
    static const Value null { ValueType::Null };

    auto it = find_if(_variables.begin(), _variables.end(), [name](const Variable & v){
        return v.name() == name;
    });

    if (it == _variables.end())
        return null;

    return it->value();  
}

Value & Record::find(const std::u16string & name)
{
    static Value null { ValueType::Null };

    auto it = find_if(_variables.begin(), _variables.end(), [name](const Variable & v){
        return v.name() == name;
    });

    if (it == _variables.end())
        return null;

    return it->value();
}

const Variable & Record::getVariableByName(const std::u16string & name) const
{
    static const Variable null { u"null", ValueType::Null };

    auto it = find_if(_variables.begin(), _variables.end(), [name](const Variable & v){
        return v.name() == name;
    });

    if (it == _variables.end())
        return null;

    return *it;  
}

const Variable & Record::getVariableByIndex(index_t index) const 
{
    if (index >= _variables.size())
        throw DrBormental("Record::getVariableByIndex", "Out of index");

    return _variables[index];
}

Value Record::invoke(const std::u16string & method_name, const VariableSet_t & arguments)
{
    const Variable & function = getVariableByName(method_name);
    if (function.type() == variable::ValueType::Null) 
        return {};

    if (function.type() != variable::ValueType::Function) 
        return {};

    try
    {
        FunctionWrapper            func(function);
        VariableSet_t              args_mutable = arguments;
        VariableSetWrapper_mutable args(args_mutable);
        
        return func.invoke(args);
    }
    catch(const std::exception & e)
    {
    }
    
    return {};
}
