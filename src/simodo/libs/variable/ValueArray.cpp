/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"
#include "simodo/bormental/DrBormental.h"

#include <cassert>

using namespace simodo::bormental;
using namespace simodo::variable;

ValueArray::ValueArray(std::vector<Value> values)
    : _dimensions({static_cast<index_t>(values.size())}), _values(values)
{}

ValueArray ValueArray::copy() const
{
    std::vector<Value> values;

    for(const Value & v : _values)
        values.push_back(v.copy());

    return { _common_type, _dimensions, values };
}

const Value & ValueArray::getValueByIndex(std::vector<index_t> index) const
{
    if (_dimensions.size() != index.size())
        throw DrBormental("ValueArray::getValueByIndex", "Out of index");

    if (index.size() != 1)
        throw DrBormental("ValueArray::getValueByIndex", "Unsupported");

    if (index[0] >= _values.size())
        throw DrBormental("ValueArray::getValueByIndex", "Out of index");

    return _values[index[0]];
}
