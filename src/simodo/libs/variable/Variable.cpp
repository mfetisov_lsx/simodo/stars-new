/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"

#include <cassert>

using namespace simodo::variable;

const Variable & Variable::origin() const
{
    if (type() != ValueType::Ref)
        return *this;

    const Variable & o = _value.getRef().origin();

    assert(o.type() != ValueType::Ref);

    return o;
}

Variable & Variable::origin()
{
    if (type() != ValueType::Ref)
        return *this;

    Variable & o = std::get<VariableRef>(_value.variant()).origin();

    assert(o.type() != ValueType::Ref);

    return o;
}

Variable Variable::copyVariable() const
{
    return { _name, _value.copy(), _location, _spec.copy() };
}

VariableRef Variable::makeReference() const
{
    if (type() == ValueType::Ref)
        return std::get<VariableRef>(variant());

    return VariableRef {*this};
}

