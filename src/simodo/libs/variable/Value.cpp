/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#include "simodo/variable/Variable.h"

#include <cassert>

using namespace simodo::variable;

Value Value::copy() const
{
    switch(_type)
    {
    case ValueType::Array:
        return std::make_shared<ValueArray>(getArray()->copy());
    case ValueType::Record:
        return std::make_shared<Record>(getRecord()->copy());
    case ValueType::Ref:
        assert(getRef().origin().type() != ValueType::Ref);
        return getRef().origin().value().copy();
    default:
        return *this;
    }
}

