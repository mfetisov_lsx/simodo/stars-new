/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_GrammarBuilder
#define SIMODO_DSL_GrammarBuilder

/*! \file GrammarBuilder.h
    \brief Формирование таблицы разбора по заданной структуре правил грамматики

    Заголовочный файл, выполняющий формирование таблицы разбора по заданной структуре правил грамматики.
*/

#include <string>
#include <map>
#include <set>

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/Grammar.h"
#include "simodo/dsl/ScriptC_Semantics.h"

#include "simodo/convert.h"

namespace simodo::dsl
{
    /*!
     * \brief Класс формирование таблицы разбора по заданной структуре правил грамматики
     *
     * Класс выполняет базовые подготовления для построения таблицы разбора по заданным правилам грамматики.
     * Является абстрактным классом.
     */
    class GrammarBuilder
    {
    protected:
        AReporter & _m;             ///< Обработчик сообщений
        std::string _path;          ///< Путь к файлам грамматики
        std::string _grammar_name;  ///< Наименование грамматики
        Grammar &   _g;             ///< Грамматика, которую нужно наполнить
        bool        _strict_rule_consistency; ///< Признак строгой проверки на согласованность правил

        std::vector<GrammarRuleTokens>       _rules;            ///< Правила
        std::multimap<std::u16string,size_t> _production_index; ///< Индекс продукций для правил грамматики _rules
        std::multimap<std::u16string,size_t> _pattern_index;    ///< Индекс символов из образца для правил грамматики _rules

    public:
        GrammarBuilder() = delete;    ///< Пустой конструктор не поддерживается!
        virtual ~GrammarBuilder() = default;

        /*!
         * \brief Конструктор построителя таблицы разбора по заданной структуре правил грамматики
         * \param m             Интерфейсная ссылка на объект обеспечения вывода информации в вызываемую программу
         * \param path          Путь к файлам грамматики
         * \param grammar_name  Наименование грамматики, которую нужно загрузить
         * \param g             Структура грамматики
         * \param strict_rule_consistency Признак строгости в проверках согласованности таблицы разбора
         */
        GrammarBuilder(AReporter & m, const std::string & path, const std::string & grammar_name, Grammar & g,
                       bool strict_rule_consistency)
            : _m(m)
            , _path(path)
            , _grammar_name(grammar_name)
            , _g(g)
            , _strict_rule_consistency(strict_rule_consistency)
        {}

        /*!
         * \brief Выполнение конструирования таблицы разбора
         *
         * Метод по заданному стартовому правилу вытягивает правила из заданного набора правил, помещая
         * их в структуру грамматики, заданную в конструкторе. Затем последовательно выполняет шаги и
         * проверки по построению таблицы разбора.
         *
         * \param rules     Контейнер с набором правил грамматики
         * \param main_rule Наименование стартового правила
         * \return          true, если построение завершилось успешно, иначе - false
         */
        bool    build(const std::vector<GrammarRuleTokens> & rules, const Token & main_rule);

        /*!
         * \brief Геттер признака строгой проверки на согласованность правил
         *
         * Данный признак нужен, если, например, нужно построить и проанализировать структуры таблицы разбора,
         * не взирая на некоторые несогласованности. Полезно, чтобы понять причину несогласованности.
         *
         * \return Признак строгой проверки на согласованность правил
         */
        bool    strict_rule_consistency() const { return _strict_rule_consistency; }

        /*!
         * \brief Проведение семантического анализа АСД-вставок в грамматике
         *
         * \attention Метод требует заполненной грамматики, т.е. перед его вызовом необходимо вызвать
         * GrammarBuilder::build.
         *
         * \param name_set  Ссылка на массив имён, который заполнется в процессе семантического анализа
         * \param scope_set Ссылка на массив зон видимостей, который заполняется в процессе семантического анализа
         * \return true, если ошибок не было выявлено, иначе - false
         */
        bool    checkGrammarAstBlocks(std::vector<SemanticName> & name_set,
                                      std::vector<SemanticScope> & scope_set) const;

        /*!
         * \brief Заполнение лексических параметров по сформированной грамматике
         *
         * Структура лексических параметров находится внутри структуры грамматики.
         *
         * \param g Ссылка на грамматику
         * \return true, если успех
         */
        static bool fillLexemeParameters(Grammar & g);

    protected:

        /*!
         * \brief Метод формирует путь к файлу описания грамматики
         * \return Путь к файлу описания грамматики
         */
        std::u16string buildPath() const;

        /*!
         * \brief Метод вытягивает по заданному стартовому правилу все связанные с ним правила из
         * заданного набора правил грамматики
         *
         * Метод вытягивает по заданному стартовому правилу все связанные с ним правила из
         * заданного набора правил грамматики и размещает их в структуре грамматики, переданной в конструкторе.
         *
         * \param rules      Контейнер с набором правил грамматики
         * \param production Наименование стартового правила
         * \return           true, если разбор завершился успешно, иначе - false
         */
        bool    extractRules(const std::vector<GrammarRuleTokens> & rules, std::u16string production);

        /*!
         * \brief Наполнение контейнера с перечнем символов грамматики (колонок таблицы разбора)
         */
        void    fillColumns() const;

        /*!
         * \brief Проверка завершённости построения таблицы разбора
         * \return true, если проверка завершилась успешно, иначе - false
         */
        bool    checkCompleteness();

        /*!
         * \brief Метод построения таблицы состояний
         */
        virtual void fillStateTable() = 0;

        /*!
         * \brief Основной метод построения таблицы разбора по заполненным перечням состояний (строк)
         * и символов грамматики (колонок)
         * \return true, если формирование завершилось успешно, иначе - false
         */
        virtual bool fillParseTable() = 0;
    };

    std::u16string getGrammarBuilderMethodName(TableBuildMethod method);

}

#endif // SIMODO_DSL_GrammarBuilder
