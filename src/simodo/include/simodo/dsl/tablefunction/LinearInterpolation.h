#ifndef SIMODO_DSL_TABLEFUNCTION_LinearInterpolation
#define SIMODO_DSL_TABLEFUNCTION_LinearInterpolation

#include "simodo/dsl/tablefunction/HyperCube.h"

template <typename Point>
struct Line;
template <typename Real>
struct Point;

template <typename Real>
Real linearInterpolation1(const Line<Point<Real>> & line, Real x);

template <typename Int, typename Real>
Real linearInterpolation(HyperCube<Int, Real> hyper_cube, std::vector<Real> args)
{
    while (1 < hyper_cube.n_dimens())
    {
        Int n_minus_one = hyper_cube.n_dimens() - 1;
        Int two_pow_n_minus_one = std::pow(Real(2), n_minus_one);
        for (Int i = 0; i < two_pow_n_minus_one; ++i)
        {
            std::vector<bool> coords1;
            coords1.resize(hyper_cube.n_dimens());
            coords1.back() = false;
            
            std::vector<bool> coords2;
            coords2.resize(hyper_cube.n_dimens());
            coords2.back() = true;
            
            for (Int j = 0, ii = i; j < n_minus_one; ++j, ii >>= 1)
            {
                coords1.at(j) = (ii % 2) != 0;
                coords2.at(j) = coords1.at(j);
            }

            hyper_cube.value(coords1) = linearInterpolation1({
                                                                { 
                                                                    hyper_cube.coord(n_minus_one)
                                                                    , hyper_cube.value(coords1)
                                                                }, {
                                                                    hyper_cube.coord(n_minus_one, true)
                                                                    , hyper_cube.value(coords2)
                                                                }
                                                            }, args.at(n_minus_one)
                                                            );
        }
        hyper_cube.decreaseNDimens();
    }

    return linearInterpolation1({
                                    { 
                                        hyper_cube.coord(0)
                                        , hyper_cube.value({{}})
                                    }, {
                                        hyper_cube.coord(0, true)
                                        , hyper_cube.value({true})
                                    }
                                },  args.front()
                                );
}

template <typename Point>
struct Line
{
    Point left, right;
};

template <typename Real>
struct Point
{
    Real x;
    Real value;
};

template <typename Real>
Real linearInterpolation1(const Line<Point<Real>> & line, Real x)
{
    Real fb = line.right.value;
    Real fa = line.left.value;
    Real b = line.right.x;
    Real a = line.left.x;
    if (b - a == 0)
    {
        return fb;
    }
    return (fb - fa) / (b - a) * (x - a) + fa; 
}

#endif // SIMODO_DSL_TABLEFUNCTION_LinearInterpolation
