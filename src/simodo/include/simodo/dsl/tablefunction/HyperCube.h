#ifndef SIMODO_DSL_TABLEFUNCTION_HyperCube
#define SIMODO_DSL_TABLEFUNCTION_HyperCube

#include <cmath>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

// n_dimens = n : Int, 1 < n < ..., количество измерений
// axes = array[n] of pair of Real, координаты точек гиперкуба на осях (задаётся двумя точками на каждой оси)
// values = array[2^(n - 1)] of pair of Real, значения в точках гиперкуба
//     индексы координат для каждой размерности
//     (пример для 4 размерного куба):
//     x4_id = 0, x3_id = 0, x2_id = 0 : y0000, y0001
//     x4_id = 0, x3_id = 0, x2_id = 1 : y0010, y0011
//     x4_id = 0, x3_id = 1, x2_id = 0 : y0100, y0101
//     x4_id = 0, x3_id = 1, x2_id = 1 : y0110, y0111
//
//     x4_id = 1, x3_id = 0, x2_id = 0 : y1000, y1001
//     x4_id = 1, x3_id = 0, x2_id = 1 : y1010, y1011
//     x4_id = 1, x3_id = 1, x2_id = 0 : y1100, y1101
//     x4_id = 1, x3_id = 1, x2_id = 1 : y1110, y1111

template <typename Int, typename Real>
class HyperCube
{
public:
    using RealPairVector = std::vector<std::pair<Real, Real>>;

    HyperCube(Int n_dimens, RealPairVector axes, RealPairVector values)
        : _n_dimens(n_dimens), _axes(std::move(axes)), _values(std::move(values))
    {
        if (_n_dimens < 1)
        {
            throw std::logic_error("Размерность гиперкуба < 1: " + std::to_string(_n_dimens));
        }

        if (Int(_axes.size()) < _n_dimens)
        {
            throw std::logic_error("Количество осей < размерности гиперкуба: "
                + std::to_string(_axes.size()) + " : " + std::to_string(_n_dimens));
        }

        Real two_pow_n_minus_one = std::pow(Real(2), _n_dimens - 1);
        if (_values.size() < two_pow_n_minus_one)
        {
            throw std::logic_error("Количество значений < размерности гиперкуба в степени (n - 1): "
                + std::to_string(_values.size()) + " : " + std::to_string(two_pow_n_minus_one));
        }
    }

    void decreaseNDimens()
    {
        if (1 < _n_dimens)
        {
            --_n_dimens;
        }
    }

    Int n_dimens()
    {
        return _n_dimens;
    }

    Real & value(std::vector<bool> coords)
    {
        if (Int(coords.size()) < _n_dimens)
        {
            throw std::logic_error("Количество координат < размерности куба: "
                + std::to_string(coords.size()) + " : " + std::to_string(_n_dimens));
        }

        Int index = 0;
        for (Int i = 1, two_pow = 1; i < _n_dimens; ++i, two_pow *= 2)
        {
            index += two_pow * (coords.at(i) ? 1 : 0);
        }
        auto & temp = _values.at(index);
        return coords.front() ? temp.second : temp.first;
    }

    Real coord(Int axis_index, bool second = false)
    {
        if (axis_index < 0 || _n_dimens <= axis_index)
        {
            throw std::logic_error("Невалидный индекс оси, размерность гиперкуба: "
                + std::to_string(axis_index) + " : " + std::to_string(_n_dimens));
        }

        auto & axis = _axes.at(axis_index);
        return second ? axis.second : axis.first;
    }
private:
    Int _n_dimens;
    RealPairVector _axes;
    RealPairVector _values;
};

#endif // SIMODO_DSL_TABLEFUNCTION_HyperCube