/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_NS_Scene
#define SIMODO_DSL_ScriptC_NS_Scene

/*! \file ScriptC_NS_Scene.h
    \brief Пространство имён сцены моделирования
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

namespace simodo::dsl
{
    struct OdeSystem;

    struct Actor
    {
        SCI_Name *              p_object;
        const AstNode *         p_equation_function_body;
        const AstNode *         p_diff_function_body;
        std::vector<SCI_Name *> dxdt_p;
        std::vector<SCI_Name *> x_p;
    };

    class ScriptC_NS_Scene : public IScriptC_Namespace
    {
        ScriptC_Interpreter *   _p_interpreter;

        double                  _t;
        double                  _tk;
        double                  _dt;

        std::vector<Actor>      _actor_set;
        const AstNode *         _p_iteration_callback_function_body;
        const AstNode *         _p_each_iteration_callback_function_body;
        size_t                  _iteration_callback_period;

        bool                    _realtime_mode_enabled;
        bool                    _time_output_enabled;

    public:
        ScriptC_NS_Scene(ScriptC_Interpreter * p_interpreter=nullptr, bool time_output_enabled=false);

        virtual ~ScriptC_NS_Scene();

        virtual SCI_Namespace_t getNamespace() override;

    public:
        double t() const { return _t; }
        void setT(double t) { _t = t; }
        double tk() const { return _tk; }
        void setTk(double tk) { _tk = tk; }
        double dt() const { return _dt; }
        void setDt(double dt) { _dt = dt; }

        const AstNode * iteration_callback_function_body() const { return _p_iteration_callback_function_body; }
        void setIterationCallbackFunctionBody(const AstNode * p_iteration_callback_function_body)
        { _p_iteration_callback_function_body = p_iteration_callback_function_body; }

        const AstNode * each_iteration_callback_function_body() const { return _p_each_iteration_callback_function_body; }
        void setEachIterationCallbackFunctionBody(const AstNode * p_each_iteration_callback_function_body)
        { _p_each_iteration_callback_function_body = p_each_iteration_callback_function_body; }

        size_t iteration_callback_period() const { return _iteration_callback_period; }
        void setIterationCallbackPeriod(size_t iteration_callback_period)
        { _iteration_callback_period = iteration_callback_period; }

        const std::vector<Actor> & actor_set() const { return _actor_set; }

        bool realtimeModeEnabled() const { return _realtime_mode_enabled; }
        void setRealtimeModeEnabled(bool enabled) { _realtime_mode_enabled = enabled; }

        void addActor (Actor actor)
        {
            _actor_set.push_back(actor);
        }

    public:
        void addObject(SCI_Name &object);
        void removeObject(SCI_Name &object);
        void start();
    };

    struct OdeSystem
    {
        using state_type = std::vector<double>;

        ScriptC_Interpreter * p_interpreter;
        ScriptC_NS_Scene *    scene;
        Actor                 actor;
        state_type            state;

        OdeSystem(ScriptC_Interpreter * interpreter, ScriptC_NS_Scene & scene, Actor & actor);

        void operator() (const state_type & x, state_type & dxdt, double t);
    };
}

#endif // SIMODO_DSL_ScriptC_NS_Scene
