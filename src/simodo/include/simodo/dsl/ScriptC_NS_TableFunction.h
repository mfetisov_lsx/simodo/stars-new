/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_NS_TableFunction
#define SIMODO_DSL_ScriptC_NS_TableFunction

/*! \file ScriptC_NS_TableFunction.h
    \brief Пространство имён 'TableFunction' для интерпретатора
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

namespace simodo::dsl
{
    /*!
     * \brief Получение ссылки на глобальное пространство имён табличных функций до 3 аргументов
     */
    class ScriptC_NS_TableFunction : public IScriptC_Namespace
    {
    public:
        virtual SCI_Namespace_t getNamespace() override;
    };

}

#endif // SIMODO_DSL_ScriptC_NS_TableFunction
