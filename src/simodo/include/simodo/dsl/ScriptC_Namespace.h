/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_Namespace
#define SIMODO_DSL_ScriptC_Namespace

/*! \file ScriptC_Namespace.h
    \brief Интерфейс пространства имён
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

namespace simodo::dsl
{
    class ScriptC_Namespace
    {
    public:
        ScriptC_NS_Scene();

        ~ScriptC_NS_Scene();

        SCI_StaticNamespace_t getNamespace() const;
    };

}

#endif // SIMODO_DSL_ScriptC_Namespace
