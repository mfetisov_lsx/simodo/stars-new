/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_Interpreter
#define SIMODO_DSL_ScriptC_Interpreter

/*! \file ScriptC_Interpreter.h
    \brief Исполяющая машина для АСД, собранного ScriptC_AstBuilder
*/

#include <vector>
#include <variant>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/SCI_Stack.h"

namespace simodo::dsl
{
    class ModulesManagement;

    const size_t STACK_MAX_SIZE = 1000;

    /*!
     * \brief Состояние обработки фрагмента кода интерпретатора
     */
    enum class SCI_RunnerCondition
    {
        Regular,        ///< Штатная работа
        Unsupported,    ///< Операция не поддерживается
        Error,          ///< Возникла ошибка
        Break,          ///< Обработка операции break
        Continue,       ///< Обработка операции continue
        Return          ///< Обработка операции return
    };

    /*!
     * \brief Определение зоны поиска имён фрагмента вызова функции
     */
    struct CallFrameElement
    {
        size_t      stack_local_boundary;
        SCI_Name *  encapsulator;
    };

    struct ModuleImportResults;

    /*!
     * \brief Исполняющая машина для АСД, собранного ScriptC_AstBuilder
     *
     * Задачи класса:
     * 1. Реализует встроенную семантику ScriptC и основную часть семантики SimpleC.
     * 2. Используется в описании грамматик DSL SIMODO.
     * 3. Выполняет тестирующие и исследовательские задачи при построении SIMODO.
     */
    class ScriptC_Interpreter
    {
        friend class SCI_NamespaceAddScope;
        friend class SCI_FrameScope;

        AReporter &             _m;                 ///< Обработчик сообщений
        ModulesManagement &     _mm;                ///< Управление модулями
        std::vector<SCI_Name *> _using_set;         ///< Набор имён операторов using
        SCI_Stack               _stack;             ///< Основной стек интерпретатора
        std::vector<CallFrameElement> _call_frame;  ///< Стек вызовов функций
        std::vector<size_t>     _scope_marks;       ///< Стек отметок зон видимости переменных на стеке интерпретатора
        std::vector<bool>       _condition_stack;   ///< Стек условий для работы if-else и тернарного оператора
        size_t                  _modules_quantity;  ///< Кол-во импортированных модулей
        SCI_Name                _return_value;      ///< Используется для возврата значений из функций
        std::atomic_bool        _stop_signal;       ///< Сигнал прерывания выполнения
        std::atomic_bool        _pause_signal;       ///< Сигнал приостановки выполнения
        std::mutex              _pause_m;
        std::condition_variable _pause_cv;

    public:
        /*!
         * \brief Конструктор исполняющей машины ScriptC
         * \param m          Обработчик сообщений
         * \param mm         Ссылка на управляющего модулями
         * \param stack_max_size Максимальный размер стека интерпретатора
         */
        ScriptC_Interpreter(AReporter & m, ModulesManagement & mm, size_t stack_max_size=STACK_MAX_SIZE);

        /*!
         * \brief Добавляет глобальный контекст (аналог оператора 'import')
         * \param ns_name   Наименование пространства имён
         * \param context   Структура глобального пространства имён
         */
        void importNamespace(std::u16string ns_name, const SCI_Namespace_t & content);

        /*!
         * \brief Перезапуск машины для последующего выполнения следующего скрипта
         *
         * Выполняет очистку состояния с целью последубщего запуска скрипта
         */
        void reset();

        /*!
         * \brief Выполнение блока АСД
         * \param ast   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition catchAst(const AstNode & ast);

        /*!
         * \brief Выполнение блока АСД
         * \param ast   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition runAst(const AstNode & ast);

        /*!
         * \brief Установка сигнала о прерывании выполнения
         */
        void stop() { _stop_signal.store(true); }

        bool stop_signal() const { return _stop_signal.load(); }

        /*!
         * \brief Установка сигнала о прерывании выполнения
         */
        void pause()
        {
            {
                std::lock_guard guard(_pause_m);
                _pause_signal.store(!_pause_signal.load());
            }
            _pause_cv.notify_all();
        }

        bool pause_signal() const { return _stop_signal.load(); }

        SCI_RunnerCondition callInnerFunction(const AstNode &node, size_t stack_count, SCI_Name *parent=nullptr, bool is_construct=false);

        size_t stack_size() const { return _stack.size(); }

        AReporter & reporter() { return _m; }

    protected:
        /*!
         * \brief Выполнение фрагмента АСД
         * \param ast   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition runInner(const AstNode & ast);

        /*!
         * \brief Загрузка модуля (с проверкой семантики) в качестве пользовательского типа
         * \param op   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition loadModuleAsType(const AstNode & op);

        /*!
         * \brief Обработка оператора using
         * \param op   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performUsingStatement(const AstNode & op);

        /*!
         * \brief Исполнение условия true для тернарного и условного операторов
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performTrueClause(const AstNode & op);

        /*!
         * \brief Исполнение условия false для тернарного и условного операторов
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performFalseClause(const AstNode & op);

        /*!
         * \brief Исполнение бинарных логических операций "и/или"
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performLogicalOperation(const AstNode & op);

        /*!
         * \brief Исполнение бинарных операций логических сравнений: ==, !=, >, >=, <, <=
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performCompareOperation(const AstNode & op);

        /*!
         * \brief Исполнение бинарных арифметических операций: +, -, *, /, %, ^
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performArithmeticOperation(const AstNode & op);

        /*!
         * \brief Исполнение унарных операций префиксного +/-
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performUnaryOperation(const AstNode & op);

        /*!
         * \brief Исполнение унарной операции логического отрицания
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performUnaryNotOperation(const AstNode & op);

        /*!
         * \brief Исполнение операции размещения на стеке значения константы
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition pushConst(const AstNode & op);

        /*!
         * \brief Исполнение операции размещения не стеке значения переменной
         * \param op        Узел АСД, который необходимо исполнить
         * \param parent    Ссылка на владеющий кортеж, если такой есть
         * \param parent_access Наследуемый уровень доступа
         * \param parent_iteration_qualificator Наследуемый квалификатор итерации
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition pushId(const AstNode & op,
                                   SCI_Name * p_parent_name = nullptr,
                                   SemanticNameAccess parent_access = SemanticNameAccess::FullAccess,
                                   AstOperation parent_iteration_qualificator = AstOperation::None);

        /*!
         * \brief Исполнение операции вызова функции (а также процедуры)
         * \param op        Узел АСД, который необходимо исполнить
         * \param is_procedure_call Признак вызова процедуры (влияет на проверки)
         * \param parent    Ссылка на владеющий кортеж, если такой есть
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition callFunction(const AstNode & op, bool is_procedure_call, SCI_Name * p_parent_name = nullptr);

        /*!
         * \brief Исполнение оператора объявления переменной
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performDeclaration(const AstNode & op);

        /*!
         * \brief Исполнение оператора объявления функции
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performFunctionDeclaration(const AstNode & op);

        /*!
         * \brief Исполнение оператора присваивания с учётом самоприсваивания: =, +=, -=, *=, /=, %=
         * \param op    Узел АСД, который необходимо исполнить
         * \param parent    Ссылка на владеющий кортеж, если такой есть
         * \param parent_access Наследуемый уровень доступа
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performAssignStatement(const AstNode & op,
                                                   const AstNode & var,
                                                   SCI_Name * p_parent_name = nullptr,
                                                   SemanticNameAccess parent_access = SemanticNameAccess::FullAccess);

        /*!
         * \brief Исполнение управляющих операторов break и continue
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performBreakContinue(const AstNode & op);

        /*!
         * \brief Исполнение оператора return
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performReturn(const AstNode & op);

        /*!
         * \brief Исполнение оператора цикла while
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performCycleWhile(const AstNode & op);

        /*!
         * \brief Исполнение оператора цикла do-while
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performCycleDoWhile(const AstNode & op);

        /*!
         * \brief Исполнение оператора цикла for
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performCycleFor(const AstNode & op);

        /*!
         * \brief Загрузка структуры данных массива
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition pushDataStructure_Array(const AstNode & op);

        /*!
         * \brief Загрузка структуры данных кортежа
         * \param op    Узел АСД, который необходимо исполнить
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition pushDataStructure_Tuple(const AstNode & op);

        /*!
         * \brief Выборочное присваивание элементов первого уровня одного кортежа другому
         *
         * \todo Нужно рекурсивно проверять все элементы кортежей
         * или ввести в кортеж ссылку на тип и проверять только тип (с учётом наследования)
         *
         * \param op        Узел АСД, который необходимо исполнить
         * \param from_name Кортеж, элементы которог нужно взять для присвоения
         * \param toType    Тип резудьтирующего кортежа
         * \param toTuple   Результирующий кортеж
         * \return Состояние обработки фрагмента кода
         */
        SCI_RunnerCondition performTupleSelectiveAssigment(const AstNode & op,
                                                           const SCI_Name & from_name,
                                                           const SCI_Name &toType,
                                                           SCI_Tuple & toTuple);

        /*!
         * \brief Выполняет поиск имени, учитывая локальный контекст, нахождение в теле класса (типа) и глобальный контекст
         * \param name_str Имя имени
         * \return Ссылка на имя в стеке
         */
        SCI_Name & findName(std::u16string name_str, SCI_Name ** p_parent = nullptr);

        /*!
         * \brief Конвертирование произвольного значения в строку без проверок типов
         * \param val Исходное значение
         * \return Значение строковое
         */
        SCI_Scalar toString(const SCI_Name & val) const;

        /*!
         * \brief Конвертирование скалярного значения в строку без проверок типов
         * \param val Исходное скалярное значение
         * \return Значение строковое
         */
        SCI_Scalar toString(const SCI_Scalar & val) const;

        /*!
         * \brief Конвертирование скалярного значения в вещественное число без проверок типов
         *
         * При некорректном типе заданного значения будет выброшено исключение.
         *
         * \param val Исходное скалярное значение
         * \return Значение вещественное
         */
        SCI_Scalar toFloat(const SCI_Scalar & val) const;

        /*!
         * \brief Формирование скалярного значения по заданному в узле типу
         * \param op Узел с типом значения
         * \return Тип
         */
        SCI_Scalar getScalar(const AstNode & op) const;

        /*!
         * \brief Формирование скалярного значения по заданному в узле типу
         * \param type Тип значения
         * \return Тип
         */
        SCI_Scalar getScalar(SemanticNameType type) const;

        /*!
         * \brief Проверка преобразования типов
         *
         * \param from Исходный тип
         * \param to   Тип результата
         * \return true - преобразование корректно, false - некорректно
         */
        bool checkTypeConversion(SemanticNameType from, SemanticNameType to) const;

        /*!
         * \brief Выполнение проверки типов операндов арифметической бинарной операции
         *
         * Помимо проверки типов метод выводит сообщения об ошибке.
         *
         * \param op    Операция
         * \param type1 Тип первого операнда
         * \param type2 Тип второго операнда
         * \return true - типы корректны, false - некорректны
         */
        bool checkArithmeticOperation(AstNode op, SemanticNameType type1, SemanticNameType type2) const;

        /*!
         * \brief Исполнение арифметической бинарной операции
         * \param op    Операция
         * \param op1   Первый операнд
         * \param op2   Второй операнд
         * \return Результирующее значение
         */
        SCI_Scalar calculateArithmeticOperation(AstNode op, SCI_Scalar op1, SCI_Scalar op2) const;

        /*!
         * \brief Формирование описания имени для интерпретатора по информации об объекте из модуля
         * \param module            Ссылка на модуль
         * \param i_semantic_name   Индекс объекта в модуле
         * \return Описание имени
         */
        SCI_Name makeNameFromModule(const ModuleImportResults & module, size_t i_semantic_name) const;

        /*!
         * \brief Получение указателя на элемент массива по индексам в АСД
         * \param name_op    Нода имени массива
         * \param array_op   Нода индекса элемента
         * \return Укзатель на элемент массива
         */
        SCI_Name * getArrayElement(const AstNode & name_op, SCI_Name &name, const AstNode & array_op);
    };

    /*!
     * \brief Преобразование кода состояние обработки фрагмента кода в строку
     * \param condition Код состояние обработки фрагмента кода
     * \return Строка с наименованием кода состояние обработки фрагмента кода
     */
    std::u16string getSCI_RunnerConditionName(SCI_RunnerCondition condition);

    /*!
     * \brief RAII-класс для добавления элемента в стеке пространтсва имён и контроля над ним
     *
     * RAII-класс для добавления элемента в стеке пространтсва имён и контроля над ним в условиях
     * рекурсивного выполнения АСД.
     */
    class SCI_NamespaceAddScope
    {
        ScriptC_Interpreter & _interpreter;

    public:
        /*!
         * \brief Конструктор класса формирования области видимости локальных имён
         * \param name_space Структура зоны видимости, которую нужно контролировать
         */
        SCI_NamespaceAddScope(ScriptC_Interpreter & interpreter);

        ~SCI_NamespaceAddScope();
    };

    /*!
     * \brief RAII-класс для работы с фреймом вызовов функций
     */
    class SCI_FrameScope
    {
        ScriptC_Interpreter & _interpreter;

    public:
        /*!
         * \brief Конструктор класса формирования области видимости локальных имён
         * \param name_space Структура зоны видимости, которую нужно контролировать
         */
        SCI_FrameScope(ScriptC_Interpreter & interpreter, size_t local_boundary, SCI_Name * encapsulation);

        ~SCI_FrameScope();
    };

}

#endif // SIMODO_DSL_ScriptC_Interpreter
