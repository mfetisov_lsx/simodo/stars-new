/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

Проект miniDB: https://bmstu.codes/lsx/elective/minidb
*/

#ifndef SIMODO_DSL_Exception_h
#define SIMODO_DSL_Exception_h

/*! \file Exception.h
    \brief Обработка базовых исключений ПОЯ

    Заголовочный файл, описывающий класс исключений, используемых в библиотеке simodo::dsl.
*/

#include <exception>
#include <string>

namespace simodo::dsl
{
    /*!
     * \brief Класс исключения
     *
     * Класс исключения предоставляет возможность работать с исключениями в библиотеке miniDB.
     *
     * Неизменяемый (immutable) класс.
     */
    class Exception : public std::exception
    {
        std::string _where; ///< Наименования класса и метода (или пространства имён и функции), где было брошено исключение
        std::string _what;  ///< Описание исключения

    public:
        /*!
         * \brief           Конструктор исключения библиотеки miniDB
         * \param where     Задаёт наименования класса и метода (или пространства имён и функции), где было брошено исключение
         * \param what      Описание исключения
         */
        Exception(std::string where, std::string what)
            : std::exception()
            , _where(where)
            , _what(what)
        {}

        virtual ~Exception() override {}

        /*!
         * \brief       Возвращает строку с наименованиями класса и метода (или пространства имён и функции), где было брошено исключение
         * \return      Строка с наименованиями класса и метода (или пространства имён и функции), где было брошено исключение
         */
        const std::string & where() const noexcept { return _where; }

        /*!
         * \brief       Возвращает описание исключения
         * \return      Строка с описанием исключения
         */
        virtual const char* what() const noexcept override { return _what.c_str(); }
    };

}

#endif // SIMODO_DSL_Exception_h
