#include <vector>
#include <iostream>

struct container_algebra
{
    template<typename S1, typename S2, typename S3, typename Op>
    void for_each3(S1 & s1, S2 & s2, S3 & s3, Op op)
    {
        using std::begin;
        using std::end;

        auto first1 = begin(s1);
        auto last1  = end(s1);
        auto first2 = begin(s2);
        auto first3 = begin(s3);

        while(first1 != last1)
            op(*first1++, *first2++, *first3++);
    }

    template<typename S1, typename S2, typename S3, typename S4, typename S5, typename S6, typename Op>
    void for_each6(S1 & s1, S2 & s2, S3 & s3, S4 & s4, S5 & s5, S6 & s6, Op op)
    {
        using std::begin;
        using std::end;

        auto first1 = begin(s1);
        auto last1  = end(s1);
        auto first2 = begin(s2);
        auto first3 = begin(s3);
        auto first4 = begin(s4);
        auto first5 = begin(s5);
        auto first6 = begin(s6);

        while(first1 != last1)
            op(*first1++, *first2++, *first3++, *first4++, *first5++, *first6++);
    }
};

struct default_operations
{
    template<typename F1 = double, typename F2 = F1>
    struct scale_sum2
    {
        const F1 alpha1;
        const F2 alpha2;

        scale_sum2(F1 a1, F2 a2)
            : alpha1(a1)
            , alpha2(a2)
        {}

        template<typename T0, typename T1, typename T2>
        void operator() (T0 & t0, const T1 & t1, const T2 & t2) const
        {
            t0 = alpha1*t1 + alpha2*t2;
        }
    };

    template<typename F1 = double, typename F2 = F1, typename F3 = F1, typename F4 = F1, typename F5 = F1>
    struct scale_sum5
    {
        const F1 alpha1;
        const F2 alpha2;
        const F2 alpha3;
        const F2 alpha4;
        const F2 alpha5;

        scale_sum5(F1 a1, F2 a2, F3 a3, F4 a4, F5 a5)
            : alpha1(a1)
            , alpha2(a2)
            , alpha3(a3)
            , alpha4(a4)
            , alpha5(a5)
        {}

        template<typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
        void operator() (T0 & t0, const T1 & t1, const T2 & t2, const T3 & t3, const T4 & t4, const T5 & t5) const
        {
            t0 = alpha1*t1 + alpha2*t2 + alpha3*t3 + alpha4*t4 + alpha5*t5;
        }
    };
};

template<typename state_type>
void resize(const state_type & in, state_type & out)
{
    using std::size;
    out.resize(size(in));
}

template<typename state_type,
         typename value_type = double,
         typename time_type  = value_type,
         typename algebra    = container_algebra,
         typename operations = default_operations>
class runge_kutta4
{
    state_type x_tmp, k1, k2, k3, k4;
    algebra    _algebra;

    void adjust_size(const state_type x)
    {
        resize(x, x_tmp);
        resize(x, k1);
        resize(x, k2);
        resize(x, k3);
        resize(x, k4);
    }

public:
    template<typename System>
    void do_step(System & system, state_type & x, time_type t, time_type dt)
    {
        adjust_size(x);
        const value_type one = 1;
        const time_type  dt2 = dt/2, dt3 = dt/3, dt6 = dt/6;

        typedef typename operations::template scale_sum2<value_type, time_type> scale_sum2;
        typedef typename operations::template scale_sum5<value_type, time_type, time_type, time_type, time_type> scale_sum5;

        system(x, k1, t);
        _algebra.for_each3(x_tmp, x, k1, scale_sum2(one,dt2));

        system(x_tmp, k2, t+dt2);
        _algebra.for_each3(x_tmp, x, k2, scale_sum2(one,dt2));

        system(x_tmp, k3, t+dt2);
        _algebra.for_each3(x_tmp, x, k3, scale_sum2(one,dt));

        system(x_tmp, k4, t+dt);
        _algebra.for_each6(x, x, k1, k2, k3, k4, scale_sum5(one,dt6,dt3,dt3,dt6));
    }
};

//typedef std::vector<double> state_type;
//typedef runge_kutta4<state_type> rk4_type;

//struct lorenz
//{
//    const double sigma, R, b;

//    lorenz(const double sigma, const double R, const double b)
//        : sigma(sigma)
//        , R(R)
//        , b(b)
//    {}

//    void operator() (const state_type & x, state_type & dxdt, double t)
//    {
//        dxdt[0] = sigma * (x[1] - x[0]);
//        dxdt[1] = R*x[0] - x[1] - x[0]*x[2];
//        dxdt[2] = -b*x[2] + x[0]*x[1];
//    }
//};

//using namespace std;

//int main()
//{
//    const int steps = 2500;
//    const double dt = 0.01;

//    rk4_type stepper;
//    lorenz   system(10.0, 28.0, 8.0/3.0);

//    state_type x { 10.0 , 1.0 , 1.0 };

//    cout << 0*dt << '\t' << x[0] << '\t' << x[1] << '\t' << x[2] << endl;

//    for(size_t n=1; n < steps; ++n)
//    {
//        stepper.do_step(system, x, n*dt, dt);

//        cout << n*dt << '\t' << x[0] << '\t' << x[1] << '\t' << x[2] << endl;
//    }
//}
