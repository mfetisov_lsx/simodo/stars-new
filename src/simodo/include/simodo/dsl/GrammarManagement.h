/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_GrammarManagement
#define SIMODO_DSL_GrammarManagement

/*! \file GrammarManagement.h
    \brief Управление грамматиками языков

    Заголовочный файл, описывающий класс управления грамматиками языков.
*/

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/Grammar.h"

#include <string>
#include <vector>
#include <map>

namespace simodo::dsl
{
    /*!
     * \brief Класс управления грамматиками
     *
     * Предполагается, что при работе в среде SIMODO может быть много текстов (файлов) на разных
     * предметно-ориентированных языках. Грамматики этих языков загружаются один раз.
     * Данный класс осуществляет управление набором грамматик.
     */
    class GrammarManagement
    {
        AReporter &                     _m;                 ///< Обработчик сообщений
        std::vector<std::string>        _paths_to_grammars; ///< Перечень путей к файлам грамматик
        std::map<std::string,Grammar>   _grammar_set;       ///< Ассоциативный контейнер с загруженными грамматиками

    public:
        GrammarManagement() = delete;   ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор управленца грамматиками
         * \param m                 Интерфейсная ссылка на объект обеспечения вывода информации в вызываемую программу
         * \param path_to_grammar   Путь к файлам грамматик
         */
        explicit GrammarManagement(AReporter & m, const std::string & path_to_grammar)
            : _m(m)
        {
            _paths_to_grammars.push_back(path_to_grammar);
        }

        /*!
         * \brief Конструктор управленца грамматиками
         * \param m                 Интерфейсная ссылка на объект обеспечения вывода информации в вызываемую программу
         * \param paths_to_grammars Перечень путей к файлам грамматик
         */
        explicit GrammarManagement(AReporter & m, const std::vector<std::string> & paths_to_grammars)
            : _m(m), _paths_to_grammars(paths_to_grammars)
        {}

        /*!
         * \brief Загрузка грамматики
         *
         * Если грамматика уже загружена, повторная загрузка не производится.
         *
         * \param need_force_reload Признак необходимости выполнить полное перестроение грамматики
         * \param grammar_name      Наименование грамматики
         * \param method            Метод построения таблицы разбора
         * \return true, если грамматика успешно загружена или была загружена ранее
        */
        bool    loadGrammar(bool need_force_reload, const std::string & grammar_name, TableBuildMethod method=TableBuildMethod::none);

        /*!
         * \brief Поиск грамматики в коллекции без загрузки
         * \param name Наименование грамматики
         * \return true, если грамматика присутствует в коллекции, иначе - false
         */
        bool    findGrammar(const std::string & name) const;

        /*!
         * \brief Получение ссылки на грамматику для работы с ней
         *
         * Грамматика должна быть загружена ранее. Иначе, возвращается ссылка на пустую структуру грамматики.
         *
         * \param name  Наименование грамматики
         * \return Константная ссылка на грамматику
         */
        const Grammar & getGrammar(const std::string & name) const;

        const std::map<std::string,Grammar> & grammar_set() const { return _grammar_set; }

        bool replaceGrammarPath(const std::vector<std::string> & paths_to_grammars);

    };
}

#endif // SIMODO_DSL_GrammarManagement
