/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Remote_NS_Chart
#define SIMODO_DSL_Remote_NS_Chart

/*! \file Remote_NS_Chart.h
    \brief Пространство имён для интерпретатора
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

#include <map>
#include <optional>

namespace simodo::dsl
{
    class Remote_NS_Chart : public IScriptC_Namespace
    {
        AReporter & _listener;

    public:
        Remote_NS_Chart() = delete;
        Remote_NS_Chart(AReporter & listener);
        virtual ~Remote_NS_Chart();

        virtual SCI_Namespace_t getNamespace() override;

        AReporter & listener() const { return _listener; }
    };

}

#endif // SIMODO_DSL_Remote_NS_Chart
