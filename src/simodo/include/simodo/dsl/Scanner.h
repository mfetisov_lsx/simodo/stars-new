/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Scanner
#define SIMODO_DSL_Scanner

/*! \file Scanner.h
    \brief Сканер входного потока

    Заголовочный файл, описывающий классы и структуры обработки символов.
*/

#include <string>
#include <istream>

#include "simodo/dsl/Token.h"
#include "simodo/dsl/Stream.h"

namespace simodo::dsl
{
    /*!
     * \brief Поддержка скарирования текста
     *
     * Содержит характеристики местоположения и методы для сравнения текущего текста с лексическими
     * примитивами при работе Tokenizer.
     */
     
    class Scanner
    {
        TokenLocation   _loc;                    ///< Информация о текущем состоянии обработки входного потока
        IStream &       _input;                  ///< Входной поток
        std::u16string  _buffer;                 ///< Текущий буфер разбора примитивов
        uint8_t         _tab_size;               ///< Размер табуляции
        uint32_t        _line;                   ///< Номер строки (с единицы)
        uint32_t        _column;                 ///< Позиция в строке (с единицы)
        uint32_t        _column_tabulated;       ///< Позиция в строке с учётом символов табуляции (с единицы)
        bool            _is_end_of_file_reached; ///< Признак достижения спец. символа конца файла
        uint32_t        _pos;                    ///< Текущая позиция при чтении потока (счётчик считанных символов)

    protected:
        /*!
         * \brief Перемещает один код теста из входного потока в буфер
         * \return true, если код был перемещён и false, если достигнут конец потока
         */
        bool    putCharToBuffer();

    public:
        Scanner() = delete; ///< Без инициализации параметров не имеет смысла

        /*!
         * \brief Конструктор сканера входного потока
         *
         * \param file_name     Имя файла
         * \param input_stream  Входной поток
         * \param tab_size      Размер табуляции
         * \param context_index Номер контекста начала разбора (используется при подсветке синтаксиса)
         */
        Scanner(const std::u16string &file_name, IStream & input_stream, uint8_t tab_size,
                context_index_t context_index=NO_TOKEN_CONTEXT_INDEX);

        /*!
         * \brief eof
         * \return true, если достигнут конец потока, иначе - false
         */
        bool eof() const { return (_buffer.empty() && (_is_end_of_file_reached || _input.eof())); }

        /*!
         * \brief setEOF
         */
        void    setEOF() { _is_end_of_file_reached = true; }

        /*!
         * \brief Сдвиг входного потока на заданное количество текстовых символов
         * \param length   Величина сдвига
         * \return true, если сдвиг произошёл и false, если достигнут конец потока
         *
         * \attention   Величина сдвига не должна превышать количество символов во внутреннем буфере.
         *              Т.е. ожидается, что сдвиг будет выполняться с проанализованным текстом.
         */
        bool    shift(size_t length);

        /*!
         * \brief Возвращает текущее состояние обработки токена
         * \return Текущее состояние обработки токена после последнего вызова shift
         */
        const TokenLocation & getLocation() const { return _loc; }

        /*!
         * \brief Запоминает текущее состояние обработки входного потока как начало токена
         * \param context_index Текущий контекст обработки маркированного токена
         */
        void    fixLocation(context_index_t context_index=NO_TOKEN_CONTEXT_INDEX);

        /*!
         * \brief Сравенние кода текстового символа с первым символом текущего состояния потока
         * \param ch   Код текстового символа, которую нужно сравнить
         * \return true, если код текстового символа совпадает с текущим содержимым потока
         */
        bool    startsWith(char16_t ch);

        /*!
         * \brief Сравенние строки с началом текущего состояния потока
         * \param str   Строка, которую нужно сравнить
         * \return true, если заданная строка совпадает с текущим содержимым потока
         */
        bool    startsWith(const std::u16string & str);

        /*!
         * \brief Проверка на содержание текущего символа входного потока в заданной строке
         * \param str   Строка символов, которые нужно проверить
         * \return true, если текущий символ содержится в заданной строке
         */
        bool    startsWithAnyOf(const std::u16string & str);

        /*!
         * \brief Получение текущего символа входного потока
         * \return Текущий символ входного потока
         */
        char16_t getFirstChar();


        char16_t getChar(size_t pos = 0);
    };
}

#endif // SIMODO_DSL_Scanner
