/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_AstBuilderHelper
#define SIMODO_DSL_AstBuilderHelper

/*! \file AstBuilderHelper.h
    \brief Вспомогательный класс построителя абстрактного синтаксического дерева

    Заголовочный файл, объявляющий интерфейс построителя абстрактного синтаксического дерева.
*/

#include <string>
#include <map>
#include <set>

#include "simodo/dsl/AstNode.h"

namespace simodo::dsl
{
    class AstStream
    {
        friend class AstBuilderHelper;

        AstNode   _ast;
        AstNode * _flow = nullptr;    ///< Указатель на текущий узел, для которого выполняется построение ветки АСД

        std::string addNode(AstOperation op, Token op_symbol, Token bound);
        std::string addNode_StepInto(AstOperation op, Token op_symbol, Token bound);
        std::string addNode_Parent_StepInto(AstOperation op, Token op_symbol, Token bound);
        std::string addNode_ShiftInto(AstOperation op, Token op_symbol, Token bound);
        std::string addNode_Branch(AstOperation op, Token op_symbol, Token bound);
        std::string addNode_Branch_StepInto(AstOperation op, Token op_symbol, Token bound);
        std::string goParent();

        std::string addNode_Assignment(AstOperation op, Token op_symbol, Token bound);
        std::string resetOperation_ProcedureCalling();

    public:
        const AstNode & ast() const { return _ast; }
    };

    [[maybe_unused]] static uint16_t DEFAULT_STREAM_NO = 55;
    [[maybe_unused]] static uint16_t EQUATION_STREAM_NO = 60;
    [[maybe_unused]] static uint16_t DIFF_STREAM_NO = 70;

    enum class AstBuilderMode
    {
        None = 0,
        Diff,
    };

    /*!
     * \brief Вспомогательный класс построителя абстрактного синтаксического дерева
     */
    class AstBuilderHelper
    {
        std::map<uint16_t,AstStream> _ast_stream_set;
        AstBuilderMode               _mode = AstBuilderMode::None;
        std::u16string               _prefix = u"";

        std::set<std::u16string>     _names;

    public:
        std::string setMode(AstBuilderMode mode);
        std::string setPrefix(std::u16string prefix);
        std::string addNode(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string addNode_StepInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string addNode_Parent_StepInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string addNode_ShiftInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string addNode_Branch(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string addNode_Branch_StepInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string goParent(uint16_t stream_no);

        std::string addNode_Assignment(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no);
        std::string resetOperation_ProcedureCalling(uint16_t stream_no);

        void drain(AstNode & ast);
        AstBuilderMode mode() const { return _mode; }

        const AstStream & getAstStream(uint16_t no);
        void removeStream(uint16_t no);

    protected:
        AstStream & getStream(uint16_t stream_no);

        std::string checkMode(AstOperation & op, Token & op_symbol, Token & bound, uint16_t & stream_no);
        std::string checkPushId(const AstNode & ast_node);
        std::string addName(const AstNode &name_node);
    };

}

#endif // SIMODO_DSL_AstBuilderHelper
