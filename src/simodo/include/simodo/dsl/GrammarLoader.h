/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_GrammarLoader
#define SIMODO_DSL_GrammarLoader

/*! \file GrammarLoader.h
    \brief Загрузчик грамматики
*/

#include "simodo/dsl/AReporter.h"
#include "simodo/dsl/Grammar.h"

#include <set>
#include <vector>

namespace simodo::dsl
{
    const   std::string DUMP_FILE_EXTENSION = ".dump";  ///< Расширение файлов дампа грамматики

    /*!
     * \brief Загрузчик параметров грамматики
     *
     * Реализует механизм загрузки параметров грамматики из ранее сохраненного дампа или
     * полного разбора и построения из файла описания грамматики.
     */
    class GrammarLoader
    {
        AReporter &                      _m;            ///< Обработчик сообщений
        const std::vector<std::string> & _paths;        ///< Перечень путей к файлам грамматики
        std::string                      _grammar_name; ///< Наименование грамматики

    public:
        GrammarLoader() = delete;        ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Инициализирующий конструктор загрузчика параметров грамматики
         * \param m             Обработчик сообщений
         * \param paths         Перечень путей к файлам грамматики
         * \param grammar_name  Наименование грамматики
         */
        GrammarLoader(AReporter & m, const std::vector<std::string> & paths, const std::string & grammar_name)
            : _m(m), _paths(paths), _grammar_name(grammar_name)
        {}

        /*!
         * \brief Выполняет загрузку параметров указанной грамматики
         *
         * Выполняет загрузку параметров указанной грамматики из дампа или использует классы парсинга и
         * построения параметров грамматики, если из дампа загрузить не удалось. После успешного завершения
         * парсинга и построения грамматики выполняет сохранение дампа в том же каталоге, где находится
         * файл грамматики. Приэтом добавляет к имени файла грамматики расширение ".dump".
         *
         * \param need_force_reload  Признак необходимости выполнить разбор и построение параметров
         *                           грамматики, не используя дамп
         * \param g                  Ссылка на структуру параметров грамматики
         * \param method             Метод построения таблицы разбора
         * \param strict_rule_consistency Признак строгой проверки согласованности правил грамматики
         * \return          Признак успешной загрузки параметров грамматики
         */
        bool    load(bool need_force_reload, Grammar & g, 
                     TableBuildMethod method=TableBuildMethod::none, bool strict_rule_consistency=true);

    protected:
        /*!
         * \brief Выполняет загрузку правил грамматики
         * \param path_to_grammar Путь к файлам грамматики
         * \param rules           Набор правил грамматики, куда будет выполняться загрузка
         * \param main_rule       Ссылка для размещения главного парвила грамматики, определённого в заданном файле
         * \param imported_files  Множество уже загруженных файлов (для предотвращения зацикливания)
         * \param declarations    Ссылка на АСД для добавления в него объявлений и определений
         * \param lex             Заполняемая загрузчиком структура лексических параметров загрузчика
         * \return      Признак успешной загрузки правил грамматики
         */
        bool    loadRules(const std::string &path_to_grammar, std::vector<GrammarRuleTokens> & rules, Token & main_rule, 
                          std::set<std::string> imported_files,
                          std::map<std::u16string,AstNode> & handlers, LexicalParameters & lex);

        /*!
         * \brief Сохранение параметров грамматики в дамп
         * \param file_name Имя файла дампа грамматики
         * \param g         Ссылка на структуру параметров грамматики
         * \return      Признак успешного сохранения параметров грамматики в заданный файла дампа
         */
        bool    storeDumpFile(const std::string &file_name, const Grammar & g);

        /*!
         * \brief Загрузка параметров грамматики из дампа
         *
         * \param file_name Имя файла дампа грамматики
         * \param g         Ссылка на структуру параметров грамматики
         * \param method    Метод построения таблицы разбора
         * \return      Признак успешной загрузки параметров грамматики из заданного файла дампа
         */
        bool    loadDumpFile(const std::string &file_name, Grammar & g, TableBuildMethod method);
    };

}

#endif // SIMODO_DSL_GrammarLoader
