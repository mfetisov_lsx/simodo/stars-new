/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_NS_LexicalParameters
#define SIMODO_DSL_ScriptC_NS_LexicalParameters

/*! \file ScriptC_NS_LexicalParameters.h
    \brief Пространство имён для интерпретатора для загрузки параметров лексики
*/

#include "simodo/dsl/LexicalParameters.h"
#include "simodo/dsl/ScriptC_Interpreter.h"

namespace simodo::dsl
{
    class ScriptC_NS_LexicalParameters : public IScriptC_Namespace
    {
    public:
        ScriptC_NS_LexicalParameters() = delete;

        ScriptC_NS_LexicalParameters(LexicalParameters & lex);
        virtual ~ScriptC_NS_LexicalParameters();

        virtual SCI_Namespace_t getNamespace() override;
    };

}

#endif // SIMODO_DSL_ScriptC_NS_LexicalParameters
