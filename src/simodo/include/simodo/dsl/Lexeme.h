/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Lexeme
#define SIMODO_DSL_Lexeme

/*! \file Lexeme.h
    \brief Лексема абстрактного языка

    Заголовочный файл, описывающий классы работы с лексемой абстрактного языка.
*/

#include <string>

namespace simodo::dsl
{
    //! Тип лексемы
    enum class LexemeType
    {
        Compound = 0,   ///< Нетерминальный символ ("нетерминал")
        Empty,          ///< Лексема неопределена (используется для обозначения пустого потока, конца файла)
        Punctuation,    ///< Разделитель, пунктуация или ключевое слово (возможно уточнение TokenQualification::Keyword)
        Id,             ///< Идентификатор (возможно уточнение TokenQualification::NationalCharacterMix)
        Annotation,     ///< Аннотация, строка символов
        Number,         ///< Число (возможно уточнение TokenQualification::Integer, TokenQualification::RealNubmer, TokenQualification::NotANumber)
        Comment,        ///< Комментарий
        NewLine,        ///< Получена новая строка
        Error           ///< Ошибка лексики (возможно уточнение TokenQualification::UnknownCharacterSet, TokenQualification::NationalCharacterMix, TokenQualification::NationalCharacterUse)
    };

    /*!
     * \brief Лексема языка
     *
     * Класс описывает абстрактную единицу языка - лексему. 
     * В отличие от токена (см. lsx::dsl::Token) не несёт информации о положении в тексте,
     * контексте и уточненом значении
     *
     * Значение лексемы, в отличие от Token, хранится без служебных элементов
     * (например, для аннотации хранится только строка значения, без кавычек)
     * 
     * Неизменяемый (immutable) класс (не изменяет своё состояние).
     */

    class Lexeme
    {
        friend class AstBuilderHelper;

        std::u16string symbol;      ///< Строка лексемы
        LexemeType     symbol_type; ///< Тип лексемы

    protected:

    public:
        /*!
         * \brief Конструктор лексемы
         * \param symbol        Текст лексемы
         * \param symbol_type   Тип лексемы
         */
        Lexeme(const char16_t * symbol, LexemeType symbol_type)
            : symbol(symbol)
            , symbol_type(symbol_type)
        {}
        
        /*!
         * \brief Конструктор лексемы
         * \param symbol        Текст лексемы
         * \param symbol_type   Тип лексемы
         */
        Lexeme(const std::u16string & symbol, LexemeType symbol_type)
            : symbol(symbol)
            , symbol_type(symbol_type)
        {}
        
        /*!
         * \brief Оператор сравенния двух лексем
         * \param lexeme   Лексема, которую нужно сравнить с нашей
         *
         *      Лексемы считаются совпадающими, если у них совпадают типы и содержимое.
         */
        bool operator == (const Lexeme & lexeme) const { return (symbol_type == lexeme.getType() && symbol == lexeme.getLexeme()); }

        /*!
         * \brief Оператор проверки лексем на неравенство
         * \param lexeme   Лексема, которую нужно сравнить с нашей
         *
         *      Лексемы считаются совпадающими, если у них совпадают типы и содержимое.
         */
        bool operator != (const Lexeme & lexeme) const { return !(lexeme == *this); }

        /*!
         * \brief Оператор сравенния двух лексем на <
         * \param lexeme   Лексема, которую нужно сравнить с нашей
         */
        bool operator < (const Lexeme & lexeme) const { return (symbol_type < lexeme.getType() ||
                                                                (symbol_type == lexeme.getType() && symbol < lexeme.getLexeme())); }

        /*!
         * \brief Получение содержимого лексемы
         * \return   Константная ссылка на строку значения лексемы
         */
        const std::u16string & getLexeme(void) const { return symbol; }

        /*!
         * \brief Получение типа лексемы
         * \return   Тип лексемы
         */
        LexemeType          getType(void)   const { return symbol_type; }
    };

    /*!
     * \brief Функция получения строкового наименования типа лексемы
     * \param type  Тип лексемы (тип LexemeType)
     * \return      Ссылка на наименование типа лексемы
     */
    std::u16string getLexemeTypeName(LexemeType type) ;

    /*!
     * \brief Возвращает мнемоническое обозначение лексемы в зависимости от её типа
     * \param lex Лексема
     * \return Мнемоническое обозначение лексемы в зависимости от её типа
     */
    std::u16string getLexemeMnemonic(const Lexeme & lex);
}

#endif // SIMODO_DSL_Lexeme
