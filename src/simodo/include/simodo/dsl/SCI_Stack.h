#ifndef SIMODO_DSL_SCI_STACK_H
#define SIMODO_DSL_SCI_STACK_H

#include <vector>

#include "simodo/dsl/SemanticBase.h"

namespace simodo::dsl
{
    class SCI_Stack
    {
        std::vector<SCI_Name>   _stack;
        size_t                  _stack_max_size;

    public:
        SCI_Stack() = delete;

        SCI_Stack(size_t stack_max_size);

        void pop(int count = 1);

        const SCI_Name & push(const SCI_Scalar & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess);
        const SCI_Name & push(const SCI_Tuple & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess);
        const SCI_Name & push(const SCI_Array & value, std::u16string name=u"", SemanticNameAccess access = SemanticNameAccess::FullAccess);
        const SCI_Name & push(const SCI_Name & value);

        size_t find(std::u16string name) const;
        size_t findLocal(std::u16string name, size_t local_boundary) const;
        size_t findGlobal(std::u16string name, size_t global_boundary) const;

        SCI_Name & at(size_t index);
        SCI_Name & top(size_t index=0);
        SCI_Name & back();

        const SCI_Name & atC(size_t index) { return at(index); }
        const SCI_Name & topC(size_t index=0) { return top(index); }
        const SCI_Name & backC() { return back(); }

        void moveTo(size_t from, size_t to);

        size_t size() const { return _stack.size(); }
        bool empty() const { return _stack.empty(); }
        size_t stack_max_size() const { return _stack_max_size; }
    };

}
#endif // SIMODO_DSL_SCI_STACK_H
