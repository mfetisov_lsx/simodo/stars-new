/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Remote_NS_Cockpit
#define SIMODO_DSL_Remote_NS_Cockpit

/*! \file Remote_NS_Cockpit.h
    \brief Пространство имён для интерпретатора
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

#include <map>
#include <optional>

namespace simodo::dsl
{
    class Remote_NS_Cockpit : public IScriptC_Namespace
    {
        AReporter & _listener;

    public:
        Remote_NS_Cockpit() = delete;
        Remote_NS_Cockpit(AReporter & listener);
        virtual ~Remote_NS_Cockpit();

        virtual SCI_Namespace_t getNamespace() override;

        AReporter & listener() const { return _listener; }
    };

}

#endif // SIMODO_DSL_Remote_NS_Cockpit
