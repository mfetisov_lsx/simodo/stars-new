/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_AstNode
#define SIMODO_DSL_AstNode

/*! \file AstNode.h
    \brief Класс элемента абстрактного синтаксического дерева (АСД) и перечисление для операции
*/

#include <string>
#include <list>

#include "simodo/dsl/Token.h"


namespace simodo::dsl
{
    /*!
     * \brief Мнемокоды операций абстрактного синтаксического дерева (АСД)
     */
    enum class AstOperation
    {
        None,                       ///< Пустая операция
        InternalPush,               ///< Внутренняя работа со сетком - положить
        InternalPop,                ///< Внутренняя работа со сетком - снять

        // Выражение (expression.fuze)

        Ternary_True,               ///< Ветка для варианта true тернарной операции
        Ternary_False,              ///< Ветка для варианта false тернарной операции
        Logical_Or,                 ///< Операция логиического "или"
        Logical_And,                ///< Операция логического "и"
        Compare_Equal,              ///< Операция сарвнения на равенство
        Compare_NotEqual,           ///< Операция сравнения на неравенство
        Compare_Less,               ///< Операция сравнения на меньше
        Compare_LessOrEqual,        ///< Операция сравнения на меньше или равно
        Compare_More,               ///< Операция сравнения на больше
        Compare_MoreOrEqual,        ///< Операция сравнения на больше или равно
        Arithmetic_Addition,        ///< Операция арифметического сложения
        Arithmetic_Subtraction,    ///< Операция арифметического вычитания
        Arithmetic_Multiplication,  ///< Операция арифметического умножения
        Arithmetic_Division,        ///< Операция арифметического деления
        Arithmetic_Modulo,          ///< Операция выделения остатка от деления
        Arithmetic_Power,           ///< Операция возведения в степень
        Unary_Plus,                 ///< Операция унарного плюса
        Unary_Minus,                ///< Операция унарного минуса
        Unary_Not,                  ///< Операция логического отрицания
        Push_Constant,              ///< Операция помещения на стек константы
        Push_Id,                    ///< Операция помещения на стек значения переменной
        Iteration_PrefixPlus,       ///< Операция префиксной положительной итерации
        Iteration_PrefixMinus,      ///< Операция префиксной отрицательной итерации
        Iteration_PosfixPlus,       ///< Операция посфиксной положительной итерации
        Iteration_PosfixMinus,      ///< Операция посфиксной отрицательной итерации
        Address_Array,              ///< Операция взятия одреса массива
        Address_Tuple,              ///< Операция взятия адреса члена кортежа
        Function_Call,              ///< Операция вызова функции

        // Операторы (scriptc0.fuze)

        Statement_Blank,            ///< Пустой оператор
        Statement_Type,             ///< Фиксация типа
        Statement_Id,               ///< Оператор объявления переменной
        Statement_Assign,           ///< Оператор присвоения значения переменной
        Statement_Assign_Addition,  ///< Оператор само-сложения со значением из правой части
        Statement_Assign_Subtraction,///< Оператор само-вычитания со значением из правой части
        Statement_Assign_Multiplication,///< Оператор само-умножения со значением из правой части
        Statement_Assign_Division,  ///< Оператор само-деления со значением из правой части
        Statement_Assign_Modulo,    ///< Оператор само-остатка со значением из правой части
        Function_Declaration,       ///< Объявления функции
        Function_Body,              ///< Фиксация начала тела функции
        Statement_Procedure_Call,   ///< Оператор вызова процедуры
        Statement_Procedure_Call_Error,///< Фиксация ошибки записи члена кортежа вместо вызова процедуры
        Statement_Block,            ///< начало блока операторов
        Statement_Break,            ///< Оператор break
        Statement_Continue,         ///< Оператор continue
        Statement_Return,           ///< Оператор return
        Statement_If_True,          ///< Ветка для варианта true условного оператора
        Statement_If_False,         ///< Ветка для варианта false условного оператора
        Statement_While,            ///< Оператор цикла while
        Statement_While_Body,       ///< Ветка тела цикла while
        Statement_DoWhile,          ///< Оператор цикла do-while
        Statement_DoWhile_Body,     ///< Ветка тела цикла do-while
        Statement_For,              ///< Оператор цикла for
        Statement_For_Body,         ///< Ветка тела цикла for

        Statement_UseAsType,        ///< Получение пользовательского типа из файла (альтернатива модуля?)
        Statement_Using,        ///< Получение пользовательского типа из файла (альтернатива модуля?)

        DataStructure_Array,        ///< Структура данных массива
        DataStructure_Tuple,        ///< Структура данных кортежа

        // Операторы (scriptc1.fuze)

        // Операторы ПОЯ

//        DSL_operation

    };

    //typedef uint16_t        AstOperation_t;

    /*!
     * \brief Узел абстрактного синтаксического дерева (АСД)
     */
    class AstNode
    {
        friend class AstBuilderHelper;

        AstNode *           _parent;            ///< Указатель на породивший узел

        AstOperation        _operation;         ///< Мнемокод операции
        Token               _operation_symbol;  ///< Токен операции
        Token               _bound;             ///< Токен границы всех операндов (включая участвующих опосредованно) и операции

        std::list<AstNode>  _branch;            ///< Поддерево данного узла

    public:
        /*!
         * \brief Пустой конструктор
         */
        AstNode()
            : _parent(nullptr)
            , _operation(AstOperation::None)
            , _operation_symbol(Token(u""))
            , _bound(Token(u""))
        {}

        /*!
         * \brief Конструктор корневого узла АСД
         * \param file_name Наименование исходного файла, для которого построено АСД
         */
        AstNode(std::u16string file_name)
            : _parent(nullptr)
            , _operation(AstOperation::None)
            , _operation_symbol(Token(file_name))
            , _bound(Token(file_name))
        {}

        /*!
         * \brief Конструктор узла АСД
         * \param parent    Указатель на породивший узел
         * \param op        Мнемокод операции
         * \param op_symbol Токен операции
         * \param bound     Токен всеохватывающей границы
         */
        AstNode(AstNode * parent, AstOperation op, Token op_symbol, Token bound)
            : _parent(parent)
            , _operation(op)
            , _operation_symbol(op_symbol)
            , _bound(bound)
        {}

        /*!
         * \brief Геттер указателя на породивший узел
         * \return Указатель на породивший узел
         */
        AstNode * getParent() { return _parent; }

        /*!
         * \brief Геттер константного указателя на породивший узел
         * \return Константный указатель на породивший узел
         */
        const AstNode * getParentC() const { return _parent; }

        /*!
         * \brief Геттер мнемокода операции
         * \return Мнемокод операции
         */
        AstOperation    getOperation() const { return _operation; }

        /*!
         * \brief Сеттер мнемокода операции
         * \param op    Мнемокод операции
         */
        void            setOperation(AstOperation op) { _operation = op; }

        /*!
         * \brief Геттер символа операции
         * \return Токен символа операции
         */
        const Token &   getSymbol() const { return _operation_symbol; }

        /*!
         * \brief Геттер границ операции
         * \return Токен границ операции
         */
        const Token &   getBound() const { return _bound; }

        /*!
         * \brief Геттер ветки поддерева операции
         * \return Ветка поддерева операции
         */
        std::list<AstNode> & getBranch() { return _branch; }

        /*!
         * \brief Геттер константной ветки поддерева операции
         * \return Константная ветка поддерева операции
         */
        const std::list<AstNode> & getBranchC() const { return _branch; }

        /*!
         * \brief Добавление узла в ветку поддерева операции
         * \param op        Мнемокод операции
         * \param op_symbol Символ операции
         * \param bound     Границы операции
         * \return Указатель на добавленный узел
         */
        AstNode *       addNode(AstOperation op, Token op_symbol, Token bound)
        {
            _branch.emplace_back(this,op,op_symbol,bound);
            return &_branch.back();
        }

        void insertBranchBegin(std::list<AstNode> other_branch)
        {
            _branch.splice(_branch.begin(), other_branch);
        }

        void insertBranchEnd(std::list<AstNode> other_branch)
        {
            _branch.splice(_branch.end(), other_branch);
        }
    };

    /*!
     * \brief Функция преобразования мнемокода операции в строковый вид
     * \param op Мнемокод операции
     * \return   Строковое представление мнемокода операции
     */
    std::u16string getAstOperationName(AstOperation op);

}

#endif // SIMODO_DSL_AstNode
