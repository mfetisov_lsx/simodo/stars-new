/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_NS_sys
#define SIMODO_DSL_ScriptC_NS_sys

/*! \file ScriptC_NS_sys.h
    \brief Пространство имён 'sys' для интерпретатора
*/

#include "simodo/dsl/ScriptC_Interpreter.h"

#include <map>
#include <optional>

namespace simodo::dsl
{
    /*!
     * \brief Получение ссылки на глобальное пространство имён системных функций и констант
     */
    class ScriptC_NS_Sys : public IScriptC_Namespace
    {
        AReporter & _listener;
        std::map<std::string, std::optional<double>> _input;

    public:
        ScriptC_NS_Sys() = delete;
        ScriptC_NS_Sys(AReporter & listener);
        virtual ~ScriptC_NS_Sys();

        virtual SCI_Namespace_t getNamespace() override;

        void setInput(std::string key, std::optional<double> value);
        bool hasInput(std::string key);
        double getInput(std::string key);

        AReporter & listener() const { return _listener; }
    };

}

#endif // SIMODO_DSL_ScriptC_NS_sys
