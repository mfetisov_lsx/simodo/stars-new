/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Token
#define SIMODO_DSL_Token

/*! \file Token.h
    \brief Токен входного потока

    Заголовочный файл, описывающий классы и структуры, поддерживающие характеристики "алфавита" на выходе сканера.
*/

#include "simodo/dsl/Lexeme.h"

#include <cstdint>

namespace simodo::dsl
{
    typedef uint32_t        context_index_t;       ///< Тип для контекстов

    const context_index_t   NO_TOKEN_CONTEXT_INDEX = UINT32_MAX; ///< Недопустимый индекс отложенного контекста, означающий его отсутствие

    //! Тип уточнений в токенах
    enum class TokenQualification
    {
        None = 0,            ///< Нет уточнений
        Keyword,             ///< Ключевое слово (пунктуация, удовлетворяющая критериям Word)
        Integer,             ///< Беззнаковое целое
        RealNubmer,          ///< Беззнаковое вещественное
        NewLine,             ///< Символ перевода строки
        NationalCharacterMix,///< Неразрешённое смешивание национального и латинского алфавитов
        NationalCharacterUse,///< Неразрешённое использование национального алфавита в качестве идентификатора
        UnknownCharacterSet, ///< Набор символов документа (потока) не соответствует ни одному из анализируемых вариантов
                             ///< (возможно, потерян знак открывающегося или закрывающегося комментария или аннотации)
        NotANumber,          ///< Ошибка при записи числа с экспоненциальной частью
                             ///< (возможно, ввод числа ешё не закончен)
    };

    /*!
     * \brief Структура, определяющая местоположение токена
     *
     * Содержит характеристики местоположения.
     * 
     * Структурный класс (не совершает никаких действий над хранимыми элементами).
     */
     
    struct TokenLocation
    {
        std::u16string file_name;         ///< Имя файла
        uint32_t    line             = 1; ///< Номер строки (с единицы)
        uint32_t    column           = 1; ///< Позиция в строке (с единицы)
        uint32_t    column_tabulated = 1; ///< Позиция в строке с учётом символов табуляции (с единицы)
        uint32_t    begin            = 0; ///< Позиция начала токена в документе
        uint32_t    end              = 0; ///< Позиция следующего символа за токеном от начала документа
        context_index_t context      = NO_TOKEN_CONTEXT_INDEX; ///< Признак отложенного контекста (если признак завершения токена не был достигнут)

        explicit TokenLocation(const std::u16string &file)
            : file_name(file)
        {}
    };
     
    /*!
     * \brief Токен лексического разбора
     *
     * Основной элемент, генерируемый сканером (см. class Scanner). Содержит параметры положения в тексте
     * и другие характеристики токена (в отличие от лексемы, которая содержит строку содержимого и тип).
     * 
     * Значение токена, в отличие от лексемы, хранится как есть вместе со служебными элементами
     * (например, для аннотации хранятся обрамляющие кавычки и внутренние служебные символы кавычек).
     *
     * \attention Значение строки токена может отличаться от строки лексемы! Принципиальное отличие сводится к тому,
     * что строка токена содержит лексический символ в том виде, в котором он присутствует в исходном файле.
     * В то время, как строка лексемы содержит обработанный лексический символ, подготовленный для работы с
     * синтаксическим анализатором и генератором кода. Например, строка токена содержит строковую константу вместе с
     * кавычками, а строка лексемы - без.
     *
     * Неизменяемый (immutable) класс (не изменяет своё состояние).
     */
     
    class Token: public Lexeme
    {
        friend class AstBuilderHelper;

        std::u16string      token;              ///< Строка токена
        TokenLocation       location;           ///< Позиция токена в исходном тексте
        TokenQualification  qualification;      ///< Уточняющий квалификатор токена
        context_index_t     lexical_markups_index;///< Индекс параметра с пометками начала и конца

    protected:

    public:
        /*!
         * \brief Пустой конструктор
         */
        Token()
            : Lexeme(u"",LexemeType::Empty)
            , token(u"")
            , location(u"")
            , qualification(TokenQualification::None)
            , lexical_markups_index(NO_TOKEN_CONTEXT_INDEX)
        {}

        /*!
         * \brief Конструктор пустого токена
         * \param file_name Наименование файла
         */
        explicit Token(const std::u16string &file_name)
            : Lexeme(u"",LexemeType::Empty)
            , token(u"")
            , location(file_name)
            , qualification(TokenQualification::None)
            , lexical_markups_index(NO_TOKEN_CONTEXT_INDEX)
        {}

        /*!
         * \brief Конструктор токена на базе лексемы
         * \param file_name Наименование файла
         * \param lexeme    Исходная лексема
         */
        Token(const std::u16string &file_name, Lexeme lexeme)
            : Lexeme(lexeme)
            , token(lexeme.getLexeme())
            , location(file_name)
            , qualification(TokenQualification::None)
            , lexical_markups_index(NO_TOKEN_CONTEXT_INDEX)
        {}

        /*!
         * \brief Конструктор с заданием разных значений для токена и лексемы
         *
         * \param token_string  Строка токена
         * \param symbol_type   Тип лексемы
         * \param lexeme_string Строка лексемы
         * \param location      Позиция токена в исходном тексте
         * \param qualification Уточнения типа токена
         */
        Token(const std::u16string & token_string, LexemeType symbol_type, const std::u16string & lexeme_string,
              const TokenLocation & location, TokenQualification qualification = TokenQualification::None)
            : Lexeme(lexeme_string,symbol_type)
            , token(token_string)
            , location(location)
            , qualification(qualification)
            , lexical_markups_index(NO_TOKEN_CONTEXT_INDEX)
        {}

        /*!
         * \brief Конструктор с заданием индекса меченных последовательностей
         *
         * \param token_string  Строка токена
         * \param symbol_type   Тип лексемы
         * \param lexeme_string Строка лексемы
         * \param location      Позиция токена в исходном тексте
         * \param markups_index Индекс меченной последовательности
         */
        Token(const std::u16string & token_string, LexemeType symbol_type, const std::u16string & lexeme_string,
              const TokenLocation & location, context_index_t markups_index)
            : Lexeme(lexeme_string,symbol_type)
            , token(token_string)
            , location(location)
            , qualification(TokenQualification::None)
            , lexical_markups_index(markups_index)
        {}

        /*!
         * \brief Конструктор с заданием одинаковых значений для токена и лексемы
         *
         * \param symbol_type   Тип лексемы
         * \param lexeme_string Строка лексемы
         * \param location      Позиция токена в исходном тексте
         * \param qualification Уточнения типа токена
         */
        Token(LexemeType symbol_type, const std::u16string & lexeme_string, const TokenLocation & location,
              TokenQualification qualification = TokenQualification::None)
            : Lexeme(lexeme_string,symbol_type)
            , token(u"")
            , location(location)
            , qualification(qualification)
            , lexical_markups_index(NO_TOKEN_CONTEXT_INDEX)
        {}

        /*!
         * \brief Получение содержимого токена
         * \return   Константная ссылка на строку значения токена
         */
        const std::u16string & getToken(void) const { return token.empty() ? getLexeme(): token; }
        
        /*!
         * \brief Получение локализации токена
         * \return   Константная ссылка на структуру локализации токена
         */
        const TokenLocation & getLocation(void) const { return location; }
        
        /*!
         * \brief Получение уточняющей характеристики токена
         * \return   Значение уточняющей характеристики токена
         */
        TokenQualification  getQualification(void) const { return qualification; }

        /*!
         * \brief Геттер индекса лексического параметра с пометками начала и конца
         *
         * См. LexicalParameters::markups.
         *
         * \return Индекс параметра с пометками начала и конца
         */
        context_index_t getLexicalMarkupsIndex() const { return lexical_markups_index; }
    };

    /*!
     * \brief Функция получения строкового наименования уточняющей характеристики токена
     * \param qualification Тип уточняющей характеристики токена (тип TokenQualification)
     * \return        Ссылка на наименование уточняющей характеристики токена
     */
    std::u16string getQualificationName(TokenQualification qualification);
}

#endif // SIMODO_DSL_Token
