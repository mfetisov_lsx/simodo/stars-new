/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ISyntaxTreeBuilder
#define SIMODO_DSL_ISyntaxTreeBuilder

/*! \file ISyntaxTreeBuilder.h
    \brief Интерфейс построителя абстрактного синтаксического дерева

    Заголовочный файл, объявляющий интерфейс построителя абстрактного синтаксического дерева.
*/

#include <string>

#include "simodo/dsl/Tokenizer.h"
#include "simodo/dsl/AstNode.h"


namespace simodo::dsl
{
    /*!
     * \brief Абстрактный построитель абстрактного синтаксического дерева (АСД) (интерфейс)
     *
     * Интерфейс используется в парсере, в котором выполняются вызовы методов данного интерфейса.
     *
     * На самом деле АСД не обязательно должно быть деревом. Структура АСД должна быть
     * удобной для последующего выполнения семантического анализа и генерации кода.
     */
    class ISyntaxTreeBuilder
    {
    public:
        virtual ~ISyntaxTreeBuilder() = default; ///< Виртуальный деструктор

        /*!
         * \brief Метод вызывается парсером перед началом разбора
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onStart() = 0;

        /*!
         * \brief Метод вызывается парсером при выполнении свёртки
         *
         * Метод вызывается, когда выполняется свёртка состояний.
         *
         * \param production    Токен продукции, на которую заменяется шаблон в стеке состояний парсера
         * \param pattern       Шаблон (набор токенов), который заменяется на продукцию в стеке состояний парсера
         * \param action        АСД для интерпретатора для формирования фрагмента АСД по данной продукции
         * \param is_done       Признак завершённой обработки данного правила. Нужен, что бы сократить число проверок
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onProduction(Token production, std::vector<Token> pattern, const AstNode & action, bool &is_done) = 0;

        /*!
         * \brief Метод вызывается парсером при получении очередного токена
         * \param t Токен, полученный парсером из входного потока
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onTerminal(const Token & t) = 0;

        /*!
         * \brief Метод вызывается парсером после завершения разбора текста
         *
         * Предполагается, что при вызове данного метода и при наличии признака успешности
         * разбора текста, нужно завершить формирование АСД.
         *
         * \param success   Признак успешности разбора текста
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onFinish(bool success) = 0;
    };

    /*!
     * \brief Класс-заглушка: генератор АСД, который ничего не генерит
     */
    class DummyTreeBuilder: public ISyntaxTreeBuilder
    {
    public:
        /*!
         * \brief Метод вызывается парсером перед началом разбора
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onStart() override { return true; }

        /*!
         * \brief Метод вызывается парсером при выполнении свёртки
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onProduction(Token , std::vector<Token>, const AstNode & , bool & ) override { return true; }

        /*!
         * \brief Метод вызывается парсером при получении очередного токена
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onTerminal(const Token & ) override { return true; }

        /*!
         * \brief Метод вызывается парсером после завершения разбора текста
         * \return Если возвращается false, парсер прекращает разбор с ошибкой, иначе - продолжает
         */
        virtual bool onFinish(bool) override { return true; }
    };

}

#endif // SIMODO_DSL_ISyntaxTreeBuilder
