/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_ScriptC_Semantics
#define SIMODO_DSL_ScriptC_Semantics

/*! \file ScriptC_Semantics.h
    \brief Проверка семантики ScriptC на АСД, собранном ScriptC_AstBuilder
*/

#include <vector>
#include <memory>

#include "simodo/dsl/SemanticNameTable.h"

namespace simodo::dsl
{
    class ModulesManagement;

    /*!
     * \brief Проверка семантики ScriptC на АСД, собранного ScriptC_AstBuilder
     */
    class ScriptC_Semantics
    {
        AReporter &                   _m;           ///< Обработчик сообщений
        ModulesManagement &           _mm;          ///< Управление модулями
        std::vector<size_t>           _using_set;   ///< Набор имён операторов using
        SemanticNameTable             _name_table;  ///< Стек пространств имён
        std::vector<SemanticNameType> _type_stack;  ///< Стек типов
        std::map<std::u16string,const AstNode *> * _functions;///< Список функций модуля

        std::vector<size_t>           _owner_stack; ///< Стек владельцев переменных для присваивания структуры

    public:
        ScriptC_Semantics() = delete;

        /*!
         * \brief Конструктор проверки семантики ScriptC
         * \param m         Обработчик сообщений
         * \param mm        Ссылка на управляющего модулями
         * \param name_set  Ссылка на массив имён, который заполняется в процессе семантического анализа
         * \param scope_set Ссылка на массив зон видимостей, который заполняется в процессе семантического анализа
         * \param functions  Указатель на массив функций модуля
         * \param module_name Имя модуля
         */
        ScriptC_Semantics(AReporter & m, ModulesManagement & mm,
                          std::vector<SemanticName> & name_set,
                          std::vector<SemanticScope> & scope_set,
                          std::map<std::u16string,const AstNode *> *functions=nullptr);

        virtual ~ScriptC_Semantics();

        /*!
         * \brief Добавляет глобальный контекст (аналог оператора 'import')
         * \param name      Наименование пространства имён
         * \param context   Структура глобального пространства имён
         */
        void importNamespace(std::u16string name, const SCI_Namespace_t &context);

        /*!
         * \brief Проверка блока АСД
         * \param ast   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        bool check(const AstNode & ast);

    protected:
        /*!
         * \brief Проверка фрагмента АСД
         * \param ast   Узел АСД, который необходимо выполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkInner(const AstNode & ast);

        /*!
         * \brief Проверка фрагмента АСД с учётом выделения новой зоны видимости
         * \param ast   Узел АСД, который необходимо выполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkInnerWithScope(const AstNode & ast);

        /*!
         * \brief Загрузка модуля (с проверкой семантики) в качестве пользовательского типа
         * \param op   Узел АСД, который необходимо выполнить
         * \return Успех проверки фрагмента кода
         */
        bool loadModuleAsType(const AstNode & op);

        /*!
         * \brief Обработка оператора using
         * \param op   Узел АСД, который необходимо выполнить
         * \return Состояние обработки фрагмента кода
         */
        bool checkUsingStatement(const AstNode & op);

        /*!
         * \brief Проверка условия true для тернарного и условного операторов
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkTrueClause(const AstNode & op);

        /*!
         * \brief Проверка условия false для тернарного и условного операторов
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkFalseClause(const AstNode & op);

        /*!
         * \brief Проверка бинарных логических операций "и/или"
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkLogicalOperation(const AstNode & op);

        /*!
         * \brief Проверка бинарных операций логических сравнений: ==, !=, >, >=, <, <=
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkCompareOperation(const AstNode & op);

        /*!
         * \brief Проверка бинарных арифметических операций: +, -, *, /, %, ^
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkArithmeticOperation(const AstNode & op);

        /*!
         * \brief Проверка унарных операций префиксного +/-
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkUnaryOperation(const AstNode & op);

        /*!
         * \brief Проверка унарной операции логического отрицания
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkUnaryNotOperation(const AstNode & op);

        /*!
         * \brief Проверка операции размещения на стеке значения константы
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool pushConst(const AstNode & op);

        /*!
         * \brief Проверка операции размещения не стеке значения переменной
         * \param op    Узел АСД, который необходимо исполнить
         * \param owner Ссылка на владеющий кортеж, если такой есть
         * \return Успех проверки фрагмента кода
         */
        bool pushId(const AstNode & op,
                    size_t owner=UNDEFINED_INDEX,
                    SemanticNameAccess owner_access=SemanticNameAccess::FullAccess,
                    AstOperation owner_iteration_qualificator=AstOperation::None);

        /*!
         * \brief Проверка операции вызова функции (а также процедуры)
         * \param op                Узел АСД, который необходимо исполнить
         * \param is_procedure_call Признак вызова процедуры (влияет на проверки)
         * \param owner Ссылка на владеющий кортеж, если такой есть
         * \return Успех проверки фрагмента кода
         */
        bool callFunction(const AstNode & op, bool is_procedure_call, size_t owner=UNDEFINED_INDEX);

        /*!
         * \brief Проверка оператора объявления переменной
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkDeclaration(const AstNode & op);

        /*!
         * \brief Проверка оператора объявления функции
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkFunctionDeclaration(const AstNode & op);

        /*!
         * \brief Проверка оператора присваивания с учётом самоприсваивания: =, +=, -=, *=, /=, %=
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkAssignStatement(const AstNode & op,
                                  const AstNode & var_ast,
                                  size_t owner=UNDEFINED_INDEX,
                                  SemanticNameAccess owner_access=SemanticNameAccess::FullAccess);

        /*!
         * \brief Проверка управляющих операторов break и continue
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkBreakContinue(const AstNode & op);

        /*!
         * \brief Проверка оператора return
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkReturn(const AstNode & op);

        /*!
         * \brief Проверка оператора цикла while
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkCycleWhile(const AstNode & op);

        /*!
         * \brief Проверка оператора цикла do-while
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkCycleDoWhile(const AstNode & op);

        /*!
         * \brief Проверка оператора цикла for
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkCycleFor(const AstNode & op);

        /*!
         * \brief Проверка определения структуры данных (всё вместе: массива и кортежа)
         *
         * \attention Проверка реализована в урезанном виде.
         *
         * \todo Необходима доработка проверки после формального описания операционной семантики и рефакторинга!
         *
         * \param op    Узел АСД, который необходимо исполнить
         * \return Успех проверки фрагмента кода
         */
        bool checkDataStructure(const AstNode & op);

        /*!
         * \brief Формирование типа значения по заданному в узле типу
         * \param op Узел с типом значения
         * \return Тип
         */
        SemanticNameType getType(const AstNode & op) const;

        /*!
         * \brief Выполнение проверки типов операндов арифметической бинарной операции
         *
         * Помимо проверки типов метод выводит сообщения об ошибке.
         *
         * \param op    Операция
         * \param t1    Тип первого операнда
         * \param t2    Тип второго операнда
         * \return true - типы корректны, false - некорректны
         */
        bool checkArithmeticOperation(AstNode op, SemanticNameType t1, SemanticNameType t2) const;

        /*!
         * \brief Исполнение арифметической бинарной операции
         * \param op    Операция
         * \param t1    Первый операнд
         * \param t2    Второй операнд
         * \return Результирующее значение
         */
        SemanticNameType calculateArithmeticOperationType(AstNode op, SemanticNameType t1, SemanticNameType t2) const;

        /*!
         * \brief Формирование представление из описания
         * \param from_owner Владелец описания
         * \param to_owner Представление
         */
        void fillTupleFromType(const std::vector<SemanticName> & name_set, size_t from_owner, size_t to_owner);

        /*!
         * \brief Проверка преобразования типов
         *
         * \param from Исходный тип
         * \param to   Тип результата
         * \return true - преобразование корректно, false - некорректно
         */
        bool checkTypeConversion(SemanticNameType from, SemanticNameType to) const;

    };

}

#endif // SIMODO_DSL_ScriptC_Semantics
