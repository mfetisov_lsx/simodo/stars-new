/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_GrammarBuilder_LR1
#define SIMODO_DSL_GrammarBuilder_LR1

/*! \file GrammarBuilder_LR1.h
    \brief Формирование таблицы разбора методом LR(1)

    Заголовочный файл, выполняющий формирование таблицы разбора методом LR(1).
*/

#include "simodo/dsl/GrammarBuilder.h"
#include "simodo/convert.h"

#include <tuple>

namespace simodo::dsl
{
    struct DepthPair
    {
        int     depth;
        size_t  weight;
    };

    struct DepthPairComparator
    {
        bool operator()(const DepthPair& top, const DepthPair& btn) const
        {
            return (top.depth == btn.depth && top.weight > btn.weight) || top.depth < btn.depth;
        }
    };

    /*!
     * \brief Класс формирование таблицы разбора по заданным правилам грамматики методом LR(1)
     */
    class GrammarBuilder_LR1 : public GrammarBuilder
    {
        std::multimap<uint64_t,size_t> _position_index;    ///< Отображение позиции состояния (rno,pos) в номер состояния

    public:
        GrammarBuilder_LR1() = delete;    ///< Пустой конструктор не поддерживается!

        /*!
         * \brief Конструктор построителя таблицы разбора методом LR(1)
         * \param m             Интерфейсная ссылка на объект обеспечения вывода информации в вызываемую программу
         * \param path          Путь к файлам грамматики
         * \param grammar_name  Наименование грамматики, которую нужно загрузить
         * \param g             Структура грамматики
         * \param strict_rule_consistency Признак строгости в проверках согласованности таблицы разбора
         */
        GrammarBuilder_LR1(AReporter & m, const std::string & path, const std::string & grammar_name, Grammar & g,
                       bool strict_rule_consistency=true)
            : GrammarBuilder(m, path, grammar_name, g, strict_rule_consistency)
        {}

    protected:
        /*!
         * \brief Метод построения таблицы состояний
         */
        virtual void fillStateTable() override;

        /*!
         * \brief Основной метод построения таблицы разбора по заполненным перечням сотяоний (строки)
         * и символов грамматики (колонок)
         * \return true, если формирование завершилось успешно, иначе - false
         */
        virtual bool fillParseTable() override;

    protected:
        /*!
         * \brief Метод построения набора состояний для заданного номера состояния (линии) таблицы разбора
         * (используется рекурсивно)
         * \param state_no Номер состояния таблицы разбора
         */
        void fillState(size_t state_no);

        /*!
         * \brief Метод поиска заданной основной позиции в наборе состояний
         * \param rno       Номер правила в перечне правил грамматики
         * \param pos       Позиция состояния внутри правила
         * \param lookahead Предпросмотры
         * \return    Номер состояния, если оно найдено или 0, если не найдено (нулевое состояние является
         * исскуственным и ни когда не может быть найдено)
         */
        size_t findMainPosition(size_t rno, size_t pos, const std::set<Lexeme> &lookahead) const;

        /*!
         * \brief Метод формирования замыканий для основной позиции заданного состояния
         *
         * Метод заполнения всех наведённых позиций (замыканий) при раскрутке символа грамматики,
         * стоящего на позиции (используется рекурсивно)
         *
         * \param state_no          Номер состояния
         * \param rno               Индекс правила грамматики
         * \param pos               Позиция
         * \param depth             Текущая глубина раскрутки
         * \param position_multiset Формируемая в данном методе коллекция позиция (для сортировки по их весам)
         */
        void fillClosure(size_t state_no, size_t rno, size_t pos, int depth,
                         std::multimap<DepthPair,FsmStatePosition,DepthPairComparator> & position_multiset);


        /*!
         * \brief Определяет и заполняет предпросмотры для сформированных неосновных позиций заданного состояния
         * \param state_no                  Номер состояния
         * \param position_initial_count    Индекс начала неосновных позиций
         * \param prod_lookahead_set        Предпросмотры продукций для заданного состояния
         */
        void fillLookahead(size_t state_no, size_t position_initial_count,
                           std::map<std::u16string,std::set<Lexeme>> & prod_lookahead_set);

        /*!
         * \brief Формирует множество терминальных символов для заданной продукции
         * \param prod      Продукция
         * \param terminals Множество терминальных символов
         * \param processed Множество обработанных нетерминалов для предотвращения зацикливания
         */
        void fillStartingTerminalsByProd(const Lexeme &prod, std::set<Lexeme> & terminals, std::set<Lexeme> & processed);

        /*!
         * \brief Заполнение конкретной ячейки в таблице разбора
         * \param line      Номер состояние (строка таблицы)
         * \param column    Номер символа грамматики (колонка таблицы)
         * \param value     Значение
         * \return true, если заполнение завершилось успешно, иначе - false
         */
        bool insertValue(size_t line, size_t column, Fsm_value_t value);

        /*!
         * \brief Добавление списка позиций как нового состояния
         *
         * Одновременно формируется индекс состояний по позициям.
         *
         * \param state     Список позиций
         * \param state_no  Номер состояния
         */
        void addState(FsmState_t state, size_t state_no);
    };

}

#endif // SIMODO_DSL_GrammarBuilder
