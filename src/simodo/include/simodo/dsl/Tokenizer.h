/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_Tokenizer
#define SIMODO_DSL_Tokenizer

/*! \file Tokenizer.h
    \brief Лексический анализатор

    Заголовочный файл, описывающий классы и структуры перевода текста входного потока в высокоуровневый алфавит (лексемы).
*/

#include <vector>
#include <string>
#include <istream>

#include "simodo/dsl/Scanner.h"
#include "simodo/dsl/LexicalParameters.h"

namespace simodo::dsl
{
    /*!
     * \brief Класс лексического анализатора
     *
     * Данный лексический анализ имеет ряд важных особенностей:
     * 1. При подсвечивании синтаксиса в среде Qt 5 нужно учитывать контекст многострочных конструкций
     * таких, как многострочные комментарии и аннотации (строки в двойных ковычках).
     * Поэтому, предусмотрена работа с отложенным контекстом (см. enum class TokenContext)
     * 2. Коментарии (однострочные и многострочные) определяются и возвращаются как токены
     * 3. Идентификаторы (тип TokenType::Word) не могут иметь символы разных алфавитов.
     * Если такое замечено, то токен возвращается с уточнением TokenQualification::NationalCharMix
     *
     * В остальном работа лексического анализатора стандартна.
     *
     * При разборе (работа Parser и т.п.) после создания объекта Tokenizer выполняются вызовы getToken,
     * пока возвращённый токен не будет типа lsx::parser::LexemeType::Empty. Этот тип токена будет возвращаться всегда,
     * если лексер достиг конца переданного документа (или переданного фрагмента документа).
     *
     * \todo Необходимо оптимизировать определение принадлежности символа (char)
     * группам чисел и слов (нужен оптимальный поиск в отсортированных множествах, сейчас это не так)
     */
     
    class Tokenizer
    {
    public:
        struct _NumberMask
        {
            std::u16string  chars;          ///< Маска числа
            LexemeType      type;           ///< Тип лексемы
            TokenQualification qualification;///< Уточняющий квалификатор токена
            number_system_t system;         ///< Система счисления
            std::vector<int> refs;      ///< Ссылки на маски-продолжения
            bool            is_statring;    ///< Признак использования данной маски в начальном определении числа
            bool            may_final;      ///< Признак не обязательного продолжения
        };
    private:
        /*!
         * \brief Служебное перечисление для передачи признака в защищённые методы
         */
        enum class NationalCharAffiliation
        {
            Latin,      ///< Идентификатор начат с латинской буквы
            National,   ///< Идентификатор начат с буквы национального алфавита
            Extra       ///< Идентификатор начат со вспомогательного символа
        };

    private:
        Scanner           scanner;  ///< Сканер входного потока
        LexicalParameters param;    ///< Лексические параметры
        std::vector<_NumberMask> numbers; ///< Маски для чисел

    public:
        Tokenizer() = delete; // Без инициализации параметров не имеет смысла

        /*!
         * \brief Конструктор лексического анализатора
         * \param file_name    Наименование входного потока
         * \param input_stream Ссылка на сходной поток
         * \param parameters   Настройки сканера
         * \param tab_size     Величина табуляции
         * \param context_no   Номер контекста начала разбора (используется при подсветке синтаксиса)
         */
        Tokenizer(const std::u16string & file_name,
                  IStream & input_stream,
                  const LexicalParameters & parameters, // TODO раскоментировать
                //   LexicalParameters & parameters, // TODO закоментировать
                  uint8_t tab_size,
                  context_index_t context_no=NO_TOKEN_CONTEXT_INDEX);

        /*!
         * \brief Получение очередного токена из заданного документа (фрагмента) без комментариев
         * \return Токен (см. класс Token)
         */
        Token getToken();

        /*!
         * \brief Получение очередного токена из заданного документа (или фрагмента) вместе с комментариями 
         * \return Токен (см. класс Token)
         */
        Token getAnyToken();

        /*!
         * \brief Возвращает текущее состояние обработки токена
         * \return Текущее состояние обработки токена после последнего вызова shift
         */
        const TokenLocation & getLocation() const { return scanner.getLocation(); }

    protected:
        /*!
         * \brief Пропуск пробельных символов
         */
        void passBlanks();

        /*!
         * \brief Обработка маркированного токена
         * \return Токен
         */
        Token scanMarkup(uint32_t murkup_index);

        /*!
         * \brief Обработка идентификатора (слова)
         * \return Токен
         */
        Token scanWord(NationalCharAffiliation first_char);

        /*!
         * \brief Обработка числа
         *
         *  (вообще-то может быть не только число)
         *
         * \param type          Тип токена
         * \param qualification Квалификатор
         * \param lexeme_str    Строка лексемы
         * \return true, если было разобрано число, иначе - false
         */
        bool scanNumber(LexemeType & type, TokenQualification & qualification, std::u16string & lexeme_str);

        /*!
         * \brief Преобразует строку символов в верхний регистр, используя заданный в параметрах латинский и национальный алфавит
         * \param s   Исходная строка
         * \return Строка, преобразованная в верхний регистр
         */
        std::u16string convertToUpper(std::u16string s) const;

        char16_t convertToUpper(char16_t ch) const;
        char16_t convertLatinToUpper(char16_t ch) const;

        bool isBlank(char16_t ch) const;

    private:
        bool makeMask(const std::u16string & mask, LexemeType type, number_system_t number_system, size_t pos, size_t index, char16_t bracket);
    };

}

#endif // SIMODO_DSL_Tokenizer
