/*
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_FuzeParser_ScriptC_h
#define SIMODO_DSL_FuzeParser_ScriptC_h

/*! \file FuzeParser_ScriptC.h
    \brief Начальный разбор примитивов ScriptC для описания АСД

    Заголовочный файл, описывающий класс начального разбора примитивов ScriptC для описания АСД.
*/

#include <string>

#include "simodo/dsl/FuzeParserBase.h"
#include "simodo/dsl/AstBuilderHelper.h"
#include "simodo/dsl/AstNode.h"
#include "simodo/dsl/Tokenizer.h"


namespace simodo::dsl
{
    /*!
     * \brief Класс начального разбора примитивов ScriptC для описания АСД
     *
     */
    class FuzeParser_ScriptC : public FuzeParserBase
    {
//        std::map<std::u16string,AstNode> &  _handlers;
        AstNode &        _ast;
        AstBuilderHelper _ast_helper;	///< Вспомогательный класс для построения АСД

    public:
        FuzeParser_ScriptC() = delete; ///< Пустой конструктор не поддерживается!

        FuzeParser_ScriptC(AReporter & m, AstNode & ast)
            : FuzeParserBase(m), _ast(ast)
        {}

        bool    parse(Tokenizer & tzer, Token & t);

    protected:

        bool    parse_ProcedureCalling(Tokenizer & tzer, Token & t);

        bool    parse_Address(Tokenizer & tzer, Token & t);

        bool    parse_ArgumentList(Tokenizer & tzer, Token & t);

        /*!
         * \brief Геттер на вспомогательный класс для построения АСД
         * \return Ссылка на вспомогательный класс для построения АСД
         */
        AstBuilderHelper & ast() { return _ast_helper; }

    };
}

#endif // SIMODO_DSL_FuzeParser_ScriptC_h
