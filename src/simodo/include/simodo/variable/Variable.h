/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_module_variable_Variable
#define simodo_module_variable_Variable

/*! \file Variable.h
    \brief Реализация переменной (именованной) для машины SBL
*/

#include "simodo/ast/Node.h"

#include <memory>
#include <string>
#include <vector>
#include <variant>

namespace simodo::variable
{
    inline const std::u16string UNDEF_STRING = u"(UNDEF)";

    /// @todo Убрать лишних друзей
    class Module_interface;
    class VariableSetWrapper;
    class Variable;
    class Value;

    typedef Value (*ModuleFunction_t) (Module_interface * p_object,
                                          const VariableSetWrapper & args);
                                                    ///< Функция-обработчик вызова метода модуля

    typedef std::pair<std::shared_ptr<Module_interface>,ModuleFunction_t>
                                        ExternalFunction_t;

    typedef const ast::Node *           InternalFunction_t;

    typedef uint16_t                    index_t;

    /*!
     * \brief Перечисление данных (типов значений для переменных и констант, а также функций),
     *        размещаемых в структурах SCI
     */
    enum class ValueType
    {
        Null        = 0,    ///< Значение не определено в результате предыдущих ошибок
        Undefined   = 1,    ///< Значение не определено, но может быть инициализировано
        Bool        = 2,    ///< Логический тип
        Int         = 3,    ///< Целочисленный тип
        Real        = 4,    ///< Вещественный тип
        String      = 5,    ///< Строковый тип
        Function    = 6,    ///< Структура с описанием вызова и параметров функции
        Record      = 7,    ///< Набор ассоциативных элементов
        Array,              ///< Массив
        Ref,                ///< Ссылка на переменную
        // Дополнительные типы для внутреннего использования
        ExtFunction,    ///< Ссылка на внешнюю функцию
        IntFunction,    ///< Ссылка на внутреннюю функцию
    };

    typedef std::vector<Variable> VariableSet_t;

    class Record
    {
        // Module_interface *  _module = nullptr; ///< 
        VariableSet_t       _variables;

    public:
        Record() = default;
        Record(VariableSet_t vars) : _variables(vars) {}
        // Record(Module_interface * module)  : _module(module) {}
        // Record(Module_interface * module, VariableSet_t vars)  
        //     : _module(module), _variables(vars) {}

    public:
    //     const Module_interface *  module() const { return _module; }
    //           Module_interface *  module()       { return _module; }
        const VariableSet_t & variables() const { return _variables; }
              VariableSet_t & variables()       { return _variables; }

    public:
        bool                exists(const std::u16string & name) const;
        const Value &       find(const std::u16string & name) const;
              Value &       find(const std::u16string & name);
        Record              copy() const;
        // Record           instantiate() const;

        const Variable &    getVariableByName(const std::u16string & name) const;
        const Variable &    getVariableByIndex(index_t index) const;

        Value               invoke(const std::u16string & method_name, const VariableSet_t & arguments);
    };

    class ValueArray
    {
        ValueType               _common_type    = ValueType::Undefined; ///< Типа элементов массива
        std::vector<index_t>    _dimensions;                            ///< Размерности массива
        std::vector<Value>      _values;                                ///< Вектор всех значений

    public:
        ValueArray() = default;
        ValueArray(ValueType common_type, std::vector<index_t> dimensions, std::vector<Value> values)
        : _common_type(common_type), _dimensions(dimensions), _values(values)
        {}
        ValueArray(std::vector<Value> values);
        // : _dimensions({static_cast<index_t>(values.size())}), _values(values)
        // {}

        ValueType                   common_type()   const { return _common_type; }
        const std::vector<index_t> &dimensions()    const { return _dimensions; }
        const std::vector<Value> &  values()        const { return _values; }
        std::vector<Value> &        values()              { return _values; }

        ValueArray                  copy()          const;

        const Value &               getValueByIndex(index_t index) const { return getValueByIndex(std::vector<index_t>{index}); }
        const Value &               getValueByIndex(std::vector<index_t> index) const;
    };

    class VariableRef
    {
        Variable *  _variable_ptr = nullptr;

    public:
        VariableRef() = default;
        explicit VariableRef(Variable & var);
        explicit VariableRef(const Variable & var_c);

        const Variable & origin() const;
        Variable & origin();
    };

    typedef std::variant<
            bool,                       ///< 0 ValueType::Bool
            int64_t,                    ///< 1 ValueType::Int
            double,                     ///< 2 ValueType::Real
            std::u16string,             ///< 3 ValueType::String
            std::shared_ptr<Record>,    ///< 4 ValueType::Record
            std::shared_ptr<ValueArray>,///< 5 ValueType::Array
            VariableRef,                ///< 6 ValueType::Reference
            ExternalFunction_t,         ///< 7 ValueType::ExtFunction
            InternalFunction_t          ///< 8 ValueType::IntFunction
            >
                        Variant_t;

    class Value 
    {
        ValueType   _type       = ValueType::Null;
        Variant_t   _variant;

    public:
        Value() = default;
        Value(const Value& other) = default;

        Value(ValueType type)               : _type(type) {}
        Value(bool value)                   : _type(ValueType::Bool), _variant(value) {} 
        Value(int64_t value)                : _type(ValueType::Int), _variant(value) {} 
        Value(int value)                    : _type(ValueType::Int), _variant(static_cast<int64_t>(value)) {} 
        Value(double value)                 : _type(ValueType::Real), _variant(value) {} 
        Value(std::u16string value)         : _type(ValueType::String), _variant(value) {} 
        Value(const char16_t * value)       : _type(ValueType::String), _variant(static_cast<std::u16string>(value)) {} 
        Value(Record value)                 : _type(ValueType::Record), _variant(std::make_shared<Record>(value)) {} 
        Value(ValueType type, Record value) : _type(type), _variant(std::make_shared<Record>(value)) {} 
        Value(std::shared_ptr<Record> value): _type(ValueType::Record), _variant(value) {} 
        Value(ValueType type, std::shared_ptr<Record> value): _type(type), _variant(value) {} 
        Value(ValueArray value)             : _type(ValueType::Array), _variant(std::make_shared<ValueArray>(value)) {} 
        Value(std::shared_ptr<ValueArray> value): _type(ValueType::Array), _variant(value) {} 
        Value(std::vector<Value> values)    : _type(ValueType::Array), _variant(std::make_shared<ValueArray>(values)) {} 
        Value(VariableRef ref)              : _type(ValueType::Ref), _variant(ref) {} 
        Value(ExternalFunction_t value)     : _type(ValueType::ExtFunction), _variant(value) {} 
        // Value(InternalFunction_t value)     : _type(ValueType::IntFunction), _variant(value) {} 

        Value& operator=(const Value& other) = default;

    public:
        ValueType                   type()              const { return _type; }
        const Variant_t &           variant()           const { return _variant; }
        Variant_t &                 variant()                 { return _variant; }

        bool                        getBool()           const { return std::get<bool>(_variant); }
        int64_t                     getInt()            const { return std::get<int64_t>(_variant); }
        double                      getReal()           const { return std::get<double>(_variant); }
        const std::u16string &      getString()         const { return std::get<std::u16string>(_variant); }
        std::shared_ptr<Record>     getRecord()         const { return std::get<std::shared_ptr<Record>>(_variant); }
        std::shared_ptr<ValueArray> getArray()          const { return std::get<std::shared_ptr<ValueArray>>(_variant); }
        const VariableRef &         getRef()            const { return std::get<VariableRef>(_variant); }
        ExternalFunction_t          getExtFunction()    const { return std::get<ExternalFunction_t>(_variant); }
        // InternalFunction_t          getIntFunction()    const { return std::get<InternalFunction_t>(_variant); }

    public:
        bool                        empty()             const { return _variant.index() == std::variant_npos; }
        Value                       copy()              const;

    };

    class Variable
    {
        std::u16string              _name;              ///< Имя переменной
        Value                       _value;             ///< Значение переменной
        inout::token::TokenLocation _location;          ///< Место объявления переменной в исходном коде
        Record                      _spec;              ///< Спецификаторы переменной (в том числе контракты)

    public:
        Variable() = default;

        Variable(std::u16string name, Value value, inout::token::TokenLocation location, Record spec)
            : _name(name), _value(value), _location(location), _spec(spec)
        {}
        Variable(std::u16string name, Value value, inout::token::TokenLocation location)
            : _name(name), _value(value), _location(location)
        {}
        Variable(std::u16string name, Value value)
            : _name(name), _value(value)
        {}
        Variable(VariableRef ref, inout::token::TokenLocation location)
            : _name(ref.origin().name()), _value(ref), _location(location), _spec(ref.origin().spec())
        {}

    public:
        const std::u16string &      name()          const { return _name; }
        const Value &               value()         const { return _value; }
        /// \todo Есть метод setValue! Вообще, нужно выбрать какой-то один стиль для сеттеров!
        Value &                     value()               { return _value; }
        const inout::token::TokenLocation & location() const { return _location; }
        const Record &              spec()          const { return _spec; }
        Record &                    spec()                { return _spec; }
        ValueType                   type()          const { return _value.type(); }
        const Variant_t &           variant()       const { return _value.variant(); }

    public:
        const Variable &            origin()        const;
        Variable &                  origin()        ;

        Variable                    copyVariable()  const;
        VariableRef                 makeReference() const;

        void                        setName(const std::u16string & name)    { _name = name; }
        void                        setValue(Value value)                   { _value = value.copy(); }
        void                        setLocation(inout::token::TokenLocation loc) { _location = loc; }
    };

    std::u16string getValueTypeName(ValueType type) noexcept;
    std::u16string toString(const Value & value, bool need_whole_info=true, bool quote_strings=true);

    inline const Variable error_variable(inout::token::TokenLocation loc={}, std::u16string name=u"") { return {name, ValueType::Null, loc}; }
    inline const Variable undef_variable(inout::token::TokenLocation loc={}, std::u16string name=u"") { return {name, ValueType::Undefined, loc}; }
}

#endif // simodo_module_variable_Variable
