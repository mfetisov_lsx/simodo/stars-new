/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_sbl_FunctionWrapper
#define simodo_sbl_FunctionWrapper

/*! \file FunctionWrapper.h
    \brief Работа с функцией SBL
*/

#include "simodo/variable/Variable.h"
#include "simodo/variable/VariableSetWrapper.h"

namespace simodo::variable
{
    class FunctionWrapper
    {
        const Variable &                _function_variable;
        const std::shared_ptr<Record>   _function_structure;

    public:
        FunctionWrapper() = delete;
        FunctionWrapper(const Variable & function_variable);

    public:
        const Variable &    function_variable() const { return _function_variable; }

    public:
        Value               invoke(VariableSetWrapper_mutable & arguments);

    protected:
        void                castArguments(VariableSetWrapper_mutable & arguments) const ;

        const Variable &    getCallingAddressVariable() const ;
        const Variable &    getReturnDeclarationVariable() const ;
        VariableSetWrapper  getArgumentDeclarationVariable() const ;
    };
}

#endif // simodo_sbl_VariableStack
