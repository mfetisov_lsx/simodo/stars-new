#ifndef simodo_variable_json_Rpc_h
#define simodo_variable_json_Rpc_h

#include "simodo/variable/Variable.h"

namespace simodo::variable::json
{

class Rpc
{
    variable::Value _value;
    bool            _is_valid  = false;
    std::u16string  _jsonrpc;
    std::u16string  _method;
    int64_t         _id = -1;

    variable::Value _fail;
    const variable::Value * _params = nullptr;
    const variable::Value * _result = nullptr;
    const variable::Value * _error = nullptr;

public:
    Rpc() = default;
    explicit Rpc(const std::string & json);
    explicit Rpc(const std::u16string & json);
    explicit Rpc(variable::Value value);
    explicit Rpc(std::u16string method, variable::Value params, int64_t id);
    explicit Rpc(std::u16string method, int64_t id);
    explicit Rpc(std::u16string method, variable::Value params);
    // Rpc(std::u16string method); // Конфликтует с первым конструктором
    explicit Rpc(variable::Value result, int64_t id);
    explicit Rpc(int64_t code, std::u16string message, variable::Value data, int64_t id);
    explicit Rpc(int64_t code, std::u16string message, int64_t id);

    const variable::Value   value()     const { return _value; }
    bool                    is_valid()  const { return _is_valid; }
    const std::u16string &  jsonrpc()   const { return _jsonrpc; }
    const std::u16string &  method()    const { return _method; }
    const variable::Value & params()    const;
    const variable::Value & result()    const;
    const variable::Value & error()     const;
    int64_t                 id()        const { return _id; }

private:
    void setupMembers();
};

}

#endif // simodo_variable_json_Rpc_h