/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_variable_json_parser_Parser
#define simodo_variable_json_parser_Parser

/*! \file Parser.h
    \brief Загрузка (разбор) JSON из файла или потока во внутренний формат класса variable::Value
*/

#include "simodo/variable/Variable.h"

#include "simodo/inout/reporter/Reporter_abstract.h"
#include "simodo/inout/token/InputStream_interface.h"
#include "simodo/inout/token/Tokenizer.h"
#include "simodo/inout/token/RdpBaseSugar.h"

#include <string>

namespace simodo::variable::json::parser
{
    class JsonRdp : public inout::token::RdpBaseSugar
    {
    public:
        JsonRdp(inout::reporter::Reporter_abstract & m) : inout::token::RdpBaseSugar(m) {}

        bool    parse(const std::string & json_file, Value &json, int tab_size = 4) const;
        bool    parse(const std::string & json_file, inout::token::InputStream_interface & stream, Value &json, int tab_size = 4) const;

    protected:
        bool    parseValue(inout::token::Tokenizer & tokenizer, const inout::token::Token & t, Value &json) const;
        bool    parseObject(inout::token::Tokenizer & tokenizer, Value &json) const;
        bool    parseArray(inout::token::Tokenizer & tokenizer, Value &json) const;

    };
}

#endif // simodo_variable_json_parser_Parser
