/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_variable_json_LexicalParametersLoader
#define simodo_variable_json_LexicalParametersLoader

/*! \file LexicalParametersLoader.h
    \brief Загрузка параметров лексики из JSON-файла.
*/

#include "simodo/inout/token/LexicalParameters.h"


namespace simodo::variable::json
{

    bool loadLexicalParameters(const std::string & file_name, inout::token::LexicalParameters & lex);

}

#endif // simodo_variable_json_LexicalParametersLoader
