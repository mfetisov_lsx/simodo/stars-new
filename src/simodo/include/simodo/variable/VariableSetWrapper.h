/*
MIT License

Copyright (c) 2022 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/loom
*/

#ifndef simodo_module_variable_VariableSetWrapper
#define simodo_module_variable_VariableSetWrapper

/*! \file VariableSetWrapper.h
    \brief Адаптер (wrapper) на VariableSet_t, ограничивающий контекст работы.
*/

#include "simodo/variable/Variable.h"

namespace simodo::variable
{
    class VariableSetWrapper_mutable
    {
    protected:
        VariableSet_t & _set;
        size_t          _begin_index;
        size_t          _end_index;

    public:
        VariableSetWrapper_mutable() = delete;
        VariableSetWrapper_mutable(VariableSet_t & set);
        VariableSetWrapper_mutable(VariableSet_t & set, size_t begin_index, size_t end_index);

        size_t begin_index() const { return _begin_index; }
        size_t end_index()   const { return _end_index; }

        Variable & at(size_t index);
        Variable & operator [] (size_t index) { return at(index); }

        const Variable & at(size_t index) const ;
        const Variable & operator [] (size_t index) const { return at(index); }

        const VariableSet_t & set() const { return _set; }

        size_t size() const;
    };

    class VariableSetWrapper
    {
    protected:
        const VariableSet_t & _set;
        size_t                _begin_index;
        size_t                _end_index;

    public:
        VariableSetWrapper() = delete;
        VariableSetWrapper(const VariableSetWrapper_mutable & wrapper);
        VariableSetWrapper(const VariableSet_t & set);
        VariableSetWrapper(const VariableSet_t & set, size_t begin_index, size_t end_index);

        size_t begin_index() const { return _begin_index; }
        size_t end_index()   const { return _end_index; }

        const Variable & at(size_t index) const;
        const Variable & operator [] (size_t index) const { return at(index); }

        const VariableSet_t & set() const { return _set; }

        size_t size() const;
    };

}

#endif // simodo_module_variable_VariableSetWrapper
