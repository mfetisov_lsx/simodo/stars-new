/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_ast_OperationCode
#define simodo_ast_OperationCode

/*! \file OperationCode.h
    \brief Тип кода операции операционной семантики.
*/

#include <cstdint>

namespace simodo::ast {

    typedef uint16_t OperationCode;

}

#endif // simodo_ast_OperationCode
