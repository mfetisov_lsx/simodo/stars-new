/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_ast_Node
#define simodo_ast_Node

/*! \file Node.h
    \brief Структуры абстрактного синтаксического дерева
*/

#include "simodo/ast/OperationCode.h"
#include "simodo/inout/token/Token.h"

#include <string>
#include <list>


namespace simodo::ast
{
    /*!
     * \brief Узел абстрактного синтаксического дерева, а также абстрактного дерева операционной семантики.
     *
     * Узел абстрактного синтаксического дерева модифицирован для использования в построении 
     * абстрактного дерева операционной семантики (АДОС, abstract operational semantic tree - AOST). 
     * Однако название оставил стандартное, чтобы подчеркнуть откуда всё происходит. Возможно,
     * когда-нибудь появится время всё переделать на базе интерфейсов (в стиле ООП), а пока пусть будет так.
     * 
     * Каждый узел АДОС является описанием семантического оператора. Смысл этого оператора 
     * раскрывается конкретным интерпретатором.
     */
    class Node {
        // Классы, которые участвуют в формировании Node
        friend class NodeFriend;

        // Основные атрибуты (должны сохраняться при сериализации)
        std::list<Node>     _branches;

        std::u16string      _semantic_host_name;    ///< Мнемокод семантики
        OperationCode       _operation;             ///< Код операции (уникален внутри владельца семантики)

        inout::token::Token _symbol;                ///< Токен операции (может не совпадать с мнемокодом операции)
        inout::token::Token _bound;                 ///< Токен границы всех операндов (включая участвующих опосредованно) и операции

    public:
        Node(std::u16string file_name=u"")
            : _semantic_host_name(u"")
            , _operation(0)
            , _symbol( { {}, u"", {{file_name}} } )
            , _bound( { {}, u"", {{file_name}} } )
        {}

        Node(std::u16string semantic_host_name, OperationCode op, inout::token::Token symbol, inout::token::Token bound)
            : _semantic_host_name(semantic_host_name)
            , _operation(op)
            , _symbol(symbol)
            , _bound(bound)
        {}

    public:
        const std::list<Node> &     branches()              const   { return _branches; }
        OperationCode               operation()             const   { return _operation; }
        const inout::token::Token & token()                 const   { return _symbol; }
        const inout::token::Token & bound()                 const   { return _bound; }
        const std::u16string &      semantic_host_name()    const   { return _semantic_host_name; }
    };
}

#endif // simodo_ast_Node
