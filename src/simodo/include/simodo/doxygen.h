/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DOXYGEN_H
#define SIMODO_DOXYGEN_H

/*! \file doxygen.h
    \brief Определения для Doxygen

    Заголовочный файл с общими для всех пространств имён объявлениями и описаниями для Doxygen.
*/

/*! \namespace simodo::dsl
    \brief Пространство имён библиотеки предметно-ориентированного языка SIMODO
*/
namespace simodo::dsl {}


#endif // SIMODO_DOXYGEN_H
