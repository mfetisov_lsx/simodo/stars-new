/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_Token
#define simodo_token_Token

/*! \file Token.h
    \brief Токен входного алфавита
*/

#include "simodo/inout/token/Lexeme.h"
#include "simodo/inout/token/TokenLocation.h"
#include "simodo/inout/token/TokenQualification.h"


namespace simodo::inout::token
{
    /*!
     * \brief Токен лексического разбора
     *
     * Основной элемент, генерируемый сканером (см. class Scanner). Содержит параметры положения в тексте
     * и другие характеристики токена (в отличие от лексемы, которая содержит строку содержимого и тип).
     * 
     * Значение токена, в отличие от лексемы, хранится как есть вместе со служебными элементами
     * (например, для аннотации хранятся обрамляющие кавычки и внутренние служебные символы кавычек).
     *
     * \attention Значение строки токена может отличаться от строки лексемы! Принципиальное отличие сводится к тому,
     * что строка токена содержит лексический символ в том виде, в котором он присутствует в исходном файле.
     * В то время, как строка лексемы содержит обработанный лексический символ, подготовленный для работы с
     * синтаксическим анализатором и генератором кода. Например, строка токена содержит строковую константу вместе с
     * кавычками, а строка лексемы - без.
     */
     
    struct Token: public Lexeme
    {
        std::u16string      token;                  ///< Строка токена
        TokenLocation       location;               ///< Позиция токена в исходном тексте
        TokenQualification  qualification           = TokenQualification::None;   ///< Уточняющий квалификатор токена
        context_index_t     lexical_markups_index   = NO_TOKEN_CONTEXT_INDEX;     ///< Индекс параметра с пометками начала и конца
    };

    inline bool operator < (const TokenLocation & lt, const TokenLocation & rt)
    { 
        if (lt.file_name == rt.file_name)
            return lt.begin < rt.begin;

        return lt.file_name < rt.file_name;
    }

    inline bool operator == (const TokenLocation & lt, const TokenLocation & rt)
    { 
        return (lt.file_name == rt.file_name && lt.begin == rt.begin);
    }

    inline bool operator < (const Token & lt, const Token & rt)
    { 
        if (lt.location.file_name == rt.location.file_name)
            return lt.location.begin < rt.location.begin;

        return lt.location.file_name < rt.location.file_name;
    }

    inline bool operator == (const Token & lt, const Token & rt)
    { 
        return (lt.location.file_name == rt.location.file_name && lt.location.begin == rt.location.begin);
    }

    /*!
     * \brief Функция получения строкового наименования уточняющей характеристики токена
     * \param qualification Тип уточняющей характеристики токена (тип TokenQualification)
     * \return        Ссылка на наименование уточняющей характеристики токена
     */
    std::u16string getQualificationName(TokenQualification qualification);
}

#endif // simodo_token_Token
