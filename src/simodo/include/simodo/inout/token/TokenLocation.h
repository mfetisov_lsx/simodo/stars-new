/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_TokenLocation
#define simodo_token_TokenLocation

/*! \file TokenLocation.h
    \brief Позиция токена в тексте.
*/

#include "simodo/inout/token/Location.h"

namespace simodo::inout::token
{
    typedef uint16_t context_index_t;  ///< Тип для контекстов

    inline constexpr context_index_t   NO_TOKEN_CONTEXT_INDEX = UINT16_MAX; ///< Недопустимый индекс отложенного контекста, означающий его отсутствие

    /*!
     * \brief Структура, определяющая местоположение токена
     *
     * Содержит характеристики местоположения.
     * 
     * Структурный класс (не совершает никаких действий над хранимыми элементами).
     */
     
    struct TokenLocation : public Location
    {
        context_index_t context = NO_TOKEN_CONTEXT_INDEX; ///< Признак отложенного контекста (если признак завершения токена не был достигнут)
    };
}

#endif // simodo_token_TokenLocation
