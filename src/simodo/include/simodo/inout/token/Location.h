/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_Location
#define simodo_token_Location

/*! \file Location.h
    \brief Структура, определяющая координаты выделения в тексте.
*/

#include <string>
#include <cstdint>

namespace simodo::inout::token
{
    /*!
     * \brief Структура, определяющая местоположение выделения в тексте.
     *
     */
     
    struct Location
    {
        std::u16string file_name        = u"";  ///< Имя файла
        uint32_t    begin               = 0;    ///< Позиция начала выделения в документе
        uint32_t    end                 = 0;    ///< Позиция следующего символа за выделением от начала документа
        uint32_t    line_begin          = 1;    ///< Номер строки (с единицы)
        uint32_t    line_end            = 1;    ///< Номер последней строки (с единицы)
        uint16_t    column              = 1;    ///< Позиция в строке (с единицы)
        uint16_t    column_tabulated    = 1;    ///< Позиция в строке с учётом символов табуляции (с единицы)
    };
}

#endif // simodo_token_Location
