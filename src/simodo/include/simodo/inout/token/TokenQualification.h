/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_TokenQualification
#define simodo_token_TokenQualification

/*! \file TokenQualification.h
    \brief Токен входного алфавита
*/

namespace simodo::inout::token
{
    //! Тип уточнений в токенах
    enum class TokenQualification
    {
        None = 0,            ///< Нет уточнений
        Keyword,             ///< Ключевое слово (пунктуация, удовлетворяющая критериям Word)
        Integer,             ///< Беззнаковое целое
        RealNumber,          ///< Беззнаковое вещественное
        NewLine,             ///< Символ перевода строки
        NationalCharacterMix,///< Неразрешённое смешивание национального и латинского алфавитов
        NationalCharacterUse,///< Неразрешённое использование национального алфавита в качестве идентификатора
        UnknownCharacterSet, ///< Набор символов документа (потока) не соответствует ни одному из анализируемых вариантов
                             ///< (возможно, потерян знак открывающегося или закрывающегося комментария или аннотации)
        NotANumber,          ///< Ошибка при записи числа с экспоненциальной частью
                             ///< (возможно, ввод числа ешё не закончен)
    };
}

#endif // simodo_token_TokenQualification
