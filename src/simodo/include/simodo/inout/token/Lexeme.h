/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_Lexeme
#define simodo_token_Lexeme

/*! \file Lexeme.h
    \brief Лексема абстрактного языка
*/

#include "simodo/inout/token/LexemeType.h"

#include <string>

namespace simodo::inout::token
{
    /*!
     * \brief Лексема языка
     *
     * Класс описывает абстрактную единицу языка - лексему. 
     * В отличие от токена (см. lsx::dsl::Token) не несёт информации о положении в тексте,
     * контексте и уточненом значении
     *
     * Значение лексемы, в отличие от Token, хранится без служебных элементов
     * (например, для аннотации хранится только строка значения, без кавычек)
     * 
     * Неизменяемый (immutable) класс (не изменяет своё состояние).
     */

    struct Lexeme
    {
        std::u16string lexeme;  ///< Строка лексемы
        LexemeType     type = LexemeType::Empty;    ///< Тип лексемы

        /*!
         * \brief Оператор сравенния двух лексем
         * \param symbol   Лексема, которую нужно сравнить с нашей
         *
         *      Лексемы считаются совпадающими, если у них совпадают типы и содержимое.
         */
        bool operator == (const Lexeme & symbol) const { return (type == symbol.type && lexeme == symbol.lexeme); }

        /*!
         * \brief Оператор проверки лексем на неравенство
         * \param symbol   Лексема, которую нужно сравнить с нашей
         *
         *      Лексемы считаются совпадающими, если у них совпадают типы и содержимое.
         */
        bool operator != (const Lexeme & symbol) const { return !(symbol == *this); }

        /*!
         * \brief Оператор сравенния двух лексем на <
         * \param symbol   Лексема, которую нужно сравнить с нашей
         */
        bool operator < (const Lexeme & symbol) const { return (type < symbol.type || (type == symbol.type && lexeme < symbol.lexeme)); }
    };

    /*!
     * \brief Функция получения строкового наименования типа лексемы
     * \param type  Тип лексемы (тип LexemeType)
     * \return      Ссылка на наименование типа лексемы
     */
    std::u16string getLexemeTypeName(LexemeType type) ;

    /*!
     * \brief Возвращает мнемоническое обозначение лексемы в зависимости от её типа
     * \param lex Лексема
     * \return Мнемоническое обозначение лексемы в зависимости от её типа
     */
    std::u16string getLexemeMnemonic(const Lexeme & lex);
}

#endif // simodo_token_Lexeme
