#ifndef _simodo_inout_log_Logger_H
#define _simodo_inout_log_Logger_H

#include "simodo/inout/log/Logger_interface.h"

#include <iostream>

namespace simodo::inout::log
{

enum class SeverityLevel
{
    Debug   = 0,
    Info    = 1,
    Warning = 2,
    Error   = 3,
    Critical= 4
};

class Logger: public log::Logger_interface
{
    std::ostream &  _out;
    std::string     _logger_name;
    SeverityLevel   _min_level;

    static std::string toString(SeverityLevel level);
    void output(SeverityLevel level, const std::string & message, const std::string & context);

public:
    Logger() = delete;
    Logger(std::ostream & out, std::string name, SeverityLevel min_level=SeverityLevel::Info) 
        : _out(out)
        , _logger_name(name) 
        , _min_level(min_level)
    {}

    virtual const std::string & name() const override 
    { return _logger_name; }
    virtual void debug(const std::string & message, const std::string & context="") override 
    { output(SeverityLevel::Debug, message, context); }
    virtual void info(const std::string & message, const std::string & context="") override
    { output(SeverityLevel::Info, message, context); }
    virtual void warning(const std::string & message, const std::string & context="") override
    { output(SeverityLevel::Warning, message, context); }
    virtual void error(const std::string & message, const std::string & context="") override
    { output(SeverityLevel::Error, message, context); }
    virtual void critical(const std::string & message, const std::string & context="") override
    { output(SeverityLevel::Critical, message, context); }
};

}

#endif
