/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_ConsoleReporter
#define simodo_token_ConsoleReporter

/*! \file ConsoleReporter.h
    \brief Консольная реализация "посетителя" (шаблон проектирования) для получения информации из среды SIMODO
*/

#include "simodo/inout/reporter/Reporter_abstract.h" 

#include <string>

namespace simodo::inout::reporter
{
    /*!
     * \brief Консольная реализация "посетителя" (шаблон проектирования) для получения информации из среды SIMODO
     */
    class ConsoleReporter: public Reporter_abstract
    {
        SeverityLevel _max_severity_level;
        SeverityLevel _max_report_level;

    public:
        ConsoleReporter(SeverityLevel max_severity=SeverityLevel::Information)
            : _max_severity_level(max_severity)
            , _max_report_level(SeverityLevel::Information)
        {}

        virtual void report(const SeverityLevel level,
                            token::Location ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge) override;

        SeverityLevel max_report_level() const { return _max_report_level; }
    };

}

#endif // simodo_token_ConsoleReporter
