/*
MIT License 

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

https://bmstu.codes/lsx/simodo
*/

#ifndef simodo_token_Reporter_abstract
#define simodo_token_Reporter_abstract

/*! \file Reporter_abstract.h
    \brief Интерфейс к выводу сообщений из библиотеки во внешнюю программу
*/

#include "simodo/inout/token/Location.h"

namespace simodo::inout::reporter
{
    //! Степень суровости ошибки
    enum class SeverityLevel
    {
        Information = 0, ///< Информационное сообщение
        Warning,         ///< Предупреждение: возможна некорректная работа
        Error,           ///< Ошибка: работа не выполнена, причина понятна и может быть описана
        Fatal,           ///< Сбой: работа не выполнена, причина не понятна
    };

    /*!
     * \brief Абстрактный класс, выполняющий передачу сообщений
     *
     *      Определяет интерфейс передачи сообщений из библиотеки. 
     *      Предполагается, что способ отображение сообщений будет реализовано в классах-наследниках.
     */
    class Reporter_abstract
    {
    public:
        virtual ~Reporter_abstract() = default; ///< Виртуальный деструктор
  
        /*!
         * \brief Пустой (интерфейсный) метод передачи сообщения
         *
         *      Метод может передавать информацию о локализации сообщения.
         *      Локализация считается на заданной, если line == 0.
         *
         *      Данный метод является единственным интерфейсным методом.
         *      Все остальные методв класса Listener осуществляют подготовку параметров и,
         *      в конечном итоге, вызывают данный метод для передачи сообщения.
         *
         * \param level     Степень суровости сообщения
         * \param loc       Позиция
         * \param briefly   Короткая запись сообщения (например, заголовок)
         * \param atlarge   Полная запись сообщения
         */
        virtual void report(const SeverityLevel level,
                            token::Location location,
                            const std::u16string & briefly,
                            const std::u16string & atlarge
                            ) = 0;

        /*!
         * \brief Передача сообщения с использованием объекта класса Token в качестве локализатора
         *
         *      В данной реализации локализация определяется по информации в объекте Token.
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param level     Степень суровости сообщения
         * \param token     Объект локализации
         * \param briefly   Короткая запись сообщения (например, заголовок)
         * \param atlarge   Полная запись сообщения (не обязательный параметр)
         */
        void    reportWithPosition(const SeverityLevel level,
                       const token::Location &location,
                       const std::u16string &briefly,
                       const std::u16string &atlarge=u"");

        /*!
         * \brief Передача которкого информационного сообщения без локализации
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param briefly   Короткая запись сообщения (например, заголовок)
         */
        void    reportInformation(const std::u16string &briefly)
        {
            report(SeverityLevel::Information, {}, briefly, u"");
        }

        /*!
         * \brief Передача информационного сообщения с локализацией
         *
         *      В данной реализации локализация определяется по информации в объекте Token.
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param token     Объект локализации
         * \param briefly   Короткая запись сообщения (например, заголовок)
         * \param atlarge   Полная запись сообщения (не обязательный параметр)
         */
        void    reportInformation(const token::Location &location,
                                  const std::u16string &briefly,
                                  const std::u16string &atlarge = u"")
        {
            reportWithPosition(SeverityLevel::Information, location, briefly, atlarge);
        }

        /*!
         * \brief Передача предупреждения с локализацией
         *
         *      В данной реализации локализация определяется по информации в объекте Token.
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param token     Объект локализации
         * \param briefly   Короткая запись сообщения (например, заголовок)
         * \param atlarge   Полная запись сообщения (не обязательный параметр)
         */
        void    reportWarning(const token::Location &location,
                              const std::u16string &briefly,
                              const std::u16string &atlarge = u"")
        {
            reportWithPosition(SeverityLevel::Warning, location, briefly, atlarge);
        }

        /*!
         * \brief Передача сообщения об ошибке с локализацией
         *
         *      В данной реализации локализация определяется по информации в объекте Token.
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param token     Объект локализации
         * \param briefly   Короткая запись сообщения (например, заголовок)
         * \param atlarge   Полная запись сообщения (не обязательный параметр)
         */
        void    reportError(const token::Location &location,
                            const std::u16string &briefly,
                            const std::u16string &atlarge = u"")
        {
            reportWithPosition(SeverityLevel::Error, location, briefly, atlarge);
        }

        /*!
         * \brief Передача короткого фатального сообщения без локализации
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param briefly   Короткая запись сообщения (например, заголовок)
         */
        void    reportFatal(const std::u16string &briefly)
        {
            report(SeverityLevel::Fatal, {}, briefly, u"");
        }

        /*!
         * \brief Передача фатального сообщения с локализацией
         *
         *      В данной реализации локализация определяется по информации в объекте Token.
         *
         *      Метод формирует данные и вызывает интерфейсный метод report.
         *
         * \param token     Объект локализации
         * \param briefly   Короткая запись сообщения (например, заголовок)
         * \param atlarge   Полная запись сообщения (не обязательный параметр)
         */
        void    reportFatal(const token::Location &location,
                            const std::u16string &briefly,
                            const std::u16string &atlarge = u"")
        {
            reportWithPosition(SeverityLevel::Fatal, location, briefly, atlarge);
        }
    };

    /*!
     * \brief Реализация пустого отображения сообщений
     *
     *      Класс NullReporter полезен, если все сообщения никак не требуется отображать.
     */
    class NullReporter: public Reporter_abstract
    {
    public:
        virtual void report(const SeverityLevel ,
                            token::Location ,
                            const std::u16string & ,
                            const std::u16string & )
        override final
        {}
    };

    /*!
     * \brief Функция получения строкового наименования уровня суровости сообщения
     * \param level     Уровень суровости (тип SeverityLevel)
     * \return          Ссылка на наименование уровня суровости
     */
    std::u16string getSeverityLevelName(SeverityLevel level);

    /*!
     * \brief Функция получения строки с описанием локации
     * \param token     Объект локализации
     * \param in_detail Признак вывода дополнительной информации.
     *                  Если true, то в скобках выводится позиция начала и конца символа от начала документа
     * \return          Строка с описанием локации
     */
    std::u16string getLocationString(const token::Location & location, bool in_detail=false);

}

#endif // simodo_token_Reporter_abstract
