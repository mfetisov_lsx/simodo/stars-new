#ifndef TextDocumentPositionParams_h
#define TextDocumentPositionParams_h

#include "simodo/variable/json/Rpc.h"

#include <string>

namespace simodo::lsp
{
    /**
     * Text documents are identified using a URI. On the protocol level, URIs are passed as strings.
    */
    struct TextDocumentIdentifier
    {
        /**
         * The text document's URI.
         */
        std::string uri;
    };

    /**
     * Position in a text document expressed as zero-based line and zero-based character offset. 
     * A position is between two characters like an ‘insert’ cursor in an editor. 
     * Special values like for example -1 to denote the end of a line are not supported.
    */
    struct Position
    {
        /**
         * Line position in a document (zero-based).
         */
        int64_t line;

        /**
         * Character offset on a line in a document (zero-based). The meaning of this
         * offset is determined by the negotiated `PositionEncodingKind`.
         *
         * If the character value is greater than the line length it defaults back
         * to the line length.
         */
        int64_t character;
    };

    /**
     * A parameter literal used in requests to pass a text document and a position inside 
     * that document. It is up to the client to decide how a selection is converted into a 
     * position when issuing a request for a text document. The client can for example 
     * honor or ignore the selection direction to make LSP request consistent with features 
     * implemented internally.
    */
    struct TextDocumentPositionParams
    {
        /**
         * The text document.
         */
        TextDocumentIdentifier textDocument;

        /**
         * The position inside the text document.
         */
        Position position;
    };

    bool parseTextDocumentIdentifier(const simodo::variable::Value & textDocument_value, 
                                     TextDocumentIdentifier & textDocument);
    bool parsePosition(const simodo::variable::Value & position_value, Position & position);
    bool parseTextDocumentPositionParams(const simodo::variable::Value & params, 
                                     TextDocumentPositionParams & documentPosition);
}
#endif // TextDocumentPositionParams_h