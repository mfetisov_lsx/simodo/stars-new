#ifndef ClientParams_h
#define ClientParams_h

#include "simodo/variable/Variable.h"

#include <set>

class ServerContext;

namespace simodo::lsp
{
    // struct WorkDoneProgressParams
    // {
    //     workDoneToken
    // };

    enum class TraceValue
    {
        off,
        messages,
        verbose
    };

    /**
     * Describes the content type that a client supports in various
     * result literals like `Hover`, `ParameterInfo` or `CompletionItem`.
     *
     * Please note that `MarkupKinds` must not start with a `$`. This kinds
     * are reserved for internal usage.
     */
    typedef std::set<std::string> MarkupKinds;
    // export namespace MarkupKind {
    //     /**
    //      * Plain text is supported as a content format
    //      */
    //     export const PlainText: 'plaintext' = 'plaintext';

    //     /**
    //      * Markdown is supported as a content format
    //      */
    //     export const Markdown: 'markdown' = 'markdown';
    // }
    // export type MarkupKind = 'plaintext' | 'markdown';

    struct HoverClientCapabilities
    {
        /**
         * Whether hover supports dynamic registration.
         */
        bool dynamicRegistration = false;

        /**
         * Client supports the follow content formats if the content
         * property refers to a `literal of type MarkupContent`.
         * The order describes the preferred format of the client.
         */
        MarkupKinds contentFormat;

    };

    struct TextDocument
    {
        /**
         * Capabilities specific to the `textDocument/hover` request.
         */
        HoverClientCapabilities hover;
    };

    struct Capabilities
    {
        /**
         * Text document specific client capabilities.
         */
        TextDocument doc;
    };

    struct ClientParams
    {
        /**
         * The process Id of the parent process that started the server. Is null if
         * the process has not been started by another process. If the parent
         * process is not alive then the server should exit (see exit notification)
         * its process.
         */
        int processId = -1;

        /**
         * The locale the client is currently showing the user interface
         * in. This must not necessarily be the locale of the operating
         * system.
         *
         * Uses IETF language tags as the value's syntax
         * (See https://en.wikipedia.org/wiki/IETF_language_tag)
         *
         * @since 3.16.0
         */
        std::string locale;

        /**
         * The rootPath of the workspace. Is null
         * if no folder is open.
         *
         * @deprecated in favour of `rootUri`.
         */
        std::string rootPath;

        /**
         * The rootUri of the workspace. Is null if no
         * folder is open. If both `rootPath` and `rootUri` are set
         * `rootUri` wins.
         *
         * @deprecated in favour of `workspaceFolders`
         */
        std::string rootUri;

        /**
         * The capabilities provided by the client (editor or tool)
         */
        Capabilities cap;

        /**
         * The initial trace setting. If omitted trace is disabled ('off').
         */
        TraceValue trace = TraceValue::off;
    };

    ClientParams parseClientParams(std::shared_ptr<simodo::variable::Record> params_object);
    Capabilities parseClientCapabilities(std::shared_ptr<simodo::variable::Record> cap_object);
    TextDocument parseTextDocument(std::shared_ptr<simodo::variable::Record> doc_object);
    HoverClientCapabilities parseHoverClientCapabilities(std::shared_ptr<simodo::variable::Record> hover_object);

}
#endif // ClientParams_h