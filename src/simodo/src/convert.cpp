/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <locale>
#include <codecvt>

#include "simodo/convert.h"

using namespace std;
using namespace simodo;

u16string simodo::convertToU16(const string &str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> convert("(*сбой*)",u"(*сбой*)");

    return convert.from_bytes(str);
}

string simodo::convertToU8(const u16string &str)
{
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>,char16_t> convert("(*сбой*)",u"(*сбой*)");

    return convert.to_bytes(str);
}

string simodo::clearNumberFractionalPart(string s)
{
    auto dot_pos = s.find_first_of('.');

    if (dot_pos == string::npos)
        dot_pos = s.find_first_of(',');

    if (dot_pos != string::npos)
    {
        auto not_0_pos = s.find_last_not_of('0');
        if (not_0_pos != string::npos)
            s = s.substr(0,not_0_pos + ((not_0_pos == dot_pos) ? 2 : 1));
    }

    return s;
}

