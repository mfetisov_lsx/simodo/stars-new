/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <cassert>
#include <cmath>

#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/Exception.h"

#include "simodo/convert.h"

//#include "simodo/dsl/ode_int.h"
#include <boost/numeric/odeint.hpp>

#include <chrono>
#include <ratio>
#include <thread>

using namespace std;
using namespace simodo::dsl;

namespace
{
    bool setT(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!sci_stack.empty());

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        scene->setT(get<double>(get<SCI_Scalar>(sci_stack.back().bulk).variant));

        sci_stack.pop();

        return true;
    }

    bool t(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, scene->t()}});

        return true;
    }

    bool setTk(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!sci_stack.empty());

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        scene->setTk(get<double>(get<SCI_Scalar>(sci_stack.back().bulk).variant));

        sci_stack.pop();

        return true;
    }

    bool tk(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, scene->tk()}});

        return true;
    }

    bool setDt(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!sci_stack.empty());

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        scene->setDt(get<double>(get<SCI_Scalar>(sci_stack.back().bulk).variant));

        sci_stack.pop();

        return true;
    }

    bool dt(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, scene->dt()}});

        return true;
    }

    bool setIterationCallback(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(sci_stack.size() >= 2);

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        scene->setIterationCallbackPeriod(get<int64_t>(get<SCI_Scalar>(sci_stack.back().bulk).variant));

        const SCI_Name & function = sci_stack.top(1);
        assert(!get<SCI_Tuple>(function.bulk).empty());
        const SCI_Name & function_ref = get<SCI_Tuple>(function.bulk).front();

        scene->setIterationCallbackFunctionBody(get<const AstNode *>(get<SCI_Scalar>(function_ref.bulk).variant));

        sci_stack.pop(2);

        return true;
    }

    bool setEachIterationCallback(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(sci_stack.size() >= 1);

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        const SCI_Name & function = sci_stack.back();
        assert(!get<SCI_Tuple>(function.bulk).empty());
        const SCI_Name & function_ref = get<SCI_Tuple>(function.bulk).front();

        scene->setEachIterationCallbackFunctionBody(get<const AstNode *>(get<SCI_Scalar>(function_ref.bulk).variant));

        sci_stack.pop(1);

        return true;
    }

    bool setRealtimeModeEnabled(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!sci_stack.empty());

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        scene->setRealtimeModeEnabled(get<bool>(get<SCI_Scalar>(sci_stack.back().bulk).variant));

        sci_stack.pop();

        return true;
    }

    bool realtimeModeEnabled(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        sci_stack.push({u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, scene->realtimeModeEnabled()}});

        return true;
    }

    bool add(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        assert(!sci_stack.empty());

        SCI_Name * obj = &sci_stack.top();

        while(obj->qualification == SemanticNameQualification::Reference)
            obj = get<SCI_Reference>(obj->bulk);

        scene->addObject(*obj);

        sci_stack.pop();

        return true;
    }

    bool removeObject(IScriptC_Namespace * p_object, SCI_Stack &sci_stack)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        assert(!sci_stack.empty());

        SCI_Name * obj = &sci_stack.top();

        while(obj->qualification == SemanticNameQualification::Reference)
            obj = get<SCI_Reference>(obj->bulk);

        scene->removeObject(*obj);

        sci_stack.pop();

        return true;
    }

    bool start(IScriptC_Namespace * p_object, SCI_Stack &)
    {
        if (p_object == nullptr)
            return false;

        ScriptC_NS_Scene * scene = static_cast<ScriptC_NS_Scene *>(p_object);

        scene->start();

        return true;
    }

}


ScriptC_NS_Scene::ScriptC_NS_Scene(ScriptC_Interpreter *p_interpreter, bool time_output_enabled)
    : _p_interpreter(p_interpreter)
    , _t(0.0)
    , _tk(0.0)
    , _dt(1.0)
    , _p_iteration_callback_function_body(nullptr)
    , _p_each_iteration_callback_function_body(nullptr)
    , _iteration_callback_period(0)
    , _realtime_mode_enabled(false)
    , _time_output_enabled(time_output_enabled)
{
}

ScriptC_NS_Scene::~ScriptC_NS_Scene()
{
}

SCI_Namespace_t ScriptC_NS_Scene::getNamespace()
{
    return {
        {u"setT", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setT}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_t_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"t", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::t}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"setTk", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setTk}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_t_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"tk", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::tk}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"setDt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setDt}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_dt_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"dt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::dt}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}}
        }},
        {u"setIterationCallback", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setIterationCallback}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_function_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction, {}}},
            {u"_period_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"setEachIterationCallback", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setEachIterationCallback}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_function_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction, {}}},
        }},
        {u"setRealtimeModeEnabled", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setRealtimeModeEnabled}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_enabled_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}}
        }},
        {u"realtimeModeEnabled", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::realtimeModeEnabled}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}}
        }},
        {u"add", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::add}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"remove", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::removeObject}}},
            {u"", SemanticNameQualification::None, {}},
            {u"_object_", SemanticNameQualification::Tuple, SCI_Tuple {}},
        }},
        {u"start", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::start}}},
            {u"", SemanticNameQualification::None, {}},
        }},
    };
}

void ScriptC_NS_Scene::addObject(SCI_Name &object)
{
    for(const auto & s : _actor_set)
        if (&object == s.p_object)
            throw Exception("addObject", "Повторное добавление объекта '" + simodo::convertToU8(object.name) + "'");

    vector<SCI_Name *> p_dxdt;
    vector<SCI_Name *> p_x;
    const AstNode *    p_equation_function_body = nullptr;
    const AstNode *    p_diff_function_body     = nullptr;
    SCI_Tuple &        tuple                    = (SCI_Tuple &) get<SCI_Tuple>(object.bulk);

    for(SCI_Name & ndxdt : tuple)
    {
        if (ndxdt.name.substr(0,5) == u"__dt_")
        {
            u16string   x_name = ndxdt.name.substr(5);
            SCI_Tuple & tuple  = get<SCI_Tuple>(object.bulk);
            size_t      i      = 0;

            for(; i < tuple.size(); ++i)
            {
                if (tuple[i].name == x_name)
                    break;
            }

            if (i == tuple.size())
                throw Exception("addObject", "Некорректная струтура объекта, переменная '" + simodo::convertToU8(x_name) + "'");

            SCI_Name & nx = tuple[i];

//            const SCI_Scalar & sc_x = get<SCI_Scalar>(nx.bulk);
//            double d = get<double>(sc_x.variant);

            p_dxdt.push_back(&ndxdt);
            p_x.push_back(&nx);

            continue;
        }

        if (ndxdt.name == u"__E")
        {
            const SCI_Tuple & function_parameters = get<SCI_Tuple>(ndxdt.bulk);
            assert(!function_parameters.empty());

            p_equation_function_body = get<const AstNode *>(get<SCI_Scalar>(function_parameters.front().bulk).variant);
        }

        if (ndxdt.name == u"__ODE")
        {
            const SCI_Tuple & function_parameters = get<SCI_Tuple>(ndxdt.bulk);
            assert(!function_parameters.empty());

            p_diff_function_body = get<const AstNode *>(get<SCI_Scalar>(function_parameters.front().bulk).variant);
        }
    }

    // if (p_dxdt.empty() || p_diff_function_body == nullptr)
    //     throw Exception("addObject", "Неполная струтура объекта");

    addActor({&object, p_equation_function_body, p_diff_function_body, p_dxdt, p_x});
}

void ScriptC_NS_Scene::removeObject(SCI_Name &object)
{
    auto a_it = find_if(_actor_set.begin()
                        , _actor_set.end()
                        , [&object](const Actor & a) -> bool { return a.p_object == &object; }
                        );
    if (a_it != _actor_set.end())
    {
        _actor_set.erase(a_it);
    }
}

OdeSystem::OdeSystem(ScriptC_Interpreter * interpreter, ScriptC_NS_Scene & scene, Actor & actor)
    : p_interpreter(interpreter)
    , scene(&scene)
    , actor(actor)
{
    state.resize(actor.x_p.size());
    for(size_t i = 0; i < state.size(); ++i)
    {
        state[i] = get<double>(get<SCI_Scalar>(actor.x_p[i]->bulk).variant);
    }
}

static void callFunction(ScriptC_Interpreter * interpreter
                        , const AstNode * function_body
                        , SCI_Name * parent = nullptr
                        , bool is_construct = false
                        )
{
    auto result = interpreter->callInnerFunction( *function_body
                                                , interpreter->stack_size()
                                                , parent
                                                , is_construct
                                                );
    if (result != SCI_RunnerCondition::Regular
        && result != SCI_RunnerCondition::Return)
    {
        throw Exception("scene", "Вызов функции завершился ошибкой");
    }
}

void OdeSystem::operator() (const state_type & x, state_type & dxdt, double t)
{
    for(size_t i=0; i < actor.x_p.size(); ++i)
        get<SCI_Scalar>(actor.x_p[i]->bulk).variant = x[i];

    double old_t = scene->t();
    scene->setT(t);

    if (actor.p_diff_function_body != nullptr)
        callFunction( p_interpreter
                    , actor.p_diff_function_body
                    , actor.p_object
                    );

    scene->setT(old_t);

    for(size_t i=0; i < actor.dxdt_p.size(); ++i)
        dxdt[i] = get<double>(get<SCI_Scalar>(actor.dxdt_p[i]->bulk).variant);
}

class SleepIterator
{
public:
    SleepIterator()
        : _sleep_debt(0)
    {}

    void begin()
    {
        _iteration_begin_time = std::chrono::steady_clock::now();
    }

    void iterate(double step)
    {
        const auto iteration_end_time = std::chrono::steady_clock::now();

        const auto elapsed_micros = std::chrono::duration_cast<std::chrono::microseconds>(iteration_end_time - _iteration_begin_time);
        _sleep_debt += std::chrono::microseconds(static_cast<int64_t>(step * 1000 * 1000)) - elapsed_micros;

        if (_sleep_debt.count() > 0)
        {
            std::this_thread::sleep_for(_sleep_debt);
            const auto after_sleep_time = std::chrono::steady_clock::now();
            _sleep_debt -= std::chrono::duration_cast<std::chrono::microseconds>(after_sleep_time - iteration_end_time);
        }
    }

private:
    std::chrono::steady_clock::time_point _iteration_begin_time;
    std::chrono::microseconds _sleep_debt;
};

void ScriptC_NS_Scene::start()
{
    if (_p_interpreter == nullptr)
        return;

    SleepIterator sleep_iterator;

    {
        if (_realtime_mode_enabled)
        {
            sleep_iterator.begin();
        }

        if (_p_interpreter && _time_output_enabled)
            _p_interpreter->reporter().reportInformation(u"#Time:"s + convertToU16(to_string(_t)));

        if (_p_iteration_callback_function_body != nullptr)
            callFunction( _p_interpreter
                        , _p_iteration_callback_function_body
                        );
        if (_p_each_iteration_callback_function_body != nullptr)
            callFunction( _p_interpreter
                        , _p_each_iteration_callback_function_body
                        );
        
        if (_realtime_mode_enabled)
        {
            sleep_iterator.iterate(_dt);
        }
    }
//    runge_kutta4<state_type> stepper;

    size_t period_count = 0;

    double t0 = _t;

    for(size_t n=1; !_p_interpreter->stop_signal(); ++n)
    {
        if (_realtime_mode_enabled)
        {
            sleep_iterator.begin();
        }

        _t = t0 + n*_dt;

        if (_tk != 0.0 && _t > _tk)
            break;

        for(Actor & a : _actor_set)
        {
            if (a.p_equation_function_body != nullptr)
            {
                callFunction( _p_interpreter
                            , a.p_equation_function_body
                            , a.p_object
                            );
            }

            if (a.p_diff_function_body != nullptr)
            {
                OdeSystem s(_p_interpreter, *this, a);
                boost::numeric::odeint::integrate(s, s.state, t0 + (n - 1) * _dt, _t, _dt);
                for(size_t i = 0; i < s.state.size(); ++i)
                {
                    get<SCI_Scalar>(s.actor.x_p[i]->bulk).variant = s.state[i];
                }
            }
        }

        period_count ++;

        if (_p_each_iteration_callback_function_body != nullptr)
            callFunction( _p_interpreter
                        , _p_each_iteration_callback_function_body
                        );

        if (period_count >= _iteration_callback_period && _p_iteration_callback_function_body != nullptr)
        {
            if (_p_interpreter && _time_output_enabled)
                _p_interpreter->reporter().reportInformation(u"#Time:"s + convertToU16(to_string(_t)));

            callFunction( _p_interpreter
                        , _p_iteration_callback_function_body
                        );
            period_count = 0;
        }
        
        if (_realtime_mode_enabled)
        {
            sleep_iterator.iterate(_dt);
        }
    }
}
