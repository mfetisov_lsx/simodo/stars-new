/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <cassert>

// #include <iostream>

#include "simodo/dsl/SemanticNameTable.h"
// #include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

SemanticNameTable::SemanticNameTable(AReporter &m,
                                     std::vector<SemanticName> & name_set,
                                     std::vector<SemanticScope> &scope_set,
                                     bool is_module_loading)
    : _m(m)
    , _name_set(name_set)
    , _scope_set(scope_set)
    , _is_module_loading(is_module_loading)
{
    _name_set.reserve(8);
    _scope_set.reserve(8);
    _work_name_stack.reserve(8);
}

std::vector<size_t> SemanticNameTable::buildElementSet(size_t owner) const
{
    vector<size_t> element_set;

    for(size_t i=owner+1; i < _name_set.size(); ++i)
        if (_name_set[i].owner == owner)
            element_set.push_back(i);

    return element_set;
}

void SemanticNameTable::importNamespace(u16string name, const SCI_Namespace_t &ns)
{
    SemanticName root_name(TokenLocation(u""), name, SemanticNameQualification::Tuple, SemanticNameType::Undefined);
    root_name.access = SemanticNameAccess::ReadOnly;

    size_t root_index = addNameDeclaration(root_name);

    for(const auto & n : ns)
        addNameFromNamespace(root_index,n);
}

size_t SemanticNameTable::addNameDeclaration(const SemanticName & name)
{
    assert(!_work_name_stack.empty());

    // cerr << "\taddNameDeclaration in depth " << _work_name_stack.size()-1
    //      << ": '" << convertToU8(name.name.getLexeme())
    //      << "' from '" << convertToU8(name.name.getLocation().file_name)
    //      << "' [" << name.name.getLocation().line << "," << name.name.getLocation().column << "]"
    //      << endl;

    size_t name_index = _name_set.size();
    _name_set.push_back(name);
    _work_name_stack.back().push_back(name_index);

    return name_index;
}

void SemanticNameTable::addNameDefinition(size_t index, const TokenLocation &location)
{
    assert(index < _name_set.size());

    _name_set[index].definition = location;
}

void SemanticNameTable::addNameReference(size_t index, const TokenLocation &location)
{
    if (index >= _name_set.size())
        return;

    size_t i = index;

    // while(_name_set[i].owner != UNDEFINED_INDEX)
    // {
    //     assert(_name_set[i].owner < _name_set.size());

    //     if (_name_set[_name_set[i].owner].qualification == SemanticNameQualification::Function)
    //         break;

    //     /// \todo test for owner looping occurred
    //     i = _name_set[i].owner;
    // }

    _name_set[i].references.push_back(location);
}

void SemanticNameTable::addNameType(size_t index, SemanticNameType type)
{
    assert(index < _name_set.size());

    _name_set[index].type = type;
}

void SemanticNameTable::assignChecked(size_t index)
{
    assert(index < _name_set.size());

    if ((_name_set[index].semantic_flags & SemanticFlag_InputForced) != SemanticFlag_InputForced)
        _name_set[index].semantic_flags = 0;
}

void simodo::dsl::SemanticNameTable::forceInputFlag(size_t index)
{
    assert(index < _name_set.size());

    _name_set[index].semantic_flags = SemanticFlag_Input | SemanticFlag_InputForced;
}

void SemanticNameTable::openScope(const TokenLocation &location)
{
    // cerr << "openScope " << _work_name_stack.size() << " file '" << convertToU8(location.file_name)
    //      << "', [" << location.line << "," << location.column << "]"
    //      << endl;

    _work_name_stack.push_back(std::vector<size_t>());
    _work_name_stack.back().reserve(500);
    _work_scope_stack.push_back(location);
}

void SemanticNameTable::closeScope(const AstNode &node)
{
    assert(!_work_name_stack.empty());
    assert(!_work_scope_stack.empty());

    const AstNode * n = & node;
    while(!n->getBranchC().empty())
        n = & n->getBranchC().back();

    const TokenLocation & location = n->getSymbol().getLocation();

    for(size_t name_index : _work_name_stack.back())
    {
        assert(name_index < _name_set.size());

        SemanticName & name = _name_set[name_index];

        name.lower_scope = location;
        name.depth = getDepth();

        // if (name.depth != DEPTH_TOP_LEVEL)
        //     name.semantic_flags = 0;

        if (name.references.empty())
        {
            /// @todo Remove it!
            // if (name.name.getLexeme().substr(0,2) == u"__") // Сгенерированные имена
            //    continue;

            if (_is_module_loading && DEPTH_TOP_LEVEL == _work_name_stack.size()) // Файл загружен в качестве типа (use as type)
                continue;

            if (name.qualification == SemanticNameQualification::Type)
                continue;

            if (name.name.getLocation().file_name.empty()) // глобальные модули
                continue;

            if (name.name.getLexeme().empty())  // возвращаемое значение функции
                continue;

            size_t owner = name.owner;

            if (name.owner != UNDEFINED_INDEX && _name_set[owner].qualification != SemanticNameQualification::Function)
                continue;

            u16string str = u"";

            switch(name.qualification)
            {
            case SemanticNameQualification::Function:
                if (name.depth == DEPTH_TOP_LEVEL)
                    name.semantic_flags |= SemanticFlag_Method;
                else
                    str = u"Функция '" + name.name.getLexeme() + u"' не используется";
                break;
            case SemanticNameQualification::Scalar:
            {
                if (name.owner != UNDEFINED_INDEX)
                {
                    assert(name.owner < _name_set.size());

                    if (_name_set[name.owner].qualification == SemanticNameQualification::Function
                     && !_name_set[name.owner].definition.file_name.empty())
                    {
                        str = u"Параметр '" + name.name.getLexeme()
                            + u"' функции '" + _name_set[name.owner].name.getLexeme()
                            + u"' не используется";
                    }
                }
                else if (name.depth == DEPTH_TOP_LEVEL)
                    name.semantic_flags |= SemanticFlag_Output;
                else
                    str = u"Переменная '" + name.name.getLexeme() + u"' не используется";
                break;
            }
            default:
                if (name.depth == DEPTH_TOP_LEVEL)
                    name.semantic_flags |= SemanticFlag_Output;
                else
                    str = u"Имя '" + name.name.getLexeme() + u"' не используется";
                break;
            }

            if (!str.empty())
                _m.reportWarning(name.name, str);
        }
    }

    // cerr << "closeScope " << _work_name_stack.size()-1 << " file '" << convertToU8(location.file_name)
    //      << "', [" << location.line << "," << location.column 
    //      << "], size: " << _work_name_stack[_work_name_stack.size()-1].size() 
    //      << endl;

    _scope_set.push_back({_work_scope_stack.back(),location});
    _work_scope_stack.pop_back();
    _work_name_stack.pop_back();
}

void SemanticNameTable::closeScope(const TokenLocation &location)
{
    assert(!_work_name_stack.empty());
    assert(!_work_scope_stack.empty());

    _scope_set.push_back({_work_scope_stack.back(),location});
    _work_scope_stack.pop_back();
    _work_name_stack.pop_back();
}

size_t SemanticNameTable::findName(const u16string &name, size_t owner) const
{
    if (owner != UNDEFINED_INDEX)
    {
        assert(owner < _name_set.size());

        size_t i = 0;

        for(; i < _name_set.size(); ++i)
            if( owner == _name_set[i].owner && name == _name_set[i].name.getLexeme())
                break;

        return (i == _name_set.size()) ? NOT_FOUND_INDEX : i;
    }

    assert(!_work_name_stack.empty());

    for(size_t i=_work_name_stack.size()-1; i < _work_name_stack.size(); --i)
    {
        for(size_t j=_work_name_stack[i].size()-1; j < _work_name_stack[i].size(); --j)
        {
            size_t index = _work_name_stack[i][j];

            assert(index < _name_set.size());

            if (_name_set[index].name.getLexeme() == name)
            {
                size_t owner = _name_set[index].owner;

                if (owner == NOT_FOUND_INDEX)
                    return index;

                assert(owner < _name_set.size());

                /// \todo Не корректно определяет переменную, если есть функция с таким же именем параметра
                /// (как минимум в модуле, загруженном через use as type)
                if (_name_set[owner].qualification == SemanticNameQualification::Function)
                    return index;
            }
        }
    }

    return NOT_FOUND_INDEX;
}

size_t SemanticNameTable::findNameOnTop(const u16string &name) const
{
    assert(!_work_name_stack.empty());

    for(size_t j=_work_name_stack.back().size()-1; j < _work_name_stack.back().size(); --j)
    {
        size_t index = _work_name_stack.back()[j];

        assert(index < _name_set.size());

        if (_name_set[index].owner == UNDEFINED_INDEX
         && _name_set[index].name.getLexeme() == name)
            return index;
    }

    return NOT_FOUND_INDEX;
}

uint16_t SemanticNameTable::getDepth() const
{
    return _work_name_stack.size();
}

void SemanticNameTable::addNameFromNamespace(size_t parent_index, const SCI_Name &name_structure)
{
    SemanticName name(TokenLocation(u""),
                      name_structure.name,
                      name_structure.qualification,
                      (name_structure.qualification == SemanticNameQualification::Scalar)
                        ? get<SCI_Scalar>(name_structure.bulk).type
                        : SemanticNameType::Undefined);

    name.access = name_structure.access;
    name.owner = parent_index;

    size_t name_index = addNameDeclaration(name);

    if (name_structure.qualification == SemanticNameQualification::Tuple ||
        name_structure.qualification == SemanticNameQualification::Function ||
        name_structure.qualification == SemanticNameQualification::Type)
    {
        const SCI_Tuple & tuple = get<SCI_Tuple>(name_structure.bulk);

        for(const auto & n : tuple)
            if (name_structure.qualification == SemanticNameQualification::Function && n.name == u"@")
                _name_set[name_index].type = get<SCI_Scalar>(n.bulk).type;
            else
                addNameFromNamespace(name_index,n);
    }
}

