/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Remote_NS_Graph3D.h"

#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string title = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);

        Remote_NS_Graph3D * graph3d = static_cast<Remote_NS_Graph3D *>(p_object);

        graph3d->listener().reportInformation(u"#Values:Graph3D.0.S.init:" + title);

        stack.pop();

        return true;
    }

    bool addSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        int64_t series_mesh = get<int64_t>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        Remote_NS_Graph3D * graph3d = static_cast<Remote_NS_Graph3D *>(p_object);

        graph3d->listener().reportInformation(u"#Values:Graph3D.0.S.addSeries:" + series_name + 
                                            u"/" + convertToU16(to_string(series_mesh)));
        stack.pop(2);

        return true;
    }

    bool addPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 4);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(3).bulk).variant);

        const SCI_Name &x_value = stack.top(2);
        double x = get<double>(get<SCI_Scalar>(x_value.bulk).variant);

        const SCI_Name &y_value = stack.top(1);
        double y = get<double>(get<SCI_Scalar>(y_value.bulk).variant);

        const SCI_Name &z_value = stack.top(0);
        double z = get<double>(get<SCI_Scalar>(z_value.bulk).variant);

        Remote_NS_Graph3D * graph3d = static_cast<Remote_NS_Graph3D *>(p_object);

        graph3d->listener().reportInformation(u"#Values:Graph3D.0.U.addPoint:" + series_name + 
                                            u"/" + convertToU16(to_string(x)) + 
                                            u"/" + convertToU16(to_string(y)) + 
                                            u"/" + convertToU16(to_string(z)));
        stack.pop(4);

        return true;
    }

    bool setPointsCount(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        int64_t points_count = get<int64_t>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        Remote_NS_Graph3D * graph3d = static_cast<Remote_NS_Graph3D *>(p_object);

        graph3d->listener().reportInformation(u"#Values:Graph3D.0.S.setPointsCount:" + series_name + 
                                            u"/" + convertToU16(to_string(points_count)));
        stack.pop(2);

        return true;
    }
}

Remote_NS_Graph3D::Remote_NS_Graph3D(AReporter &listener)
    : _listener(listener)
{
}

Remote_NS_Graph3D::~Remote_NS_Graph3D()
{
}

SCI_Namespace_t Remote_NS_Graph3D::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addSeries}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"series_mesh", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addPoint}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setPointsCount", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPointsCount}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"points_count", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"Mesh", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Bar", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
            {u"Cube", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
            {u"Pyramid", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(3)}},
            {u"Cone", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(4)}},
            {u"Cylinder", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(5)}},
            {u"BevelBar", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(6)}},
            {u"BevelCube", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(7)}},
            {u"Sphere", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(8)}},
            {u"Minimal", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(9)}},
            {u"Arrow", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(10)}},
            {u"Point", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(11)}},
        }},
    };
}


