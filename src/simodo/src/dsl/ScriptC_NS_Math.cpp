/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>
#include <algorithm>
#include <cmath>
#include <random>

#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/Exception.h"

#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

namespace
{
    const SCI_Variant_t & getVariant(const SCI_Name & n)
    {
        return get<SCI_Scalar>(n.bulk).variant;
    }

    template <typename T>
    T getScalar(const SCI_Name & n)
    {
        return get<T>(getVariant(n));
    }

    template <typename T>
    void stackPush(SCI_Stack &, T);

    template <>
    void stackPush(SCI_Stack & s, int64_t val)
    {
        s.push({u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar{SemanticNameType::Int, val}
            });
    }

    template <>
    void stackPush(SCI_Stack & s, double val)
    {
        s.push({u""
            , SemanticNameQualification::Scalar
            , SCI_Scalar{SemanticNameType::Float, val}
            });
    }

    bool SCI_sin(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = sin(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_cos(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = cos(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_tan(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = tan(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_asin(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = asin(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_acos(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = acos(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_atan(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = atan(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_sqrt(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = sqrt(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_exp(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = exp(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_ln(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = log(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_tf1(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        SCI_Name * obj = &sci_stack.top(1);

        while(obj->qualification == SemanticNameQualification::Reference)
            obj = get<SCI_Reference>(obj->bulk);

        const SCI_Tuple & tuple = get<SCI_Tuple>(obj->bulk);
        double            x     = get<double>(get<SCI_Scalar>(sci_stack.top(0).bulk).variant);

        const SCI_Name * args_name = nullptr;
        const SCI_Name * func_name = nullptr;

        for(const SCI_Name & n : tuple)
            if (n.name == u"args")
                args_name = &n;
            else if (n.name == u"func")
                func_name = &n;

        if (args_name == nullptr || args_name->qualification != SemanticNameQualification::Array)
            throw Exception("SCI_tf1",
                            "Кортеж '" + simodo::convertToU8(sci_stack.top(1).name) +
                            "' не содержит массив с именем 'args'");

        if (func_name == nullptr || func_name->qualification != SemanticNameQualification::Array)
            throw Exception("SCI_tf1",
                            "Кортеж '" + simodo::convertToU8(sci_stack.top(1).name) +
                            "' не содержит массив с именем 'func'");

        const SCI_Array & args_array = get<SCI_Array>(args_name->bulk);
        const SCI_Array & func_array = get<SCI_Array>(func_name->bulk);

        if (args_array.dimensions.size() != 1 || func_array.dimensions.size() != 1 ||
            args_array.values.size() != func_array.values.size() || args_array.values.size() == 0)
            throw Exception("SCI_tf1",
                            "Массивы 'args' и 'func' кортежа'" + simodo::convertToU8(sci_stack.top(1).name) +
                            "' должны иметь одну размерность одинакового не нулевого размера");

        double f = 0;

        if (x < get<double>(get<SCI_Scalar>(args_array.values.front().bulk).variant))
            f = get<double>(get<SCI_Scalar>(func_array.values.front().bulk).variant);
        else
        {
            size_t i = 1;
            for(; i < args_array.values.size(); ++i)
                if (x < get<double>(get<SCI_Scalar>(args_array.values[i].bulk).variant))
                {
                    double x1 = get<double>(get<SCI_Scalar>(args_array.values[i-1].bulk).variant);
                    double x2 = get<double>(get<SCI_Scalar>(args_array.values[i].bulk).variant);
                    double f1 = get<double>(get<SCI_Scalar>(func_array.values[i-1].bulk).variant);
                    double f2 = get<double>(get<SCI_Scalar>(func_array.values[i].bulk).variant);

                    f = f1 + (f2-f1) * (x-x1) / (x2-x1);

                    break;
                }

            if (i == args_array.values.size())
                f = get<double>(get<SCI_Scalar>(func_array.values.back().bulk).variant);
        }

        sci_stack.pop(2);
        sci_stack.push({u"", SemanticNameQualification::Scalar, SCI_Scalar{SemanticNameType::Float, f}});

        return true;
    }

    bool SCI_random_float(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto min = getScalar<double>(sci_stack.top(1));
        auto max = getScalar<double>(sci_stack.top(0));

        thread_local std::mt19937 random_float_generator((std::random_device())());
        std::uniform_real_distribution<double> distributor(min, max);

        sci_stack.pop(2);
        auto val = double(distributor(random_float_generator));
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_random_int(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto min = getScalar<int64_t>(sci_stack.top(1));
        auto max = getScalar<int64_t>(sci_stack.top(0));

        thread_local std::mt19937 random_float_generator((std::random_device())());
        std::uniform_int_distribution<int64_t> distributor(min, max);

        sci_stack.pop(2);
        auto val = int64_t(distributor(random_float_generator));
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_abs(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = abs(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_absInt(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = abs(getScalar<int64_t>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_atan2(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto y = getScalar<double>(sci_stack.top(1));
        auto x = getScalar<double>(sci_stack.top(0));

        auto val = atan2(y, x);
        sci_stack.pop(2);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_sign(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = getScalar<double>(sci_stack.back());
        val = signbit(val) ? -1.0 : 1.0;
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_signInt(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = getScalar<int64_t>(sci_stack.back());
        val = signbit(val) ? -1 : 1;
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_limit(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 3);

        auto val = getScalar<double>(sci_stack.top(2));
        auto min = getScalar<double>(sci_stack.top(1));
        auto max = getScalar<double>(sci_stack.top(0));

        if (max < min)
        {
            throw Exception("math.limit", "max < min : " + to_string(max) + " < " + to_string(min));
        }

        val = clamp(val, min, max);
        sci_stack.pop(3);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_limitInt(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 3);

        auto val = getScalar<int64_t>(sci_stack.top(2));
        auto min = getScalar<int64_t>(sci_stack.top(1));
        auto max = getScalar<int64_t>(sci_stack.top(0));

        if (max < min)
        {
            throw Exception("math.limit", "max < min : " + to_string(max) + " < " + to_string(min));
        }

        val = clamp(val, min, max);
        sci_stack.pop(3);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_round(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = round(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_floor(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = floor(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_trunc(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = trunc(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_toFloat(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = double(getScalar<int64_t>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_toInt(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = int64_t(getScalar<double>(sci_stack.back()));
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_pi(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        stackPush<double>(sci_stack, M_PI);
        return true;
    }

    bool SCI_e(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        stackPush<double>(sci_stack, M_E);
        return true;
    }

    bool SCI_toRad(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = 0.01745329251 * getScalar<double>(sci_stack.back());
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_toDeg(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = 57.2957795131 * getScalar<double>(sci_stack.back());
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_vec2Len(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 2);

        auto x = getScalar<double>(sci_stack.top(1));
        auto y = getScalar<double>(sci_stack.top(0));

        auto val = sqrt(x * x + y * y);
        sci_stack.pop(2);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_vec3Len(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(sci_stack.size() >= 3);

        auto x = getScalar<double>(sci_stack.top(2));
        auto y = getScalar<double>(sci_stack.top(1));
        auto z = getScalar<double>(sci_stack.top(0));

        auto val = sqrt(x * x + y * y + z * z);
        sci_stack.pop(3);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_notZero(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = getScalar<double>(sci_stack.back());
        val = (val == 0.0)
                ? numeric_limits<double>().epsilon()
                : val;
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_toZero_2Pi(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = getScalar<double>(sci_stack.back());
        while (val < 0.0)
        {
            val += 2 * M_PI;
        }
        while (2 * M_PI < val)
        {
            val -= 2 * M_PI;
        }
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }

    bool SCI_toMinusPi_Pi(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(!sci_stack.empty());

        auto val = getScalar<double>(sci_stack.back());
        while (val < -M_PI)
        {
            val += 2 * M_PI;
        }
        while (M_PI < val)
        {
            val -= 2 * M_PI;
        }
        sci_stack.pop(1);
        stackPush(sci_stack, val);
        return true;
    }
}


SCI_Namespace_t ScriptC_NS_Math::getNamespace()
{
    return {
        {u"sin", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_sin}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"cos", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_cos}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"tan", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_tan}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"asin", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_asin}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"acos", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_acos}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"atan", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_atan}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"sqrt", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_sqrt}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"exp", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_exp}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"ln", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_ln}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"tf1", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_tf1}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"_tuple_", SemanticNameQualification::None, {}},
            {u"_arg_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"abs", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_abs}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"absInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_absInt}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"sign", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_sign}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"signInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_signInt}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"_value_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"atan2", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_atan2}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_y_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"_x_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"random_int", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_random_int}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"random_float", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_random_float}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"limit", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_limit}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"limitInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_limitInt}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"round", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_round}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"floor", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_floor}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"trunc", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_trunc}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"val", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toFloat", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_toFloat}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"i", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"toInt", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_toInt}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
                {u"f", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"const_pi", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_pi}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"const_e", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_e}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toRad", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_toRad}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"deg", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toDeg", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_toDeg}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"rad", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"vec2Len", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_vec2Len}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"vec3Len", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_vec3Len}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"y", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"notZero", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_notZero}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toMinusPi_Pi", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_toMinusPi_Pi}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"Zero_2Pi", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"toZero_2Pi", SemanticNameQualification::Function, SCI_Tuple {
                {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, SCI_toZero_2Pi}}},
                {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
                {u"MinusPi_Pi", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
    };
}
