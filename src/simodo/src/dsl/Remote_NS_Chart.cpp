/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Remote_NS_Chart.h"

#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string title = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);

        Remote_NS_Chart * chart = static_cast<Remote_NS_Chart *>(p_object);

        chart->listener().reportInformation(u"#Values:Chart.0.S.init:" + title);

        stack.pop();

        return true;
    }

    bool initFixed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 5);

        u16string title = get<u16string>(get<SCI_Scalar>(stack.top(4).bulk).variant);

        const SCI_Name &x_min_value = stack.top(3);
        double x_min = get<double>(get<SCI_Scalar>(x_min_value.bulk).variant);
        const SCI_Name &x_max_value = stack.top(2);
        double x_max = get<double>(get<SCI_Scalar>(x_max_value.bulk).variant);

        const SCI_Name &y_min_value = stack.top(1);
        double y_min = get<double>(get<SCI_Scalar>(y_min_value.bulk).variant);
        const SCI_Name &y_max_value = stack.top(0);
        double y_max = get<double>(get<SCI_Scalar>(y_max_value.bulk).variant);

        Remote_NS_Chart * chart = static_cast<Remote_NS_Chart *>(p_object);

        chart->listener().reportInformation(
            u"#Values:Chart.0.S.initFixed:" + title
            + u"/" + convertToU16(to_string(x_min))
            + u"/" + convertToU16(to_string(x_max))
            + u"/" + convertToU16(to_string(y_min))
            + u"/" + convertToU16(to_string(y_max))
        );

        stack.pop(3);

        return true;
    }

    bool addSeries(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        u16string series_name  = get<u16string>(get<SCI_Scalar>(stack.top(1).bulk).variant);
        int64_t   series_style = get<int64_t>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        Remote_NS_Chart * chart = static_cast<Remote_NS_Chart *>(p_object);

        chart->listener().reportInformation(u"#Values:Chart.0.S.addSeries:" + series_name + 
                                            u"/" + convertToU16(to_string(series_style)));
        stack.pop(2);

        return true;
    }

    bool addPoint(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        u16string series_name = get<u16string>(get<SCI_Scalar>(stack.top(2).bulk).variant);

        const SCI_Name &x_value = stack.top(1);
        double x = get<double>(get<SCI_Scalar>(x_value.bulk).variant);

        const SCI_Name &y_value = stack.top(0);
        double y = get<double>(get<SCI_Scalar>(y_value.bulk).variant);

        Remote_NS_Chart * chart = static_cast<Remote_NS_Chart *>(p_object);

        chart->listener().reportInformation(u"#Values:Chart.0.U.addPoint:" + series_name + 
                                            u"/" + convertToU16(to_string(x)) + 
                                            u"/" + convertToU16(to_string(y)));
        stack.pop(3);

        return true;
    }

    bool show(IScriptC_Namespace * p_object, SCI_Stack &)
    {
        if (p_object == nullptr)
            return false;

        Remote_NS_Chart * chart = static_cast<Remote_NS_Chart *>(p_object);

        chart->listener().reportInformation(u"#Values:Chart.0.S.show:");

        return true;
    }

}

Remote_NS_Chart::Remote_NS_Chart(AReporter &listener)
    : _listener(listener)
{
}

Remote_NS_Chart::~Remote_NS_Chart()
{
}

SCI_Namespace_t Remote_NS_Chart::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"initFixed", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::initFixed}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"x_min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"x_max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y_min", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"y_max", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"addSeries", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addSeries}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"series_style", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addPoint", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::addPoint}}},
            {u"", SemanticNameQualification::None, {}},
            {u"series_name", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"show", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::show}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"Style", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Line", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Spline", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(1)}},
            {u"Scatter", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(2)}},
        }},
    };
}


