/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/GrammarManagement.h"
#include "simodo/dsl/GrammarLoader.h"

using namespace std;
using namespace simodo::dsl;


bool GrammarManagement::loadGrammar(bool need_force_reload, const string &grammar_name, TableBuildMethod method)
{
    auto it = _grammar_set.find(grammar_name);

    if (it != _grammar_set.end() && !need_force_reload)
        return true;

    Grammar       g;
    GrammarLoader loader(_m, _paths_to_grammars, grammar_name);

    bool ok = loader.load(need_force_reload, g, method);

    if (ok)
    {
        if (it != _grammar_set.end())
            it->second = g;
        else
            _grammar_set.emplace(grammar_name,g);
    }

    return ok;
}

bool GrammarManagement::findGrammar(const string &name) const
{
    return (_grammar_set.find(name) != _grammar_set.end());
}

const Grammar &GrammarManagement::getGrammar(const string &name) const
{
    static Grammar g_;

    auto i = _grammar_set.find(name);

    if (i != _grammar_set.end())
        return i->second;

    return g_;
}

bool GrammarManagement::replaceGrammarPath(const std::vector<std::string> & paths_to_grammars)
{
    _paths_to_grammars = paths_to_grammars;
    return true;
}
