/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/FuzeParser_ScriptC.h"

using namespace std;
using namespace simodo::dsl;

bool FuzeParser_ScriptC::parse(Tokenizer &tzer, Token &t)
{
    // "{" { <вызов> ";" } "}"
    //  ^
    ast().addNode_StepInto(AstOperation::Statement_Block, t, t, DEFAULT_STREAM_NO);

    t = tzer.getToken();

    while(t.getType() != LexemeType::Empty)
    {
        // "{" { <вызов> ";" } "}"
        //                      ^
        if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u"}")
        {
            ast().addNode(AstOperation::None, t, t, DEFAULT_STREAM_NO);
            ast().goParent(DEFAULT_STREAM_NO);

            t = tzer.getToken();
            break;
        }

        // "{" { <вызов> ";" } "}"
        //       ^
        if (!parse_ProcedureCalling(tzer,t))
            return false;

        // "{" { <вызов> ";" } "}"
        //                ^
        if (t.getType() != LexemeType::Punctuation || t.getLexeme() != u";")
        {
            reportUnexpected(t, u"';'");
            return false;
        }

        t = tzer.getToken();
    }

    _ast_helper.drain(_ast);

    return true;
}

bool FuzeParser_ScriptC::parse_ProcedureCalling(Tokenizer &tzer, Token &t)
{
    // <адрес>
    // ^
    if (!parse_Address(tzer,t))
        return false;

    ast().resetOperation_ProcedureCalling(DEFAULT_STREAM_NO);

    return true;
}

bool FuzeParser_ScriptC::parse_Address(Tokenizer &tzer, Token &t)
{
    // ID ["[" NUMBER "]"] | ["(" [<список параметров>] ")"] ["." <адрес>]
    // ^
    if (t.getType() != LexemeType::Id)
    {
        reportUnexpected(t, u"идентификатор");
        return false;
    }

    Token id = t;

    t = tzer.getToken();

    // ID ["[" NUMBER "]"] | ["(" [<список параметров>] ")"] ["." <адрес>]
    //      ^                  ^                               ^
    if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u"[")
    {
        ast().addNode(AstOperation::Push_Id, id, id, DEFAULT_STREAM_NO);
        ast().addNode_Branch_StepInto(AstOperation::Address_Array, t, t, DEFAULT_STREAM_NO);
        ast().goParent(DEFAULT_STREAM_NO);

        t = tzer.getToken();

        // ID "[" NUMBER "]" ["." <адрес>]
        //        ^
        if (t.getType() != LexemeType::Number || t.getQualification() != TokenQualification::Integer)
        {
            reportUnexpected(t, u"целочисленный индекс");
            return false;
        }

        ast().addNode(AstOperation::Push_Constant, t, t, DEFAULT_STREAM_NO);

        t = tzer.getToken();

        // ID "[" NUMBER "]" ["." <адрес>]
        //                ^
        if (t.getType() != LexemeType::Punctuation || t.getLexeme() != u"]")
        {
            reportUnexpected(t, u"']'");
            return false;
        }

        t = tzer.getToken();

        ast().goParent(DEFAULT_STREAM_NO);
    }
    else if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u"(")
    {
        ast().addNode_StepInto(AstOperation::Function_Call, id, id, DEFAULT_STREAM_NO);

        // ID "(" [<список параметров>] ")" ["." <адрес>]
        //     ^
        t = tzer.getToken();

        // ID "(" [<список параметров>] ")" ["." <адрес>]
        //         ^                     ^
        if (t.getType() != LexemeType::Punctuation || t.getLexeme() != u")")
        {
            if (!parse_ArgumentList(tzer,t))
                return false;

            // { ID "(" [<список параметров>] ")" ["." <адрес>] }
            //                                 ^
            if (t.getType() != LexemeType::Punctuation || t.getLexeme() != u")")
            {
                reportUnexpected(t, u"')'");
                return false;
            }
        }

        t = tzer.getToken();

        ast().goParent(DEFAULT_STREAM_NO);
    }
    else
    {
        ast().addNode(AstOperation::Push_Id, id, id, DEFAULT_STREAM_NO);
    }

    // ID ["[" NUMBER "]"] | ["(" [<список параметров>] ")"] ["." <адрес>]
    //                                                         ^
    if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u".")
    {
        ast().addNode_Branch_StepInto(AstOperation::Address_Tuple, t, t, DEFAULT_STREAM_NO);
        ast().goParent(DEFAULT_STREAM_NO);

        t = tzer.getToken();

        // ID ["[" NUMBER "]"] | ["(" [<список параметров>] ")"] ["." <адрес>]
        //                                                            ^
        if (!parse_Address(tzer,t))
            return false;

        ast().goParent(DEFAULT_STREAM_NO);
    }

    return true;
}

bool FuzeParser_ScriptC::parse_ArgumentList(Tokenizer &tzer, Token &t)
{
    while(t.getType() != LexemeType::Empty)
    {
        // { NUMBER | ANNOTATION | "true" | "false" | <адрес> [","] }
        //   ^        ^            ^        ^         ^
        if (t.getType() == LexemeType::Number || t.getType() == LexemeType::Annotation)
        {
            ast().addNode(AstOperation::Push_Constant, t, t, DEFAULT_STREAM_NO);

            t = tzer.getToken();
        }
        else if (t.getType() == LexemeType::Punctuation
             && t.getQualification() == TokenQualification::Keyword
             && (t.getToken() == u"true" || t.getToken() == u"false"))
        {
            ast().addNode(AstOperation::Push_Constant, t, t, DEFAULT_STREAM_NO);

            t = tzer.getToken();
        }
        else if (!parse_Address(tzer,t))
            return false;

        // { NUMBER | ANNOTATION | <адрес> [","] }
        //                                   ^
        if (t.getType() != LexemeType::Punctuation || t.getLexeme() != u",")
            break;

        t = tzer.getToken();
    }

    return true;
}

