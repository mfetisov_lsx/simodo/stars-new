/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>
#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/FuzeParser.h"
#include "simodo/dsl/GrammarBuilder_SLR.h"
#include "simodo/dsl/GrammarBuilder_LR1.h"
#include "simodo/dsl/Parser.h"

#include "simodo/convert.h"
#include "simodo/inout/convert/functions.h"

//#if (__GNUC__ > 7)
#include <filesystem>
//#else
//#include <experimental/filesystem>
//using namespace std::experimental;
//#endif

using namespace std;
using namespace simodo::dsl;

const u16string GRAMMAR_DUMP_SIGNATURE   = u"SIMODO.grammar.dump.v04"s;
const size_t    REASONABLE_STRING_LENGTH = 2000;
const size_t    MAX_STRING_LENGTH        = REASONABLE_STRING_LENGTH * 2;

bool GrammarLoader::load(bool need_force_reload, Grammar &g, TableBuildMethod method, bool strict_rule_consistency)
{
    filesystem::path grammar_path;
    auto             it = _paths.begin();

    for(; it != _paths.end(); ++it) 
    {
        grammar_path = filesystem::path(*it) / (_grammar_name + FUZE_FILE_EXTENSION);

        if (filesystem::exists(grammar_path))
            break;
    }

    if (it == _paths.end())
    {
        _m.reportFatal(u"Файл описания грамматики '" + convertToU16(_grammar_name) + u"' не найден");
        return false;
    }

    // Сначала пробуем загрузить дамп грамматики
    // В дампе хранится метод разбора этой грамматики, он может нам пригодиться

    string           path_to_grammar = *it;
    filesystem::path dump_path = filesystem::path(path_to_grammar) / "dump" / ".";

    if (filesystem::exists(dump_path))
        dump_path = filesystem::path(path_to_grammar) / "dump" / (_grammar_name + DUMP_FILE_EXTENSION);
    else
        dump_path = filesystem::path(path_to_grammar) / (_grammar_name + DUMP_FILE_EXTENSION);

    bool was_dump_loaded;

    if (filesystem::exists(dump_path))
        was_dump_loaded = loadDumpFile(dump_path.string(), g, method);
    else
        was_dump_loaded = false;

    TableBuildMethod prev_method = g.build_method;  // Запоминаем с каким методом работала грамматика

    if (need_force_reload)
        was_dump_loaded = false;
    else if (was_dump_loaded)
    {
        auto fuze_time = filesystem::last_write_time(grammar_path);
        auto dump_time = filesystem::last_write_time(dump_path);

        if (fuze_time >= dump_time)
            was_dump_loaded = false;

        if (method != TableBuildMethod::none && method != prev_method)
            was_dump_loaded = false;
    }

    if (was_dump_loaded)
        return true;

    if (method == TableBuildMethod::none)
        method = prev_method;

    // Чистим грамматику

    g = Grammar();

    // Грузим и парсим грамматику

    // Необходимые переменные
    std::vector<GrammarRuleTokens>  rules;
    Token                           main_rule;
    set<string>                     dummy_import;

    // Грузим правила

    if (!loadRules(path_to_grammar,rules,main_rule,dummy_import,g.handlers,g.lexical))
    {
        _m.reportFatal(u"Не удалось загрузить правила грамматики '" + convertToU16(_grammar_name) + u"'");
        return false;
    }

    // Строим таблицу

    unique_ptr<GrammarBuilder>  builder;

    if (method == TableBuildMethod::SLR)
        builder = make_unique<GrammarBuilder_SLR>(_m, path_to_grammar, _grammar_name, g, strict_rule_consistency);
    else
        builder = make_unique<GrammarBuilder_LR1>(_m, path_to_grammar, _grammar_name, g, strict_rule_consistency);

    if (!builder->build(rules, main_rule))
    {
        _m.reportFatal(u"Не удалось построить таблицу грамматики");
        return false;
    }

    // Проверяем семантику блоков АСД в полученной грамматике

    std::vector<SemanticName>   name_set;
    std::vector<SemanticScope>  scope_set;

    if (!builder->checkGrammarAstBlocks(name_set, scope_set))
    {
        _m.reportFatal(u"Обнаружены ошибки в блоках АСД грамматики");
        return false;
    }

    // Сохраняем дамп грамматики

    if (!storeDumpFile(dump_path.string(),g))
        _m.reportInformation(u"Не удалось сохранить дамп грамматики");

    return true;
}

bool GrammarLoader::loadRules(const std::string &path_to_grammar, std::vector<GrammarRuleTokens> &rules, Token &main_rule, std::set<string> imported_files,
                              std::map<u16string, AstNode> &handlers, LexicalParameters &lex)
{
    filesystem::path grammar_path = filesystem::path(path_to_grammar) / (_grammar_name + META_FILE_EXTENSION);

    if (filesystem::exists(grammar_path))
    {
/*
 * Работа с метаграмматикой не поддерживается до тех пор,
 * пока не будет реализована работа через АСД
 *
        // Работаем с метаграмматикой

        // 1.   Загружаем грамматику описания грамматики
        //      Для этого используем наш же класс для загрузки грамматики грамматик

        Grammar       g;
        GrammarLoader loader(_m, _path, "grammar");

        if (!loader.load(false, g))
            return false;

        // 2.   Выполняем разбор заданной грамматики
        //      При выполнении парсинга будет сформирован набор правил заданной грамматики

        Parser             parser(grammar_path.string(), _m, g);
        AstBuilder_Grammar builder(_m, _path, imported_files, declarations, rules);

        return parser.parse(builder);
*/
        _m.reportFatal(u"Работа с метаграмматикой не поддерживается");
        return false;
    }

    // Работаем с начальной грамматикой

    FuzeParser fuze(_m, path_to_grammar, _grammar_name, imported_files);

    return fuze.parse(rules, main_rule, handlers, lex);
}

namespace
{
    bool storeString(ostream &os, const u16string &s)
    {
        size_t  len = s.size()*sizeof(s[0]);

        assert(len < MAX_STRING_LENGTH);

        os.write(reinterpret_cast<char *>(&len), sizeof(len));

        if (len > 0)
            os.write(reinterpret_cast<const char *>(s.c_str()), static_cast<streamsize>(len));

        return true;
    }

    u16string loadString(istream &is)
    {
        size_t  len;
        is.read(reinterpret_cast<char *>(&len), sizeof(len));

        assert(len <= MAX_STRING_LENGTH);

        char16_t b[MAX_STRING_LENGTH+1];

        if (len > 0)
            is.read(reinterpret_cast<char *>(b), static_cast<streamsize>(len));

        b[len/sizeof(b[0])] = 0;

        return u16string(b);
    }

    bool storeLexeme(ostream &os, const Lexeme &s)
    {
        storeString(os, s.getLexeme());

        LexemeType  type = s.getType();
        os.write(reinterpret_cast<char *>(&type), sizeof(type));

        return true;
    }

    Lexeme loadLexeme(istream &is)
    {
        u16string   value(loadString(is));
        LexemeType  type;

        is.read(reinterpret_cast<char *>(&type), sizeof(type));

        return Lexeme(value.c_str(), type);
    }

    bool storeToken(ostream &os, const Token &t)
    {
        storeLexeme(os, t);
        storeString(os, t.getToken());

        const TokenLocation loc = t.getLocation();
        storeString(os, loc.file_name);
        os.write(reinterpret_cast<const char *>(&loc.line), sizeof(loc.line));
        os.write(reinterpret_cast<const char *>(&loc.column), sizeof(loc.column));
        os.write(reinterpret_cast<const char *>(&loc.column_tabulated), sizeof(loc.column_tabulated));
        os.write(reinterpret_cast<const char *>(&loc.begin), sizeof(loc.begin));
        os.write(reinterpret_cast<const char *>(&loc.end), sizeof(loc.end));
        os.write(reinterpret_cast<const char *>(&loc.context), sizeof(loc.context));

        TokenQualification q = t.getQualification();
        os.write(reinterpret_cast<const char *>(&q), sizeof(q));

        return true;
    }

    Token loadToken(istream &is)
    {
        Lexeme    lex          = loadLexeme(is);
        u16string token_string = loadString(is);

        TokenLocation loc(loadString(is));
        is.read(reinterpret_cast<char *>(&loc.line), sizeof(loc.line));
        is.read(reinterpret_cast<char *>(&loc.column), sizeof(loc.column));
        is.read(reinterpret_cast<char *>(&loc.column_tabulated), sizeof(loc.column_tabulated));
        is.read(reinterpret_cast<char *>(&loc.begin), sizeof(loc.begin));
        is.read(reinterpret_cast<char *>(&loc.end), sizeof(loc.end));
        is.read(reinterpret_cast<char *>(&loc.context), sizeof(loc.context));

        TokenQualification q;
        is.read(reinterpret_cast<char *>(&q), sizeof(q));

        return Token(token_string, lex.getType(), lex.getLexeme(), loc, q);
    }

    bool storeAstNode(ostream &os, const AstNode &node)
    {
        storeString(os, u"");   // Sup for loom version instead of '...node.semantic_host_name()'

        AstOperation op = node.getOperation();
        os.write(reinterpret_cast<const char *>(&op), sizeof(op));

        storeToken(os, node.getSymbol());
        storeToken(os, node.getBound());

        size_t branch_element_count = node.getBranchC().size();
        os.write(reinterpret_cast<const char *>(&branch_element_count), sizeof(branch_element_count));
        for(const AstNode & bn : node.getBranchC())
            storeAstNode(os, bn);

        return true;
    }
    AstNode loadAstNode(istream &is, AstNode * parent)
    {
        loadString(is); // Sup for loom version instead of 'std::u16string host_name = ...'

        AstOperation op;
        is.read(reinterpret_cast<char *>(&op), sizeof(op));

        Token symbol = loadToken(is);
        Token bound = loadToken(is);

        AstNode node(parent, op, symbol, bound);

        size_t branch_element_count;
        is.read(reinterpret_cast<char *>(&branch_element_count), sizeof(branch_element_count));

        for(size_t i=0; i < branch_element_count; ++i)
            node.getBranch().emplace_back(loadAstNode(is, &node));

        return node;
    }

    bool storeMarkup(ostream &os, const MarkupSymbol &ms)
    {
        storeString(os,ms.start);
        storeString(os,ms.end);
        storeString(os,ms.ignore_sign);

        LexemeType  type = ms.type;
        os.write(reinterpret_cast<char *>(&type), sizeof(type));

        return true;
    }
    MarkupSymbol loadMarkup(istream &is)
    {
        std::u16string start        = loadString(is);
        std::u16string end          = loadString(is);
        std::u16string ignore_sign  = loadString(is);

        LexemeType type;
        is.read(reinterpret_cast<char *>(&type), sizeof(type));

        return {start, end, ignore_sign, type};
    }

    bool storeMask(ostream &os, const NumberMask &mask)
    {
        storeString(os,mask.chars);

        LexemeType type = mask.type;
        os.write(reinterpret_cast<const char *>(&type), sizeof(type));

        number_system_t system = mask.system;
        os.write(reinterpret_cast<const char *>(&system), sizeof(system));

        return true;
    }
    NumberMask loadMask(istream &is)
    {
        std::u16string chars        = loadString(is);

        LexemeType type;
        is.read(reinterpret_cast<char *>(&type), sizeof(type));

        number_system_t system;
        is.read(reinterpret_cast<char *>(&system), sizeof(system));

        return {chars, type, system};
    }

}

bool GrammarLoader::storeDumpFile(const string &file_name, const Grammar & g)
{
    ofstream os(file_name,ios::binary);

    if (!os)
        return false;

    // LSX_GRAMMAR_DUMP_SIGNATURE
    storeString(os,GRAMMAR_DUMP_SIGNATURE);
    // build_method
    os.write(reinterpret_cast<const char *>(&g.build_method), sizeof(g.build_method));
    // lexical
    size_t markups_count = g.lexical.markups.size();
    os.write(reinterpret_cast<char *>(&markups_count), sizeof(markups_count));
    for(const MarkupSymbol & ms : g.lexical.markups)
        storeMarkup(os, ms);
    size_t masks_count = g.lexical.masks.size();
    os.write(reinterpret_cast<char *>(&masks_count), sizeof(masks_count));
    for(const NumberMask & mask : g.lexical.masks)
        storeMask(os, mask);
    storeString(os, g.lexical.national_alphabet);
    storeString(os, g.lexical.id_extra_symbols);
    uint16_t may_national_letters_use = g.lexical.may_national_letters_use;
    os.write(reinterpret_cast<char *>(&may_national_letters_use), sizeof(may_national_letters_use));
    uint16_t may_national_letters_mix = g.lexical.may_national_letters_mix;
    os.write(reinterpret_cast<char *>(&may_national_letters_mix), sizeof(may_national_letters_mix));
    uint16_t is_case_sensitive = g.lexical.is_case_sensitive;
    os.write(reinterpret_cast<char *>(&is_case_sensitive), sizeof(is_case_sensitive));
    storeString(os, g.lexical.nl_substitution);
    // handlers
    size_t handlers_count = g.handlers.size();
    os.write(reinterpret_cast<char *>(&handlers_count), sizeof(handlers_count));
    for(const auto & [handler_name,handler_ast] : g.handlers)
    {
        storeString(os, handler_name);
        storeAstNode(os, handler_ast);
    }
    // rules
    size_t rulesCount = g.rules.size();
    os.write(reinterpret_cast<char *>(&rulesCount), sizeof(rulesCount));
    for(const GrammarRule & r : g.rules)
    {
        storeString(os, r.production);
        size_t patternCount = r.pattern.size();
        os.write(reinterpret_cast<char *>(&patternCount), sizeof(patternCount));
        for(const Lexeme & s:r.pattern)
            storeLexeme(os, s);

        storeAstNode(os, r.reduce_action);

        RuleReduceDirection direction = r.reduce_direction;
        os.write(reinterpret_cast<char *>(&direction), sizeof(direction));
    }
    // columns
    size_t columnsCount = g.columns.size();
    os.write(reinterpret_cast<char *>(&columnsCount), sizeof(columnsCount));
    for(const Lexeme & si : g.columns)
        storeLexeme(os, si);
    // first_compound_index
    os.write(reinterpret_cast<const char *>(&g.first_compound_index), sizeof(g.first_compound_index));
    // states
    size_t linesCount = g.states.size();
    os.write(reinterpret_cast<char *>(&linesCount), sizeof(linesCount));
    for(const FsmState_t & state : g.states)
    {
        size_t positionsCount = state.size();
        os.write(reinterpret_cast<char *>(&positionsCount), sizeof(positionsCount));
        for(const FsmStatePosition & p : state)
        {
            size_t main = p.is_main ? 1 : 0;
            size_t rno = p.rule_no;
            size_t pos = p.position;

            int next = p.next_state_no;

            os.write(reinterpret_cast<char *>(&rno), sizeof(rno));
            os.write(reinterpret_cast<char *>(&pos), sizeof(pos));
            os.write(reinterpret_cast<char *>(&next), sizeof(next));
            os.write(reinterpret_cast<char *>(&main), sizeof(main));

            // lookahead

            size_t lookahead_count = p.lookahead.size();

            os.write(reinterpret_cast<char *>(&lookahead_count), sizeof(lookahead_count));
            for(const Lexeme & lex : p.lookahead)
                storeLexeme(os, lex);
        }
    }
    // parse_table
    size_t FSMtableCount = g.parse_table.size();
    os.write(reinterpret_cast<char *>(&FSMtableCount), sizeof(FSMtableCount));
    for (auto [key,value] : g.parse_table)
    {
        os.write(reinterpret_cast<const char *>(&key), sizeof(key));
        os.write(reinterpret_cast<char *>(&value), sizeof(value));
    }
    // key_bound
    os.write(reinterpret_cast<const char *>(&g.key_bound), sizeof(g.key_bound));
    // value_bound
    os.write(reinterpret_cast<const char *>(&g.value_bound), sizeof(g.value_bound));
    // "end"
    storeString(os,u"end");

    os.close();
    return true;
}

bool GrammarLoader::loadDumpFile(const string &file_name, Grammar & g, TableBuildMethod method)
{
    SIMODO_INOUT_CONVERT_STD_IFSTREAM_IOS(is, file_name, ios::binary);

    if (!is)
        return false;

    // GRAMMAR_DUMP_SIGNATURE
    bool success = (loadString(is) == GRAMMAR_DUMP_SIGNATURE);

    // build_method
    is.read(reinterpret_cast<char *>(&g.build_method), sizeof(g.build_method));

    if (!success || (method != TableBuildMethod::none && g.build_method != method))
        return false;

    // lexical
    size_t markups_count;
    is.read(reinterpret_cast<char *>(&markups_count), sizeof(markups_count));
    g.lexical.markups.reserve(markups_count);
    for(size_t i=0; i < markups_count; ++i)
    {
        MarkupSymbol ms = loadMarkup(is);

        g.lexical.markups.push_back(ms);
    }
    size_t masks_count;
    is.read(reinterpret_cast<char *>(&masks_count), sizeof(masks_count));
    g.lexical.masks.reserve(masks_count);
    for(size_t i=0; i < masks_count; ++i)
    {
        NumberMask mask = loadMask(is);

        g.lexical.masks.push_back(mask);
    }
    g.lexical.national_alphabet = loadString(is);
    g.lexical.id_extra_symbols = loadString(is);
    uint16_t may_national_letters_use;
    is.read(reinterpret_cast<char *>(&may_national_letters_use), sizeof(may_national_letters_use));
    g.lexical.may_national_letters_use = (may_national_letters_use != 0);
    uint16_t may_national_letters_mix;
    is.read(reinterpret_cast<char *>(&may_national_letters_mix), sizeof(may_national_letters_mix));
    g.lexical.may_national_letters_mix = (may_national_letters_mix != 0);
    uint16_t is_case_sensitive;
    is.read(reinterpret_cast<char *>(&is_case_sensitive), sizeof(is_case_sensitive));
    g.lexical.is_case_sensitive = (is_case_sensitive != 0);
    g.lexical.nl_substitution = loadString(is);

    // handlers
    size_t handlers_count;
    is.read(reinterpret_cast<char *>(&handlers_count), sizeof(handlers_count));
    for(size_t i_handler=0; i_handler < handlers_count; ++i_handler)
    {
        u16string handler_name = loadString(is);
        AstNode   handler_ast  = loadAstNode(is, nullptr);

        g.handlers.insert({handler_name, handler_ast});
    }

    // rules
    size_t rulesCount;
    is.read(reinterpret_cast<char *>(&rulesCount), sizeof(rulesCount));
    g.rules.reserve(rulesCount);
    for(size_t iRule=0; iRule < rulesCount; ++iRule)
    {
        u16string production = loadString(is);

        size_t patternCount;
        is.read(reinterpret_cast<char *>(&patternCount), sizeof(patternCount));

        vector<Lexeme> pattern;
        pattern.reserve(patternCount);
        for(size_t iPat=0; iPat < patternCount; ++iPat)
            pattern.emplace_back(loadLexeme(is));

        AstNode node = loadAstNode(is, nullptr);

        RuleReduceDirection direction;
        is.read(reinterpret_cast<char *>(&direction), sizeof(direction));

        g.rules.emplace_back(production, pattern, node, direction);
    }

    // columns
    size_t columnsCount;
    is.read(reinterpret_cast<char *>(&columnsCount), sizeof(columnsCount));
    g.columns.reserve(columnsCount);
    for(size_t iCol=0; iCol < columnsCount; ++iCol)
        g.columns.emplace_back(loadLexeme(is));

    // first_compound_index
    is.read(reinterpret_cast<char *>(&g.first_compound_index), sizeof(g.first_compound_index));

    // states
    size_t linesCount;
    is.read(reinterpret_cast<char *>(&linesCount), sizeof(linesCount));
    g.states.reserve(linesCount);
    for(size_t iLine=0; iLine < linesCount; ++iLine)
    {
        FsmState_t line;

        size_t positionsCount;
        is.read(reinterpret_cast<char *>(&positionsCount), sizeof(positionsCount));
        line.reserve(positionsCount);
        for(size_t iPos=0; iPos < positionsCount; ++iPos)
        {
            size_t      rno;
            size_t      pos;
            int         next;
            size_t      main;
            set<Lexeme> lookahead;

            is.read(reinterpret_cast<char *>(&rno), sizeof(rno));
            is.read(reinterpret_cast<char *>(&pos), sizeof(pos));
            is.read(reinterpret_cast<char *>(&next), sizeof(next));
            is.read(reinterpret_cast<char *>(&main), sizeof(main));
            // lookahead
            size_t lookahead_count;
            is.read(reinterpret_cast<char *>(&lookahead_count), sizeof(lookahead_count));
            for(size_t i=0; i < lookahead_count; ++i)
            {
                Lexeme lex = loadLexeme(is);
                lookahead.insert(lex);
            }
            line.emplace_back(rno,pos,lookahead,main,next);
        }
        g.states.emplace_back(line);
    }

    // parse_table
    size_t FSMtableCount;
    is.read(reinterpret_cast<char *>(&FSMtableCount), sizeof(FSMtableCount));
    for(size_t iCount=0; iCount < FSMtableCount; ++iCount)
    {
        Fsm_key_t   key;
        Fsm_value_t value;

        is.read(reinterpret_cast<char *>(&key), sizeof(key));
        is.read(reinterpret_cast<char *>(&value), sizeof(value));

        g.parse_table.emplace(key,value);
    }

    // key_bound
    is.read(reinterpret_cast<char *>(&g.key_bound), sizeof(g.key_bound));

    // value_bound
    is.read(reinterpret_cast<char *>(&g.value_bound), sizeof(g.value_bound));

    // "end"
    u16string end = loadString(is);
    if (end == u"end")
        success = true;

    is.close();

    GrammarBuilder::fillLexemeParameters(g);

    return success;
}
