/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <fstream>
#include <cassert>

#include "simodo/dsl/FuzeParserBase.h"

#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

bool FuzeParserBase::reportUnexpected(Token &t, const u16string &expected) const
{
    _m.reportError(t, u"Недопустимый символ '" + t.getLexeme() + u"'" + (expected.empty() ? u"" : u", ожидается " + expected));

    return false;
}
