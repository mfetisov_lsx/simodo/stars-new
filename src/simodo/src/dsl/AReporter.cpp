/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <locale>
#include <codecvt>

#include "simodo/dsl/AReporter.h"
#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

void AReporter::report(const SeverityLevel level, const Token &token, const u16string &briefly, const u16string &atlarge)
{
    report( level,
            token.getLocation(),
            briefly,
            atlarge.empty() ? (u"Позиция разбора: " + getTokenLocationString(token)) : atlarge);
}

std::u16string simodo::dsl::getSeverityLevelName(const SeverityLevel level)
{
    switch(level)
    {
    case SeverityLevel::Information:
        return u"";
    case SeverityLevel::Warning:
        return u"Предупреждение: ";
    case SeverityLevel::Error:
        return u"Ошибка: ";
    case SeverityLevel::Fatal:
        return u"Сбой! ";
    }
    return u"";
}

u16string simodo::dsl::getTokenLocationString(const Token &token, bool in_detail)
{
    /// \todo Нужно убрать лишнее преобразование кодировок
    u16string str = token.getLocation().file_name
               + u":" + convertToU16(to_string(token.getLocation().line))
               + u":" + convertToU16(to_string(token.getLocation().column_tabulated));

    if (in_detail)
        str += u"[" + convertToU16(to_string(token.getLocation().begin))
                + u"," + convertToU16(to_string(token.getLocation().end))
                + u"]";
               
    return str;
}
