/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <vector>
#include <cassert>

#include "simodo/dsl/Parser.h"

#include "simodo/convert.h"
#include "simodo/inout/convert/functions.h"

using namespace std;
using namespace simodo::dsl;

static uint16_t MAX_ERROR_COUNT = 5;
static size_t   MAX_ERROR_RECOVER_ATTEMPTS = 5;


Parser::Parser(const string &file_name, AReporter &m, const Grammar &g)
    : _file_name(file_name), _m(m), _g(g)
{
}

bool Parser::parse(ISyntaxTreeBuilder &builder)
{
    SIMODO_INOUT_CONVERT_STD_IFSTREAM(in, _file_name);

    if (!in)
    {
        _m.reportFatal(u"Ошибка при открытии файла '" + convertToU16(_file_name) + u"'");
        return false;
    }

    FileStream stream(in);

    return parse(stream, builder);
}

bool Parser::parse(IStream & stream, ISyntaxTreeBuilder &builder)
{
    assert(!_g.parse_table.empty());
    assert(_g.first_compound_index < _g.columns.size());

    LexicalParameters param = makeLexicalParameters(_g);

    Tokenizer tzer(convertToU16(_file_name), stream, param, 4);

    Token token = checkToken(tzer.getToken());
    Token delayed_token = Token( LexemeType::Error, token.getLexeme(), token.getLocation() );

    while(token.getType() == LexemeType::NewLine)
        token = checkToken(tzer.getToken());

    // input is empty
    if (token.getType() == LexemeType::Empty)
        return true;

    size_t   column_no   = _g.getTerminalColumnIndex(token);
    bool     success     = true;
    bool     end         = false;
    uint16_t error_count = 0;

    vector<ParserState> states;
    states.emplace_back(0,_g.first_compound_index,Token(convertToU16(_file_name),_g.columns[_g.first_compound_index]));

    // Вызов генератора
    if (!builder.onStart())
        success = false;
    if (!builder.onTerminal(token))
        success = false;

    // Цикл разбора
    while(!end && error_count < MAX_ERROR_COUNT)
    {
        size_t state_no = states.back().getStateNo();

        if (!_g.findFsmValue(state_no,column_no))
        {
            reportSyntaxError(states, token);
            error_count ++;
            success = false;

            // Пытаемся восстановиться после ошибки методом "паники"
            /// \todo Нужно будет доработать алгоритм восстановления после ошибок на что-то более интеллектуальное
            size_t i = 0;
            for(; i < MAX_ERROR_RECOVER_ATTEMPTS; ++i) // Ограничиваем кол-во попыток
            {
                if (delayed_token.getType() != LexemeType::Error)
                {
                    token = delayed_token;
                    delayed_token = Token( LexemeType::Error, token.getLexeme(), token.getLocation() );
                }
                else
                {
                    token = checkToken(tzer.getToken());
                    while(token.getType() == LexemeType::NewLine)
                        token = checkToken(tzer.getToken());
                }
                column_no = _g.getTerminalColumnIndex(token);
                if (_g.findFsmValue(state_no,column_no) || token.getType() == LexemeType::Empty)
                    break;
            }

            if (i == MAX_ERROR_RECOVER_ATTEMPTS || !_g.findFsmValue(state_no,column_no))
                break;
        }

        Fsm_value_t   fsm_value = _g.getFsmValue(state_no,column_no);
        FsmActionType action    = _g.unpackFsmAction(fsm_value);

        switch(action)
        {
        case FsmActionType::Acceptance:
            end = true;
            break;

        case FsmActionType::Error:
            _m.reportError(token,u"Синтаксическая ошибка");
            error_count ++;
            success = false;
            break;

        case FsmActionType::Reduce:
            {
                size_t              rule_no = _g.unpackFsmLocation(fsm_value);
                const GrammarRule & r       = _g.rules.at(rule_no);

                assert(states.size() >= r.pattern.size());

                // Вызов генератора
                TokenLocation prod_location(states[states.size()-r.pattern.size()].getLocation());
                vector<Token> pattern;
                pattern.reserve(r.pattern.size());

                for(size_t i=0; i < r.pattern.size() ; ++i)
                {
                    Token t = states[states.size()-r.pattern.size()+i];
                    if (i > 0)
                    {
                        prod_location.line = min(prod_location.line,t.getLocation().line);
                        prod_location.column = (prod_location.line == t.getLocation().line)
                                ? min(prod_location.column,t.getLocation().column) : prod_location.column;
                        prod_location.column_tabulated = (prod_location.line == t.getLocation().line)
                                ? min(prod_location.column_tabulated,t.getLocation().column_tabulated) : prod_location.column_tabulated;
                        prod_location.begin = min(prod_location.begin,t.getLocation().begin);
                        prod_location.end = max(prod_location.end,t.getLocation().end);
                    }
                    pattern.emplace_back(t);
                }

                if (error_count == 0)
                {
                    /// \todo make error handling more flexible!
                    bool is_done = false;
                    if (!builder.onProduction(Token(LexemeType::Compound,r.production,prod_location),pattern,r.reduce_action,is_done))
                    {
                        success = false;
                        end = true;
                        break;
                    }
                }

                size_t symbNo = _g.getCompaundColumnIndex(r.production);

                assert(symbNo < _g.columns.size());
                assert(states.size() > r.pattern.size());

                size_t lineNo = states[states.size()-r.pattern.size()-1].getStateNo();

                if (!_g.findFsmValue(lineNo,symbNo))
                {
                    reportSyntaxError(states, token);
                    error_count ++;
                    success = false;

                    // Восстановление после ошибки путём подстановки подходящего символа
                    // (доходим до 1, т.к. 0 - это конец файла)
                    for(symbNo=_g.first_compound_index-1; symbNo > 0; --symbNo)
                        if (_g.findFsmValue(lineNo,symbNo))
                        {
                            token = Token(convertToU16(_file_name),_g.columns[symbNo]);

                            if (isLexemeValid(states,token))
                            {
                                column_no = symbNo;
                                break;
                            }
                        }

                    if (symbNo == 0)
                    {
                        end = true;
                        break;
                    }
                }

                for(size_t i = 0; i < r.pattern.size(); ++i)
                    states.pop_back();

                Fsm_value_t   val = _g.getFsmValue(lineNo,symbNo);
                FsmActionType act = _g.unpackFsmAction(val);

                if (act == FsmActionType::Shift)
                {
                    size_t st = _g.unpackFsmLocation(val);
                    states.emplace_back(st, symbNo, Token(LexemeType::Compound,r.production,prod_location));
                }
                else
                {
                    _m.report(SeverityLevel::Fatal, token, u"При обработке приведения возникло недопустимое состояние");
                    success = false;
                    end = true;
                    break;
                }
            }
            break;

        case FsmActionType::Shift:
            {
                Fsm_value_t val   = _g.getFsmValue(state_no,column_no);
                size_t      shift = _g.unpackFsmLocation(val);

                ParserState  ps(shift,column_no,token);

                // Фрагмент кода, анализирующего возможность замены символа новой строки на заданный терминал
                // (обычно это символ ';')

                // Логика проверки возможности замены следующая:
                // 1. Если подменяемый символ не допустим грамматикой языка - замена не производится
                // 2. Если следующий символ после новой строки допустим грамматикой языка - замена не производится

                // Чтобы проверить допустимость символа-замены и следующего символа недостаточно проверить текущее
                // состояние разбора, так как оба символа могут участвовать в текущем контексте, но во вложенных
                // структурах (например, символ '}' может быть допустим грамматикой в выражении для лямбды и нужно
                // раскрутить стек состояний, чтобы убедиться, что мы находимся действительно в обработке лямбды,
                // а не в конце выражения и символ '}' не является концом блока операторов).
                // Для проверки допустимости токена в данном контексте по глубине стека состояний используется
                // метод isTokenValid.

                if (delayed_token.getType() != LexemeType::Error)
                {
                    token = delayed_token;
                    delayed_token = Token( LexemeType::Error, token.getLexeme(), token.getLocation() );
                }
                else
                {
                    token = checkToken(tzer.getToken());

                    if (token.getType() == LexemeType::NewLine)
                    {
                        Token nl_token = token;

                        while(token.getType() == LexemeType::NewLine)
                            token = checkToken(tzer.getToken());

                        // Для подмены символа замещения нужно, чтобы следующий символ не соответствовал грамматике

                        vector<ParserState> full_stack  = states;

                        full_stack.push_back(ps);

                        if (!isLexemeValid(full_stack,token))
                        {
                            // Не соответствует...

                            nl_token = checkToken(Token( LexemeType::Punctuation, nl_token.getLexeme(), nl_token.getLocation() ));

                            // Для подмены символа замещения нужно, чтобы символ замещения соответствовал грамматике

                            if (isLexemeValid(full_stack,nl_token))
                            {
                                // Соответствует...

                                delayed_token = token;
                                token = nl_token;
                            }
                        }
                    }
                }

                // Штатная обработка

                column_no = _g.getTerminalColumnIndex(token);

                // Вызов генератора
                if (!builder.onTerminal(token))
                    success = false;

                if (column_no == _g.columns.size())
                {
                    // Полученный символ (токен) не предусмотрен данной грамматикой.
                    // Такое может быть, например, если грамматика не предусматривает числовых констант, а она таки записана.
                    // Или, что чаще бывает, введены ошибочные символы, которых нет в лексике грамматики (LexemeType::Error)
                    reportSyntaxError(states, token);
                    error_count ++;
                    success = false;

                    // Простейший вариант восстановления - попытаться заменить данный токен на один из ожидаемых
                    // (начинаем с конца, т.к. в конце списка, как правило, наиболее обобщённые токены)
                    // (доходим до 1, т.к. 0 - это конец файла)
                    /// \todo Нужно будет доработать алгоритм восстановления на что-то более интеллектуальное
                    for(size_t i=_g.first_compound_index-1; i > 0; --i)
                        if (_g.findFsmValue(shift,i))
                        {
                            token = Token(convertToU16(_file_name),_g.columns[i]);
                            column_no = i;
                            break;
                        }
                }
                states.push_back(ps);
            }
            break;
        }
    }

    if (error_count == MAX_ERROR_COUNT)
        _m.reportInformation(u"Превышено допустимое количество ошибок, разбор прерван");
    else if (!end)
        _m.reportInformation(u"Не удалось выполнить восстановление состояния разбора после синтаксической ошибки, разбор прерван");

    if (!builder.onFinish(success))
        success = false;

    return success;
}

Token Parser::checkToken(Token t) const
{
    switch(t.getQualification())
    {
    case TokenQualification::NotANumber:
        _m.reportError(t, u"Последовательность '" + t.getLexeme() + u"' интерпретирована как ошибочно записанное число");
        break;
    // Будет обработано позже - не нужно дублировать сообщения
//    case TokenQualification::UnknownCharacterSet:
//        _m.reportWarning(t, u"Последовательность '" + t.getLexeme() + u"' не принадлежит к разрешённому алфавиту символов.");
//        break;
    case TokenQualification::NationalCharacterMix:
        _m.reportError(t, u"Последовательность '" + t.getLexeme() + u"' содержит знаки различных алфавитов");
        break;
    case TokenQualification::NationalCharacterUse:
        _m.reportError(t, u"Последовательность '" + t.getLexeme() + u"' содержит знаки национального алфавита");
        break;
    default:
        break;
    }

    return t;
}

void Parser::reportSyntaxError(const std::vector<ParserState> &states, const Token &t)
{
    assert(!states.empty());

    const ParserState & parser_state = states.back();
    u16string           briefly      = u"";
    u16string           atlarge;
    vector<u16string>   expected;

    if (t.getType() == LexemeType::Empty)
        briefly += u"Преждевременный конец файла";
    else
        briefly += u"Неподходящий входной символ '" + t.getLexeme() + u"'.";

    const FsmState_t & si = _g.states.at(parser_state.getStateNo());

    // Просматриваем позиции текущего состояния
    for(const FsmStatePosition &p : si)
    {
        const GrammarRule &rule = _g.rules.at(p.rule_no);

        if (atlarge.empty())
        {
            if (p.position == 0)
                atlarge = u"При разборе правила '" + rule.production;
            else
            {
                if (p.position == rule.pattern.size())
                    atlarge = u"После разбора правила '" + rule.production;
                else if (rule.pattern.at(p.position-1).getType() == LexemeType::Compound)
                    atlarge = u"После разбора правила '" + rule.pattern.at(p.position-1).getLexeme();
                else
                    atlarge = u"При разборе правила '" + rule.production;

            }
            atlarge += u"' (состояние КА = " + convertToU16(to_string(parser_state.getStateNo())) + u") ";

            if (t.getType() == LexemeType::Empty)
                atlarge += u"достигнут конец файла";
            else
                atlarge += u"получен неподходящий символ '" + t.getLexeme() + u"'";

            atlarge += u", вместо ожидаемых символов: ";
        }

        // Ожидаемые символы позиций текущего состояния более точно описывают ситуацию
//        if (p.position < rule.pattern.size())
//        {
//            u16string expSymbol = getLexemeMnemonic(rule.pattern.at(p.position));

//            if (find(expected.begin(), expected.end(), expSymbol) == expected.end())
//                expected.emplace_back(expSymbol);
//        }
    }

    for(size_t i=0; i < _g.first_compound_index; ++i)
        if (isLexemeValid(states,_g.columns[i]))
            expected.emplace_back(getLexemeMnemonic(_g.columns.at(i)));

    // Отображаем перечень ожидаемых символов
    for(auto st=expected.begin(); st != expected.end(); ++st)
    {
        if (st != expected.begin())
            atlarge += u", ";
        atlarge += *st;
    }

    atlarge += u"\nПозиция разбора: " + getTokenLocationString(t);

    _m.reportError(t, briefly, atlarge);
}

bool Parser::isLexemeValid(std::vector<ParserState> states, const Lexeme &lexeme)
{
    assert(!states.empty());

    // Для определения допустимости заданного символа в текущем контексте выполняется следующий алгоритм:

    // Необходимо для каждого состояния стека состояний, начиная с верхнего, убедиться, что:
    // 1. если предусмотрен сдвиг или завершение, то символ является разрешённым, возвращаем true
    // 2. иначе: для заданного символа предусмотрена свёртка, спускаемся ниже по стеку состояний.
    // 3. достижение конца стека не должно произойти (по идее).

    size_t column_no = _g.getTerminalColumnIndex(lexeme);

    if (column_no >= _g.columns.size())
        return false;

    while(!states.empty())
    {
        const ParserState & ps         = states.back();
        size_t              state_no   = ps.getStateNo();

        if (!_g.findFsmValue(state_no, column_no))
            return false;

        Fsm_value_t         fsm_value  = _g.getFsmValue(state_no, column_no);
        FsmActionType       fsm_action = _g.unpackFsmAction(fsm_value);

        if (fsm_action == FsmActionType::Error)
            return false;

        if (fsm_action == FsmActionType::Shift || fsm_action == FsmActionType::Acceptance)
            return true;

        size_t              rule_no      = _g.unpackFsmLocation(fsm_value);
        const GrammarRule & rule         = _g.rules.at(rule_no);
        size_t              pattern_size = rule.pattern.size();

        for(size_t i=0; i < pattern_size; ++i)
            states.pop_back();

        size_t symbNo = _g.getCompaundColumnIndex(rule.production);
        assert(symbNo < _g.columns.size());

        size_t lineNo = states.back().getStateNo();

        if(!_g.findFsmValue(lineNo,symbNo))
            return false;

        Fsm_value_t   val    = _g.getFsmValue(lineNo,symbNo);

        FsmActionType act    = _g.unpackFsmAction(val);
        assert(act == FsmActionType::Shift);

        size_t st = _g.unpackFsmLocation(val);

        states.emplace_back(st, symbNo, Token(LexemeType::Compound,rule.production,ps.getLocation()));
    }

    return false;
}

LexicalParameters simodo::dsl::Parser::makeLexicalParameters(const Grammar &g)
{
//    LexicalParameters param;

//    param.markups =
//    {
//        {u"/*", u"*/", u"", LexemeType::Comment},
//        {u"//", u"", u"", LexemeType::Comment},
//        {u"\"", u"\"", u"\\", LexemeType::Annotation}
//    };

//    u16string punct;

//    for(size_t i=0; i < g.first_compound_index; ++i)
//    {
//        if (g.columns[i].getType() != LexemeType::Punctuation)
//            continue;

//        const u16string & lexeme = g.columns[i].getLexeme();

//        if (lexeme.size() == 1)
//            punct += lexeme;
//        else if (!lexeme.empty())
//            param.punctuation_words.emplace_back(lexeme);
//    }

//    param.punctuation_chars = punct;

//    param.may_national_letters_use = true;
//    param.may_national_letters_mix = true;

//    return param;

    return g.lexical;
}

