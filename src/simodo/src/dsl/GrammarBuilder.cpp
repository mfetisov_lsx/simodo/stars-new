/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <cassert>

#include "simodo/dsl/GrammarBuilder.h"
#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_NS_LexicalParameters.h"
#include "simodo/dsl/ModulesManagement.h"

#include "simodo/convert.h"

//#if (__GNUC__ > 7)
#include <filesystem>
//#else
//#include <experimental/filesystem>
//using namespace std::experimental;
//#endif

using namespace std;
using namespace simodo::dsl;


bool GrammarBuilder::build(const std::vector<GrammarRuleTokens> &rules, const Token &main_rule)
{
    if (rules.empty())
    {
        _m.reportError(Token(buildPath()),
                       u"Не заданы правила грамматики '" + convertToU16(_grammar_name) + u"'");
        return false;
    }

    Token main_production = (main_rule.getToken().empty() ? rules[0].production : main_rule);

    // Добавляем обобщённое правило
    vector<Token> pattern {
        {u"", Lexeme(main_production.getToken(),LexemeType::Compound)},
        {u"", Lexeme(u"",LexemeType::Empty)},
    };
    _rules.push_back({Token(u"",Lexeme(main_production.getToken()+u"'",LexemeType::Compound)),
                      pattern,
                      AstNode(),
                      RuleReduceDirection::Undefined});

    // Вытягиваем из заданных правил грамматики только те, что относятся к основному правилу
    // Заполняется член класса _rules
    if(!extractRules(rules, main_production.getToken()))
        return false;

    if (_rules.size() <= 1)
    {
        _m.reportError(main_production,
                       u"Не удалось извлечь правила грамматики '" + convertToU16(_grammar_name) + u"'");
        return false;
    }

    // Заполняем индекс продукций для правил грамматики
    for(size_t i=0; i < _rules.size(); ++i)
        _production_index.emplace(_rules[i].production.getToken(),i);

    // Заполняем индекс символов из образца для правил грамматики
    for(size_t i=0; i < _rules.size(); ++i)
        for(const Lexeme & s : _rules[i].pattern)
            if (s.getType() == LexemeType::Compound)
            {
                // Повторы пар (s, индекс на правило) нам не нужны - проверяем на наличие
                auto   range = _pattern_index.equal_range(s.getLexeme());
                auto & it    = range.first;
                for(; it != range.second; ++it)
                    if (it->second == i)
                        break;

                if (it == range.second)
                    _pattern_index.emplace(s.getLexeme(),i);
            }

    // Проверяем какие правила не были собраны (не используются)
    if (_rules.size() <= rules.size())
        for(const GrammarRuleTokens & r : rules)
            if (_production_index.end() == _production_index.find(r.production.getToken()))
                _m.reportWarning(r.production,
                               u"Нетерминал '" + r.production.getToken() + u"' не используется в грамматике '"
                               + convertToU16(_grammar_name) + u"'");

    // Проверяем, что все нетерминалы имеют продукции
    bool ok = true;
    for(const auto & [p,i] : _pattern_index)
    {
        u16string last;
        if (last != p)
        {
            if (_production_index.find(p) == _production_index.end())
            {
                assert(i < _rules.size());
                size_t j = 0;
                for(; j < _rules[i].pattern.size(); ++j)
                    if (p == _rules[i].pattern[j].getToken())
                        break;

                Token t = (j < _rules[i].pattern.size()) ? _rules[i].pattern[j] : _rules[i].production;

                _m.reportError(t,
                               u"Нетерминал '" + p + u"' не имеет продукции для грамматики '"
                               + convertToU16(_grammar_name) + u"'");
                ok = false;
            }
            last = p;
        }
    }
    if (!ok)
        return false;

    // Копируем _rules в _g.rules для работы наследуемого кода (всё равно нужно копировать)
    for(const GrammarRuleTokens & r : _rules)
    {
        std::vector<Lexeme> p;

        for(const Token & t : r.pattern)
            p.push_back(t);

        _g.rules.emplace_back(r.production.getToken(), p, r.reduce_action, r.reduce_direction);
    }

    // Заполняем перечень состояний автомата разбора
    fillStateTable();

    // Вычисляем границу для значений таблицы разбора
    assert(max(_g.rules.size(),_g.states.size()) < UINT32_MAX/5);
    _g.value_bound = static_cast<Fsm_value_t>(max(_g.rules.size(),_g.states.size())) + 1;

    // Заполняем значения колонок
    fillColumns();

    // Определяем "вычисляемые" параметры лексики
    fillLexemeParameters(_g);

    // Вычисляем границу для ключа таблицы разбора
    assert(_g.columns.size()*_g.states.size() < UINT32_MAX/2);
    _g.key_bound = static_cast<Fsm_key_t>(_g.columns.size()) + 1;

    // Формируем таблицу разбора
    if (!fillParseTable())
        return false;

    // Проверяем полноту наполнения
    return checkCompleteness();
}

bool GrammarBuilder::checkGrammarAstBlocks(std::vector<SemanticName> &name_set, std::vector<SemanticScope> &scope_set) const
{
    bool ok = true;

    ModulesManagement mm(_m, _m, nullptr, _path);

    {
        ScriptC_Semantics            analyzer(_m, mm, name_set, scope_set);
        ScriptC_NS_LexicalParameters ns_lex(_g.lexical);
        AstNode                      ast(u"");

        analyzer.importNamespace(u"lex", ns_lex.getNamespace());
        analyzer.importNamespace(u"ast", AstBuilder(_m, ast, {}).getNamespace());

        for(const auto & [handler_name,handler_ast] : _g.handlers)
        {
            if (analyzer.check(handler_ast))
            {
                if (handler_name == u"lex")
                {
                    ScriptC_Interpreter machine(_m,mm);

                    machine.importNamespace(u"lex", ns_lex.getNamespace());

                    SCI_RunnerCondition res = machine.catchAst(handler_ast);

                    if (res != SCI_RunnerCondition::Regular)
                        ok = false;
                }
            }
            else
                ok = false;
        }
    }

    ScriptC_Semantics analyzer(_m, mm, name_set, scope_set);
    AstNode           ast(u"");

    analyzer.importNamespace(u"ast", AstBuilder(_m, ast, {}).getNamespace());

    for(const GrammarRule & r : _g.rules)
        if (!r.reduce_action.getBranchC().empty())
            if (!analyzer.check(r.reduce_action))
                ok = false;

    return ok;
}

bool GrammarBuilder::fillLexemeParameters(Grammar &g)
{
    assert(!g.columns.empty());
    assert(g.first_compound_index < g.columns.size());

    u16string punct;

    for(size_t i=0; i < g.first_compound_index; ++i)
    {
        if (g.columns[i].getType() != LexemeType::Punctuation)
            continue;

        const u16string & lexeme = g.columns[i].getLexeme();

        if (lexeme.size() == 1)
            punct += lexeme;
        else if (!lexeme.empty())
            g.lexical.punctuation_words.emplace_back(lexeme);
    }

    g.lexical.punctuation_chars = punct;

    return true;
}

u16string GrammarBuilder::buildPath() const
{
    filesystem::path grammar_path = filesystem::path(_path) / (_grammar_name + FUZE_FILE_EXTENSION);
    return grammar_path.u16string();
}

bool GrammarBuilder::extractRules(const std::vector<GrammarRuleTokens> &rules, u16string production)
{
    bool   ok = true;
    size_t rule_no = 0;
    size_t patt_no = 0;

    _rules.reserve(rules.size());

    while(!production.empty())
    {
        // Вытягиваем все связанные с заданной продукцией правила
        for(const GrammarRuleTokens & r : rules)
            if (r.production.getToken() == production)
            {
                // Убеждаемся, что это правило не дубль
                size_t i = 0;
                for(; i < _rules.size(); ++i)
                {
                    if (_rules[i].production.getToken() != production)
                        continue;

                    if (_rules[i].pattern.size() != r.pattern.size())
                        continue;

                    size_t j = 0;
                    for(; j < _rules[i].pattern.size(); ++j)
                        if (_rules[i].pattern[j] != r.pattern[j])
                            break;

                    if (j == _rules[i].pattern.size())
                        break;
                }
                if (i != _rules.size())
                {
                    _m.reportError(r.pattern[0],
                                   u"Правило для продукции '" + production + u"' дублируется");
                    ok = false;
                    continue;
                }

                // Добавляем правило в грамматику
                _rules.push_back(r);
            }

        production = u"";

        // Специально избавляемся от рекурсии, чтобы упорядочить перечень правил по продукциям
        do
        {
            patt_no ++;

            if (patt_no == _rules[rule_no].pattern.size())
            {
                rule_no ++;

                if (rule_no == _rules.size())
                    break;

                patt_no = 0;
            }

            assert(!_rules[rule_no].pattern.empty());

            if (_rules[rule_no].pattern[patt_no].getType() == LexemeType::Compound)
            {
                // Сначала убеждаемся, что этого нетерминала нет в нашем списке (оптимизируем)
                const u16string & att_prod = _rules[rule_no].pattern[patt_no].getLexeme();

                size_t i=1;
                for(; i < _rules.size(); ++i)
                    if (_rules[i].production.getToken() == att_prod)
                        break;

                // Не нашли, значит обрабатываем
                if (i == _rules.size())
                    production = att_prod;
            }
        }
        while(production.empty());
    }

    return ok;
}

void GrammarBuilder::fillColumns() const
{
    // Сначала заполняются терминальные символы грамматики
    for(const GrammarRule & r : _g.rules)
        for(const Lexeme &s : r.pattern)
            if (s.getType() != LexemeType::Compound)
                if (find(_g.columns.begin(),_g.columns.end(),s) == _g.columns.end())
                    _g.columns.push_back(s);

    // Затем идут нетерминалы
    _g.first_compound_index = _g.columns.size();

    for(const GrammarRule &r : _g.rules)
        for(const Lexeme & s : r.pattern)
            if (s.getType() == LexemeType::Compound)
                if (find(_g.columns.begin(),_g.columns.end(),s) == _g.columns.end())
                    _g.columns.push_back(s);
}

bool GrammarBuilder::checkCompleteness()
{
    bool ok = true;

    // Проверяем, что все приведения попали в таблицу
    // (правило 0 не должно участвовать)
    for(size_t i=1; i < _g.rules.size(); ++i)
    {
        bool found = false;
        for(const auto & [key,value] : _g.parse_table)
        {
            FsmActionType action   = _g.unpackFsmAction(value);
            size_t        location = _g.unpackFsmLocation(value);

            if (action == FsmActionType::Reduce)
                if (location == i)
                {
                    found = true;
                    break;
                }
        }
        if (!found)
        {
            assert(i < _rules.size());
            _m.reportFatal(_rules[i].production,
                           u"Правило " + convertToU16(to_string(i))
                           + u" грамматики '" + convertToU16(_grammar_name)
                           + u"' не участвует в разборе.");
            ok = false;
        }
    }

    return ok || !_strict_rule_consistency;
}

u16string simodo::dsl::getGrammarBuilderMethodName(TableBuildMethod method)
{
    switch(method)
    {
    case TableBuildMethod::SLR:
        return u"SLR";
    case TableBuildMethod::LR1:
        return u"LR1";
//    case TableBuildMethod::LALR:
//        return u"LALR";
    case TableBuildMethod::none:
        return u"none";
    default:
        return u"*****";
    }
}
