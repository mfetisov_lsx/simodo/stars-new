/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/SCI_Stack.h"
#include "simodo/dsl/Exception.h"

using namespace std;
using namespace simodo::dsl;

SCI_Stack::SCI_Stack(size_t stack_max_size)
    : _stack_max_size(stack_max_size)
{
    _stack.reserve(stack_max_size);
}

void SCI_Stack::pop(int count)
{
    while(count-- > 0)
        _stack.pop_back();
}

const SCI_Name & SCI_Stack::push(const SCI_Scalar & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Scalar, value, access});

    return _stack.back();
}

const SCI_Name &SCI_Stack::push(const SCI_Tuple & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Tuple, value, access});

    return _stack.back();
}

const SCI_Name &SCI_Stack::push(const SCI_Array & value, u16string name, SemanticNameAccess access)
{
    push({name, SemanticNameQualification::Array, value, access});

    return _stack.back();
}

const SCI_Name &SCI_Stack::push(const SCI_Name & value)
{
    if (_stack.size() == _stack_max_size)
        throw Exception("SCI_Stack::push", "Переполнение размера стека");

    _stack.push_back(value);

    return _stack.back();
}

size_t SCI_Stack::findLocal(u16string name, size_t local_boundary) const
{
    for(size_t i=_stack.size()-1; i >= local_boundary && i < _stack.size(); --i)
        if (_stack[i].name == name)
            return i;

    return UNDEFINED_INDEX;
}

size_t SCI_Stack::findGlobal(u16string name, size_t global_boundary) const
{
    for(size_t i=global_boundary-1; i < _stack.size(); --i)
        if (_stack[i].name == name)
            return i;

    return UNDEFINED_INDEX;
}

size_t SCI_Stack::find(u16string name) const
{
    for(size_t i=_stack.size()-1; i < _stack.size(); --i)
        if (_stack[i].name == name)
            return i;

    return UNDEFINED_INDEX;
}

SCI_Name &SCI_Stack::at(size_t index)
{
    if (index >= _stack.size())
        throw Exception("SCI_Stack::at", "Выход за пределы стека");

    return _stack[index];
}

SCI_Name &SCI_Stack::top(size_t index)
{
    if (index >= _stack.size())
        throw Exception("SCI_Stack::top", "Выход за пределы стека");

    return _stack[_stack.size()-index-1];
}

SCI_Name &SCI_Stack::back()
{
    return top();
}

void SCI_Stack::moveTo(size_t from, size_t to)
{
    if (from >= _stack.size() || to >= _stack.size())
        throw Exception("SCI_Stack::moveTo", "Выход за пределы стека");

    _stack[to] = move(_stack[from]);
}
