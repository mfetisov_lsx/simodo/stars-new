/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <cassert>
#include <cstdint>

#include "simodo/dsl/GrammarBuilder_LR1.h"
#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;


namespace {
    uint64_t packPositionKey(size_t rno, size_t pos)
    {
        return rno + pos*UINT32_MAX;
    }

}


void GrammarBuilder_LR1::fillStateTable()
{
    FsmState_t state;
    state.emplace_back(FsmStatePosition(0,0,{Lexeme(u"",LexemeType::Empty)}));
    addState(state,0);

    fillState(0);

    _g.build_method = TableBuildMethod::LR1;
}

void GrammarBuilder_LR1::fillState(size_t state_no)
{
    // При входе в этот метод в заданном состоянии (state_no) уже определены основные позиции
    // Этот метод вызывается рекурсивно, единожды для каждого состояния

    /// \attention Код данного метода (как и всего данного класса) выполняет активную модификацию
    /// структур грамматики, заданной в параметре _g. Поэтому, крайне не желательно создавать
    /// ссылки на члены класса _g. Массивы могут реаллокироваться. Будьте осторожны.
    /// Работайте с индексами, они не меняются.

    multimap<DepthPair,FsmStatePosition,DepthPairComparator>
                                position_multiset;
    map<u16string,set<Lexeme>>  prod_lookahead_set;

    // Первым делом необходимо дополнить заданное состояние (state_no) "замыканиями",
    // т.е. символами, которые всегда идут в первой позиции для каждой основной позиции.

    assert(state_no < _g.states.size());
    size_t position_initial_count = _g.states[state_no].size(); // размер массива в цикле не является инвариантом (мы его меняем)

    for(size_t i=0; i < position_initial_count; ++i)
    {
        // Точно известно, что основные позиции состояния добавляются в начало
        if (!_g.states[state_no][i].is_main)
            break;

        size_t rno = _g.states[state_no][i].rule_no;
        size_t pos = _g.states[state_no][i].position;

        assert(rno < _g.rules.size());

        auto it_prod_lookahead = prod_lookahead_set.find(_g.rules[rno].production);

        if (it_prod_lookahead == prod_lookahead_set.end())
            prod_lookahead_set.insert({_g.rules[rno].production,_g.states[state_no][i].lookahead});
        else
            for(const Lexeme & l : _g.states[state_no][i].lookahead)
                it_prod_lookahead->second.insert(l);

        // Находим замыкания (формируем отсортированный по уровням и продукциям перечень замыканий)
        // Такое предварительное формирование необходимо для достижения симметрии в обработке разных
        // описаний одной и той же грамматики
        if (pos < _g.rules[rno].pattern.size())
            fillClosure(state_no, rno, pos, 0, position_multiset);
    }

    // Записываем отсортированные не основные позиции в наше состояние
    // (теперь внутреннее представление грамматики является инвариантным)
    for(const auto & [key,p] : position_multiset)
        _g.states[state_no].push_back(p);

    // Устанавливаем предпросмотры для всех неосновных позиций
    // (для основных позиций они должны быть заданы перед вызовом метода)

    fillLookahead(state_no, position_initial_count, prod_lookahead_set);

    // Состояния, которые нужно будет заполнить
    set<size_t> new_states;

    // Текущее состяние создано, готовим следующие состояния
    // Проходим по всем позициям нашего состояния и для каждого перехода
    // создаем следующее состояние.
    // Проставляем значение next для позиций.

    for(size_t i_pos_in_state=0; i_pos_in_state < _g.states[state_no].size(); ++i_pos_in_state)
    {
        size_t current_rno = _g.states[state_no][i_pos_in_state].rule_no;
        size_t current_pos = _g.states[state_no][i_pos_in_state].position;

        assert(current_rno < _g.rules.size());
        const GrammarRule & current_rule = _g.rules[current_rno];

        // Важно убедиться, что есть куда переходить
        if (current_pos == current_rule.pattern.size())
            continue;
        // и эта позиция не использована ранее в этом же цикле (не установлено состояние перехода)
        if (_g.states[state_no][i_pos_in_state].next_state_no > 0)
            continue;

        assert(current_pos < current_rule.pattern.size());
        const Lexeme & current_symbol = current_rule.pattern[current_pos];

        // Для символа конца не создаётся новых состояний
        if (current_symbol.getType() == LexemeType::Empty )
            continue;

        // Убеждаемся, что нужного основного состояния ещё нет
        size_t find_state = findMainPosition(current_rno, current_pos+1, prod_lookahead_set[_g.rules[current_rno].production]);
        size_t next_state_no;
        if (find_state == 0) // (совпадение с нулевым состоянием невозможно, т.к. оно создаётся искусственно)
        {
            // Точного совпадения не нашли
            // Создаем следующее состояние и нашу следующую основную позицию
            next_state_no = _g.states.size();
            _g.states[state_no][i_pos_in_state].next_state_no = next_state_no;

            FsmState_t new_state;
            new_state.emplace_back(current_rno, current_pos+1, prod_lookahead_set[_g.rules[current_rno].production], true, 0);
            addState(new_state,next_state_no);
            new_states.insert(next_state_no);
        }
        else
            _g.states[state_no][i_pos_in_state].next_state_no = next_state_no = find_state;

        // Находим позицию с тем же переходным символом
        for(size_t i_pos_add=i_pos_in_state+1; i_pos_add < _g.states[state_no].size(); ++i_pos_add)
        {
            if (_g.states[state_no][i_pos_add].next_state_no != 0)
                continue;

            size_t add_rno = _g.states[state_no][i_pos_add].rule_no;
            size_t add_pos = _g.states[state_no][i_pos_add].position;

            assert(add_rno < _g.rules.size());
            const GrammarRule & add_rule = _g.rules[add_rno];

            if (add_pos < add_rule.pattern.size())
                if (current_symbol == add_rule.pattern[add_pos])
                {
                    // Проверяем, что такой основной позиции ещё нет
                    bool found = false;
                    auto it = _g.states[next_state_no].begin();
                    for(; it != _g.states[next_state_no].end() && it->is_main; ++it)
                        if (it->rule_no == add_rno)
                        {
                            found = true;
                            break;
                        }
                    // Добавляем основную позицию
                    _g.states[state_no][i_pos_add].next_state_no = next_state_no;
                    if (!found)
                    {
                        _g.states[next_state_no].emplace(it, add_rno,add_pos+1,prod_lookahead_set[_g.rules[add_rno].production],true,0);
                        _position_index.insert({packPositionKey(add_rno,add_pos+1),next_state_no});
                        new_states.insert(next_state_no);
                    }
                }

//                {
//                    _g.states[state_no][i_pos_add].next_state_no = next_state_no;

//                    if (find_state == 0)
//                    {
//                        // Добавляем похожую позицию как основную для нового состояния
//                        _g.states[next_state_no].emplace_back(add_rno,add_pos+1,prod_lookahead_set[_g.rules[add_rno].production],true,0);
//                        _position_index.insert({packPositionKey(add_rno,add_pos+1),next_state_no});
//                    }
//                    else if (add_pos == add_rule.pattern.size()-1)
//                    {
//                        // Данный блок кода добавлен исключительно для того,
//                        // чтобы выявлять неоднозначности грамматики типа R-R,
//                        // которые в противном случае остаются непроявленными.
//                        // Кроме того, дерево переходов состояния становиться согласованным
//                        // и полным, а ошибку можно увидеть визуально.
//                        // Сами ошибки выявляются чуть позже
//                        // (см. GrammarBuilder_LR1::fillParseTable и GrammarBuilder_LR1::insertValue)
//                        // Актуально для SLR-метода, для LR-метода оставил на всякий случай.
//                        size_t i = 0;
//                        for(; i < _g.states[next_state_no].size(); ++i)
//                            if (_g.states[next_state_no][i].rule_no == add_rno)
//                                break;

//                        if (i == _g.states[next_state_no].size())
//                        {
//                            _g.states[next_state_no].emplace_back(add_rno,add_pos+1,prod_lookahead_set[_g.rules[add_rno].production],true,0);
//                            _position_index.insert({packPositionKey(add_rno,add_pos+1),next_state_no});
//                        }
//                    }
//                }
        }

        // Вызываем себя рекурсивно, чтобы заполнить созданное состояние по аналогии
//        if (find_state == 0)
//            fillState(next_state_no);
    }

    for(auto i : new_states)
        fillState(i);
}

size_t GrammarBuilder_LR1::findMainPosition(size_t rno, size_t pos, const set<Lexeme> & lookahead) const
{
    auto range = _position_index.equal_range(packPositionKey(rno,pos));

    for(auto &it=range.first; it != range.second; ++it)
    {
        size_t it_state = it->second;

//    for(size_t it_state=0; it_state < _g.states.size(); ++it_state) // Оставил не оптимизированный вариант закомментированным
//    {
        FsmState_t & state = _g.states[it_state];

        for(size_t it_pos=0; it_pos < state.size(); ++it_pos)
        {
            FsmStatePosition & p = state[it_pos];

            // Проверяем только основные позиции, и они всегда в начале
            if (!p.is_main)
                break;

            if (rno == p.rule_no && pos == p.position)
            {
                if (pos == _g.rules[rno].pattern.size()
                        /// \todo Данный алгоритм осторожничает, не оптимизируя случаи нескольких основных закрываемых позиций.
                        /// Можно ещё немного оптимизировать количество состояний без потери качества, если разрешать слияние с
                        /// не единственными закрываемыми позициями.
                        /// Для этого в состоянии не должно быть незакрываемых позиций и все позиции должны быть основными.
                        && state.size() == 1)
                {
                    for(const Lexeme & l : lookahead)
                        p.lookahead.insert(l);
                    return it_state;
                }

                if (lookahead == p.lookahead)
                    return it_state;
            }
        }
    }

    return 0;
}

void GrammarBuilder_LR1::fillClosure(size_t state_no, size_t rno, size_t pos, int depth,
                                     std::multimap<DepthPair, FsmStatePosition, DepthPairComparator> &position_multiset)
{
    auto production_range = _production_index.equal_range(_g.rules[rno].pattern[pos].getLexeme());

    // Просматриваем продукции заданного символа
    for(auto &it=production_range.first; it != production_range.second; ++it)
    {
        size_t next_rno = it->second;

        bool find = false;
        for(auto & [key_depth,p] : position_multiset)
            if (p.rule_no == next_rno && p.position == 0)
            {
                find = true;
                break;
            }

        if (!find)
        {
            int find_depth = INT32_MAX;
            for(auto & [key_depth,p] : position_multiset)
                if (_g.rules[p.rule_no].production == _g.rules[next_rno].production)
                {
                    find_depth = key_depth.depth;
                    break;
                }

            if (find_depth > depth)
                find_depth = depth;

            position_multiset.insert({{find_depth,_g.rules[next_rno].pattern.size()},FsmStatePosition(next_rno, 0, false, 0)});
            fillClosure(state_no, next_rno, 0, depth+1, position_multiset);
        }
    }
}

void GrammarBuilder_LR1::fillLookahead(size_t state_no, size_t position_initial_count, map<u16string,set<Lexeme>> & prod_lookahead_set)
{
    assert(state_no < _g.states.size());

    for(size_t i=position_initial_count; i < _g.states[state_no].size(); ++i)
    {
        size_t rno = _g.states[state_no][i].rule_no;

        assert(rno < _g.rules.size());

        set<Lexeme> lookahead;

        for(size_t parent=0; parent < i; ++parent)
        {
            size_t prev_rno = _g.states[state_no][parent].rule_no;
            size_t prev_pos = _g.states[state_no][parent].position;

            if(prev_pos < _g.rules[prev_rno].pattern.size())
                if(_g.rules[prev_rno].pattern[prev_pos].getLexeme() == _g.rules[rno].production)
                {
                    if (prev_pos == _g.rules[prev_rno].pattern.size()-1)
                    {
                        // Последний элемент
                        for(const Lexeme & l : _g.states[state_no][parent].lookahead)
                            lookahead.insert(l);
                    }
                    else if (_g.rules[prev_rno].pattern[prev_pos+1].getType() != LexemeType::Compound)
                    {
                        // Следующий элемент является терминалом и определяет предпросмотр
                        lookahead.insert(_g.rules[prev_rno].pattern[prev_pos+1]);
                    }
                    else
                    {
                        // Определяем терминалы, с которых может начинаться следующий нетерминал

                        set<Lexeme> processed;

                        fillStartingTerminalsByProd(_g.rules[prev_rno].pattern[prev_pos+1], lookahead, processed);
                    }
                }
        }

        auto it_prod_lookahead = prod_lookahead_set.find(_g.rules[rno].production);

        if (it_prod_lookahead == prod_lookahead_set.end())
            prod_lookahead_set.insert({_g.rules[rno].production,lookahead});
        else
            for(const Lexeme & l : lookahead)
                it_prod_lookahead->second.insert(l);

        _g.states[state_no][i].lookahead = prod_lookahead_set[_g.rules[rno].production];
    }
}

void GrammarBuilder_LR1::fillStartingTerminalsByProd(const Lexeme &prod, set<Lexeme> &terminals, std::set<Lexeme> &processed)
{
    auto range = _production_index.equal_range(prod.getLexeme());

    // Просматриваем продукции заданного символа
    for(auto &it=range.first; it != range.second; ++it)
    {
        size_t rno = it->second;
        const Lexeme & pattern0 = _g.rules[rno].pattern[0];

        if (pattern0.getType() != LexemeType::Compound)
        {
            if (terminals.end() == terminals.find(pattern0))
                terminals.insert(pattern0);
        }
        else if (pattern0 != prod)
            if (processed.end() == processed.find(pattern0))
            {
                processed.insert(pattern0);
                fillStartingTerminalsByProd(pattern0,terminals,processed);
            }
    }
}

bool GrammarBuilder_LR1::fillParseTable()
{
    bool success = true;

    for(size_t i_state=0; i_state < _g.states.size(); ++i_state)
    {
        const FsmState_t &state = _g.states.at(i_state);

        // Сдвиги имеют больший приоритет (правоассоциативная грамматика)
        for(const FsmStatePosition &p : state)
            if (const GrammarRule & r = _g.rules.at(p.rule_no); p.position < r.pattern.size())
            {
                const Lexeme & s = r.pattern.at(p.position);

                if (p.next_state_no == 0)    // 0 не в последней позиции означает завершение
                {
                    assert(s.getType() == LexemeType::Empty);

                    // Принятие (acceptence)
                    if (!insertValue(i_state, _g.getColumnIndex(s), _g.packFsmValue(FsmActionType::Acceptance,0)))
                        success = false;
                }
                else
                    // p.next != 0 означает наличие перехода в соотв-щее состояние НКА
                    // Сдвиг (shift)
                    if (!insertValue(i_state, _g.getColumnIndex(s), _g.packFsmValue(FsmActionType::Shift,static_cast<size_t>(p.next_state_no))))
                        success = false;
            }

        // Свёртки обрабатываем позже, чтобы при конфликтах принимать сдвиги
        // Правила по умолчанию правоассоциативные
        // (свёртки не будут записываться в таблицу, если соотв. позиция занята; кроме правил, помеченных как левоассациативные)
        for(const FsmStatePosition &p : state)
        {
            if (!p.is_main)    // В свёртке могут участвовать только основные позиции, которые находятся всегда в начале списка
                break;

            if (p.rule_no == 0 || p.next_state_no != 0)
                continue;

            const GrammarRule &r = _g.rules.at(p.rule_no);

            assert(p.position == r.pattern.size());

            // Свёртка (reduce)

            for(const Lexeme & s : p.lookahead)
                if (!insertValue(i_state, _g.getColumnIndex(s), _g.packFsmValue(FsmActionType::Reduce,p.rule_no)))
                    success = false;
        }
    }

    return success;
}

bool GrammarBuilder_LR1::insertValue(size_t line, size_t column, Fsm_value_t value)
{
    if (!_g.findFsmValue(line,column))
    {
        // Заданный вариант обработки отсутствует в таблице разбора - просто добавляем
        _g.parse_table.emplace(_g.packFsmKey(line,column),value);
        return true;
    }

    Fsm_value_t exist_value = _g.getFsmValue(line,column);

    FsmActionType exist_action = _g.unpackFsmAction(exist_value);
    FsmActionType new_action   = _g.unpackFsmAction(value);

    // Если вариант обработки в таблице совпадает с заданным - всё ОК
    if (exist_action == new_action && exist_value == value)
        return true;

    // Вариант обработки существует в таблице разбора и он отличается от нашего - нужно принять решение какой из них использовать

    const Lexeme & s = _g.columns.at(column);

    size_t exist_location = _g.unpackFsmLocation(exist_value);
    size_t new_location   = _g.unpackFsmLocation(value);

    u16string sout = convertToU16(_grammar_name) + u"'. Состояние " + convertToU16(to_string(line)) + u", символ "
            + getLexemeMnemonic(s) + u" (#" + convertToU16(to_string(column)) + u"). Конфликтуют " + getFsmActionChar(exist_action) + convertToU16(to_string(exist_location))
            + u" и " + getFsmActionChar(new_action) + convertToU16(to_string(new_location)) + u". ";

    const Token & t = (new_action == FsmActionType::Shift)
            ? _rules[1].pattern[0]
            : _rules[new_location].pattern[_rules[new_location].pattern.size()-1];

    if (exist_action == FsmActionType::Shift && new_action == FsmActionType::Reduce)
    {
        assert(new_location < _g.rules.size());
        assert(new_location < _rules.size());
        if (_g.rules[new_location].reduce_direction == RuleReduceDirection::RightAssociative)
        {
            sout += u"Принят " + getFsmActionChar(exist_action) + convertToU16(to_string(exist_location)) + u".";
        }
        else
        {
            _g.parse_table.at(_g.packFsmKey(line,column)) = value;
            sout += u"Принят " + getFsmActionChar(new_action) + convertToU16(to_string(new_location)) + u".";
        }

        if (_g.rules[new_location].reduce_direction == RuleReduceDirection::Undefined)
            _m.reportError(t, u"Неоднозначность грамматики '" + sout);
//        else
//            _m.reportInformation(t, u"Неоднозначность грамматики '" + sout);

        return true;
    }

    _m.reportError(t, u"Критическая неоднозначность грамматики '" + sout);

    return !_strict_rule_consistency;
}

void GrammarBuilder_LR1::addState(FsmState_t state, size_t state_no)
{
    _g.states.emplace_back(state);

    for(const FsmStatePosition & p : state)
    {
        if (!p.is_main)
            break;

        _position_index.insert({packPositionKey(p.rule_no,p.position),state_no});
    }
}
