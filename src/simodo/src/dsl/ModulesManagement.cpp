/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/ModulesManagement.h"

#include "simodo/convert.h"

#include "simodo/dsl/ScriptC_Semantics.h"
#include "simodo/dsl/GrammarManagement.h"
#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/Parser.h"
#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_TableFunction.h"

#include <filesystem>
namespace fs = std::filesystem;

using namespace std;
using namespace simodo::dsl;

namespace
{
static ModuleImportResults dummy_result;

}

ModulesManagement::ModulesManagement(AReporter &m, 
                                     AReporter &rtm, 
                                     GrammarManagement *gm, 
                                     string path_to_grammars,
                                     FileContentProvider_interface * p_content_provider,
                                     std::vector<std::pair<std::u16string,IScriptC_Namespace*>> 
                                                additional_namespaces)
    : _m(m)
    , _rtm(rtm)
    , _gm(gm)
    , _path_to_grammars(path_to_grammars)
    , _p_content_provider(p_content_provider)
    , _additional_namespaces(additional_namespaces)
{
}

const ModuleImportResults & ModulesManagement::loadModule(const Token & module_file_token, 
                                                          const Token & grammar_name_token)
{
    if (_gm == nullptr && _path_to_grammars.empty()) {
        _m.reportError(module_file_token, u"Контекст использования не предусматривает работу с модулями");
        return dummy_result;
    }

    fs::path module_path = module_file_token.getLocation().file_name;
    if (module_path.empty()) {
        _m.reportError(module_file_token, u"Невозможно определить контекст подключения модуля");
        return dummy_result;
    }

    module_path.replace_filename(module_file_token.getLexeme());

    if (!fs::exists(module_path) || !fs::is_regular_file(module_path)) {
        _m.reportError(module_file_token, u"Файл '" + module_file_token.getLexeme() + u"' не найден");
        return dummy_result;
    }

    if (auto it = _module_set.find(module_path.string()); it != _module_set.end())
    {
        if (!(it->second.ok))
            _m.reportError(module_file_token, u"Ошибки в модуле '" + module_file_token.getLexeme() + u"'");
        return it->second;
    }

    u16string grammar_name = grammar_name_token.getLexeme();
    if (grammar_name.empty())
        grammar_name = module_path.extension().u16string().substr(1);

    auto [it_module,it_ok] = _module_set.insert({
        module_path.string(),
        {AstNode(module_path.u16string()), {}, {}, true}});

    if (!it_ok) {
        _m.reportError(module_file_token, u"Сбой при добавлении модуля '" + module_file_token.getLexeme() + u"'");
        return dummy_result;
    }

    bool                  ok     = it_ok;
    ModuleImportResults & module = it_module->second;

    Grammar               g;
    const Grammar *       p_g;

    if (_gm == nullptr) {
        std::vector<std::string> paths {_path_to_grammars};
        GrammarLoader            loader(_m, paths, convertToU8(grammar_name));

        if (!loader.load(false, g))
        {
            _m.reportError(grammar_name_token.getLexeme().empty() ? module_file_token : grammar_name_token, 
                           u"Невозможно загрузить грамматику '" + grammar_name + u"'");
            return dummy_result;
        }
        p_g = &g;
    }
    else {
        if (!_gm->loadGrammar(false,convertToU8(grammar_name))) {
            _m.reportError(grammar_name_token.getLexeme().empty() ? module_file_token : grammar_name_token, 
                           u"Невозможно загрузить грамматику '" + grammar_name + u"'");
            return dummy_result;
        }
        p_g = & _gm->getGrammar(convertToU8(grammar_name));
    }

    Parser      p(module_path.string(), _m, *p_g);
    AstBuilder  builder(_m, module.ast, p_g->handlers);

    if (_p_content_provider) {
        std::unique_ptr<IStream> is = _p_content_provider->stream(module_path.string());

        ok = is ? p.parse(*is, builder) : false;
    }
    else
        ok = p.parse(builder);

    if (!ok) {
        _m.reportError(module_file_token, u"Ошибки синтаксиса в модуле '" + module_file_token.getLexeme() + u"'");
        module.ok = false;
        return module;
    }

    std::vector<SemanticScope> scope_set;

    ScriptC_Semantics checker(_m, *this, module.names, scope_set, &module.functions);

    ScriptC_NS_Sys           sys(_rtm);
    ScriptC_NS_Math          math;
    ScriptC_NS_Scene         scene;
    ScriptC_NS_TableFunction tf;

    checker.importNamespace(u"sys", sys.getNamespace());
    checker.importNamespace(u"math", math.getNamespace());
    checker.importNamespace(u"scene", scene.getNamespace());
    checker.importNamespace(u"tf", tf.getNamespace());

    for(const auto & [ad_name, ad_namespace] : _additional_namespaces)
        checker.importNamespace(ad_name, ad_namespace->getNamespace());

    if (!checker.check(module.ast)) {
        _m.reportError(module_file_token, u"Ошибки семантики в модуле '" + module_file_token.getLexeme() + u"'");
        module.ok = false;
        return module;
    }

    return module;
}

const ModuleImportResults &ModulesManagement::getModuleNames(string module_path) const
{
    // if (_mm != nullptr)
    //     return _mm->getModuleNames(module_path);

    if (auto it_module_names = _module_set.find(module_path); it_module_names != _module_set.end())
        return it_module_names->second;

    return dummy_result;
}

