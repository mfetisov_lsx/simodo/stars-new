/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <exception>
#include <locale>
#include <cstring>

#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/ModulesManagement.h"
#include "simodo/dsl/Exception.h"

#include "simodo/convert.h"

#define SCI_DEBUG_no

using namespace std;
using namespace simodo::dsl;

class InterpreterServiceException : public Exception
{
public:
    InterpreterServiceException() : Exception("InterpreterServiceException","Служебная обработка исключений") {}
};


namespace
{
    static SCI_Name dummy_name {u"", SemanticNameQualification::None, {}};

}


ScriptC_Interpreter::ScriptC_Interpreter(AReporter &m, ModulesManagement &mm, size_t stack_max_size)
    : _m(m)
    , _mm(mm)
    , _stack(stack_max_size)
    , _modules_quantity(0)
    , _stop_signal(false)
    , _pause_signal(false)
{
}

void ScriptC_Interpreter::importNamespace(u16string ns_name, const SCI_Namespace_t &ns_content)
{
    _stack.push({ns_name, SemanticNameQualification::Tuple, ns_content, SemanticNameAccess::ReadOnly});

    _modules_quantity++;
}

void ScriptC_Interpreter::reset()
{
    assert(_stack.size() == _modules_quantity);
    assert(_scope_marks.empty());
    assert(_condition_stack.empty());

    _stop_signal.store(false);
    _pause_signal.store(false);
}

SCI_RunnerCondition ScriptC_Interpreter::catchAst(const AstNode &ast)
{
    SCI_RunnerCondition state = SCI_RunnerCondition::Regular;

#ifndef SCI_DEBUG
    try
    {
#endif
        SCI_NamespaceAddScope scope(*this);

        state = runInner(ast);

#ifndef SCI_DEBUG
    }
    catch (const InterpreterServiceException & )
    {
        state = SCI_RunnerCondition::Error;
    }
    catch (...)
    {
        state = SCI_RunnerCondition::Error;
    }
#endif

    return state;
}

SCI_RunnerCondition ScriptC_Interpreter::runAst(const AstNode &ast)
{
    SCI_NamespaceAddScope scope(*this);

    SCI_RunnerCondition state = runInner(ast);

    return state;
}

SCI_RunnerCondition ScriptC_Interpreter::callInnerFunction(const AstNode &node, size_t stack_count, SCI_Name *parent, bool is_construct)
{
    SCI_FrameScope        call_frame(*this, stack_count, parent);
    SCI_NamespaceAddScope scope(*this);

    SCI_RunnerCondition state = runInner(node);

    if (is_construct && parent != nullptr && parent->qualification == SemanticNameQualification::Type)
    {
        assert(!_scope_marks.empty() && _scope_marks.back() <=_stack.size());

        SCI_Tuple & parent_tuple = get<SCI_Tuple>(parent->bulk);

        for(size_t i_stack=_scope_marks.back(); i_stack < _stack.size(); ++i_stack)
        {
            const SCI_Name & name_on_stack = _stack.atC(i_stack);
            size_t           i_name        = 0;

            for(; i_name < parent_tuple.size(); ++i_name)
                if (parent_tuple[i_name].name == name_on_stack.name)
                {
//                    if (parent_tuple[i_name].qualification != name_on_stack.qualification ||
//                        (parent_tuple[i_name].qualification == SemanticNameQualification::Scalar &&
//                         get<SCI_Scalar>(parent_tuple[i_name].bulk).type != get<SCI_Scalar>(name_on_stack.bulk).type))
//                    {
//                        throw Exception("callInnerFunction", "Сбой в инициализации членов кортежа при конструировании");
//                    }

                    parent_tuple[i_name] = name_on_stack;
                    break;
                }

            if (i_name == parent_tuple.size())
            {
                throw Exception("callInnerFunction", "Сбой в инициализации членов кортежа при конструировании");
            }
        }
    }

    return state;
}

SCI_RunnerCondition ScriptC_Interpreter::runInner(const AstNode &ast)
{
//    _m.reportInformation(u"ScriptC_Interpreter::runInner start:\t_stack.size = " + convertToU16(to_string(_stack.size())) +
//                         u", _condition_stack.size = " + convertToU16(to_string(_condition_stack.size())));

    SCI_RunnerCondition state = SCI_RunnerCondition::Regular;

    for(const auto & n : ast.getBranchC())
    {
#ifndef SCI_DEBUG
        try
        {
#endif
            {
                std::unique_lock lock(_pause_m);
                _pause_cv.wait(lock, [this] { return !_pause_signal.load(); });
            }

            if (_stop_signal.load())
            {
                throw Exception("runInner", "Выполнение прервано");
            }

            switch(n.getOperation())
            {
            case AstOperation::None:
                break;
//            case AstOperation::InternalPush:
//                _stack.push(SCI_Scalar {});
//                break;
            case AstOperation::InternalPop:
                _stack.pop();
                break;
            case AstOperation::Ternary_True:
            case AstOperation::Statement_If_True:
                state = performTrueClause(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Ternary_False:
            case AstOperation::Statement_If_False:
                state = performFalseClause(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Logical_Or:
            case AstOperation::Logical_And:
                state = performLogicalOperation(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Compare_Equal:
            case AstOperation::Compare_NotEqual:
            case AstOperation::Compare_Less:
            case AstOperation::Compare_LessOrEqual:
            case AstOperation::Compare_More:
            case AstOperation::Compare_MoreOrEqual:
                state = performCompareOperation(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Arithmetic_Addition:
            case AstOperation::Arithmetic_Subtraction:
            case AstOperation::Arithmetic_Multiplication:
            case AstOperation::Arithmetic_Division:
            case AstOperation::Arithmetic_Modulo:
            case AstOperation::Arithmetic_Power:
                state = performArithmeticOperation(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Unary_Plus:
            case AstOperation::Unary_Minus:
                state = performUnaryOperation(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Unary_Not:
                state = performUnaryNotOperation(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Push_Constant:
                state = pushConst(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Push_Id:
                state = pushId(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Function_Call:
            case AstOperation::Statement_Procedure_Call:
                state = callFunction(n, n.getOperation() == AstOperation::Statement_Procedure_Call);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_Blank:
                _m.reportWarning(n.getSymbol(), u"Желательно пользоваться пустым блоком вместо пустого оператора");
                break;

            case AstOperation::Statement_Id:
                state = performDeclaration(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_Assign:
            case AstOperation::Statement_Assign_Addition:
            case AstOperation::Statement_Assign_Subtraction:
            case AstOperation::Statement_Assign_Multiplication:
            case AstOperation::Statement_Assign_Division:
            case AstOperation::Statement_Assign_Modulo:
                assert(!n.getBranchC().empty());
                state = performAssignStatement(n, n.getBranchC().front());
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_Procedure_Call_Error:
                _m.reportError(n.getBound(), u"Некорректная запись адреса в качестве оператора");
                return SCI_RunnerCondition::Error;

            case AstOperation::Statement_Block:
                state = runAst(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_Break:
            case AstOperation::Statement_Continue:
                return performBreakContinue(n);

            case AstOperation::Statement_Return:
                return performReturn(n);

            case AstOperation::Statement_While:
                state = performCycleWhile(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_DoWhile:
                state = performCycleDoWhile(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_For:
                state = performCycleFor(n);
                if (state != SCI_RunnerCondition::Regular)
                    return state;
                break;

            case AstOperation::Statement_UseAsType:
                state = loadModuleAsType(n);
                break;

            case AstOperation::Statement_Using:
                state = performUsingStatement(n);
                break;

            case AstOperation::DataStructure_Array:
                state = pushDataStructure_Array(n);
                break;

            case AstOperation::DataStructure_Tuple:
                state = pushDataStructure_Tuple(n);
                break;

            default:
                state = SCI_RunnerCondition::Unsupported;
                break;
            }

            if (state == SCI_RunnerCondition::Unsupported)
            {
                _m.reportError(n.getSymbol(),
                               u"Операция '" + getAstOperationName(n.getOperation()) +
                               u"' не поддерживается. Выполнение прервано");
                return SCI_RunnerCondition::Error;
            }
        }
#ifndef SCI_DEBUG
        catch (const InterpreterServiceException & e)
        {
            throw;
        }
        catch (const Exception & e)
        {
            _m.reportError(n.getSymbol(),
                           u"Произошло исключение в методе " + convertToU16(e.where()) +
                           u" при обработке оператора " + getAstOperationName(n.getOperation()) +
                           u" ('" + n.getSymbol().getLexeme() +
                           u"')\nОписание исключения: " + convertToU16(e.what()));
            _stop_signal.store(true);
            throw InterpreterServiceException();
        }
        catch (const exception & e)
        {
            _m.reportError(n.getSymbol(),
                           u"Произошло исключение при обработке оператора " + getAstOperationName(n.getOperation()) +
                           u" ('" + n.getSymbol().getLexeme() +
                           u"')\nОписание исключения: " + convertToU16(e.what()));
            _stop_signal.store(true);
            throw InterpreterServiceException();
        }
        catch (...)
        {
            _m.reportError(n.getSymbol(),
                           u"Произошло исключение при обработке оператора " + getAstOperationName(n.getOperation()) +
                           u" ('" + n.getSymbol().getLexeme() + u"')");
            _stop_signal.store(true);
            throw;
        }
    }
#endif

//    _m.reportInformation(u"ScriptC_Interpreter::runInner end:\t_stack.size = " + convertToU16(to_string(_stack.size())) +
//                         u", _condition_stack.size = " + convertToU16(to_string(_condition_stack.size())));

    return state;
}

SCI_RunnerCondition ScriptC_Interpreter::loadModuleAsType(const AstNode &op)
{
    Token id_name_token;
    Token module_file_token;
    Token type_token;
    Token grammar_name_token;

    if (!op.getBranchC().front().getBranchC().empty())
        grammar_name_token = op.getBranchC().front().getBranchC().front().getSymbol();

    for(const AstNode & s : op.getBranchC())
        switch(s.getOperation())
        {
        case AstOperation::None:
            module_file_token = s.getSymbol();
            break;
        case AstOperation::Statement_Type:
            type_token = s.getSymbol();
            break;
        case AstOperation::Statement_Id:
            id_name_token = s.getSymbol();
            break;
        default:
            break;
        }

    const ModuleImportResults & module = _mm.loadModule(module_file_token, grammar_name_token);

    if (!module.ok)
        return SCI_RunnerCondition::Error;

    SCI_Name type { type_token.getLexeme(), SemanticNameQualification::Type, SCI_Tuple {} };

    SCI_Tuple & content = get<SCI_Tuple>(type.bulk);

    content.push_back({ u"_", SemanticNameQualification::Function, SCI_Tuple {
                                   {u"_", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction, &module.ast}},
                                   {u"",  SemanticNameQualification::None,{}}
                        }});

    for(size_t i_root_object=0; i_root_object < module.names.size(); ++i_root_object)
    {
        const SemanticName & n = module.names[i_root_object];

        if (n.depth == DEPTH_TOP_LEVEL && n.owner == UNDEFINED_INDEX)
            content.push_back(makeNameFromModule(module, i_root_object));
    }

    _stack.push(type);

    callInnerFunction(module.ast, _stack.size(), &_stack.top(), true);

    if (!id_name_token.getLexeme().empty())
    {
        SCI_Name variable = _stack.back();

        variable.name = id_name_token.getLexeme();
        variable.qualification = SemanticNameQualification::Tuple;

        _stack.push(variable);
    }

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performUsingStatement(const AstNode &op)
{
    SCI_Name & name = findName(op.getSymbol().getLexeme());

    if (name.name.empty())
    {
        _m.reportError(op.getSymbol(), u"Имя '" + op.getSymbol().getLexeme() + u"' не найдено");
        return SCI_RunnerCondition::Error;
    }

    if (name.qualification != SemanticNameQualification::Tuple)
    {
        _m.reportError(op.getSymbol(), u"Имя '" + op.getSymbol().getLexeme() + u"' должно быть кортежем (модулем) в зоне видимости");
        return SCI_RunnerCondition::Error;
    }

    _using_set.push_back(&name);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performTrueClause(const AstNode &op)
{
    assert(!_stack.empty());

    SCI_RunnerCondition state = SCI_RunnerCondition::Regular;

    if (_stack.back().qualification != SemanticNameQualification::Scalar)
    {
        _m.reportError(op.getSymbol(), u"Условие должно иметь скалярное значение логического типа");
        return SCI_RunnerCondition::Error;
    }

    const SCI_Scalar & scalar = get<SCI_Scalar>(_stack.back().bulk);

    if (scalar.type != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Условие должно иметь логический тип");
        return SCI_RunnerCondition::Error;
    }

    bool condition = get<bool>(scalar.variant);

    _stack.pop();

    if (condition)
    {
        state = runInner(op);
        if (state != SCI_RunnerCondition::Regular)
            return state;
    }

    _condition_stack.emplace_back(condition);
    return state;
}

SCI_RunnerCondition ScriptC_Interpreter::performFalseClause(const AstNode &op)
{
    assert(!_condition_stack.empty());

    SCI_RunnerCondition state     = SCI_RunnerCondition::Regular;
    bool                condition = _condition_stack.back();

    _condition_stack.pop_back();

    if (!condition)
    {
        state = runInner(op);
        if (state != SCI_RunnerCondition::Regular)
            return state;
    }

    return state;
}

SCI_RunnerCondition ScriptC_Interpreter::performLogicalOperation(const AstNode &op)
{
    assert(_stack.size() >= 2);

    if (_stack.top(1).qualification != SemanticNameQualification::Scalar ||
        _stack.top(0).qualification != SemanticNameQualification::Scalar)
    {
        _m.reportError(op.getSymbol(), u"Операнды для логической операции 'и'/'или' должны иметь скалярное значение");
        return SCI_RunnerCondition::Error;
    }

    /// \todo Нужно оптимизировать - править прямо на стеке!
    /// \attention  Правка прямо на стеке скалярных значений приводит к неопределённому поведению!
    ///             Скорее всего это связано с использованием вложенных вариантов. Вероятно,
    ///             стандартная реализация варианта не предусматривает правку вложенной части "по месту".
    ///             Хотя может быть и другое объяснение.

    const SCI_Scalar & operand1 = get<SCI_Scalar>(_stack.top(1).bulk);
    const SCI_Scalar & operand2 = get<SCI_Scalar>(_stack.top(0).bulk);

    if (operand1.type != SemanticNameType::Bool ||
        operand2.type != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Операнды для логической операции 'и'/'или' должны иметь логический тип");
        return SCI_RunnerCondition::Error;
    }

    SCI_Scalar res = operand1;

    if (op.getOperation() == AstOperation::Logical_Or)
        res.variant = (get<bool>(res.variant) || get<bool>(operand2.variant));
    else
        res.variant = (get<bool>(res.variant) && get<bool>(operand2.variant));

    _stack.pop(2);
    _stack.push(res);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performCompareOperation(const AstNode &op)
{
    assert(_stack.size() >= 2);

    if (_stack.top(1).qualification != SemanticNameQualification::Scalar ||
        _stack.top(0).qualification != SemanticNameQualification::Scalar)
    {
        _m.reportError(op.getSymbol(), u"Операнды для операции сравнения должны иметь скалярное значение");
        return SCI_RunnerCondition::Error;
    }

    /// \todo Нужно оптимизировать - править прямо на стеке!
    /// \attention  Правка прямо на стеке скалярных значений приводит к неопределённому поведению!
    ///             Скорее всего это связано с использованием вложенных вариантов. Вероятно,
    ///             стандартная реализация варианта не предусматривает правку вложенной части "по месту".
    ///             Хотя может быть и другое объяснение.

    const SCI_Scalar & operand1 = get<SCI_Scalar>(_stack.top(1).bulk);
    const SCI_Scalar & operand2 = get<SCI_Scalar>(_stack.top(0).bulk);

    if (operand1.type != operand2.type)
    {
        _m.reportError(op.getSymbol(), u"Операнды для операции сравнения должны иметь одинаковый тип");
        return SCI_RunnerCondition::Error;
    }

    if ((op.getOperation() == AstOperation::Compare_Equal || op.getOperation() == AstOperation::Compare_NotEqual)
            && operand1.type == SemanticNameType::Float)
    {
        // _m.reportWarning(op.getSymbol(), u"Операции сравнения '==' и '!=' для вещественных типов являются небезопасными");
    }

    SCI_Scalar res = operand1;

    switch(op.getOperation())
    {
    case AstOperation::Compare_Equal:
        res.variant = (res.variant == operand2.variant);
        break;
    case AstOperation::Compare_NotEqual:
        res.variant = (res.variant != operand2.variant);
        break;
    case AstOperation::Compare_Less:
        res.variant = (res.variant < operand2.variant);
        break;
    case AstOperation::Compare_LessOrEqual:
        res.variant = (res.variant <= operand2.variant);
        break;
    case AstOperation::Compare_More:
        res.variant = (res.variant > operand2.variant);
        break;
    case AstOperation::Compare_MoreOrEqual:
        res.variant = (res.variant >= operand2.variant);
        break;
    default:
        assert(false);
        break;
    }

    res.type = SemanticNameType::Bool;

    _stack.pop(2);
    _stack.push(res);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performArithmeticOperation(const AstNode &op)
{
    assert(_stack.size() >= 2);

    if (_stack.top(1).qualification != SemanticNameQualification::Scalar ||
        _stack.top(0).qualification != SemanticNameQualification::Scalar)
    {
        _m.reportError(op.getSymbol(), u"Операнды для арифметических операций должны иметь скалярное значение");
        return SCI_RunnerCondition::Error;
    }

    /// \todo Нужно оптимизировать - править прямо на стеке!
    /// \attention  Правка прямо на стеке скалярных значений приводит к неопределённому поведению!
    ///             Скорее всего это связано с использованием вложенных вариантов. Вероятно,
    ///             стандартная реализация варианта не предусматривает правку вложенной части "по месту".
    ///             Хотя может быть и другое объяснение.

    const SCI_Scalar & operand1 = get<SCI_Scalar>(_stack.top(1).bulk);
    const SCI_Scalar & operand2 = get<SCI_Scalar>(_stack.top(0).bulk);

    // Проверки
    if (!checkArithmeticOperation(op,operand1.type,operand2.type))
        return SCI_RunnerCondition::Error;

    // Операция
    SCI_Scalar res = calculateArithmeticOperation(op, operand1, operand2);

    _stack.pop(2);
    _stack.push(res);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performUnaryOperation(const AstNode &op)
{
    assert(!_stack.empty());

    if (_stack.back().qualification != SemanticNameQualification::Scalar)
    {
        _m.reportError(op.getSymbol(), u"Недопустимое применение операции унарного плюса/минуса");
        return SCI_RunnerCondition::Error;
    }

    SCI_Scalar operand = get<SCI_Scalar>(_stack.back().bulk);

    if (operand.type != SemanticNameType::Int && operand.type != SemanticNameType::Float)
    {
        _m.reportError(op.getSymbol(), u"Недопустимое применение операции унарного плюса/минуса");
        return SCI_RunnerCondition::Error;
    }

    if (op.getOperation() == AstOperation::Unary_Minus)
    {
        if (operand.type == SemanticNameType::Int)
            operand.variant = -get<int64_t>(operand.variant);
        else
            operand.variant = -get<double>(operand.variant);

        _stack.pop();
        _stack.push(operand);
    }

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performUnaryNotOperation(const AstNode &op)
{
    assert(!_stack.empty());

    if (_stack.back().qualification != SemanticNameQualification::Scalar)
    {
        _m.reportError(op.getSymbol(), u"Операнд для логического отрицания должен иметь скалярное значение");
        return SCI_RunnerCondition::Error;
    }

    SCI_Scalar operand = get<SCI_Scalar>(_stack.back().bulk);

    if (operand.type != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Операнд для логического отрицания должен иметь логический тип");
        return SCI_RunnerCondition::Error;
    }
    operand.variant = !get<bool>(operand.variant);

    _stack.pop();
    _stack.push(operand);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::pushConst(const AstNode &op)
{
    if (op.getSymbol().getType() == LexemeType::Annotation)
    {
        SCI_Scalar res;
        res.type = SemanticNameType::String;
        res.variant = op.getSymbol().getLexeme();
        _stack.push(res);
        return SCI_RunnerCondition::Regular;
    }

    if (op.getSymbol().getType() == LexemeType::Number)
    {
        SCI_Scalar res;

        if (op.getSymbol().getQualification() == TokenQualification::RealNubmer)
        {
            res.type = SemanticNameType::Float;
            string number_string = convertToU8(op.getSymbol().getLexeme());

            auto * le = localeconv();
            if(strcmp(le->decimal_point,".") != 0)
            {
                size_t dot_pos = number_string.find('.');
                if(string::npos != dot_pos)
                    number_string.replace(dot_pos,1,le->decimal_point,1);
            }

            double number = stod(number_string);
            res.variant = number;
        }
        else if (op.getSymbol().getQualification() == TokenQualification::Integer)
        {
            res.type = SemanticNameType::Int;
            res.variant = static_cast<int64_t>(stoll(convertToU8(op.getSymbol().getLexeme())));
        }
        else
        {
            assert(false);
        }
        _stack.push(res);
        return SCI_RunnerCondition::Regular;
    }

    if (op.getSymbol().getType() == LexemeType::Punctuation)
    {
        SCI_Scalar res {SemanticNameType::Bool, op.getSymbol().getLexeme() == u"true"};

        _stack.push(res);
        return SCI_RunnerCondition::Regular;
    }

    assert(false);
    return SCI_RunnerCondition::Error;
}

SCI_RunnerCondition ScriptC_Interpreter::pushId(const AstNode & op,
                                                SCI_Name * p_parent_name,
                                                SemanticNameAccess parent_access,
                                                AstOperation parent_iteration_qualificator)
{
    SCI_Name * parent = p_parent_name;
    SCI_Name * p_name = &findName(op.getSymbol().getLexeme(), &parent);

    if (p_name->name.empty())
    {
        _m.reportError(op.getSymbol(),u"Имя '" + op.getSymbol().getLexeme() + u"' не определено");
        return SCI_RunnerCondition::Error;
    }

    while(p_name->qualification == SemanticNameQualification::Reference)
        p_name = get<SCI_Reference>(p_name->bulk);

    if (p_name->qualification == SemanticNameQualification::Type)
    {
        _m.reportError(op.getSymbol(), u"Недопустимое использование типа " + op.getSymbol().getLexeme());
        return SCI_RunnerCondition::Error;
    }

    if (parent != nullptr)
        parent_access = strongestAccess(parent_access,parent->access);

    // Определяем квалификатор переменной

    const AstNode * var_op                     = nullptr;
    AstOperation    address_array_qualificator = AstOperation::None;
    AstOperation    address_tuple_qualificator = AstOperation::None;
    AstOperation    iteration_qualificator     = parent_iteration_qualificator;
    const AstNode * array_op                   = nullptr;

    for(const AstNode & o : op.getBranchC())
        switch(o.getOperation())
        {
        case AstOperation::Address_Array:
            array_op = &o;
            address_array_qualificator = o.getOperation();
            break;
        case AstOperation::Address_Tuple:
            address_tuple_qualificator = o.getOperation();
            break;
        case AstOperation::Iteration_PosfixPlus:
        case AstOperation::Iteration_PrefixPlus:
        case AstOperation::Iteration_PosfixMinus:
        case AstOperation::Iteration_PrefixMinus:
            assert(iteration_qualificator == AstOperation::None);
            iteration_qualificator = o.getOperation();
            break;
        default:
            assert(var_op == nullptr);
            var_op = &o;
            break;
        }

    // Обрабатываем квалификатор

    if (address_array_qualificator == AstOperation::Address_Array)
    {
        assert(array_op != nullptr);

        p_name = getArrayElement(op, *p_name, *array_op);

        if (p_name == nullptr)
            return SCI_RunnerCondition::Error;
    }

    if (address_tuple_qualificator == AstOperation::Address_Tuple)
    {
        assert(var_op != nullptr);

        SCI_RunnerCondition state = SCI_RunnerCondition::Regular;

        if (var_op->getOperation() == AstOperation::Push_Id)
        {
            state = pushId(*var_op,
                           p_name,
                           strongestAccess(p_name->access,parent_access),
                           iteration_qualificator);
        }
        else if (var_op->getOperation() == AstOperation::Statement_Procedure_Call ||
            var_op->getOperation() == AstOperation::Function_Call)
        {
            state = callFunction(*var_op,
                         var_op->getOperation() == AstOperation::Statement_Procedure_Call,
                         p_name);
        }
        else
            assert(false);

        return state;
    }

    SCI_Name & name = *p_name;

    // Проверка доступа пушим

    SemanticNameAccess access_level = strongestAccess(parent_access,name.access);

    if (iteration_qualificator != AstOperation::None)
    {
        /// \todo Нужно проверять, чтобы среди предков не было типов или функций!!!

        if (access_level != SemanticNameAccess::FullAccess)
        {
            _m.reportError(op.getSymbol(),
                           u"Присваивание элементу кортежа со спецификатором доступа " + getSemanticNameAccessName(access_level));
            return SCI_RunnerCondition::Error;
        }

        if (name.qualification != SemanticNameQualification::Scalar ||
            get<SCI_Scalar>(name.bulk).type != SemanticNameType::Int)
        {
            _m.reportError(op.getSymbol(),
                           u"Для использования итерации переменная '"+op.getSymbol().getLexeme()+
                           u"' должна иметь целочисленный тип");
            return SCI_RunnerCondition::Error;
        }

        // Итерация и пуш

        SCI_Scalar & scalar = get<SCI_Scalar>(name.bulk);

        if (iteration_qualificator == AstOperation::Iteration_PrefixPlus)
            scalar.variant = get<int64_t>(scalar.variant) + 1;
        else if (iteration_qualificator == AstOperation::Iteration_PrefixMinus)
            scalar.variant = get<int64_t>(scalar.variant) - 1;

        _stack.push(scalar);

        if (iteration_qualificator == AstOperation::Iteration_PosfixPlus)
            scalar.variant = get<int64_t>(scalar.variant) + 1;
        else if (iteration_qualificator == AstOperation::Iteration_PosfixMinus)
            scalar.variant = get<int64_t>(scalar.variant) - 1;
    }
    else if (access_level == SemanticNameAccess::Closed)
    {
        _m.reportError(op.getSymbol(),
                       u"Использование элемента кортежа со спецификатором доступа " + getSemanticNameAccessName(access_level));
        return SCI_RunnerCondition::Error;
    }
    else if (name.qualification == SemanticNameQualification::Tuple /*|| name.qualification == SemanticNameQualification::Array*/)
        _stack.push({name.name, SemanticNameQualification::Reference, (SCI_Name *) &name, access_level});
    else
        _stack.push(name);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::callFunction(const AstNode &op, bool is_procedure_call, SCI_Name *p_parent_name)
{
    SCI_Name * parent = p_parent_name;
    SCI_Name * p_function = &findName(op.getSymbol().getLexeme(), &parent);

    /// \todo Алгоритм нужно объединить с pushId: Можно рассматривать скобки как операцию вызова
    /// Или выделить общую часть

    if (p_function->name.empty())
    {
        _m.reportError(op.getSymbol(),u"Функция '" + op.getSymbol().getLexeme() + u"' не определена");
        return SCI_RunnerCondition::Error;
    }

    SCI_Name & function = * p_function;

    if (function.qualification != SemanticNameQualification::Function)
    {
        _m.reportError(op.getSymbol(), u"Имя '" + op.getSymbol().getLexeme() + u"' не является функцией");
        return SCI_RunnerCondition::Error;
    }

    const SCI_Tuple & parameters = get<SCI_Tuple>(function.bulk);
    assert(parameters.size() >= 2);

    size_t parameters_quantity = parameters.size() - 2;

    if (!is_procedure_call && parameters[1].qualification == SemanticNameQualification::None)
    {
        _m.reportError(op.getSymbol(),
                       u"Функция '"+op.getSymbol().getLexeme()+
                       u"' не возвращает значений и не может быть использована в выражении");
        return SCI_RunnerCondition::Error;
    }

    // Запоминаем размер стека, чтобы проверить соответствие с количеством параметров функции

    //TODO: Вообще-то лучше проверять кол-во параметров в АСД, а не на стеке (см. ниже).
    // Делать доп. уровень вложенности нежелательно, т.к. поиск операции Function_Call,
    // чтобы заменить её на Statement_Procedure_Call будет чрезмерно сложным

    size_t stack_count = _stack.size();

    // Обрабатываем параметры
    SCI_RunnerCondition state = runInner(op);
    if (state != SCI_RunnerCondition::Regular)
        return state;

    assert(parameters[0].qualification == SemanticNameQualification::Scalar);
    const SCI_Scalar & function_body = get<SCI_Scalar>(parameters[0].bulk);

    if (parameters_quantity != _stack.size()-stack_count)
    {
        /// \todo Можно реализовать параметры по умолчанию - просто добавлять недостающие величины

        _m.reportError(op.getSymbol(), u"Некорректное количество параметров функции '"+op.getSymbol().getLexeme()+u"'");
        return SCI_RunnerCondition::Error;
    }

    // Вызываем функцию
    if (function_body.type == SemanticNameType::ExtFunction)
    {
        for(size_t i=0; i+stack_count < _stack.size(); ++i) {
            const SCI_Name & parameter_bulk = parameters[i+2];
            SCI_Name &       stack_bulk     = _stack.at(i+stack_count);

            if (parameter_bulk.qualification == SemanticNameQualification::Scalar
             && stack_bulk.qualification == SemanticNameQualification::Scalar) {
                const SCI_Scalar & parameter_scalar = get<SCI_Scalar>(parameter_bulk.bulk);
                const SCI_Scalar & stack_scalar     = get<SCI_Scalar>(stack_bulk.bulk);

                if (parameter_scalar.type != stack_scalar.type) {
                    if (parameter_scalar.type == SemanticNameType::Float && stack_scalar.type == SemanticNameType::Int) {
                        double float_value = get<int64_t>(stack_scalar.variant);
                        SCI_Name & stack_variable = _stack.at(i+stack_count);
                        stack_variable = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, float_value}};
                    }
                    else {
                        _m.reportError(op.getSymbol(),
                                    u"Некорректный тип аргумента " + convertToU16(to_string(i+1)) +
                                    u" функции '" + op.getSymbol().getLexeme() + u"'");
                        return SCI_RunnerCondition::Error;
                    }
                }
            }
        }

        SCI_FrameScope      call_frame(*this, stack_count, parent);

        SCI_ExtFunction     ext_function = get<SCI_ExtFunction>(function_body.variant);
        SCI_RunnerCondition state        = (ext_function.second (ext_function.first, _stack))
                                         ? SCI_RunnerCondition::Regular
                                         : SCI_RunnerCondition::Error;
        return state;
    }

    if (function_body.type == SemanticNameType::Undefined)
    {
        _m.reportError(op.getSymbol(), u"Функция '"+op.getSymbol().getLexeme()+u"' не определена");
        return SCI_RunnerCondition::Error;
    }

    for(size_t i=0; i+stack_count < _stack.size(); ++i)
    {
        const SCI_Name & parameter_name = parameters[i+2];
        SCI_Name * p_stack_parameter = &_stack.at(i+stack_count);

        while(p_stack_parameter->qualification == SemanticNameQualification::Reference)
            p_stack_parameter = get<SCI_Reference>(p_stack_parameter->bulk);

        SCI_Name & stack_name = *p_stack_parameter;

        if (stack_name.qualification != SemanticNameQualification::Scalar)
        {
            if (stack_name.qualification != SemanticNameQualification::Tuple)
            {
                _m.reportError(op.getSymbol(),
                               u"Недопустимый тип аргумента " + convertToU16(to_string(i+1)) +
                               u" функции '" + op.getSymbol().getLexeme() + u"'");
                return SCI_RunnerCondition::Error;
            }
        }
        else
        {
            if (parameter_name.qualification != stack_name.qualification ||
                parameter_name.qualification != SemanticNameQualification::Scalar)
            {
                _m.reportError(op.getSymbol(),
                               u"Некорректный квалификационный тип аргумента " + convertToU16(to_string(i+1)) +
                               u" функции '" + op.getSymbol().getLexeme() + u"'");
                return SCI_RunnerCondition::Error;
            }

            const SCI_Scalar & parameter_value = get<SCI_Scalar>(parameter_name.bulk);
            const SCI_Scalar & stack_value = get<SCI_Scalar>(stack_name.bulk);

            if (parameter_value.type != stack_value.type)
            {
                if (parameter_value.type == SemanticNameType::Float && stack_value.type == SemanticNameType::Int) {
                    if (_stack.at(i+stack_count).qualification == SemanticNameQualification::Reference) {
                        _m.reportError(op.getSymbol(),
                                    u"Внутренний сбой при передаче на стек ссылки, вместо значения для типа аргумента " + convertToU16(to_string(i+1)) +
                                    u" функции '" + op.getSymbol().getLexeme() + u"'");
                        return SCI_RunnerCondition::Error;
                    }
                    double f_value = get<int64_t>(stack_value.variant);
                    SCI_Name & stack_variable = _stack.at(i+stack_count);
                    stack_variable = {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, f_value}};
                }
                else {
                    _m.reportError(op.getSymbol(),
                                u"Некорректный тип аргумента " + convertToU16(to_string(i+1)) +
                                u" функции '" + op.getSymbol().getLexeme() + u"'");
                    return SCI_RunnerCondition::Error;
                }
            }
        }

        _stack.at(i+stack_count).name = parameter_name.name;
    }

    const AstNode & node = *get<const AstNode *>(function_body.variant);

    _return_value = {u"", SemanticNameQualification::None,{}};

    state = callInnerFunction(node, stack_count, parent);

    _stack.pop(parameters_quantity);

    if (state != SCI_RunnerCondition::Regular && state != SCI_RunnerCondition::Return)
        return state;

    if (parameters[1].qualification != SemanticNameQualification::None &&
        state != SCI_RunnerCondition::Return)
    {
        _m.reportError(node.getSymbol(),
                       u"Функция '" + op.getSymbol().getLexeme() + u"' должна возвращать значение");
        return SCI_RunnerCondition::Error;
    }

    if (state == SCI_RunnerCondition::Return)
        _stack.push(_return_value);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performDeclaration(const AstNode &op)
{
    assert(!op.getBranchC().empty());

    {
        const AstNode & back_node = op.getBranchC().back();

        if (back_node.getOperation() == AstOperation::Function_Declaration || back_node.getOperation() == AstOperation::Function_Body)
            return performFunctionDeclaration(op);
    }

    // Проверки

    assert(!_scope_marks.empty());

    if (size_t index = _stack.find(op.getSymbol().getLexeme()); index != NOT_FOUND_INDEX && index >= _scope_marks.back())
    {
        _m.reportError(op.getSymbol(),
                       u"Имя '" + op.getSymbol().getLexeme() + u"' уже объявлено в текущей зоне видимости");
        return SCI_RunnerCondition::Error;
    }

    bool       is_auto = (op.getBranchC().front().getSymbol().getLexeme() == u"auto");
    SCI_Scalar scalar_test = getScalar(op.getBranchC().front());
    size_t     type_index  = UNDEFINED_INDEX;

    if (scalar_test.type == SemanticNameType::Undefined && !is_auto)
    {
        // Пользовательский тип?

        u16string type_name  = op.getBranchC().front().getSymbol().getLexeme();

        type_index = _stack.find(type_name);

        if (type_index == NOT_FOUND_INDEX ||
            _stack.atC(type_index).qualification != SemanticNameQualification::Type)
        {
            _m.reportError(op.getSymbol(), u"Переменная '" + op.getSymbol().getLexeme() + u"' имеет недопустимый тип");
            return SCI_RunnerCondition::Error;
        }
    }

    vector<uint16_t> dimensions;
    bool             is_array = false;

    auto it = op.getBranchC().begin();  // type

    ++ it;  // array dimensions or assignment

    if (it != op.getBranchC().end() && it->getOperation() == AstOperation::None && it->getSymbol().getLexeme() == u"[")
    {
        // Размерность массива

        is_array = true;

        if (is_auto)
        {
            _m.reportError(op.getSymbol(),
                           u"Спецификатор 'auto' недопустим с определением размерности для переменной '" + op.getSymbol().getLexeme() + u"'");
            return SCI_RunnerCondition::Error;
        }

        size_t last_stack_count = _stack.size();

        SCI_RunnerCondition state = runInner(*it);
        if (state != SCI_RunnerCondition::Regular)
            return state;

        assert(_stack.size() >= last_stack_count);

        size_t dimentions_count = _stack.size() - last_stack_count;
        size_t i                = dimentions_count;

        dimensions.reserve(dimentions_count);

        while(i != 0)
        {
            const SCI_Name & dim_name = _stack.top(i-1);

            if (dim_name.qualification != SemanticNameQualification::Scalar ||
                get<SCI_Scalar>(dim_name.bulk).type != SemanticNameType::Int ||
                get<int64_t>(get<SCI_Scalar>(dim_name.bulk).variant) <= 0 ||
                get<int64_t>(get<SCI_Scalar>(dim_name.bulk).variant) > UINT16_MAX)
            {
                _m.reportError(it->getSymbol(),
                               u"Размерность массива должна быть целым числом в диапазоне от 1 до " + convertToU16(to_string(UINT16_MAX)));
                return SCI_RunnerCondition::Error;
            }

            int64_t dim = get<int64_t>(get<SCI_Scalar>(dim_name.bulk).variant);

            dimensions.push_back(static_cast<uint16_t>(dim));

            -- i;
        }

        _stack.pop(dimentions_count);

        ++ it;
    }

    if (it != op.getBranchC().end() && it->getOperation() == AstOperation::Statement_Assign)
    {
        // Присваивание

        assert(!it->getBranchC().empty());

        // Выполнение выражения в присваивании при объявлении переменной
        SCI_RunnerCondition state = runInner(*it);
        if (state != SCI_RunnerCondition::Regular)
            return state;

        SCI_Name * p_stack_name = &_stack.back();

        while(p_stack_name->qualification == SemanticNameQualification::Reference)
            p_stack_name = get<SCI_Reference>(p_stack_name->bulk);

        SCI_Name & stack_name = *p_stack_name;

        if (is_auto)
        {
        }
        else if (is_array)
        {
            if (stack_name.qualification != SemanticNameQualification::Array)
            {
                _m.reportError(op.getSymbol(), u"Квалификатор выражения в операторе присваивания не соответствует типу переменной");
                return SCI_RunnerCondition::Error;
            }

            SCI_Array & array_on_stack = get<SCI_Array>(stack_name.bulk);

            if (dimensions.empty())
            {
                dimensions = array_on_stack.dimensions;
            }
            else
            {
                if (dimensions.size() != array_on_stack.dimensions.size())
                {
                    _m.reportError(op.getSymbol(), u"Количество размерностей массива у обявления и определения не равно");
                    return SCI_RunnerCondition::Error;
                }

                for(size_t i=0; i < dimensions.size(); ++i)
                    if (dimensions[i] != array_on_stack.dimensions[i])
                    {
                        _m.reportError(op.getSymbol(), u"Размерность " + convertToU16(to_string(i+1)) + u" объявления и определения не совпадают");
                        return SCI_RunnerCondition::Error;
                    }
            }

            if (type_index == UNDEFINED_INDEX)
            {
                // Встроенный тип
                for(const SCI_Name & n : array_on_stack.values)
                    if (n.qualification != SemanticNameQualification::Scalar ||
                        get<SCI_Scalar>(n.bulk).type != scalar_test.type)
                    {
                        _m.reportError(op.getSymbol(), u"Типы объявления и определения не совпадают");
                        return SCI_RunnerCondition::Error;
                    }
            }
            else
            {
                // Массив кортежей. Выборочное присваивание
                SCI_Name variable = _stack.atC(type_index);

                variable.name = u"";
                variable.qualification = SemanticNameQualification::Tuple;

                SCI_Array array {dimensions,{}};

                size_t dim_size = 1;

                for(auto n : dimensions)
                    dim_size *= n;

                array.values.resize(dim_size, variable);

                for(size_t i_array=0; i_array < array.values.size(); ++i_array)
                {
                    SCI_Name * p_name_in_array_on_stack = &array_on_stack.values[i_array];

                    while(p_name_in_array_on_stack->qualification == SemanticNameQualification::Reference)
                        p_name_in_array_on_stack = get<SCI_Reference>(p_name_in_array_on_stack->bulk);

                    const SCI_Name & name_in_array_on_stack = *p_name_in_array_on_stack;
                    SCI_Tuple &      tuple                  = get<SCI_Tuple>(array.values[i_array].bulk);

                    SCI_RunnerCondition state = performTupleSelectiveAssigment(op,name_in_array_on_stack,_stack.atC(type_index),tuple);
                    if (state != SCI_RunnerCondition::Regular)
                        return state;
                }

                _stack.pop();
                _stack.push(array, op.getSymbol().getLexeme());

                return SCI_RunnerCondition::Regular;
            }
        }
        else if (type_index == UNDEFINED_INDEX)
        {
            // Встроенный тип

            if (stack_name.qualification != SemanticNameQualification::Scalar)
            {
                _m.reportError(op.getSymbol(), u"Квалификатор выражения в операторе присваивания не соответствует типу переменной");
                return SCI_RunnerCondition::Error;
            }

            SCI_Scalar & stack_scalar = get<SCI_Scalar>(stack_name.bulk);

            if (stack_scalar.type != scalar_test.type)
            {
                if (stack_scalar.type == SemanticNameType::Int && scalar_test.type == SemanticNameType::Float)
                {
                    stack_scalar.type = scalar_test.type;
                    stack_scalar.variant = static_cast<double>(get<int64_t>(stack_scalar.variant));
                }
                else
                {
                    _m.reportError(it -> getSymbol(), u"Тип выражения в операторе присваивания не соответствует типу переменной");
                    return SCI_RunnerCondition::Error;
                }
            }
        }
        else
        {
            // Пользовательский тип (без массивов)

            SCI_Name    variable    = _stack.atC(type_index);
            SCI_Tuple & var_content = get<SCI_Tuple>(variable.bulk);

            variable.name = op.getSymbol().getLexeme();
            variable.qualification = SemanticNameQualification::Tuple;

            SCI_RunnerCondition state = performTupleSelectiveAssigment(op,stack_name,_stack.atC(type_index),var_content);
            if (state != SCI_RunnerCondition::Regular)
                return state;

            _stack.pop();
            _stack.push(variable);

            return SCI_RunnerCondition::Regular;
        }

        stack_name.name = op.getSymbol().getLexeme();
    }
    else
    {
        if (is_auto)
        {
            _m.reportError(op.getSymbol(), u"Некорректное использование ключевого слова 'auto'");
            return SCI_RunnerCondition::Error;
        }

        SCI_Name variable;

        if (type_index == UNDEFINED_INDEX)
        {
            // Встроенный тип
            variable = {op.getSymbol().getLexeme(), SemanticNameQualification::Scalar, scalar_test};
        }
        else
        {
            // Пользовательский тип
            variable = _stack.atC(type_index);

            variable.name = op.getSymbol().getLexeme();
            variable.qualification = SemanticNameQualification::Tuple;
        }

        if (!dimensions.empty())
        {
            // Массив
            SCI_Name array_variable {op.getSymbol().getLexeme(), SemanticNameQualification::Array, SCI_Array {dimensions,{}}};

            _stack.push(array_variable);

            SCI_Array & array = get<SCI_Array>(_stack.top().bulk);

            size_t dim_size = 1;

            for(auto n : dimensions)
                dim_size *= n;

            variable.name = u"";

            array.values.resize(dim_size, variable);
        }
        else
            _stack.push(variable);
    }

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performFunctionDeclaration(const AstNode &op)
{
    // Проверки
    if (op.getParentC() != nullptr && op.getParentC()->getParentC() != nullptr)
    {
        _m.reportError(op.getSymbol(), u"Объявление и определение функции недопустимо внутри блока операторов");
        return SCI_RunnerCondition::Error;
    }

    size_t found_index = _stack.find(op.getSymbol().getLexeme());

    if (found_index != NOT_FOUND_INDEX &&
        _stack.atC(found_index).qualification != SemanticNameQualification::Function)
    {
        _m.reportError(op.getSymbol(),
                       u"Функция '" + op.getSymbol().getLexeme() +
                       u"' конфликтует с ранее объявленной не функцией");
        return SCI_RunnerCondition::Error;
    }

    // Заполняем описание

    assert(!op.getBranchC().empty());

    /// \todo Функция может возвращать не только скаляр

    SCI_Scalar function_type = getScalar(op.getBranchC().front());

    SCI_Tuple parameters
    {
        { u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::IntFunction,static_cast<const AstNode *>(nullptr)}}
    };

    if (function_type.type == SemanticNameType::Undefined)
        parameters.push_back({ u"", SemanticNameQualification::None, {}});
    else
        parameters.push_back({ u"", SemanticNameQualification::Scalar, function_type});

    auto it_element = op.getBranchC().begin();
    it_element ++;
    assert(it_element != op.getBranchC().end());

    auto it_last_parameters = (found_index != NOT_FOUND_INDEX) ? get<SCI_Tuple>(_stack.atC(found_index).bulk).begin() : SCI_Tuple::iterator();

    if (found_index != NOT_FOUND_INDEX)
    {
        assert(it_last_parameters != get<SCI_Tuple>(_stack.atC(found_index).bulk).end());
        it_last_parameters ++;
        assert(it_last_parameters != get<SCI_Tuple>(_stack.atC(found_index).bulk).end());

        if (it_last_parameters->qualification != SemanticNameQualification::Scalar ||
            get<SCI_Scalar>(it_last_parameters->bulk).type != function_type.type)
        {
            _m.reportError(op.getSymbol(),
                           u"Тип функции '" + op.getSymbol().getLexeme() +
                           u"' не соответствует ранее описанному");
            return SCI_RunnerCondition::Error;
        }

        it_last_parameters ++;
    }

    size_t param_count = 1;
    for(const AstNode & p : it_element->getBranchC())
    {
        SCI_Name * parent = nullptr;
        const SCI_Name & type = findName(p.getSymbol().getLexeme(), &parent);

        if (!type.name.empty() && type.qualification != SemanticNameQualification::Type)
        {
            _m.reportError(p.getSymbol(), u"Имя '" + p.getSymbol().getLexeme() + u"' не является типом");
            return SCI_RunnerCondition::Error;
        }

        u16string param_name = (!p.getBranchC().empty()) ? p.getBranchC().front().getSymbol().getLexeme() : u"";

        SCI_Name param = { param_name, SemanticNameQualification::Scalar, getScalar(p) };

        u16string param_count_string = !param.name.empty()
                ? (u"'" + param.name + u"'")
                : convertToU16(to_string(param_count));

        if (get<SCI_Scalar>(param.bulk).type == SemanticNameType::Undefined && type.name.empty())
        {
            _m.reportError(op.getSymbol(),
                           u"Параметр " + param_count_string +
                           u" функции '" + op.getSymbol().getLexeme() + u"' имеет недопустимый тип");
            return SCI_RunnerCondition::Error;
        }

        if (found_index != NOT_FOUND_INDEX)
        {
            if (it_last_parameters == get<SCI_Tuple>(_stack.atC(found_index).bulk).end())
            {
                _m.reportError(op.getSymbol(),
                               u"Количество параметров функции '" + op.getSymbol().getLexeme() +
                               u"' не соответствует ранее описанному");
                return SCI_RunnerCondition::Error;
            }

            if (get<SCI_Scalar>(it_last_parameters->bulk).type != get<SCI_Scalar>(param.bulk).type)
            {
                _m.reportError(op.getSymbol(),
                               u"Тип параметра " + param_count_string +
                               u" функции '" + op.getSymbol().getLexeme() +
                               u"' не соответствует ранее описанному");
                return SCI_RunnerCondition::Error;
            }

            it_last_parameters ++;
        }

        parameters.push_back(param);
        param_count ++;
    }

    if (found_index != NOT_FOUND_INDEX && it_last_parameters != get<SCI_Tuple>(_stack.atC(found_index).bulk).end())
    {
        _m.reportError(op.getSymbol(),
                       u"Количество параметров функции '" + op.getSymbol().getLexeme() +
                       u"' не соответствует ранее описанному");
        return SCI_RunnerCondition::Error;
    }

    // Определение функции

    it_element ++;
    if(it_element != op.getBranchC().end())
    {
        get<SCI_Scalar>(parameters.front().bulk).type = SemanticNameType::IntFunction;
        const AstNode * node = &(*it_element);
        get<SCI_Scalar>(parameters.front().bulk).variant = node;
    }
    else
    {
        get<SCI_Scalar>(parameters.front().bulk).type = SemanticNameType::Undefined;
        get<SCI_Scalar>(parameters.front().bulk).variant = SCI_UNDEF_STRING;
    }

    _stack.push(SCI_Name {op.getSymbol().getLexeme(), SemanticNameQualification::Function, parameters});

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performAssignStatement(const AstNode &op,
                                                                const AstNode &var,
                                                                SCI_Name * p_parent_name,
                                                                SemanticNameAccess parent_access)
{
    SCI_Name * parent = p_parent_name;
    SCI_Name * p_name = &findName(var.getSymbol().getLexeme(), &parent);

    if (p_name->name.empty())
    {
        _m.reportError(var.getSymbol(),u"Переменная '" + var.getSymbol().getLexeme() + u"' не определена");
        return SCI_RunnerCondition::Error;
    }

    if (p_name->qualification == SemanticNameQualification::Reference)
        p_name = get<SCI_Reference>(p_name->bulk);

    if (parent != nullptr)
        parent_access = strongestAccess(parent_access,parent->access);

    SemanticNameAccess access_level = strongestAccess(parent_access, p_name->access);

    if (p_name->qualification == SemanticNameQualification::Type)
    {
        _m.reportError(op.getSymbol(), u"Недопустимое использование типа " + var.getSymbol().getLexeme());
        return SCI_RunnerCondition::Error;
    }

    if (!var.getBranchC().empty())
    {
        if (access_level != SemanticNameAccess::FullAccess)
        {
            _m.reportError(op.getSymbol(),
                           u"Присваивание элементу кортежа со спецификатором доступа " + getSemanticNameAccessName(access_level));
            return SCI_RunnerCondition::Error;
        }

        auto it = var.getBranchC().begin();

        if (var.getBranchC().front().getOperation() == AstOperation::Address_Array)
        {
            p_name = getArrayElement(op, *p_name, var.getBranchC().front());

            if (p_name == nullptr)
                return SCI_RunnerCondition::Error;

            ++it;
        }

        if (it->getOperation() == AstOperation::Address_Tuple)
        {
            ++it;
            if (it->getOperation() != AstOperation::Push_Id)
            {
                _m.reportError(op.getSymbol(),
                               u"Операция '"+op.getSymbol().getLexeme()+u"' для имени " + it->getSymbol().getLexeme() + u" не поддерживается");
                return SCI_RunnerCondition::Error;
            }

            return performAssignStatement(op, *it, p_name, access_level);
        }
    }

    SCI_Name & name = *p_name;

    // Проверки

    // Вычисляем правую часть
    SCI_RunnerCondition state = runInner(op.getBranchC().back());
    if (state != SCI_RunnerCondition::Regular)
        return state;

    assert(!_stack.empty());

    SCI_Name * p_rvalue_origin = &_stack.top();

    while(p_rvalue_origin->qualification == SemanticNameQualification::Reference)
        p_rvalue_origin = get<SCI_Reference>(p_rvalue_origin->bulk);

    SCI_Name & rvalue = *p_rvalue_origin;

    if (rvalue.qualification != name.qualification)
    {
        _m.reportError(op.getSymbol(), u"Тип выражения в операторе присваивания не соответствует типу переменной (разные квалификаторы)");
        return SCI_RunnerCondition::Error;
    }

    // Присваиваем
    switch(op.getOperation())
    {
    case AstOperation::Statement_Assign:
        if (name.qualification == SemanticNameQualification::Scalar)
        {
            SCI_Scalar &       name_scalar    = get<SCI_Scalar>(name.bulk);
            const SCI_Scalar & onstack_scalar = get<SCI_Scalar>(rvalue.bulk);

            if (name_scalar.type != onstack_scalar.type)
            {
                if (onstack_scalar.type == SemanticNameType::Int && name_scalar.type == SemanticNameType::Float)
                {
                    name_scalar.variant = static_cast<double>(get<int64_t>(onstack_scalar.variant));
                }
                else
                {
                    _m.reportError(op.getSymbol(), u"Тип выражения в операторе присваивания не соответствует типу переменной (оба скаляры)");
                    return SCI_RunnerCondition::Error;
                }
            }
            else
                name_scalar.variant = onstack_scalar.variant;
        }
        else
            name.bulk = rvalue.bulk;
        break;
    case AstOperation::Statement_Assign_Addition:
    case AstOperation::Statement_Assign_Subtraction:
    case AstOperation::Statement_Assign_Multiplication:
    case AstOperation::Statement_Assign_Division:
    case AstOperation::Statement_Assign_Modulo:
    {
        if (name.qualification != SemanticNameQualification::Scalar)
        {
            _m.reportError(op.getSymbol(), u"Операция недопустима с не скалярным типом");
            return SCI_RunnerCondition::Error;
        }

        SCI_Scalar &       name_scalar    = get<SCI_Scalar>(name.bulk);
        const SCI_Scalar & onstack_scalar = get<SCI_Scalar>(rvalue.bulk);

        // Проверки типов
        if (!checkArithmeticOperation(op,name_scalar.type,onstack_scalar.type))
            return SCI_RunnerCondition::Error;

        SCI_Scalar res = calculateArithmeticOperation(op, {name_scalar.type, name_scalar.variant}, onstack_scalar);

        if (res.type != name_scalar.type)
        {
            _m.reportError(op.getSymbol(), u"Тип выражения в операторе комбинированного присваивания не соответствует типу переменной");
            return SCI_RunnerCondition::Error;
        }
        name_scalar.variant = res.variant;
        break;
    }
    default:
        assert(false);
        break;
    }

    _stack.pop();

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performBreakContinue(const AstNode &op)
{
    const AstNode * parent = op.getParentC();

    while(parent != nullptr)
    {
        if (parent->getOperation() == AstOperation::Statement_For ||
                parent->getOperation() == AstOperation::Statement_While ||
                parent->getOperation() == AstOperation::Statement_DoWhile)
            break;
        parent = parent -> getParentC();
    }

    if (parent == nullptr)
    {
        _m.reportError(op.getSymbol(),
                       u"Оператор " + op.getSymbol().getLexeme() + u" не находится внутри тела цикла");
        return SCI_RunnerCondition::Error;
    }

    return op.getOperation() == AstOperation::Statement_Break ? SCI_RunnerCondition::Break : SCI_RunnerCondition::Continue;
}

SCI_RunnerCondition ScriptC_Interpreter::performReturn(const AstNode &op)
{
    const AstNode * parent = op.getParentC();

    while(parent != nullptr)
    {
        if (parent->getOperation() == AstOperation::Function_Body)
            break;
        parent = parent -> getParentC();
    }

    if (parent == nullptr)
    {
        _m.reportError(op.getSymbol(),
                       u"Оператор " + op.getSymbol().getLexeme() + u" не находится внутри тела функции");
        return SCI_RunnerCondition::Error;
    }

    assert(parent->getParentC() != nullptr);

    const AstNode & func_decl_node = *(parent->getParentC());

    auto it = func_decl_node.getBranchC().begin();

    assert(it != func_decl_node.getBranchC().end());

    SCI_Scalar func_decl_ret_val {getScalar(*it)};

    if (func_decl_ret_val.type == SemanticNameType::Undefined && !op.getBranchC().empty())
    {
        _m.reportError(op.getBranchC().front().getSymbol(),
                       u"Функция '" + func_decl_node.getSymbol().getLexeme() + u"' не должна возвращать значений");
        return SCI_RunnerCondition::Error;
    }

    if (func_decl_ret_val.type != SemanticNameType::Undefined && op.getBranchC().empty())
    {
        _m.reportError(op.getSymbol(),
                       u"Функция '" + func_decl_node.getSymbol().getLexeme() + u"' должна возвращать значение");
        return SCI_RunnerCondition::Error;
    }

    if (!op.getBranchC().empty())
    {
        SCI_RunnerCondition status = runInner(op);

        if (status == SCI_RunnerCondition::Error)
            return status;

        assert(!_stack.empty());

        if (_stack.back().qualification != SemanticNameQualification::Scalar ||
            !checkTypeConversion(get<SCI_Scalar>(_stack.back().bulk).type,func_decl_ret_val.type))
        {
            _m.reportError(op.getBranchC().front().getSymbol(),
                           u"Некорректный тип возвращаемого значения функции '" + func_decl_node.getSymbol().getLexeme() + u"'");
            return SCI_RunnerCondition::Error;
        }

        _return_value = _stack.top();

        _stack.pop();
    }

    return SCI_RunnerCondition::Return;
}

SCI_RunnerCondition ScriptC_Interpreter::performCycleWhile(const AstNode &op)
{
    assert(op.getBranchC().size() == 2); /// \todo Bad!

    while(true)
    {
        SCI_RunnerCondition state = runInner(op.getBranchC().front());
        if (state != SCI_RunnerCondition::Regular)
            return state;

        assert(!_stack.empty());

        if (_stack.back().qualification != SemanticNameQualification::Scalar ||
            get<SCI_Scalar>(_stack.back().bulk).type != SemanticNameType::Bool)
        {
            _m.reportError(op.getSymbol(), u"Условие цикла while должно иметь логический тип");
            return SCI_RunnerCondition::Error;
        }

        bool condition = get<bool>(get<SCI_Scalar>(_stack.back().bulk).variant);

        _stack.pop();

        if (!condition)
            break;

        state = runInner(op.getBranchC().back());

        if (state == SCI_RunnerCondition::Continue || state == SCI_RunnerCondition::Regular)
            continue;
        else if (state == SCI_RunnerCondition::Break)
            break;
        else
            return state;
    }

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performCycleDoWhile(const AstNode &op)
{
    assert(op.getBranchC().size() == 2);

    while(true)
    {
        SCI_RunnerCondition state = runInner(op.getBranchC().front());

        if (state == SCI_RunnerCondition::Continue || state == SCI_RunnerCondition::Regular)
        {}
        else if (state == SCI_RunnerCondition::Break)
            break;
        else
            return state;

        state = runInner(op.getBranchC().back());
        if (state != SCI_RunnerCondition::Regular)
            return state;

        assert(!_stack.empty());

        if (_stack.back().qualification != SemanticNameQualification::Scalar ||
            get<SCI_Scalar>(_stack.back().bulk).type != SemanticNameType::Bool)
        {
            _m.reportError(op.getBranchC().back().getSymbol(), u"Условие цикла do-while должно иметь логический тип");
            return SCI_RunnerCondition::Error;
        }

        bool condition = get<bool>(get<SCI_Scalar>(_stack.back().bulk).variant);

        _stack.pop();

        if (!condition)
            break;
    }

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performCycleFor(const AstNode &op)
{
    assert(!op.getBranchC().empty());

    SCI_NamespaceAddScope scope(*this);

    auto it = op.getBranchC().begin();

    if (!it->getBranchC().empty())
    {
        // Перый фрагмент цикла for - определение итератора
        auto it_declaration = it;

        SCI_RunnerCondition state = runInner(*it_declaration);
        if (state != SCI_RunnerCondition::Regular)
            return state;
    }

    it++;
    assert(it != op.getBranchC().end());

    // Второй фрагмент цикла for - условие
    auto it_condition = it;

    it++;
    assert(it != op.getBranchC().end());

    // Третий фрагмент цикла for - итерация
    auto it_iteration = it;

    it++;
    assert(it != op.getBranchC().end());

    // Четвёртый фрагмент цикла for - тело
    auto it_body = it;

    // Теперь всё вместе!
    while(true)
    {
        if (!it_condition->getBranchC().empty())
        {
            SCI_RunnerCondition state = runInner(*it_condition);
            if (state != SCI_RunnerCondition::Regular)
                return state;

            assert(!_stack.empty());

            if (_stack.back().qualification != SemanticNameQualification::Scalar ||
                get<SCI_Scalar>(_stack.back().bulk).type != SemanticNameType::Bool)
            {
                _m.reportError(it_condition->getSymbol(),u"Условие цикла for должно иметь логический тип");
                return SCI_RunnerCondition::Error;
            }

            bool condition = get<bool>(get<SCI_Scalar>(_stack.back().bulk).variant);

            _stack.pop();

            if (!condition)
                break;
        }

        if (!it_body->getBranchC().empty())
        {
            SCI_RunnerCondition state = runInner(*it_body);

            if (state == SCI_RunnerCondition::Continue || state == SCI_RunnerCondition::Regular)
            {
            }
            else if (state == SCI_RunnerCondition::Break)
                break;
            else
                return state;
        }

        if (!it_iteration->getBranchC().empty())
        {
            SCI_RunnerCondition state = runInner(*it_iteration);
            _stack.pop();
            if (state != SCI_RunnerCondition::Regular)
                return state;
        }
    }

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::pushDataStructure_Array(const AstNode &op)
{
    assert(op.getOperation() == AstOperation::DataStructure_Array);

    size_t last_stack_size = _stack.size();

    SCI_RunnerCondition state = runInner(op);
    if (state != SCI_RunnerCondition::Regular)
        return state;

    assert(_stack.size() >= last_stack_size);

    size_t new_elements_count = _stack.size() - last_stack_size;

    if (new_elements_count == 0)
    {
        _m.reportError(op.getSymbol(), u"Пустые размерности не поддерживаются");
        return SCI_RunnerCondition::Error;
    }

    const SCI_Name & first_element_on_stack = _stack.atC(last_stack_size);

    vector<uint16_t> dimensions {static_cast<uint16_t>(new_elements_count)};
    vector<SCI_Name> values;

    if (first_element_on_stack.qualification == SemanticNameQualification::Array)
    {
        const SCI_Array & first_array_on_stack = get<SCI_Array>(first_element_on_stack.bulk);

        dimensions.insert(dimensions.end(), first_array_on_stack.dimensions.begin(), first_array_on_stack.dimensions.end());

        for(size_t i=last_stack_size; i < _stack.size(); ++i)
        {
            const SCI_Name & n = _stack.atC(i);

            if (n.qualification != SemanticNameQualification::Array)
            {
                _m.reportError(op.getSymbol(), u"Размерность должна состоять из однотипных элементов");
                return SCI_RunnerCondition::Error;
            }

            const SCI_Array & a = get<SCI_Array>(n.bulk);

            if (a.dimensions.size() != first_array_on_stack.dimensions.size())
            {
                _m.reportError(op.getSymbol(), u"Элементы должны иметь одинаковую размерность");
                return SCI_RunnerCondition::Error;
            }

            values.insert(values.end(), a.values.begin(), a.values.end());
        }

        size_t dim_size = 1;

        for(uint16_t d : dimensions)
            dim_size *= d;

        if (dim_size != values.size())
        {
            _m.reportError(op.getSymbol(), u"Некорректное количество элементов");
            return SCI_RunnerCondition::Error;
        }
    }
    else
    {
        for(size_t i=last_stack_size; i < _stack.size(); ++i)
        {
            const SCI_Name & n = _stack.atC(i);

            if (n.qualification != first_element_on_stack.qualification ||
                (n.qualification == SemanticNameQualification::Scalar &&
                 get<SCI_Scalar>(n.bulk).type != get<SCI_Scalar>(first_element_on_stack.bulk).type))
            {
                if (n.qualification == SemanticNameQualification::Scalar
                 && first_element_on_stack.qualification == SemanticNameQualification::Scalar
                 && get<SCI_Scalar>(n.bulk).type == SemanticNameType::Int
                 && get<SCI_Scalar>(first_element_on_stack.bulk).type == SemanticNameType::Float) {
                    double float_value = get<int64_t>(get<SCI_Scalar>(n.bulk).variant);
                    values.push_back({n.name,SemanticNameQualification::Scalar,SCI_Scalar{SemanticNameType::Float,float_value}});
                }
                else {
                    _m.reportError(op.getSymbol(), u"Массив должен состоять из элементов одного типа");
                    return SCI_RunnerCondition::Error;
                }
            }
            else
                values.push_back(n);
        }
    }

    _stack.pop(new_elements_count);
    _stack.push(SCI_Array {dimensions, values});

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::pushDataStructure_Tuple(const AstNode &op)
{
    assert(op.getOperation() == AstOperation::DataStructure_Tuple);

    SCI_Tuple tuple;

    for(const AstNode & o : op.getBranchC())
        if (!o.getBranchC().empty())
        {
            size_t last_stack_size = _stack.size();

            SCI_RunnerCondition state = runInner(o);
            if (state != SCI_RunnerCondition::Regular)
                return state;

            assert(_stack.size() == last_stack_size+1);

            SCI_Name & element = _stack.top();

            element.name = o.getSymbol().getLexeme();

            for(const SCI_Name & n : tuple)
                if (!element.name.empty() && element.name == n.name)
                {
                    _m.reportError(o.getSymbol(), u"Недопустимо повторение имени '" + element.name + u"' в именованной структуре");
                    return SCI_RunnerCondition::Error;
                }

            tuple.push_back(element);

            _stack.pop();
        }

    _stack.push(tuple);

    return SCI_RunnerCondition::Regular;
}

SCI_RunnerCondition ScriptC_Interpreter::performTupleSelectiveAssigment(const AstNode & op,
                                                                        const SCI_Name &from_name,
                                                                        const SCI_Name &toType,
                                                                        SCI_Tuple & toTuple)
{
    if (from_name.qualification != SemanticNameQualification::Tuple)
    {
        _m.reportError(op.getSymbol(), u"Квалификатор выражения в операторе присваивания не соответствует типу переменной");
        return SCI_RunnerCondition::Error;
    }

    /// \todo Нужно рекурсивно проверять все элементы кортежей
    /// или ввести в кортеж ссылку на тип и проверять только тип (с учётом наследования)

    // Проверяем соответствие присватываемого значения типу переменной (похоже на утиную типизацию)

    for(const SCI_Name & n : get<SCI_Tuple>(from_name.bulk))
    {
        size_t i = 0;
        for(; i < toTuple.size(); ++i)
        {
            if (n.name == toTuple[i].name)
            {
                if(n.qualification != toTuple[i].qualification ||
                   (n.qualification == SemanticNameQualification::Scalar &&
                    get<SCI_Scalar>(n.bulk).type != get<SCI_Scalar>(toTuple[i].bulk).type))
                {
                    if (n.qualification == SemanticNameQualification::Scalar
                     && toTuple[i].qualification == SemanticNameQualification::Scalar
                     && get<SCI_Scalar>(n.bulk).type == SemanticNameType::Int
                     && get<SCI_Scalar>(toTuple[i].bulk).type == SemanticNameType::Float)
                    {
                        double float_value = get<int64_t>(get<SCI_Scalar>(n.bulk).variant);
                        toTuple[i] = {n.name, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, float_value}};
                        break;
                    }

                    _m.reportError(op.getSymbol(), u"Элемент '" + n.name + u"' присваиваемого кортежа имеет недопустимый тип");
                    return SCI_RunnerCondition::Error;
                }

                toTuple[i] = n;
                break;
            }
        }

        if (i == toTuple.size())
        {
            _m.reportError(op.getSymbol(), u"Тип '" + toType.name + u"' не содержит элемент '" + n.name + u"'");
            return SCI_RunnerCondition::Error;
        }
    }

    return SCI_RunnerCondition::Regular;
}

SCI_Name & ScriptC_Interpreter::findName(u16string name_str, SCI_Name ** p_parent)
{
    if (p_parent != nullptr && (*p_parent) != nullptr)
    {
        SCI_Tuple & tuple = get<SCI_Tuple>((*p_parent)->bulk);

        size_t i = 0;
        for(; i < tuple.size(); ++i)
            if (tuple[i].name == name_str)
                break;

        if (i < tuple.size())
            return tuple[i];

        return dummy_name;
    }

    size_t local_boundary  = _call_frame.empty() ? 0 : _call_frame.back().stack_local_boundary;
    size_t global_boundary = _call_frame.empty() ? _stack.size() : _call_frame.front().stack_local_boundary;
    size_t local_index     = _stack.findLocal(name_str, local_boundary);

    if (local_index != NOT_FOUND_INDEX)
        return _stack.at(local_index);

    if (!_call_frame.empty())
    {
        if (_call_frame.back().encapsulator != nullptr)
        {
            SCI_Tuple & tuple = get<SCI_Tuple>(_call_frame.back().encapsulator->bulk);

            for(SCI_Name & n : tuple)
                if (n.name == name_str)
                    return n;
        }

        size_t global_index = _stack.findGlobal(name_str, global_boundary);

        if (global_index != NOT_FOUND_INDEX)
            return _stack.at(global_index);
    }

    if (p_parent != nullptr)
        for(SCI_Name * pn : _using_set)
        {
            SCI_Tuple & tuple = get<SCI_Tuple>(pn->bulk);

            size_t i = 0;
            for(; i < tuple.size(); ++i)
                if (tuple[i].name == name_str)
                    break;

            if (i < tuple.size())
            {
                (*p_parent) = pn;
                return tuple[i];
            }
        }

    return dummy_name;
}

SCI_Scalar ScriptC_Interpreter::toString(const SCI_Scalar &val) const
{
    if (val.type == SemanticNameType::String)
        return val;

    SCI_Scalar res;
    res.type = SemanticNameType::String;

    switch(val.type)
    {
    case SemanticNameType::Bool:
        res.variant = get<bool>(val.variant) ? u"true"s : u"false"s;
        break;
    case SemanticNameType::Int:
        res.variant = convertToU16(to_string(get<int64_t>(val.variant)));
        break;
    case SemanticNameType::Float:
        res.variant = simodo::convertToU16(simodo::clearNumberFractionalPart(to_string(get<double>(val.variant))));
        break;
    default:
        res.variant = SCI_UNDEF_STRING;
        break;
    }

    return res;
}

SCI_Scalar ScriptC_Interpreter::toFloat(const SCI_Scalar &val) const
{
    if (val.type == SemanticNameType::Float)
        return val;

    assert(val.type == SemanticNameType::Int);

    SCI_Scalar res;
    res.type = SemanticNameType::Float;
    res.variant = static_cast<double>(get<int64_t>(val.variant));
    return res;
}

SCI_Scalar ScriptC_Interpreter::getScalar(const AstNode &op) const
{
    const u16string & lexeme = op.getSymbol().getLexeme();

    if (lexeme == u"int")
        return getScalar(SemanticNameType::Int);

    if (lexeme == u"float")
        return getScalar(SemanticNameType::Float);

    if (lexeme == u"bool")
        return getScalar(SemanticNameType::Bool);

    if (lexeme == u"string")
        return getScalar(SemanticNameType::String);

    return { SemanticNameType::Undefined, SCI_UNDEF_STRING };
}

SCI_Scalar ScriptC_Interpreter::getScalar(SemanticNameType type) const
{
    switch(type)
    {
    case SemanticNameType::Int:
        return { type, static_cast<int64_t>(0) };
    case SemanticNameType::Float:
        return { type, 0.0 };
    case SemanticNameType::Bool:
        return { type, false };
    case SemanticNameType::String:
        return { type, u""s };
    case SemanticNameType::Undefined:
        return { type, {} };
    default:
        throw Exception("getScalar", "Недопустимый тип");
    }

    return { SemanticNameType::Undefined, SCI_UNDEF_STRING };
}

bool ScriptC_Interpreter::checkTypeConversion(SemanticNameType from, SemanticNameType to) const
{
    switch(to)
    {
    case SemanticNameType::Undefined:
        return false;
    case SemanticNameType::String:
        return true;
    case SemanticNameType::Float:
        if (from == SemanticNameType::Int)
            return true;
        [[fallthrough]];
    default:
        return (from == to);
    }

    return false;
}

bool ScriptC_Interpreter::checkArithmeticOperation(AstNode op, SemanticNameType type1, SemanticNameType type2) const
{
    if (type1 == SemanticNameType::String || type2 == SemanticNameType::String)
    {
        if (op.getOperation() != AstOperation::Arithmetic_Addition && op.getOperation() != AstOperation::Statement_Assign_Addition)
        {
            _m.reportError(op.getSymbol(),u"Недопустимая операция со строками");
            return false;
        }
        if (type1 == SemanticNameType::Undefined ||
            type1 == SemanticNameType::ExtFunction ||
            type1 == SemanticNameType::IntFunction ||
            type2 == SemanticNameType::Undefined ||
            type2 == SemanticNameType::ExtFunction ||
            type2 == SemanticNameType::IntFunction)
        {
            _m.reportError(op.getSymbol(),u"Недопустимая комбинация типов операндов арифметической операции");
            return false;
        }
    }
    else if (type1 == SemanticNameType::Undefined ||
             type1 == SemanticNameType::Bool ||
             type1 == SemanticNameType::ExtFunction ||
             type1 == SemanticNameType::IntFunction ||
             type2 == SemanticNameType::Undefined ||
             type2 == SemanticNameType::Bool ||
             type2 == SemanticNameType::ExtFunction ||
             type2 == SemanticNameType::IntFunction)
    {
        _m.reportError(op.getSymbol(),u"Недопустимая комбинация типов операндов арифметической операции");
        return false;
    }

    if ((type1 == SemanticNameType::Float || type2 == SemanticNameType::Float) &&
        (op.getOperation() == AstOperation::Arithmetic_Modulo || op.getOperation() == AstOperation::Statement_Assign_Modulo))
    {
        _m.reportError(op.getSymbol(),u"Недопустимая операция");
        return false;
    }

    return true;
}

SCI_Scalar ScriptC_Interpreter::calculateArithmeticOperation(AstNode op, SCI_Scalar op1, SCI_Scalar op2) const
{
    //TODO: Нужно добавить проверку срабатывания граничных значений для вещественных операций (в т.ч. при делении)

    SCI_Scalar res;

    switch(op.getOperation())
    {
    case AstOperation::Arithmetic_Addition:
    case AstOperation::Statement_Assign_Addition:
        if (op1.type == SemanticNameType::String ||
            op2.type == SemanticNameType::String)
        {
            if (op1.type != SemanticNameType::String)
                op1 = toString(op1);
            if (op2.type != SemanticNameType::String)
                op2 = toString(op2);

            res.type    = SemanticNameType::String;
            res.variant = get<u16string>(op1.variant) + get<u16string>(op2.variant);
        }
        else if (op1.type == SemanticNameType::Float ||
                 op2.type == SemanticNameType::Float)
        {
            if (op1.type != SemanticNameType::Float)
                op1 = toFloat(op1);
            if (op2.type != SemanticNameType::Float)
                op2 = toFloat(op2);

            res.type    = SemanticNameType::Float;
            res.variant = get<double>(op1.variant) + get<double>(op2.variant);
        }
        else
        {
            res.type    = SemanticNameType::Int;
            res.variant = get<int64_t>(op1.variant) + get<int64_t>(op2.variant);
        }
        break;
    case AstOperation::Arithmetic_Subtraction:
    case AstOperation::Statement_Assign_Subtraction:
        if (op1.type == SemanticNameType::Float ||
            op2.type == SemanticNameType::Float)
        {
            if (op1.type != SemanticNameType::Float)
                op1 = toFloat(op1);
            if (op2.type != SemanticNameType::Float)
                op2 = toFloat(op2);

            res.type    = SemanticNameType::Float;
            res.variant = get<double>(op1.variant) - get<double>(op2.variant);
        }
        else
        {
            res.type    = SemanticNameType::Int;
            res.variant = get<int64_t>(op1.variant) - get<int64_t>(op2.variant);
        }
        break;
    case AstOperation::Arithmetic_Multiplication:
    case AstOperation::Statement_Assign_Multiplication:
        if (op1.type == SemanticNameType::Float ||
            op2.type == SemanticNameType::Float)
        {
            if (op1.type != SemanticNameType::Float)
                op1 = toFloat(op1);
            if (op2.type != SemanticNameType::Float)
                op2 = toFloat(op2);

            res.type    = SemanticNameType::Float;
            res.variant = get<double>(op1.variant) * get<double>(op2.variant);
        }
        else
        {
            res.type    = SemanticNameType::Int;
            res.variant = get<int64_t>(op1.variant) * get<int64_t>(op2.variant);
        }
        break;
    case AstOperation::Arithmetic_Division:
    case AstOperation::Statement_Assign_Division:
        if (op1.type == SemanticNameType::Float ||
            op2.type == SemanticNameType::Float)
        {
            if (op1.type != SemanticNameType::Float)
                op1 = toFloat(op1);
            if (op2.type != SemanticNameType::Float)
                op2 = toFloat(op2);

            double op2_double = get<double>(op2.variant);

            if (abs(op2_double) <= +0.0) // abs и "+" - для красоты, можно не писать )
            {
                //TODO: Для вещественных чисел результатом деления на ноль будет "бесконечное" число, не аппаратное исключение.
                // Нужно подумать, что с ним можно делать. А пока кидаем программное исключение, как для целых.
                throw overflow_error("Деление на ноль");
            }

            res.type    = SemanticNameType::Float;
            res.variant = get<double>(op1.variant) / op2_double;
        }
        else
        {
            int64_t op2int = get<int64_t>(op2.variant);

            if (op2int == 0)
            {
                throw overflow_error("Деление на ноль");
            }

            res.type    = SemanticNameType::Int;
            res.variant = get<int64_t>(op1.variant) / op2int;
        }
        break;
    case AstOperation::Arithmetic_Modulo:
    case AstOperation::Statement_Assign_Modulo:
        {
            int64_t res1   = get<int64_t>(op1.variant);
            int64_t res2   = get<int64_t>(op2.variant);
            int64_t result = res1 % res2;
            res.type       = SemanticNameType::Int;
            res.variant    = result;
        }
        break;
    case AstOperation::Arithmetic_Power:
        {
            res.type = SemanticNameType::Float;
            if (op1.type != SemanticNameType::Float)
                op1 = toFloat(op1);

            if (op2.type == SemanticNameType::Float)
                res.variant = pow(get<double>(op1.variant), get<double>(op2.variant));
            else
                res.variant = pow(get<double>(op1.variant), get<int64_t>(op2.variant));
        }
        break;
    default:
        assert(true);
        break;
    }

    return res;
}

SCI_Name ScriptC_Interpreter::makeNameFromModule(const ModuleImportResults &module, size_t i_semantic_name) const
{
    assert(i_semantic_name < module.names.size());

    const SemanticName & sn = module.names[i_semantic_name];

    switch(sn.qualification)
    {
    case SemanticNameQualification::Scalar:
        return {sn.name.getLexeme(), sn.qualification, getScalar(sn.type)};
        break;
    case SemanticNameQualification::Function:
    case SemanticNameQualification::Tuple:
    case SemanticNameQualification::Type:
    {
        SCI_Name tuple { sn.name.getLexeme(), sn.qualification, SCI_Tuple {} };

        SCI_Tuple & content = get<SCI_Tuple>(tuple.bulk);

        if (sn.qualification == SemanticNameQualification::Function)
        {
            auto it = _mm.modules().find(convertToU8(sn.name.getLocation().file_name));
            if (it == _mm.modules().end())
                throw Exception("makeNameFromModule",
                                "Сбой при поиске модуля '" + convertToU8(sn.name.getLocation().file_name) + "'");

            const AstNode * p_function_ast = nullptr;

            if (tuple.name == u"_")
                p_function_ast = &it->second.ast;
            else
            {
                auto it_function = it->second.functions.find(sn.name.getLexeme());
                if (it_function == it->second.functions.end())
                    throw Exception("makeNameFromModule", "Функция модуля не найдена");

                p_function_ast = it_function->second;
            }

            content.push_back({
                                  sn.name.getLexeme(),
                                  SemanticNameQualification::Scalar,
                                  SCI_Scalar {SemanticNameType::IntFunction,p_function_ast}
                              });
        }

        for(size_t i_element=0; i_element < module.names.size(); ++i_element)
        {
            const SemanticName & sn = module.names[i_element];
            if (sn.owner == i_semantic_name)
            {
                if (sn.name.getLexeme().empty() && sn.type == SemanticNameType::Undefined)
                    content.push_back({u"", SemanticNameQualification::None, {}});
                else
                    content.push_back(makeNameFromModule(module, i_element));
            }
        }
        return tuple;
        break;
    }
    case SemanticNameQualification::Array:
    {
        vector<SCI_Name> array {{u"", SemanticNameQualification::Scalar, getScalar(sn.type)}};

        return {sn.name.getLexeme(), sn.qualification, SCI_Array {{1},array}};
        break;
    }
    default:
        throw Exception("makeNameFromModule", "Недопустимый квалификатор");
        break;
    }

    return {};
}

SCI_Name *ScriptC_Interpreter::getArrayElement(const AstNode &name_op, SCI_Name &name, const AstNode &array_op)
{
    if (name.qualification != SemanticNameQualification::Array)
    {
        _m.reportError(name_op.getSymbol(),
                       u"Переменная '" + name_op.getSymbol().getLexeme() + u"' не является массивом");
        return nullptr;
    }

    SCI_Array & array = get<SCI_Array>(name.bulk);

    size_t last_stack_size = _stack.size();

    SCI_RunnerCondition state = runInner(array_op);
    if (state != SCI_RunnerCondition::Regular)
        return nullptr;

    assert(_stack.size() >= last_stack_size);

    size_t new_elements_count = _stack.size() - last_stack_size;

    if (new_elements_count != array.dimensions.size())
    {
        _m.reportError(array_op.getSymbol(),
                       u"Количество индексов не соответствует размерности массива '" + name_op.getSymbol().getLexeme() + u"'");
        return nullptr;
    }

    size_t array_element_pointer = 0;
    size_t dimention_multiplier = 1;

    for(size_t i=new_elements_count-1; i < new_elements_count; --i)
    {
        const SCI_Name & n = _stack.atC(last_stack_size+i);

        if (n.qualification != SemanticNameQualification::Scalar ||
            get<SCI_Scalar>(n.bulk).type != SemanticNameType::Int ||
            get<int64_t>(get<SCI_Scalar>(n.bulk).variant) < 0 ||
            get<int64_t>(get<SCI_Scalar>(n.bulk).variant) > UINT16_MAX)
        {
            _m.reportError(array_op.getSymbol(),
                           u"Индекс размерности должен иметь целочисленный тип в диапазоне от 0 до " +
                           convertToU16(to_string(UINT16_MAX)));
            return nullptr;
        }

        int64_t index = get<int64_t>(get<SCI_Scalar>(n.bulk).variant);

        if (index >= array.dimensions[i])
        {
            _m.reportError(array_op.getSymbol(), u"Индекс превысил границы размерности");
            return nullptr;
        }

        array_element_pointer += dimention_multiplier * index;
        dimention_multiplier *= array.dimensions[i];
    }

    assert(array_element_pointer < array.values.size());

    _stack.pop(new_elements_count);

    return &array.values[array_element_pointer];
}

u16string simodo::dsl::getSCI_RunnerConditionName(SCI_RunnerCondition condition)
{
    u16string s;

    switch(condition)
    {
    case SCI_RunnerCondition::Regular:
        s = u"Regular";
        break;
    case SCI_RunnerCondition::Unsupported:
        s = u"Unsupported";
        break;
    case SCI_RunnerCondition::Error:
        s = u"Error";
        break;
    case SCI_RunnerCondition::Break:
        s = u"Break";
        break;
    case SCI_RunnerCondition::Continue:
        s = u"Continue";
        break;
    case SCI_RunnerCondition::Return:
        s = u"Return";
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        s = SCI_UNDEF_STRING;
        break;
    }

    return s;
}


SCI_NamespaceAddScope::SCI_NamespaceAddScope(ScriptC_Interpreter &interpreter)
    : _interpreter(interpreter)
{
    interpreter._scope_marks.push_back(interpreter._stack.size());
}

SCI_NamespaceAddScope::~SCI_NamespaceAddScope()
{
    assert(_interpreter._stack.size() >= _interpreter._scope_marks.back());

    _interpreter._stack.pop(_interpreter._stack.size()-_interpreter._scope_marks.back());
    _interpreter._scope_marks.pop_back();
}

SCI_FrameScope::SCI_FrameScope(ScriptC_Interpreter &interpreter, size_t local_boundary, SCI_Name *encapsulation)
    : _interpreter(interpreter)
{
    if (encapsulation == nullptr)
        for (size_t i=interpreter._call_frame.size()-1; i < interpreter._call_frame.size(); --i)
            if (interpreter._call_frame[i].encapsulator != nullptr)
            {
                encapsulation = interpreter._call_frame[i].encapsulator;
                break;
            }

    interpreter._call_frame.push_back({local_boundary,encapsulation});
}

SCI_FrameScope::~SCI_FrameScope()
{
    _interpreter._call_frame.pop_back();
}
