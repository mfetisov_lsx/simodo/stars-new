/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/FuzeParser.h"
#include "simodo/dsl/Stream.h"

#include "simodo/convert.h"
#include "simodo/inout/convert/functions.h"

//#if (__GNUC__ > 7)
#include <filesystem>
//#else
//#include <experimental/filesystem>
//using namespace std::experimental;
//#endif

using namespace std;
using namespace simodo::dsl;

//static u16string SEPORATOR_STRING {u"import"};
inline u16string IMPORT_STRING {u"include"};
inline u16string MAIN_STRING {u"main"};

bool FuzeParser::parse(std::vector<GrammarRuleTokens> &rules, Token &main_rule, std::map<u16string, AstNode> &handlers, LexicalParameters &lex)
{
    filesystem::path grammar_path = filesystem::path(_path) / (_grammar_name + FUZE_FILE_EXTENSION);

    if (_files.find(grammar_path.string()) != _files.end())
        return true;    // Данный файл уже загружен

    _files.insert(grammar_path.string());

    SIMODO_INOUT_CONVERT_STD_IFSTREAM(in, grammar_path.string());

    if (!in)
    {
        reporter().reportFatal(u"Ошибка при открытии файла '" + grammar_path.u16string() + u"'");
        return false;
    }

    FileStream stream(in);

    return parse(stream, rules, main_rule, handlers, lex);
}

bool FuzeParser::parse(IStream &stream, std::vector<GrammarRuleTokens> &rules, Token &main_rule, std::map<std::u16string,AstNode> &handlers, LexicalParameters &lex)
{
    filesystem::path grammar_path = filesystem::path(_path) / (_grammar_name + FUZE_FILE_EXTENSION);

    lex.markups = {
        {u"/*", u"*/", u"", LexemeType::Comment},
        {u"//", u"", u"", LexemeType::Comment},
        {u"\"", u"\"", u"\\", LexemeType::Annotation}
    };
    lex.masks = {
        {u"N", LexemeType::Number, 10}
    };
    lex.punctuation_chars = u"=|;><.{}[](),";
    lex.punctuation_words = {
        IMPORT_STRING, MAIN_STRING,
        ANNOTATION_STRING, NUMBER_STRING, ID_STRING,
        u"true", u"false"
    };
    lex.may_national_letters_use = true;
    lex.may_national_letters_mix = true;

    Tokenizer   tzer(grammar_path.u16string(), stream, lex, _tab_size);

    Token t = tzer.getToken();

    while(t.getType() != LexemeType::Empty)
    {
        while(t.getType() == LexemeType::Punctuation && t.getLexeme() == u";")
            t = tzer.getToken();

        if (t.getType() == LexemeType::Empty)
            break;

        if (t.getType() == LexemeType::Punctuation && t.getQualification() == TokenQualification::Keyword)
        {
            if (!parseKeyword(rules, main_rule, handlers, tzer, t))
                return false;

            t = tzer.getToken();
        }
        else if (t.getType() == LexemeType::Id)
        {
            Token id = t;

            t = tzer.getToken();

            if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u"{")
            {
                AstNode ast(u"");

                FuzeParser_ScriptC scriptc_parser(reporter(), ast);

                scriptc_parser.parse(tzer, t);

                if (auto it = handlers.find(id.getLexeme()); it != handlers.end())
                    it->second.insertBranchBegin(ast.getBranch());
                else
                    handlers.insert({id.getLexeme(),ast});
            }
            else if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u"=")
            {
                if (!parseProduction(id, rules, tzer, t))
                    return false;
            }
            else
                return reportUnexpected(t);
        }
        else
            return reportUnexpected(t);
    }

    return true;
}

bool FuzeParser::parseKeyword(std::vector<GrammarRuleTokens> &rules, Token &main_rule, std::map<u16string, AstNode> &handlers, Tokenizer &tzer, Token &t) const
{
    if ( t.getLexeme() == IMPORT_STRING)
    {
        t = tzer.getToken();

        if (t.getType() == LexemeType::Annotation || t.getType() == LexemeType::Id)
        {
            string grammar_name = convertToU8(t.getLexeme());

            t = tzer.getToken();

            if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u";")
            {
                FuzeParser          fuze(reporter(), _path, grammar_name, _files, _tab_size);
                Token               dummy(u"");
                LexicalParameters   lex;

                return fuze.parse(rules, dummy, handlers, lex);
            }

            return reportUnexpected(t, u"';'");
        }

        return reportUnexpected(t, u"наименование грамматики");
    }
    else if (t.getLexeme() == MAIN_STRING)
    {
        Token op = t;

        t = tzer.getToken();

        if (t.getType() == LexemeType::Id)
        {
            Token main_production = t;

            t = tzer.getToken();

            if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u";")
            {
                if (!main_rule.getToken().empty())
                {
                    reporter().reportWarning(main_production,
                                             u"Наименование главной продукции грамматики '"
                                             + convertToU16(_grammar_name) + u"' было задано ранее");
                    return false;
                }
                main_rule = main_production;
                return true;
            }

            return reportUnexpected(t, u"';'");
        }

        return reportUnexpected(t, u"идентификатор продукции");
    }

    return reportUnexpected(t, u"ключевое слово '"+ IMPORT_STRING + u"' или '" + MAIN_STRING + u"'");
}

bool FuzeParser::parseProduction(const Token & production, std::vector<GrammarRuleTokens> &rules, Tokenizer &tzer, Token &t) const
{
    if (t.getType() != LexemeType::Punctuation || t.getLexeme() != u"=")
        return reportUnexpected(t, u"'='");

    t = tzer.getToken();

    while(t.getType() != LexemeType::Empty)
    {
        vector<Token>       pattern;
        AstNode             ast;
        RuleReduceDirection reduce_direction = RuleReduceDirection::Undefined;

        if (!parsePattern(tzer,t,pattern,ast,reduce_direction))
            return false;

        if (pattern.empty())
        {
            reporter().reportError(t, u"Грамматика '" + convertToU16(_grammar_name) + u"'. Правая часть правила не может быть пустой");
            return false;
        }

        rules.push_back({production,pattern,ast,reduce_direction});

        if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u";")
        {
            return true;
        }
        else if (t.getType() == LexemeType::Punctuation && (t.getLexeme() == u"|" || t.getLexeme() == u"="))
        {
            t = tzer.getToken();
            continue;
        }
        else
            return reportUnexpected(t, u"'|', '=' или ';'");
    }

    reporter().reportError(t, u"Грамматика '" + convertToU16(_grammar_name) + u"'. Преждевременный конец файла");
    return false;
}

bool FuzeParser::parsePattern(Tokenizer &tzer,
                              Token &t,
                              std::vector<Token> &pattern,
                              AstNode &ast,
                              RuleReduceDirection &direction) const
{
    while(t.getType() != LexemeType::Empty)
    {
        LexemeType type = LexemeType::Empty;

        if (t.getType() == LexemeType::Annotation)
        {
            type = LexemeType::Punctuation;
        }
        else if (t.getType() == LexemeType::Id)
        {
            type = LexemeType::Compound;
        }
        else if (t.getType() == LexemeType::Punctuation)
        {
            if (t.getQualification() == TokenQualification::Keyword)
            {
                if (t.getLexeme() == ANNOTATION_STRING)
                    type = LexemeType::Annotation;
                else if (t.getLexeme() == NUMBER_STRING)
                    type = LexemeType::Number;
                else if (t.getLexeme() == ID_STRING)
                    type = LexemeType::Id;
                else
                    return reportUnexpected(t, u"ключевое слово '" + ANNOTATION_STRING + u"', '" + NUMBER_STRING + u"' или '" + ID_STRING + u"'");
            }
            else if (t.getLexeme() == u"<" || t.getLexeme() == u">")
            {
                direction = (t.getLexeme() == u"<") ? RuleReduceDirection::LeftAssociative : RuleReduceDirection::RightAssociative;

                t = tzer.getToken();

                if (t.getType() == LexemeType::Punctuation && t.getLexeme() == u"{")
                {
                    FuzeParser_ScriptC scriptc_parser(reporter(), ast);

                    return scriptc_parser.parse(tzer, t);
                }

                return true;
            }
            else if (t.getLexeme() == u"{")
            {
                FuzeParser_ScriptC scriptc_parser(reporter(), ast);

                return scriptc_parser.parse(tzer, t);
            }
            else
                return true;
        }
        else
            return true;

        pattern.emplace_back(t.getToken(),type,t.getLexeme(),t.getLocation());

        t = tzer.getToken();
    }

    return true;
}
