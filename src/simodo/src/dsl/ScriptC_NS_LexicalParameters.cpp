/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>
#include <cmath>

#include "simodo/dsl/ScriptC_NS_LexicalParameters.h"

#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

namespace
{
    simodo::dsl::LexicalParameters * p_lex = nullptr;

    bool clearMarkups(IScriptC_Namespace * , SCI_Stack & )
    {
        assert(p_lex != nullptr);

        p_lex->markups.clear();
        return true;
    }

    bool addMarkup(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 4);

        SCI_Scalar & start_value         = get<SCI_Scalar>(sci_stack.top(3).bulk);
        SCI_Scalar & end_value           = get<SCI_Scalar>(sci_stack.top(2).bulk);
        SCI_Scalar & ignore_sign_value   = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & type_value          = get<SCI_Scalar>(sci_stack.top(0).bulk);

        std::u16string start = get<std::u16string>(start_value.variant);
        std::u16string end = get<std::u16string>(end_value.variant);
        std::u16string ignore_sign = get<std::u16string>(ignore_sign_value.variant);
        int64_t type = get<int64_t>(type_value.variant);

        p_lex->markups.push_back({start, end, ignore_sign, static_cast<LexemeType>(type)});

        sci_stack.pop(4);

        return true;
    }

    bool clearMasks(IScriptC_Namespace * , SCI_Stack & )
    {
        assert(p_lex != nullptr);

        p_lex->masks.clear();
        return true;
    }

    bool addMask(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 3);

        SCI_Scalar & chars_value     = get<SCI_Scalar>(sci_stack.top(2).bulk);
        SCI_Scalar & type_value      = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & system_value    = get<SCI_Scalar>(sci_stack.top(0).bulk);

        std::u16string chars = get<std::u16string>(chars_value.variant);
        int64_t type = get<int64_t>(type_value.variant);
        int64_t system = get<int64_t>(system_value.variant);

        p_lex->masks.push_back({chars, static_cast<LexemeType>(type), static_cast<number_system_t>(system)});

        sci_stack.pop(3);

        return true;
    }

    bool setNationalAlphabet(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 1);

        SCI_Scalar & national_alphabet_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        std::u16string national_alphabet = get<std::u16string>(national_alphabet_value.variant);

        p_lex->national_alphabet = national_alphabet;

        sci_stack.pop();

        return true;
    }

    bool setIdExtraSymbols(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 1);

        SCI_Scalar & id_extra_symbols_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        std::u16string id_extra_symbols = get<std::u16string>(id_extra_symbols_value.variant);

        p_lex->id_extra_symbols = id_extra_symbols;

        sci_stack.pop();

        return true;
    }

    bool setNationalLettersUse(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 1);

        SCI_Scalar & may_national_letters_use_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        bool may_national_letters_use = get<bool>(may_national_letters_use_value.variant);

        p_lex->may_national_letters_use = may_national_letters_use;

        sci_stack.pop();

        return true;
    }

    bool setNationalLettersMix(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 1);

        SCI_Scalar & may_national_letters_mix_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        bool may_national_letters_mix = get<bool>(may_national_letters_mix_value.variant);

        p_lex->may_national_letters_mix = may_national_letters_mix;

        sci_stack.pop();

        return true;
    }

    bool setCaseSensitive(IScriptC_Namespace * , SCI_Stack &sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 1);

        SCI_Scalar & is_case_sensitive_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        bool is_case_sensitive = get<bool>(is_case_sensitive_value.variant);

        p_lex->is_case_sensitive = is_case_sensitive;

        sci_stack.pop();

        return true;
    }

    bool setNewLineSubstitution(IScriptC_Namespace * , SCI_Stack & sci_stack)
    {
        assert(p_lex != nullptr);
        assert(sci_stack.size() >= 1);

        SCI_Scalar & substituted_symbol_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        std::u16string substituted_symbol = get<std::u16string>(substituted_symbol_value.variant);

        p_lex->nl_substitution = substituted_symbol;

        sci_stack.pop();

        return true;
    }

}

ScriptC_NS_LexicalParameters::ScriptC_NS_LexicalParameters(simodo::dsl::LexicalParameters &lex)
{
    p_lex = &lex;
}

ScriptC_NS_LexicalParameters::~ScriptC_NS_LexicalParameters()
{
    p_lex = nullptr;
}

SCI_Namespace_t ScriptC_NS_LexicalParameters::getNamespace()
{
    return {
        {u"clearMarkups", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, clearMarkups}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"addMarkup", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addMarkup}}},
            {u"", SemanticNameQualification::None, {}},
            {u"start", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"end", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"ignore_sign", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"type", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"clearMasks", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, clearMasks}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"addMask", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addMask}}},
            {u"", SemanticNameQualification::None, {}},
            {u"chars", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"type", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"system", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"setNationalAlphabet", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setNationalAlphabet}}},
            {u"", SemanticNameQualification::None, {}},
            {u"national_alphabet", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"setIdExtraSymbols", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setIdExtraSymbols}}},
            {u"", SemanticNameQualification::None, {}},
            {u"id_extra_symbols", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"setNationalLettersUse", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setNationalLettersUse}}},
            {u"", SemanticNameQualification::None, {}},
            {u"may_national_letters_use", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
        }},
        {u"setNationalLettersMix", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setNationalLettersMix}}},
            {u"", SemanticNameQualification::None, {}},
            {u"may_national_letters_mix", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
        }},
        {u"setCaseSensitive", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setCaseSensitive}}},
            {u"", SemanticNameQualification::None, {}},
            {u"is_case_sensitive", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
        }},
        {u"setNewLineSubstitution", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setNewLineSubstitution}}},
            {u"", SemanticNameQualification::None, {}},
            {u"substituted_symbol", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"LexemeType", SemanticNameQualification::Tuple, SCI_Tuple {
             {u"Id", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Id)}},
             {u"Empty", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Empty)}},
             {u"Error", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Error)}},
             {u"Number", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Number)}},
             {u"Comment", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Comment)}},
             {u"Compound", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Compound)}},
             {u"Annotation", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Annotation)}},
             {u"Punctuation", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::Punctuation)}},
             {u"NewLine", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(LexemeType::NewLine)}},
        }},
    };
}
