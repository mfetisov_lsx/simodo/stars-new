/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/SemanticBase.h"

//#include "simodo/convert.h"

using namespace std;
using namespace simodo::dsl;

SemanticNameAccess simodo::dsl::strongestAccess(SemanticNameAccess one, SemanticNameAccess two)
{
    return (one == SemanticNameAccess::Closed || two == SemanticNameAccess::Closed)
            ? SemanticNameAccess::Closed
            : (one == SemanticNameAccess::ReadOnly || two == SemanticNameAccess::ReadOnly)
              ? SemanticNameAccess::ReadOnly
              : SemanticNameAccess::FullAccess;
}

u16string simodo::dsl::getSemanticNameTypeName(SemanticNameType type)
{
    u16string s;

    switch(type)
    {
    case SemanticNameType::Undefined:
        s = u"void";
        break;
    case SemanticNameType::Bool:
        s = u"bool";
        break;
    case SemanticNameType::Int:
        s = u"int";
        break;
    case SemanticNameType::Float:
        s = u"float";
        break;
    case SemanticNameType::String:
        s = u"string";
        break;
    case SemanticNameType::ExtFunction:
        s = u"ExtFunction";
        break;
    case SemanticNameType::IntFunction:
        s = u"IntFunction";
        break;
    default:
        s = SCI_UNDEF_STRING;
        break;
    }

    return s;
}


u16string simodo::dsl::getSemanticNameQualificationName(SemanticNameQualification qua)
{
    u16string s;

    switch(qua)
    {
    case SemanticNameQualification::None:
        s = u"None";
        break;
    case SemanticNameQualification::Scalar:
        s = u"Scalar";
        break;
    case SemanticNameQualification::Tuple:
        s = u"Tuple";
        break;
    case SemanticNameQualification::Array:
        s = u"Array";
        break;
    case SemanticNameQualification::Function:
        s = u"Function";
        break;
    case SemanticNameQualification::Type:
        s = u"Type";
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        s = SCI_UNDEF_STRING;
        break;
    }

    return s;
}

u16string simodo::dsl::getSemanticNameAccessName(SemanticNameAccess qua)
{
    u16string s;

    switch(qua)
    {
    case SemanticNameAccess::FullAccess:
        s = u"FullAccess";
        break;
    case SemanticNameAccess::ReadOnly:
        s = u"ReadOnly";
        break;
    case SemanticNameAccess::Closed:
        s = u"Closed";
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        s = SCI_UNDEF_STRING;
        break;
    }

    return s;
}
