/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/Lexeme.h"

using namespace std;
using namespace simodo::dsl;

std::u16string simodo::dsl::getLexemeTypeName(LexemeType type)
{
    switch(type)
    {
    case LexemeType::Compound:
        return u"Compound";
    case LexemeType::Empty:
        return u"Empty";
    case LexemeType::Punctuation:
        return u"Punctuation";
    case LexemeType::Id:
        return u"Word";
    case LexemeType::Annotation:
        return u"Annotation";
    case LexemeType::Number:
        return u"Number";
    case LexemeType::Comment:
        return u"Comment";
    case LexemeType::Error:
        return u"Error";
    default:
        return u"*****";
    }
}

u16string simodo::dsl::getLexemeMnemonic(const Lexeme &lex)
{
    u16string s;

    switch(lex.getType())
    {
    case LexemeType::Id:
        s = u"(идентификатор)";
        break;
    case LexemeType::Empty:
        s = u"(конец файла)";
        break;
    case LexemeType::Error:
        s = u"(ошибка лексики)";
        break;
    case LexemeType::Number:
        s = u"(число)";
        break;
    case LexemeType::Comment:
        s = u"(комментарий)";
        break;
    case LexemeType::Compound:
        s = lex.getLexeme();
        break;
    case LexemeType::Annotation:
        s = u"(строковая константа)";
        break;
    case LexemeType::Punctuation:
        s = u"'" + lex.getLexeme() + u"'";
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        s = u"(***)";
        break;
    }

    return s;
}

