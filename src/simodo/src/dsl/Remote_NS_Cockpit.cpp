/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/Remote_NS_Cockpit.h"

#include "simodo/dsl/Exception.h"
#include "simodo/convert.h"
#include "simodo/version.h"

#include <chrono>

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    namespace utils
    {
        double toDouble(const SCI_Name & n)
        {
            try {
                auto & sn = get<SCI_Scalar>(n.bulk);
                return (sn.type == SemanticNameType::Int) ? static_cast<double>(get<int64_t>(sn.variant)) : get<double>(sn.variant);
            } catch(std::exception & e)
            {}

            throw Exception("SCI_tf", "Неизвестный тип чисел: qualification=" + std::to_string(static_cast<int>(n.qualification)));
        };
    }

    bool init(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(!stack.empty());

        u16string title = get<u16string>(get<SCI_Scalar>(stack.top().bulk).variant);

        Remote_NS_Cockpit * cockpit = static_cast<Remote_NS_Cockpit *>(p_object);

        cockpit->listener().reportInformation(u"#Values:Cockpit.0.S.init:" + title);

        stack.pop();

        return true;
    }

    bool setPosition(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        auto x = ::utils::toDouble(stack.top(2));
        auto height = ::utils::toDouble(stack.top(1));
        auto z = ::utils::toDouble(stack.top(0));

        Remote_NS_Cockpit * cockpit = static_cast<Remote_NS_Cockpit *>(p_object);

        cockpit->listener().reportInformation(u"#Values:Cockpit.0.U.setPosition:" + convertToU16(to_string(x)) + 
                                            u"/" + convertToU16(to_string(height)) + 
                                            u"/" + convertToU16(to_string(z)));
        stack.pop(3);

        return true;
    }

    bool setAngles(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 3);

        auto pitch = ::utils::toDouble(stack.top(2));
        auto course = ::utils::toDouble(stack.top(1));
        auto roll = ::utils::toDouble(stack.top(0));

        Remote_NS_Cockpit * cockpit = static_cast<Remote_NS_Cockpit *>(p_object);

        cockpit->listener().reportInformation(u"#Values:Cockpit.0.U.setAngles:" + convertToU16(to_string(pitch)) + 
                                            u"/" + convertToU16(to_string(course)) + 
                                            u"/" + convertToU16(to_string(roll)));
        stack.pop(3);

        return true;
    }

    bool setSpeed(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 1);

        auto speed = ::utils::toDouble(stack.top(0));

        Remote_NS_Cockpit * cockpit = static_cast<Remote_NS_Cockpit *>(p_object);

        cockpit->listener().reportInformation(u"#Values:Cockpit.0.U.setSpeed:" + convertToU16(to_string(speed)));

        stack.pop(1);

        return true;
    }

    bool setVerticalSpeedSlide(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 2);

        auto vertical_speed = ::utils::toDouble(stack.top(1));
        auto slide = ::utils::toDouble(stack.top(0));

        Remote_NS_Cockpit * cockpit = static_cast<Remote_NS_Cockpit *>(p_object);

        cockpit->listener().reportInformation(u"#Values:Cockpit.0.U.setVerticalSpeedSlide:" + convertToU16(to_string(vertical_speed)) + 
                                            u"/" + convertToU16(to_string(slide)));
        stack.pop(2);

        return true;
    }

    bool getKeyState(IScriptC_Namespace * p_object, SCI_Stack &stack)
    {
        if (p_object == nullptr)
            return false;

        assert(stack.size() >= 1);

        /// @todo Не придумал как эту часть сделать - просто заглушка

        // auto * key_obj = &stack.top(0);
        
        // while(key_obj->qualification == SemanticNameQualification::Reference)
        //     key_obj = get<SCI_Reference>(key_obj->bulk);
        
        // std::u16string key = get<std::u16string>(get<SCI_Scalar>(stack.top(0).bulk).variant);

        // auto cockpit = static_cast<Remote_NS_Cockpit *>(p_object);

        // bool state = cockpit->getKeyState(simodo::convertToU8(key));
        bool state = false;

        stack.pop(1);
        stack.push({
            u"",
            SemanticNameQualification::Scalar,
            SCI_Scalar {SemanticNameType::Bool, state}
        });

        return true;
    }
}

Remote_NS_Cockpit::Remote_NS_Cockpit(AReporter &listener)
    : _listener(listener)
{
}

Remote_NS_Cockpit::~Remote_NS_Cockpit()
{
}

SCI_Namespace_t Remote_NS_Cockpit::getNamespace()
{
    return {
        {u"init", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::init}}},
            {u"", SemanticNameQualification::None, {}},
            {u"title", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"setPosition", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setPosition}}},
            {u"", SemanticNameQualification::None, {}},
            {u"x", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"height", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"z", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setAngles", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setAngles}}},
            {u"", SemanticNameQualification::None, {}},
            {u"pitch", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"course", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"roll", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setSpeed", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setSpeed}}},
            {u"", SemanticNameQualification::None, {}},
            {u"speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"setVerticalSpeedAndSlide", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::setVerticalSpeedSlide}}},
            {u"", SemanticNameQualification::None, {}},
            {u"vertical_speed", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
            {u"slide", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Float, {}}},
        }},
        {u"getKeyState", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, ::getKeyState}}},
            {u"", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Bool, {}}},
            {u"key", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
    };
}


