/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/Exception.h"

#define LOGGER_

using namespace std;
using namespace simodo::dsl;

namespace
{
#ifdef LOGGER
    AReporter *  logger_  = nullptr;
#endif

    static bool setMode(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert(!sci_stack.empty());

        SCI_Scalar & mode_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t mode = get<int64_t>(mode_value.variant);

        if (mode != static_cast<int64_t>(AstBuilderMode::Diff))
        {
            throw Exception("setMode", "Недопустимый режим");
        }

        string res = builder->helper().setMode(static_cast<AstBuilderMode>(mode));

        if (!res.empty())
            throw Exception("setMode", res);

        sci_stack.pop(1);

        return true;
    }

    static bool setPrefix(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert(!sci_stack.empty());

        SCI_Scalar & prefix_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        u16string prefix = get<u16string>(prefix_value.variant);

        string res = builder->helper().setPrefix(prefix);

        if (!res.empty())
            throw Exception("setPrefix", res);

        sci_stack.pop(1);

        return true;
    }

    static bool addNode(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert(sci_stack.size() >= 3);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        if (index_value.type == SemanticNameType::String)
        {
            u16string value = get<u16string>(index_value.variant);
            Token     token(LexemeType::Id, value, TokenLocation(u""s));

            string res = builder->helper().addNode(
                            static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                            token,
                            token,
                            builder->current_stream_no()
                    );

            if (!res.empty())
                throw Exception("addNode", res);
        }
        else if (index_value.type == SemanticNameType::Int)
        {
            int64_t index = get<int64_t>(index_value.variant);

            assert((&builder->current_pattern()) != nullptr);
            assert((&builder->current_production()) != nullptr);

            if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
            {
                throw Exception("addNode", "Выход индекса за пределы массива");
            }

            string res = builder->helper().addNode(
                            static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                            builder->current_pattern()[static_cast<size_t>(index)],
                            builder->current_production(),
                            builder->current_stream_no()
                    );

            if (!res.empty())
                throw Exception("addNode", res);
        }
        else
        {
            throw Exception("addNode", "Недопустимый тип индекса");
        }

        sci_stack.pop(3);

        return true;
    }

    static bool addNode_StepInto(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode_StepInto");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert((&builder->current_pattern()) != nullptr);
        assert((&builder->current_production()) != nullptr);

        assert(sci_stack.size() >= 3);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t index = get<int64_t>(index_value.variant);

        if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
        {
            throw Exception("addNode_StepInto", "Выход индекса за пределы массива");
        }

        string res = builder->helper().addNode_StepInto(
                        static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                        builder->current_pattern()[static_cast<size_t>(index)],
                        builder->current_production(),
                        builder->current_stream_no()
                );

        if (!res.empty())
            throw Exception("addNode_StepInto", res);

        sci_stack.pop(3);

        return true;
    }

    static bool addNode_Parent_StepInto(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode_Parent_StepInto");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert((&builder->current_pattern()) != nullptr);
        assert((&builder->current_production()) != nullptr);

        assert(sci_stack.size() >= 2);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t index = get<int64_t>(index_value.variant);

        if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
        {
            throw Exception("addNode_Parent_StepInto", "Выход индекса за пределы массива");
        }

        string res = builder->helper().addNode_Parent_StepInto(
                        static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                        builder->current_pattern()[static_cast<size_t>(index)],
                        builder->current_production(),
                        builder->current_stream_no()
                );

        if (!res.empty())
            throw Exception("addNode_Parent_StepInto", res);

        sci_stack.pop(2);

        return true;
    }

    static bool addNode_ShiftInto(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode_ShiftInto");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert((&builder->current_pattern()) != nullptr);
        assert((&builder->current_production()) != nullptr);

        assert(sci_stack.size() >= 2);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t index = get<int64_t>(index_value.variant);

        if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
        {
            throw Exception("addNode_ShiftInto", "Выход индекса за пределы массива");
        }

        string res = builder->helper().addNode_ShiftInto(
                        static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                        builder->current_pattern()[static_cast<size_t>(index)],
                        builder->current_production(),
                        builder->current_stream_no()
                );

        if (!res.empty())
            throw Exception("addNode_ShiftInto", res);

        sci_stack.pop(2);

        return true;
    }

    static bool addNode_Branch(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode_Branch");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert((&builder->current_pattern()) != nullptr);
        assert((&builder->current_production()) != nullptr);

        assert(sci_stack.size() >= 2);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t index = get<int64_t>(index_value.variant);

        if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
        {
            throw Exception("addNode_Branch", "Выход индекса за пределы массива");
        }

        string res = builder->helper().addNode_Branch(
                        static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                        builder->current_pattern()[static_cast<size_t>(index)],
                        builder->current_production(),
                        builder->current_stream_no()
                );

        if (!res.empty())
            throw Exception("addNode_Branch", res);

        sci_stack.pop(2);

        return true;
    }

    static bool addNode_Branch_StepInto(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode_Branch_StepInto");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert((&builder->current_pattern()) != nullptr);
        assert((&builder->current_production()) != nullptr);

        assert(sci_stack.size() >= 2);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t index = get<int64_t>(index_value.variant);

        if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
        {
            throw Exception("addNode_Branch_StepInto", "Выход индекса за пределы массива");
        }

        string res = builder->helper().addNode_Branch_StepInto(
                        static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                        builder->current_pattern()[static_cast<size_t>(index)],
                        builder->current_production(),
                        builder->current_stream_no()
                );

        if (!res.empty())
            throw Exception("addNode_Branch_StepInto", res);

        sci_stack.pop(2);

        return true;
    }

    static bool goParent(IScriptC_Namespace * p_object, SCI_Stack & )
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::goParent");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);


        string res = builder->helper().goParent(builder->current_stream_no());

        if (!res.empty())
            throw Exception("goParent", res);

        return true;
    }

    static bool addNode_Assignment(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::addNode_Assignment");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert((&builder->current_pattern()) != nullptr);
        assert((&builder->current_production()) != nullptr);

        assert(sci_stack.size() >= 2);

        SCI_Scalar & op_value    = get<SCI_Scalar>(sci_stack.top(1).bulk);
        SCI_Scalar & index_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t index = get<int64_t>(index_value.variant);

        if (index < 0 || index >= static_cast<int64_t>(builder->current_pattern().size()))
        {
            throw Exception("addNode_Assignment", "Выход индекса за пределы массива");
        }

        string res = builder->helper().addNode_Assignment(
                        static_cast<AstOperation>(get<int64_t>(op_value.variant)),
                        builder->current_pattern()[static_cast<size_t>(index)],
                        builder->current_production(),
                        builder->current_stream_no()
                );

        if (!res.empty())
            throw Exception("addNode_Assignment", res);

        sci_stack.pop(2);

        return true;
    }

    static bool resetOperation_ProcedureCalling(IScriptC_Namespace * p_object, SCI_Stack & )
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::resetOperation_ProcedureCalling");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        string res = builder->helper().resetOperation_ProcedureCalling(builder->current_stream_no());

        if (!res.empty())
            throw Exception("resetOperation_ProcedureCalling", res);

        return true;
    }

    static bool setStreamNo(IScriptC_Namespace * p_object, SCI_Stack & sci_stack)
    {
#ifdef LOGGER
        if (logger_ != nullptr)
            logger_->reportInformation(u"/tAstBuilder::setStreamNo");
#endif

        if (p_object == nullptr)
            return false;

        AstBuilder * builder = static_cast<AstBuilder *>(p_object);

        assert(!sci_stack.empty());

        SCI_Scalar & no_value = get<SCI_Scalar>(sci_stack.top(0).bulk);

        int64_t no = get<int64_t>(no_value.variant);

        if (no < 0 || no > UINT16_MAX)
        {
            throw Exception("setStreamNo", "Выход номера потока за лимиты");
        }

        builder->setCurrentStreamNo(no);

        sci_stack.pop();

        return true;
    }

}


AstBuilder::AstBuilder(AReporter &reporter, AstNode &ast_root, const std::map<u16string, AstNode> &handlers)
    : _m(reporter)
    , _ast(ast_root)
    , _handlers(handlers)
    , _mm(_m,_m,nullptr)
    , _machine(reporter,_mm)
{
    _current_production = nullptr;
    _current_pattern    = nullptr;

    _machine.importNamespace(u"ast", getNamespace());
}

bool AstBuilder::onStart()
{
    auto it = _handlers.find(u"on_Start");

    if (it == _handlers.end())
        return true;

    _machine.reset();

#ifdef LOGGER
    logger_ = &_m;
#endif

    SCI_RunnerCondition state = _machine.catchAst(it->second);

#ifdef LOGGER
    logger_ = nullptr;
#endif

    if (state != SCI_RunnerCondition::Regular)
        return false;

    if (_helper.mode() == AstBuilderMode::Diff)
    {
        Token bound(LexemeType::Punctuation,u"float",_ast.getSymbol().getLocation(),TokenQualification::Keyword);

        _helper.addNode_StepInto(AstOperation::Statement_Id, Token(LexemeType::Id,u"__E",bound.getLocation()), bound, EQUATION_STREAM_NO);
        _helper.addNode(AstOperation::Statement_Type, Token(LexemeType::Punctuation,u"void",bound.getLocation(),TokenQualification::Keyword), bound, EQUATION_STREAM_NO);
        _helper.addNode(AstOperation::Function_Declaration, Token(LexemeType::Punctuation,u"(",bound.getLocation()), bound, EQUATION_STREAM_NO);
        _helper.addNode_StepInto(AstOperation::Function_Body, Token(LexemeType::Punctuation,u"{",bound.getLocation()), bound, EQUATION_STREAM_NO);

        _helper.addNode_StepInto(AstOperation::Statement_Id, Token(LexemeType::Id,u"__ODE",bound.getLocation()), bound, DIFF_STREAM_NO);
        _helper.addNode(AstOperation::Statement_Type, Token(LexemeType::Punctuation,u"void",bound.getLocation(),TokenQualification::Keyword), bound, DIFF_STREAM_NO);
        _helper.addNode(AstOperation::Function_Declaration, Token(LexemeType::Punctuation,u"(",bound.getLocation()), bound, DIFF_STREAM_NO);
        _helper.addNode_StepInto(AstOperation::Function_Body, Token(LexemeType::Punctuation,u"{",bound.getLocation()), bound, DIFF_STREAM_NO);
    }

    return true;
}

bool AstBuilder::onProduction(Token production, vector<Token> pattern, const AstNode &action, bool &is_done)
{
    is_done = true;

    if (action.getBranchC().empty())
        return true;

    _current_production = &production;
    _current_pattern    = &pattern;

    _machine.reset();

#ifdef LOGGER
    logger_ = &_m;
#endif

    SCI_RunnerCondition state = _machine.catchAst(action);

#ifdef LOGGER
    logger_ = nullptr;
#endif

    _current_production = nullptr;
    _current_pattern    = nullptr;

    return (state == SCI_RunnerCondition::Regular);
}

bool checkAstFunctionEmpty(const AstNode & ast)
{
    if (ast.getBranchC().empty() || ast.getBranchC().front().getOperation() != AstOperation::Statement_Id)
        return true;

    const AstNode & ast_Statement_Id = ast.getBranchC().front();

    if (ast_Statement_Id.getBranchC().empty() || ast_Statement_Id.getBranchC().back().getOperation() != AstOperation::Function_Body)
        return true;

    const AstNode & ast_Function_Body = ast_Statement_Id.getBranchC().back();

    return ast_Function_Body.getBranchC().empty();
}

bool AstBuilder::onFinish(bool )
{
    if (_helper.mode() == AstBuilderMode::Diff)
    {
        Token bound(LexemeType::Punctuation,u"float",_ast.getSymbol().getLocation(),TokenQualification::Keyword);

        const AstStream & equation = helper().getAstStream(EQUATION_STREAM_NO);

        if (checkAstFunctionEmpty(equation.ast()))
            _helper.removeStream(EQUATION_STREAM_NO);
        else
        {
            _helper.addNode(AstOperation::None, Token(LexemeType::Punctuation,u"}",bound.getLocation()), bound, EQUATION_STREAM_NO);
            _helper.goParent(EQUATION_STREAM_NO);
            _helper.goParent(EQUATION_STREAM_NO);
        }


        const AstStream & diff = helper().getAstStream(DIFF_STREAM_NO);

        if (checkAstFunctionEmpty(diff.ast()))
            _helper.removeStream(DIFF_STREAM_NO);
        else
        {
            _helper.addNode(AstOperation::None, Token(LexemeType::Punctuation,u"}",bound.getLocation()), bound, DIFF_STREAM_NO);
            _helper.goParent(DIFF_STREAM_NO);
            _helper.goParent(DIFF_STREAM_NO);
        }
    }

    auto it = _handlers.find(u"on_Finish");

    if (it != _handlers.end())
    {
        _machine.reset();

#ifdef LOGGER
    logger_ = &_m;
#endif

        SCI_RunnerCondition state = _machine.catchAst(it->second);

#ifdef LOGGER
    logger_ = nullptr;
#endif

        if (state != SCI_RunnerCondition::Regular)
            return false;
    }

    _helper.drain(_ast);

    return true;
}

SCI_Namespace_t AstBuilder::getNamespace()
{
    return {
        {u"setMode", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setMode}}},
            {u"", SemanticNameQualification::None, {}},
            {u"mode"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"setPrefix", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setPrefix}}},
            {u"", SemanticNameQualification::None, {}},
            {u"prefix"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
        }},
        {u"addNode", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode}}},
            {u"", SemanticNameQualification::None, {}},
            {u"host"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
//            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::None, {}},
        }},
        {u"addNode_StepInto", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode_StepInto}}},
            {u"", SemanticNameQualification::None, {}},
            {u"host"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, {}}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addNode_Parent_StepInto", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode_Parent_StepInto}}},
            {u"", SemanticNameQualification::None, {}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addNode_ShiftInto", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode_ShiftInto}}},
            {u"", SemanticNameQualification::None, {}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addNode_Branch", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode_Branch}}},
            {u"", SemanticNameQualification::None, {}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"addNode_Branch_StepInto", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode_Branch_StepInto}}},
            {u"", SemanticNameQualification::None, {}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"goParent", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, goParent}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"addNode_Assignment", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, addNode_Assignment}}},
            {u"", SemanticNameQualification::None, {}},
            {u"op"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
            {u"index"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"resetOperation_ProcedureCalling", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, resetOperation_ProcedureCalling}}},
            {u"", SemanticNameQualification::None, {}},
        }},
        {u"setStream", SemanticNameQualification::Function, SCI_Tuple {
            {u"@", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::ExtFunction, SCI_ExtFunction {this, setStreamNo}}},
            {u"", SemanticNameQualification::None, {}},
            {u"no"s, SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, {}}},
        }},
        {u"Mode", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"None", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstBuilderMode::None)}},
            {u"Diff", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstBuilderMode::Diff)}},
        }},
        {u"Stream", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"Default", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(DEFAULT_STREAM_NO)}},
            {u"Equation", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(EQUATION_STREAM_NO)}},
            {u"Diff", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(DIFF_STREAM_NO)}},
        }},
        {u"host", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"fuze", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, u"fuze"s}},
            {u"SBL", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::String, u"SBL"s}},
        }},
        {u"op", SemanticNameQualification::Tuple, SCI_Tuple {
            {u"None", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::None)}},
            {u"InternalPush", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::InternalPush)}},
            {u"InternalPop", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::InternalPop)}},
            {u"Ternary_True", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Ternary_True)}},
            {u"Ternary_False", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Ternary_False)}},
            {u"Logical_Or", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Logical_Or)}},
            {u"Logical_And", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Logical_And)}},
            {u"Compare_Equal", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Compare_Equal)}},
            {u"Compare_NotEqual", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Compare_NotEqual)}},
            {u"Compare_Less", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Compare_Less)}},
            {u"Compare_LessOrEqual", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Compare_LessOrEqual)}},
            {u"Compare_More", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Compare_More)}},
            {u"Compare_MoreOrEqual", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Compare_MoreOrEqual)}},
            {u"Arithmetic_Addition", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Arithmetic_Addition)}},
            {u"Arithmetic_Subtraction", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Arithmetic_Subtraction)}},
            {u"Arithmetic_Multiplication", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Arithmetic_Multiplication)}},
            {u"Arithmetic_Division", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Arithmetic_Division)}},
            {u"Arithmetic_Modulo", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Arithmetic_Modulo)}},
            {u"Arithmetic_Power", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Arithmetic_Power)}},
            {u"Unary_Plus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Unary_Plus)}},
            {u"Unary_Minus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Unary_Minus)}},
            {u"Unary_Not", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Unary_Not)}},
            {u"Push_Constant", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Push_Constant)}},
            {u"Push_Id", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Push_Id)}},
            {u"Iteration_PrefixPlus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Iteration_PrefixPlus)}},
            {u"Iteration_PrefixMinus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Iteration_PrefixMinus)}},
            {u"Iteration_PosfixPlus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Iteration_PosfixPlus)}},
            {u"Iteration_PosfixMinus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Iteration_PosfixMinus)}},
            {u"Address_Array", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Address_Array)}},
            {u"Address_Tuple", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Address_Tuple)}},
            {u"Function_Call", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Function_Call)}},

            {u"Statement_Blank", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Blank)}},
            {u"Statement_Type", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Type)}},
            {u"Statement_Id", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Id)}},
            {u"Statement_Assign", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Assign)}},
            {u"Statement_Assign_Addition", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Assign_Addition)}},
            {u"Statement_Assign_Subtraction", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Assign_Subtraction)}},
            {u"Statement_Assign_Multiplication", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Assign_Multiplication)}},
            {u"Statement_Assign_Division", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Assign_Division)}},
            {u"Statement_Assign_Modulo", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Assign_Modulo)}},
            {u"Function_Declaration", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Function_Declaration)}},
            {u"Function_Body", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Function_Body)}},
            {u"Statement_Procedure_Call", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Procedure_Call)}},
            {u"Statement_Procedure_Call_Error", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Procedure_Call_Error)}},
            {u"Statement_Block", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Block)}},
            {u"Statement_Break", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Break)}},
            {u"Statement_Continue", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Continue)}},
            {u"Statement_Return", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Return)}},
            {u"Statement_If_True", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_If_True)}},
            {u"Statement_If_False", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_If_False)}},
            {u"Statement_While", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_While)}},
            {u"Statement_While_Body", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_While_Body)}},
            {u"Statement_DoWhile", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_DoWhile)}},
            {u"Statement_DoWhile_Body", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_DoWhile_Body)}},
            {u"Statement_For", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_For)}},
            {u"Statement_For_Body", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_For_Body)}},
            {u"Statement_UseAsType", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_UseAsType)}},
            {u"Statement_Using", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::Statement_Using)}},
            {u"DataStructure_Array", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::DataStructure_Array)}},
            {u"DataStructure_Tuple", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(AstOperation::DataStructure_Tuple)}},

            {u"PushConstant", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"PushVariable", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"ObjectElement", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"FunctionCall", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"ProcedureCheck", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Print", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Block", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Pop", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},

            {u"ArrayElement", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},

            {u"RecordStructure", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"ArrayStructure", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Import", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"ContractDefinition", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"TypeDefinition", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Announcement", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Declaration", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"DeclarationCompletion", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Assignment", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"Initialize", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
            {u"GroupInitialize", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},

              {u"AssignmentAddition", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
              {u"AssignmentSubstruction", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
              {u"AssignmentMultiplication", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
              {u"AssignmentDivision", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
              {u"AssignmentModulo", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},

          {u"Plus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Minus", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Not", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"PreAdd", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"PreSub", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"PosAdd", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"PosSub", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Or", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"And", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Equal", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"NotEqual", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Less", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"LessOrEqual", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"More", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"MoreOrEqual", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Addition", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Subtraction", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Multiplication", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Division", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Modulo", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Power", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},

          {u"Ternary", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"If", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"FunctionDeclaration", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Break", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Continue", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Return", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"While", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"DoWhile", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"For", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Switch", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Throw", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Try", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Catch", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Finally", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"Blank", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"ForIn", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
          {u"ForStandart", SemanticNameQualification::Scalar, SCI_Scalar {SemanticNameType::Int, static_cast<int64_t>(0)}},
        }},
    };
}
