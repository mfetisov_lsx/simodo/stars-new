/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/AstBuilderHelper.h"

using namespace std;
using namespace simodo::dsl;

string AstStream::addNode(AstOperation op, Token op_symbol, Token bound)
{
    // Создаём ноду, остаёмся в старой
    _flow -> addNode(op, op_symbol, bound);
    return "";
}

string AstStream::addNode_StepInto(AstOperation op, Token op_symbol, Token bound)
{
    // Создаём ноду и переключаемся на неё
    _flow = _flow -> addNode(op, op_symbol, bound);
    return "";
}

string AstStream::addNode_Parent_StepInto(AstOperation op, Token op_symbol, Token bound)
{
    if (_flow -> getParent() == nullptr)
        return "Невозможно переключиться на вышестоящий узел";

    // Создаём ноду в вышестоящей и переключаемся на неё
    _flow = _flow -> getParent() -> addNode(op, op_symbol, bound);
    return "";
}

string AstStream::addNode_ShiftInto(AstOperation op, Token op_symbol, Token bound)
{
    if (_flow->getBranch().empty())
        return "Текущий узел пуст";

    // Запоминаем ноду с типом
    AstNode last_node = _flow->getBranch().back();
    _flow->getBranch().pop_back();
    // Добавляем новую ветку и перемещаем туда запомненную ноду
    _flow = _flow -> addNode(op, op_symbol, bound);
    _flow -> getBranch().push_back(last_node);
    return "";
}

string AstStream::addNode_Branch(AstOperation op, Token op_symbol, Token bound)
{
    if (_flow->getBranch().empty())
        return "Текущий узел пуст";

    // Добавляем ноду в последнюю ветку текущей ноды
    _flow -> getBranch().back().addNode(op, op_symbol, bound);
    return "";
}

string AstStream::addNode_Branch_StepInto(AstOperation op, Token op_symbol, Token bound)
{
    if (_flow->getBranch().empty())
        return "Текущий узел пуст";

    // Добавляем ноду в последнюю ветку текущей ноды и переключаемся на неё
    _flow = _flow -> getBranch().back().addNode(op, op_symbol, bound);
    return "";
}

string AstStream::goParent()
{
    if (_flow -> getParent() == nullptr)
        return "Невозможно переключиться на вышестоящий узел";

    // Возвращаемся на вышестоящую ноду
    _flow = _flow -> getParent();
    return "";
}

string AstStream::addNode_Assignment(AstOperation op, Token op_symbol, Token bound)
{
    // Нужно переставить ноды с адресом / именем переменной внутрь оператора
    AstNode address;
    AstNode address_type;

    if (_flow->getBranch().empty())
        return "Текущий узел пуст";

    if (_flow -> getBranch().back().getOperation() != AstOperation::Push_Id)
    {
        address_type = _flow -> getBranch().back();
        _flow->getBranch().pop_back();

        if (_flow->getBranch().empty())
            return "Текущий узел пуст";

        address = _flow -> getBranch().back();
        _flow->getBranch().pop_back();
    }
    else
    {
        address = _flow -> getBranch().back();
        _flow->getBranch().pop_back();
    }

    // Формируем новую ветку с присваиванием
    _flow = _flow -> addNode(op, op_symbol, bound);
    _flow -> getBranch().emplace_back(address);
    if (address_type.getOperation() != AstOperation::None)
        _flow -> getBranch().back().getBranch().emplace_back(address_type);
    // Создаём ноду для выражения с правой части и переключаемся на нее
    _flow = _flow -> addNode(op, op_symbol, bound);
    return "";
}

string AstStream::resetOperation_ProcedureCalling()
{
    AstNode * p_node = _flow;

    while(p_node->getOperation() != AstOperation::Function_Call)
    {
        if (p_node -> getBranch().empty())
            break;
        p_node = &(p_node -> getBranch().back());
    }

    if (p_node->getOperation() == AstOperation::Function_Call)
        p_node->setOperation(AstOperation::Statement_Procedure_Call);
    else
    {
        // ВНИМАНИЕ! Для процедуры синтаксически правильной будет запись:
        //           foo("oops").pooh;
        // Помечаем как ошибку (семантическую)
        p_node->setOperation(AstOperation::Statement_Procedure_Call_Error);
    }
    return "";
}

string AstBuilderHelper::setMode(AstBuilderMode mode)
{
    _mode = mode;

    return "";
}

string AstBuilderHelper::setPrefix(u16string prefix)
{
    _prefix = prefix;

    return "";
}

string AstBuilderHelper::addNode(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode(op, op_symbol, bound);
}

string AstBuilderHelper::addNode_StepInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode_StepInto(op, op_symbol, bound);
}

string AstBuilderHelper::addNode_Parent_StepInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode_Parent_StepInto(op, op_symbol, bound);
}

string AstBuilderHelper::addNode_ShiftInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode_ShiftInto(op, op_symbol, bound);
}

string AstBuilderHelper::addNode_Branch(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode_Branch(op, op_symbol, bound);
}

string AstBuilderHelper::addNode_Branch_StepInto(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode_Branch_StepInto(op, op_symbol, bound);
}

string AstBuilderHelper::goParent(uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    return stream.goParent();
}

string AstBuilderHelper::addNode_Assignment(AstOperation op, Token op_symbol, Token bound, uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    string res = checkMode(op, op_symbol, bound, stream_no);
    if (!res.empty())
        return res;

    return stream.addNode_Assignment(op, op_symbol, bound);
}

string AstBuilderHelper::resetOperation_ProcedureCalling(uint16_t stream_no)
{
    AstStream & stream = getStream(stream_no);

    return stream.resetOperation_ProcedureCalling();
}

void AstBuilderHelper::drain(AstNode &ast)
{
    if (_mode == AstBuilderMode::Diff)
    {
        if (auto it = _ast_stream_set.find(EQUATION_STREAM_NO); it != _ast_stream_set.end())
            checkPushId(it->second._ast);

        if (auto it = _ast_stream_set.find(DIFF_STREAM_NO); it != _ast_stream_set.end())
            checkPushId(it->second._ast);
    }

    for(auto & [no,s] : _ast_stream_set)
    {
        for(auto & child : s._ast.getBranch())
            child._parent = &ast;               /// \attention Такие неявные операции требуют глубокого рефакторинга

        ast.getBranch().splice(ast.getBranch().end(), s._ast.getBranch());
    }
}

const AstStream &AstBuilderHelper::getAstStream(uint16_t no)
{
    static AstStream dummy;

    auto it = _ast_stream_set.find(no);

    if (it == _ast_stream_set.end())
        return dummy;

    return it->second;
}

void AstBuilderHelper::removeStream(uint16_t no)
{
    _ast_stream_set.erase(no);
}

AstStream &AstBuilderHelper::getStream(uint16_t stream_no)
{
    auto it = _ast_stream_set.find(stream_no);

    if (it != _ast_stream_set.end())
        return it->second;

    auto [it_new, ok] = _ast_stream_set.insert({stream_no, AstStream()});

    AstStream & s = it_new->second;

    s._flow = &s._ast;

    return s;
}

string AstBuilderHelper::checkMode(AstOperation &op, Token &op_symbol, Token &/*bound*/, uint16_t &/*stream_no*/)
{
    if (op == AstOperation::Push_Id)
    {
        if (!_prefix.empty())
        {
            op_symbol.symbol = _prefix + op_symbol.symbol;
        }
    }

    return "";
}

string AstBuilderHelper::checkPushId(const AstNode &ast_node)
{
    for(const AstNode & child : ast_node.getBranchC())
        if (child.getOperation() == AstOperation::Push_Id && child.getBranchC().empty())
        {
            const u16string & name = child.getSymbol().getLexeme();

            string res = addName(child);

            if (!res.empty())
                return res;

            if (_mode == AstBuilderMode::Diff && name.substr(0,5) == u"__dt_")
            {
                AstNode extracted_name_node(const_cast<AstNode *>(child.getParentC()), 
                                            child.getOperation(), 
                                            Token(LexemeType::Punctuation,
                                                  child.getSymbol().getLexeme().substr(5),
                                                  child.getSymbol().getLocation()
                                                  ), 
                                            child.getBound()
                                            );

                res = addName(extracted_name_node);

                if (!res.empty())
                    return res;
            }
        }
        else if(!checkPushId(child).empty())
            break;

    return "";
}

std::string AstBuilderHelper::addName(const AstNode &name_node)
{
    const u16string & name = name_node.getSymbol().getLexeme();

    if (_names.find(name) == _names.end()) 
    {
        string res = addNode_StepInto(AstOperation::Statement_Id, 
                                      name_node.getSymbol(), 
                                      name_node.getBound(), 
                                      DEFAULT_STREAM_NO
                                      );

        if (!res.empty())
            return res;

        res = addNode(AstOperation::Statement_Type, 
                      Token(LexemeType::Punctuation,
                            u"float",
                            name_node.getSymbol().getLocation(),
                            TokenQualification::Keyword
                            ), 
                      name_node.getBound(), 
                      DEFAULT_STREAM_NO
                      );

        if (!res.empty())
            return res;

        res = goParent(DEFAULT_STREAM_NO);
        if (!res.empty())
            return res;

        _names.insert(name);
    }

    return "";
}
