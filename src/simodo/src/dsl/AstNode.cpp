/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include "simodo/dsl/AstNode.h"

using namespace std;
using namespace simodo::dsl;

u16string simodo::dsl::getAstOperationName(AstOperation op)
{
    u16string str;

    switch(op)
    {
    case AstOperation::None:
        str = u"None";
        break;
    case AstOperation::InternalPush:
        str = u"InternalPush";
        break;
    case AstOperation::InternalPop:
        str = u"InternalPop";
        break;
    case AstOperation::Ternary_True:
        str = u"Ternary_True";
        break;
    case AstOperation::Ternary_False:
        str = u"Ternary_False";
        break;
    case AstOperation::Logical_Or:
        str = u"Logical_Or";
        break;
    case AstOperation::Logical_And:
        str = u"Logical_And";
        break;
    case AstOperation::Compare_Equal:
        str = u"Compare_Equal";
        break;
    case AstOperation::Compare_NotEqual:
        str = u"Compare_NotEqual";
        break;
    case AstOperation::Compare_Less:
        str = u"Compare_Less";
        break;
    case AstOperation::Compare_LessOrEqual:
        str = u"Compare_LessOrEqual";
        break;
    case AstOperation::Compare_More:
        str = u"Compare_More";
        break;
    case AstOperation::Compare_MoreOrEqual:
        str = u"Compare_MoreOrEqual";
        break;
    case AstOperation::Arithmetic_Addition:
        str = u"Arithmetic_Addition";
        break;
    case AstOperation::Arithmetic_Subtraction:
        str = u"Arithmetic_Subtraction";
        break;
    case AstOperation::Arithmetic_Multiplication:
        str = u"Arithmetic_Multiplication";
        break;
    case AstOperation::Arithmetic_Division:
        str = u"Arithmetic_Division";
        break;
    case AstOperation::Arithmetic_Modulo:
        str = u"Arithmetic_Modulo";
        break;
    case AstOperation::Arithmetic_Power:
        str = u"Arithmetic_Power";
        break;
    case AstOperation::Unary_Plus:
        str = u"Unary_Plus";
        break;
    case AstOperation::Unary_Minus:
        str = u"Unary_Minus";
        break;
    case AstOperation::Unary_Not:
        str = u"Unary_Not";
        break;
    case AstOperation::Push_Constant:
        str = u"Push_Constant";
        break;
    case AstOperation::Push_Id:
        str = u"Push_Id";
        break;
    case AstOperation::Iteration_PrefixPlus:
        str = u"Iteration_PrefixPlus";
        break;
    case AstOperation::Iteration_PrefixMinus:
        str = u"Iteration_PrefixMinus";
        break;
    case AstOperation::Iteration_PosfixPlus:
        str = u"Iteration_PosfixPlus";
        break;
    case AstOperation::Iteration_PosfixMinus:
        str = u"Iteration_PosfixMinus";
        break;
    case AstOperation::Address_Array:
        str = u"Address_Array";
        break;
    case AstOperation::Address_Tuple:
        str = u"Address_Tuple";
        break;
    case AstOperation::Function_Call:
        str = u"Function_Call";
        break;
    case AstOperation::Statement_Blank:
        str = u"Statement_Blank";
        break;
    case AstOperation::Statement_Type:
        str = u"Statement_Type";
        break;
    case AstOperation::Statement_Id:
        str = u"Statement_Id";
        break;
    case AstOperation::Statement_Assign:
        str = u"Statement_Assign";
        break;
    case AstOperation::Statement_Assign_Addition:
        str = u"Statement_Assign_Addition";
        break;
    case AstOperation::Statement_Assign_Subtraction:
        str = u"Statement_Assign_Subtraction";
        break;
    case AstOperation::Statement_Assign_Multiplication:
        str = u"Statement_Assign_Multiplication";
        break;
    case AstOperation::Statement_Assign_Division:
        str = u"Statement_Assign_Division";
        break;
    case AstOperation::Statement_Assign_Modulo:
        str = u"Statement_Assign_Modulo";
        break;
    case AstOperation::Function_Declaration:
        str = u"Function_Declaration";
        break;
    case AstOperation::Function_Body:
        str = u"Function_Body";
        break;
    case AstOperation::Statement_Procedure_Call:
        str = u"Statement_Procedure_Call";
        break;
    case AstOperation::Statement_Procedure_Call_Error:
        str = u"Statement_Procedure_Call_Error";
        break;
    case AstOperation::Statement_Block:
        str = u"Statement_Block";
        break;
    case AstOperation::Statement_Break:
        str = u"Statement_Break";
        break;
    case AstOperation::Statement_Continue:
        str = u"Statement_Continue";
        break;
    case AstOperation::Statement_Return:
        str = u"Statement_Return";
        break;
    case AstOperation::Statement_If_True:
        str = u"Statement_If_True";
        break;
    case AstOperation::Statement_If_False:
        str = u"Statement_If_False";
        break;
    case AstOperation::Statement_While:
        str = u"Statement_While";
        break;
    case AstOperation::Statement_While_Body:
        str = u"Statement_While_Body";
        break;
    case AstOperation::Statement_DoWhile:
        str = u"Statement_DoWhile";
        break;
    case AstOperation::Statement_DoWhile_Body:
        str = u"Statement_DoWhile_Body";
        break;
    case AstOperation::Statement_For:
        str = u"Statement_For";
        break;
    case AstOperation::Statement_For_Body:
        str = u"Statement_For_Body";
        break;
    case AstOperation::Statement_UseAsType:
        str = u"Statement_UseAsType";
        break;
    case AstOperation::Statement_Using:
        str = u"Statement_Using";
        break;
    case AstOperation::DataStructure_Array:
        str = u"DataStructure_Array";
        break;
    case AstOperation::DataStructure_Tuple:
        str = u"DataStructure_Tuple";
        break;
//    case AstOperation::DSL_operation:
//        str = u"DSL_operation";
//        break;
    default:
        str = u"***";
        break;
    }

    return str;
}
