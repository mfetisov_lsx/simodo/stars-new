/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <exception>

// #include <iostream>
// #include <iomanip>

#include "simodo/dsl/ScriptC_Semantics.h"
#include "simodo/dsl/ModulesManagement.h"
#include "simodo/dsl/Exception.h"

#include "simodo/convert.h"

#include "simodo/dsl/GrammarManagement.h"
#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/Parser.h"
#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"

#if (__GNUC__ > 7) || defined(__clang__)
#include <filesystem>
#else
#include <experimental/filesystem>
using namespace std::experimental;
#endif

#define SCI_DEBUG_no

using namespace std;
using namespace simodo::dsl;

ScriptC_Semantics::ScriptC_Semantics(AReporter &m,
                                     ModulesManagement &mm,
                                     std::vector<SemanticName> &name_set,
                                     std::vector<SemanticScope> &scope_set,
                                     std::map<u16string, const AstNode *> *functions)
    : _m(m)
    , _mm(mm)
    , _name_table(m, name_set, scope_set, functions != nullptr)
    , _functions(functions)
{
    _name_table.openScope(TokenLocation(u""));
}

ScriptC_Semantics::~ScriptC_Semantics()
{
    _name_table.closeScope(AstNode());
}

void ScriptC_Semantics::importNamespace(u16string name, const SCI_Namespace_t &context)
{
    _name_table.importNamespace(name,context);
}

bool ScriptC_Semantics::check(const AstNode & ast)
{
    _name_table.openScope(ast.getSymbol().getLocation());

    bool ok = false;

#ifndef SCI_DEBUG
    try
    {
#endif
        ok = checkInner(ast);
#ifndef SCI_DEBUG
    }
    catch (const Exception & e)
    {
        _m.reportError(ast.getSymbol(),
                       u"Произошёл сбой в методе " + convertToU16(e.where()) + u". Анализ прерван\n"+
                       convertToU16(e.what()));
    }
    catch (const exception & e)
    {
        _m.reportError(ast.getSymbol(),
                       u"Произошёл сбой, анализ прерван\n" + convertToU16(e.what()));
    }
    catch (...)
    {
        _m.reportError(ast.getSymbol(),
                       u"Произошёл сбой, анализ прерван");
    }
#endif

    _name_table.closeScope(ast);

    return ok;
}

bool ScriptC_Semantics::checkInner(const AstNode &ast)
{
    // static size_t nest_count = 0;
    // nest_count ++;

    bool state = true;

    for(auto it = ast.getBranchC().begin(); it != ast.getBranchC().end(); ++it)
    {
        // auto start = chrono::high_resolution_clock::now();

        const auto & n = *it;

        switch(n.getOperation())
        {
        case AstOperation::None:
//        case AstOperation::InternalPush:
            break;

        case AstOperation::InternalPop:
            _type_stack.pop_back();
            break;

        case AstOperation::Ternary_True:
        case AstOperation::Statement_If_True:
            if (!checkTrueClause(n))
                state = false;
            break;

        case AstOperation::Ternary_False:
        case AstOperation::Statement_If_False:
            if (!checkFalseClause(n))
                state = false;
            break;

        case AstOperation::Logical_Or:
        case AstOperation::Logical_And:
            if (!checkLogicalOperation(n))
                state = false;
            break;

        case AstOperation::Compare_Equal:
        case AstOperation::Compare_NotEqual:
        case AstOperation::Compare_Less:
        case AstOperation::Compare_LessOrEqual:
        case AstOperation::Compare_More:
        case AstOperation::Compare_MoreOrEqual:
            if (!checkCompareOperation(n))
                state = false;
            break;

        case AstOperation::Arithmetic_Addition:
        case AstOperation::Arithmetic_Subtraction:
        case AstOperation::Arithmetic_Multiplication:
        case AstOperation::Arithmetic_Division:
        case AstOperation::Arithmetic_Modulo:
        case AstOperation::Arithmetic_Power:
            if (!checkArithmeticOperation(n))
                state = false;
            break;

        case AstOperation::Unary_Plus:
        case AstOperation::Unary_Minus:
            if (!checkUnaryOperation(n))
                state = false;
            break;

        case AstOperation::Unary_Not:
            if (!checkUnaryNotOperation(n))
                state = false;
            break;

        case AstOperation::Push_Constant:
            if (!pushConst(n))
                state = false;
            break;

        case AstOperation::Push_Id:
            if (!pushId(n))
                state = false;
            break;

        case AstOperation::Function_Call:
        case AstOperation::Statement_Procedure_Call:
            if (!callFunction(n, n.getOperation() == AstOperation::Statement_Procedure_Call))
                state = false;
            break;

        case AstOperation::Statement_Blank:
            _m.reportWarning(n.getSymbol(),
                             u"Использование пустого оператора небезопасно, желательно пользоваться пустым блоком");
            break;

        case AstOperation::Statement_Id:
            if (!checkDeclaration(n))
                state = false;
            break;

        case AstOperation::Statement_Assign:
        case AstOperation::Statement_Assign_Addition:
        case AstOperation::Statement_Assign_Subtraction:
        case AstOperation::Statement_Assign_Multiplication:
        case AstOperation::Statement_Assign_Division:
        case AstOperation::Statement_Assign_Modulo:
            assert(!n.getBranchC().empty());
            if (!checkAssignStatement(n, n.getBranchC().front()))
                state = false;
            break;

        case AstOperation::Statement_Procedure_Call_Error:
            _m.reportError(n.getBound(), u"Некорректная запись адреса в качестве оператора");
            // nest_count --;
            return false;

        case AstOperation::Statement_Block:
            if (!checkInnerWithScope(n))
                state = false;
            break;

        case AstOperation::Statement_Break:
        case AstOperation::Statement_Continue:
            // checkBreakContinue(n);
            if (!checkBreakContinue(n))
                state = false;
            break;

        case AstOperation::Statement_Return:
            // checkReturn(n);
            if (!checkReturn(n))
                state = false;
            break;

        case AstOperation::Statement_While:
            if (!checkCycleWhile(n))
                state = false;
            break;

        case AstOperation::Statement_DoWhile:
            if (!checkCycleDoWhile(n))
                state = false;
            break;

        case AstOperation::Statement_For:
            if (!checkCycleFor(n))
                state = false;
            break;

        case AstOperation::Statement_UseAsType:
            if (!loadModuleAsType(n))
                state = false;
            break;

        case AstOperation::Statement_Using:
            if (!checkUsingStatement(n))
                state = false;
            break;

        case AstOperation::DataStructure_Tuple:
            {
                if (!it->getBranchC().empty()) {
                    _name_table.openScope(it->getSymbol().getLocation());
                    _name_table.closeScope(it->getBranchC().back().getSymbol().getLocation());
                }
            }
            [[fallthrough]];
        case AstOperation::DataStructure_Array:
            if (!checkDataStructure(n))
                state = false;
            break;

        default:
            _m.reportFatal(n.getSymbol(),
                           u"Операция '" + getAstOperationName(n.getOperation()) +
                           u"' не поддерживается");
            // nest_count --;
            return false;
        }

        // auto elapsed = chrono::duration_cast<chrono::milliseconds>(chrono::high_resolution_clock::now() - start);
        // cerr << setw(3) << nest_count-1
        //      << ", " << setw(6) << elapsed.count() 
        //      << ", '" << convertToU8(getAstOperationName(n.getOperation())) 
        //      << "', '" << convertToU8(n.getSymbol().getLocation().file_name) 
        //      << "', " << n.getSymbol().getLocation().line
        //      << ", " << n.getSymbol().getLocation().column
        //      << endl;
    }

    // nest_count --;
    return state;
}

bool ScriptC_Semantics::checkInnerWithScope(const AstNode &ast)
{
    _name_table.openScope(ast.getSymbol().getLocation());

    bool ok = checkInner(ast);

    _name_table.closeScope(ast);

    return ok;
}

bool ScriptC_Semantics::loadModuleAsType(const AstNode &op)
{
    Token id_name_token;
    Token module_file_token;
    Token type_token;
    Token grammar_name_token;

    if (!op.getBranchC().front().getBranchC().empty())
        grammar_name_token = op.getBranchC().front().getBranchC().front().getSymbol();

    for(const AstNode & s : op.getBranchC())
        switch(s.getOperation())
        {
        case AstOperation::None:
            module_file_token = s.getSymbol();
            break;
        case AstOperation::Statement_Type:
            type_token = s.getSymbol();
            break;
        case AstOperation::Statement_Id:
            id_name_token = s.getSymbol();
            break;
        default:
            break;
        }

    const ModuleImportResults & module = _mm.loadModule(module_file_token, grammar_name_token);

    if (!module.ok) {
        // _m.reportError(op.getSymbol(), u"Ошибки в модуле");
        return false;
    }

    if (NOT_FOUND_INDEX != _name_table.findNameOnTop(type_token.getLexeme())) {
        _m.reportError(type_token,
                       u"Имя '" + type_token.getLexeme() + u"' уже объявлено в текущей зоне видимости");
        return false;
    }

    SemanticName type(type_token.getLocation(), type_token.getLexeme(), SemanticNameQualification::Type, SemanticNameType::Undefined);

    size_t owner = _name_table.addNameDeclaration(type);

    _name_table.openScope(op.getSymbol().getLocation());

    SemanticName body(module.ast.getSymbol().getLocation(), u"_", SemanticNameQualification::Function, SemanticNameType::IntFunction);
    body.owner = owner;
    size_t body_index = _name_table.addNameDeclaration(body);

    SemanticName body_return_value(module.ast.getSymbol().getLocation(), u"", SemanticNameQualification::None, SemanticNameType::Undefined);
    body_return_value.owner = body_index;
    _name_table.addNameDeclaration(body_return_value);

    size_t count = module.names.size();
    for(size_t i=0; i < count; ++i)
    {
        const SemanticName & n = module.names[i];

        if (n.depth == DEPTH_TOP_LEVEL && n.owner == UNDEFINED_INDEX)
            if (n.qualification == SemanticNameQualification::Scalar ||
                n.qualification == SemanticNameQualification::Tuple ||
                n.qualification == SemanticNameQualification::Array ||
                n.qualification == SemanticNameQualification::Function)
            {
                SemanticName element = n;
                element.owner = owner;
                size_t element_index = _name_table.addNameDeclaration(element);
                fillTupleFromType(module.names, i, element_index);
            }
    }

    _name_table.closeScope(op);

    if (!id_name_token.getLexeme().empty())
    {
        if (NOT_FOUND_INDEX != _name_table.findNameOnTop(id_name_token.getLexeme())) {
            _m.reportError(id_name_token,
                        u"Имя '" + id_name_token.getLexeme() + u"' уже объявлено в текущей зоне видимости");
            return false;
        }

        SemanticName var(op.getBranchC().back().getSymbol().getLocation(), id_name_token.getLexeme(), SemanticNameQualification::Tuple, SemanticNameType::Undefined);
        var.type_index = owner;
        var.definition = op.getBranchC().back().getSymbol().getLocation();
        _name_table.addNameReference(owner, type_token.getLocation());

        size_t var_index = _name_table.addNameDeclaration(var);

        _name_table.openScope(op.getBranchC().back().getSymbol().getLocation());
        fillTupleFromType(_name_table.name_set(), owner, var_index);
        _name_table.closeScope(op.getBranchC().back());
    }

    return true;
}

bool ScriptC_Semantics::checkUsingStatement(const AstNode &op)
{
    size_t  name_index  = _name_table.findName(op.getSymbol().getLexeme());

    if (name_index == NOT_FOUND_INDEX)
    {
        _m.reportError(op.getSymbol(), u"Имя '" + op.getSymbol().getLexeme() + u"' не найдено");
        return false;
    }

    const SemanticName & name = _name_table.name_set()[name_index];

    if (name.qualification != SemanticNameQualification::Tuple)
    {
        _m.reportError(op.getSymbol(), u"Имя '" + op.getSymbol().getLexeme() + u"' должно быть кортежем (модулем) в зоне видимости");
        return false;
    }

    _using_set.push_back(name_index);

    return true;
}

bool ScriptC_Semantics::checkTrueClause(const AstNode &op)
{
    if (_type_stack.empty())
        throw Exception("checkTrueClause", "_type_stack.empty()");

    bool state = true;

    if (_type_stack.back() != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Условие должно иметь логический тип");
        state = false;
    }

    _type_stack.pop_back();

    if (!checkInner(op))
        state = false;

    return state;
}

bool ScriptC_Semantics::checkFalseClause(const AstNode &op)
{
    size_t stack_last_size = _type_stack.size();

    bool ok = checkInner(op);

    if (_type_stack.size() > stack_last_size)
        _type_stack.pop_back();

    return ok;
}

bool ScriptC_Semantics::checkLogicalOperation(const AstNode &op)
{
    if (_type_stack.size() < 2)
        throw Exception("checkLogicalOperation", "_type_stack.size() < 2");

    bool state = true;

    if (_type_stack[_type_stack.size()-2] != SemanticNameType::Bool ||
        _type_stack[_type_stack.size()-1] != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Операнды для логической операции 'и'/'или' должны иметь логический тип");
        state = false;
    }

    _type_stack.pop_back();
    _type_stack.pop_back();
    _type_stack.emplace_back(SemanticNameType::Bool);

    return state;
}

bool ScriptC_Semantics::checkCompareOperation(const AstNode &op)
{
    if (_type_stack.size() < 2)
        throw Exception("checkCompareOperation", "_type_stack.size() < 2");

    bool state = true;

    if (_type_stack[_type_stack.size()-2] != _type_stack[_type_stack.size()-1])
    {
        _m.reportError(op.getSymbol(), u"Операнды для операции сравнения должны иметь одинаковый тип");
        state = false;
    }

    if ((op.getOperation() == AstOperation::Compare_Equal || op.getOperation() == AstOperation::Compare_NotEqual)
            && _type_stack[_type_stack.size()-1] == SemanticNameType::Float)
    {
        _m.reportWarning(op.getSymbol(), u"Операции сравнения '==' и '!=' для вещественных типов являются небезопасными");
    }

    _type_stack.pop_back();
    _type_stack.pop_back();
    _type_stack.emplace_back(SemanticNameType::Bool);

    return state;
}

bool ScriptC_Semantics::checkArithmeticOperation(const AstNode &op)
{
    if (_type_stack.size() < 2)
        throw Exception("checkArithmeticOperation", "_type_stack.size() < 2");

    bool state = true;

    SemanticNameType t1 = _type_stack[_type_stack.size()-2];
    SemanticNameType t2 = _type_stack[_type_stack.size()-1];

    //TODO: Нужна проверка на операции с не скалярными значениями - реализовать при рефакторинге семантического анализатора!
//    if (_stack.top(1).value.qualification != SemanticNameQualification::Scalar ||
//        _stack.top(0).value.qualification != SemanticNameQualification::Scalar)
//    {
//        _m.reportError(op.getSymbol(), u"Операнды для арифметических операций должны иметь скалярное значение");
//        return false;
//    }

    // Проверки
    if (!checkArithmeticOperation(op,t1,t2))
        state = false;

    // Операция
    SemanticNameType res = calculateArithmeticOperationType(op, t1, t2);

    _type_stack.pop_back();
    _type_stack.pop_back();
    _type_stack.emplace_back(res);

    return state;
}

bool ScriptC_Semantics::checkUnaryOperation(const AstNode &op)
{
    if (_type_stack.empty())
        throw Exception("checkUnaryOperation", "_type_stack.empty()");

    bool state = true;

    if (_type_stack.back() != SemanticNameType::Int && _type_stack.back() != SemanticNameType::Float)
    {
        _m.reportError(op.getSymbol(), u"Недопустимое применение операции унарного плюса/минуса");
        state = false;
    }

    return state;
}

bool ScriptC_Semantics::checkUnaryNotOperation(const AstNode &op)
{
    if (_type_stack.empty())
        throw Exception("checkUnaryNotOperation", "_type_stack.empty()");

    bool state = true;

    if (_type_stack.back() != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Операнд для логического отрицания должен иметь логический тип");
        state = false;
    }

    return state;
}

bool ScriptC_Semantics::pushConst(const AstNode &op)
{
    if (op.getSymbol().getType() == LexemeType::Annotation)
    {
        _type_stack.emplace_back(SemanticNameType::String);
        return true;
    }

    if (op.getSymbol().getType() == LexemeType::Number)
    {
        SemanticNameType res;

        if (op.getSymbol().getQualification() == TokenQualification::RealNubmer)
        {
            res = SemanticNameType::Float;
        }
        else if (op.getSymbol().getQualification() == TokenQualification::Integer)
        {
            res = SemanticNameType::Int;
        }
        else
        {
            assert(false);
        }
        _type_stack.emplace_back(res);
        return true;
    }

    if (op.getSymbol().getType() == LexemeType::Punctuation)
    {
        _type_stack.emplace_back(SemanticNameType::Bool);
        return true;
    }

    assert(false);
    return false;
}

bool ScriptC_Semantics::pushId(const AstNode & op, size_t owner, SemanticNameAccess owner_access, AstOperation owner_iteration_qualificator)
{
    // Находим переменную с указанным в АСД именем

    size_t name_index = _name_table.findName(op.getSymbol().getLexeme(), owner);

    if (name_index == NOT_FOUND_INDEX && owner == UNDEFINED_INDEX)
    {
        for(size_t i=0; i < _using_set.size(); ++i)
        {
            name_index = _name_table.findName(op.getSymbol().getLexeme(), _using_set[i]);

            if (name_index != NOT_FOUND_INDEX)
            {
                owner = _using_set[i];
                const SemanticName & owner_name = _name_table.name_set()[owner];
                owner_access = strongestAccess(owner_name.access, owner_access);
                break;
            }
        }
    }

    if (name_index == NOT_FOUND_INDEX)
    {
        if (owner == UNDEFINED_INDEX)
            _m.reportError(op.getSymbol(), u"Переменная '" + op.getSymbol().getLexeme() + u"' не определена");
        else
            _m.reportError(op.getSymbol(),
                           u"Переменная '" + op.getSymbol().getLexeme() +
                           u"' не является членом кортежа '" + _name_table.name_set()[owner].name.getLexeme() + u"'");
        return false;
    }

    const SemanticName & name = _name_table.name_set()[name_index];

    _name_table.addNameReference(name_index, op.getSymbol().getLocation());

    if (name.definition.file_name.empty()) 
        _name_table.forceInputFlag(name_index);

    bool ok = true;

    if (_name_table.name_set()[name_index].qualification == SemanticNameQualification::Type)
    {
        _m.reportError(op.getSymbol(), u"Недопустимое использование типа " + op.getSymbol().getLexeme());
        ok = false;
    }

    // Определяем квалификатор переменной

    const AstNode * var_op                 = nullptr;
    AstOperation    address_qualificator   = AstOperation::None;
    AstOperation    iteration_qualificator = owner_iteration_qualificator;
    const AstNode * arr_op                 = nullptr;

    for(const AstNode & o : op.getBranchC())
        switch(o.getOperation())
        {
        case AstOperation::Address_Array:
            arr_op = &o;
            [[fallthrough]];
        case AstOperation::Address_Tuple:
            address_qualificator = o.getOperation();
            break;
        case AstOperation::Iteration_PosfixPlus:
        case AstOperation::Iteration_PrefixPlus:
        case AstOperation::Iteration_PosfixMinus:
        case AstOperation::Iteration_PrefixMinus:
            assert(iteration_qualificator == AstOperation::None);
            iteration_qualificator = o.getOperation();
            break;
        default:
            assert(var_op == nullptr);
            var_op = &o;
            break;
        }

    // Обрабатываем квалификатор

    if (address_qualificator == AstOperation::Address_Tuple)
    {
        assert(var_op != nullptr);

        if (var_op->getOperation() == AstOperation::Push_Id)
        {
            if (!pushId(*var_op, name_index, strongestAccess(_name_table.name_set()[name_index].access,owner_access),iteration_qualificator))
                ok = false;
        }
        else if (var_op->getOperation() == AstOperation::Statement_Procedure_Call ||
                 var_op->getOperation() == AstOperation::Function_Call)
        {
            if (!callFunction(*var_op,
                              var_op->getOperation() == AstOperation::Statement_Procedure_Call,
                              name_index))
                ok = false;
        }
        else
        {
            _m.reportError(var_op->getSymbol(), u"Недопустимое использование элемента кортежа");
            ok = false;
        }

        return ok;
    }

    if (address_qualificator == AstOperation::Address_Array)
    {
        if (name.qualification != SemanticNameQualification::Array && name.qualification != SemanticNameQualification::Tuple)
        {
            _m.reportError(op.getSymbol(),
                           u"Переменная '" + op.getSymbol().getLexeme() + u"' не является массивом");
            ok = false;
        }

        if (ok && arr_op != nullptr)
        {
            size_t last_stack_count = _type_stack.size();

            if (!checkInner(*arr_op))
                ok = false;

            if (name.dimensions != 0 && name.dimensions != _type_stack.size() - last_stack_count)
            {
                _m.reportError(op.getSymbol(),
                               u"Размерность массива '" + op.getSymbol().getLexeme() + u"' не соответствует заданному индексу");
                ok = false;
            }

            while(_type_stack.size() > last_stack_count)
                _type_stack.pop_back();
        }
    }

    // Пушим переменную (может являться членом кортежа)

    SemanticNameAccess access_level = strongestAccess(owner_access,_name_table.name_set()[name_index].access);

    if (iteration_qualificator != AstOperation::None)
    {
        if (access_level != SemanticNameAccess::FullAccess)
        {
            _m.reportError(op.getSymbol(),
                           u"Присваивание элементу кортежа со спецификатором доступа " + getSemanticNameAccessName(access_level));
            ok = false;
        }

        if (name.type != SemanticNameType::Int)
        {
            _m.reportError(op.getSymbol(),
                           u"Для использования итерации переменная '"+op.getSymbol().getLexeme()+
                           u"' должна иметь целочисленный тип");
            ok = false;
        }
    }
    else if (access_level == SemanticNameAccess::Closed)
    {
        _m.reportError(op.getSymbol(),
                       u"Использование элемента кортежа со спецификатором доступа " + getSemanticNameAccessName(access_level));
        ok = false;
    }

    _type_stack.push_back(name.type);

    return ok;
}

bool ScriptC_Semantics::callFunction(const AstNode &op, bool is_procedure_call, size_t owner)
{
    // Получаем структуру с определением функции

    bool    ok          = true;
    size_t  name_index  = _name_table.findName(op.getSymbol().getLexeme(), owner);

    if (name_index == NOT_FOUND_INDEX && owner == UNDEFINED_INDEX)
    {
        for(size_t o : _using_set)
        {
            name_index = _name_table.findName(op.getSymbol().getLexeme(), o);

            if (name_index != NOT_FOUND_INDEX)
            {
                owner = o;
                break;
            }
        }
    }

    if (name_index == NOT_FOUND_INDEX)
    {
        if (owner == UNDEFINED_INDEX)
            _m.reportError(op.getSymbol(),u"Функция '" + op.getSymbol().getLexeme() + u"' не объявлена");
        else
        {
            assert(owner < _name_table.name_set().size());

            _m.reportError(op.getSymbol(),
                           u"Функция '" + op.getSymbol().getLexeme() +
                           u"' не является членом кортежа '" + _name_table.name_set()[owner].name.getLexeme() + u"'");
        }
        ok = false;
    }

    SemanticName undefined_name(op.getSymbol().getLocation(), op.getSymbol().getLexeme());

    const SemanticName & function = (name_index == NOT_FOUND_INDEX) ? undefined_name : _name_table.name_set()[name_index];

    size_t return_element = _name_table.findName(u"", name_index);

    if (name_index != NOT_FOUND_INDEX)
    {
        if (function.qualification != SemanticNameQualification::Function)
        {
            _m.reportError(op.getSymbol(), u"Имя '" + op.getSymbol().getLexeme() + u"' не является функцией");
            ok = false;
        }
        else if (ok && !is_procedure_call && NOT_FOUND_INDEX == return_element)
        {
            _m.reportError(op.getSymbol(),
                           u"Функция '" + op.getSymbol().getLexeme() +
                           u"' не возвращает значений и не может быть использована в выражении");
            ok = false;
        }

        // if (owner == UNDEFINED_INDEX)
            _name_table.addNameReference(name_index, op.getSymbol().getLocation());
    }

    //TODO: Вообще-то лучше проверять кол-во параметров в АСД, а не на стеке (см. ниже).
    // Делать доп. уровень вложенности нежелательно, т.к. поиск операции Function_Call,
    // чтобы заменить её на Statement_Procedure_Call будет чрезмерно сложным
    /// \todo unclear

    // Запоминаем размер стека, чтобы проверить соответствие с количеством параметров функции
    size_t stack_count = _type_stack.size();

    // Обрабатываем параметры
    if (!checkInner(op))
        ok = false;

    if (name_index != NOT_FOUND_INDEX && ok)
    {
        vector<size_t> parameter_set = _name_table.buildElementSet(name_index);

        if (parameter_set.size() - 1 != _type_stack.size()-stack_count)
        {
            _m.reportError(op.getSymbol(),
                           u"Некорректное количество параметров функции '" + op.getSymbol().getLexeme() + u"'");
            ok = false;
        }

        // Вызываем функцию
        if (function.type == SemanticNameType::Undefined)
        {
            _m.reportError(op.getSymbol(), u"Функция '" + op.getSymbol().getLexeme() + u"' не определена");
            ok = false;
        }
        else if (ok) // Иначе возможен выход за границу parameter_set
        {
            for(size_t i=0; i+stack_count < _type_stack.size(); ++i)
            {
                SemanticNameQualification param_qualification = _name_table.name_set()[parameter_set[i+1]].qualification;
                SemanticNameType          param_type          = _name_table.name_set()[parameter_set[i+1]].type;

                if (param_qualification == SemanticNameQualification::None)
                    continue;

                // В данной модели семантического анализа невозможно определить корректность типов аргумента для не скаляра
                if (param_qualification != SemanticNameQualification::Scalar)
                    continue;

                if (param_type != _type_stack[i+stack_count])
                {
                    if (_type_stack[i+stack_count] == SemanticNameType::Undefined) {
                        _type_stack[i+stack_count] = param_type;
                        continue;
                    }
                    if (param_type == SemanticNameType::Float && _type_stack[i+stack_count] == SemanticNameType::Int) {
                        _type_stack[i+stack_count] = param_type;
                        continue;
                    }

                    _m.reportError(op.getSymbol(),
                                   u"Несоотносимый тип аргумента " + convertToU16(to_string(i+1)) +
                                   u" (" + getSemanticNameTypeName(_type_stack[i+stack_count]) +
                                   u") и параметра (" + getSemanticNameTypeName(param_type) +
                                   u") функции '" + op.getSymbol().getLexeme() + u"'");
                    ok = false;
                }
            }
        }

//TODO: Проверка есть в интепретаторе, нужно повторить в семантическом анализе
//        const AstNode & node = *get<const AstNode *>(function->value.variant);

//        if (function->tuple.front()->value.type != SCI_VariantType::Undefined &&
//                _type_stack.size() == stack_count)
//        {
//            _m.reportError(node.getSymbol(),
//                           u"Функция '" + op.getSymbol().getLexeme() + u"' должна возвращать значение");
//            state = false;
//        }
    }

    // Очищаем стек
    for (size_t i=_type_stack.size(); i > stack_count; --i)
        _type_stack.pop_back();

    if (NOT_FOUND_INDEX != return_element)
        _type_stack.push_back(_name_table.name_set()[return_element].type);
    else
        _type_stack.push_back(SemanticNameType::Undefined);

    return ok;
}

bool ScriptC_Semantics::checkDeclaration(const AstNode &op)
{
    assert(!op.getBranchC().empty());

    {
        const AstNode & back_node = op.getBranchC().back();

        if (back_node.getOperation() == AstOperation::Function_Declaration || back_node.getOperation() == AstOperation::Function_Body)
            return checkFunctionDeclaration(op);
    }

    bool ok = true;

    // Проверки
    if (NOT_FOUND_INDEX != _name_table.findNameOnTop(op.getSymbol().getLexeme()))
    {
        _m.reportError(op.getSymbol(),
                       u"Имя '" + op.getSymbol().getLexeme() + u"' уже объявлено в текущей зоне видимости");
        ok = false;
    }

    SemanticNameType type       = getType(op.getBranchC().front());
    size_t           type_index = UNDEFINED_INDEX;
    const Token      type_token = op.getBranchC().front().getSymbol();
    bool             is_auto    = (type_token.getLexeme() == u"auto");
    SemanticNameQualification qualification = (type_index != UNDEFINED_INDEX) ? SemanticNameQualification::Tuple : SemanticNameQualification::Scalar;
    size_t           dimensions_count = 0;

    if (is_auto)
        qualification = SemanticNameQualification::None;
    else if (type == SemanticNameType::Undefined)
    {
        type_index = _name_table.findName(type_token.getLexeme());

        if (type_index == UNDEFINED_INDEX || _name_table.name_set()[type_index].qualification != SemanticNameQualification::Type)
        {
            _m.reportError(type_token, u"Переменная '" + op.getSymbol().getLexeme() + u"' имеет недопустимый тип");
            ok = false;
        }
    }            

    auto it = op.getBranchC().begin();  // type

    ++ it;  // array dimensions or assignment

    if (it != op.getBranchC().end() && it->getOperation() == AstOperation::None && it->getSymbol().getLexeme() == u"[")
    {
        qualification = SemanticNameQualification::Array;

        // array dimensions!
        if (is_auto)
        {
            _m.reportError(op.getSymbol(),
                           u"Спецификатор 'auto' недопустим с определением размерности для переменной '" + op.getSymbol().getLexeme() + u"'");
            ok = false;
        }

        size_t last_stack_count = _type_stack.size();

        if (!checkInner(*it))
            ok = false;

        dimensions_count = _type_stack.size() - last_stack_count;

        while(_type_stack.size() > last_stack_count)
        {
            if (ok && _type_stack[_type_stack.size()-1] != SemanticNameType::Int)
            {
                _m.reportError(op.getSymbol(), u"Размерность должна иметь целочисленный тип");
                ok = false;
            }
            _type_stack.pop_back();
        }

        ++ it;
    }

    SemanticName temp_var(op.getSymbol().getLocation(), op.getSymbol().getLexeme(), qualification, type);
    temp_var.dimensions     = dimensions_count;
    temp_var.semantic_flags = SemanticFlag_Input;
    temp_var.type_index     = type_index;

    size_t var_index = _name_table.addNameDeclaration(temp_var);

    {
        SemanticName & var = const_cast<SemanticName &>(_name_table.name_set()[var_index]);
        if (type_index < _name_table.name_set().size()) {
            var.qualification = SemanticNameQualification::Tuple;
            _name_table.addNameReference(type_index, type_token.getLocation());
            fillTupleFromType(_name_table.name_set(), type_index, var_index);
        }
    }

    if (it != op.getBranchC().end() && it->getOperation() == AstOperation::Statement_Assign)
    {
        _name_table.addNameDefinition(var_index, op.getSymbol().getLocation());
        {
            SemanticName & var = const_cast<SemanticName &>(_name_table.name_set()[var_index]);
            var.semantic_flags = 0;
        }

        assert(!it->getBranchC().empty());

        size_t last_stack_count = _type_stack.size();

        // Присваивание
        // Выполнение выражения в присваивании при объявлении переменной
        _owner_stack.push_back(var_index);
        if (!checkInner(*it))
            ok = false;
        _owner_stack.pop_back();

        /// @attention Вектор _name_table.name_set может быть перестроен в методе checkInner!
        /// Поэтому переопределяем var
        SemanticName & var = const_cast<SemanticName &>(_name_table.name_set()[var_index]);

        if (_type_stack.size() > last_stack_count)
        {
            if (is_auto)
            {
                if (it->getBranchC().front().getOperation() == AstOperation::DataStructure_Array)
                {
                    var.qualification = SemanticNameQualification::Array;
                    var.dimensions = 0; /// \todo Нужно делать проверку на размерность
                }
                else if (it->getBranchC().front().getOperation() == AstOperation::DataStructure_Tuple)
                {
                    var.qualification = SemanticNameQualification::Tuple;
                }
                else
                    var.qualification = SemanticNameQualification::Scalar;

                var.type = _type_stack.back();
            }
            else if (_type_stack.back() != var.type)
            {
                if (_type_stack.back() == SemanticNameType::Int && var.type == SemanticNameType::Float)
                {
                }
                else if (_type_stack.back() != SemanticNameType::Undefined && var.type != SemanticNameType::Undefined)
                {
                    _m.reportError(it -> getSymbol(),
                                   u"Тип выражения в операторе присваивания (" +
                                       getSemanticNameTypeName(_type_stack.back()) + u") не соответствует типу переменной (" +
                                       getSemanticNameTypeName(var.type) + u")");
                    ok = false;
                }
            }

            _type_stack.pop_back();
        }
    }
    else if (is_auto)
    {
        _m.reportError(op.getSymbol(), u"Некорректное использование ключевого слова 'auto'");
        ok = false;
    }

    return ok;
}

bool ScriptC_Semantics::checkFunctionDeclaration(const AstNode &op)
{
    bool ok = true;

    // Проверки
    if (op.getParentC() != nullptr && op.getParentC()->getParentC() != nullptr)
    {
        _m.reportError(op.getSymbol(), u"Объявление и определение функции недопустимо внутри блока операторов");
        ok = false;
    }

    size_t declared_index = _name_table.findNameOnTop(op.getSymbol().getLexeme());

    if (declared_index != NOT_FOUND_INDEX)
    {
        assert(declared_index < _name_table.name_set().size());

        if (ok && _name_table.name_set()[declared_index].qualification != SemanticNameQualification::Function)
        {
            _m.reportError(op.getSymbol(),
                           u"Функция '" + op.getSymbol().getLexeme() +
                           u"' конфликтует с ранее объявленной переменной");
            ok = false;
        }
    }

    // Заполняем описание

    assert(!op.getBranchC().empty());

    SemanticName fun_new_declaration(op.getSymbol().getLocation(), op.getSymbol().getLexeme(), SemanticNameQualification::Function, SemanticNameType::Undefined);
    SemanticName ret_new_declaration(TokenLocation(u""), u""s, SemanticNameQualification::Scalar, getType(op.getBranchC().front()));

    auto it = op.getBranchC().begin();
    it ++;
    assert(it != op.getBranchC().end());

    vector<size_t> declared_parameters;

    if (declared_index != NOT_FOUND_INDEX)
    {
        assert(declared_index < _name_table.name_set().size());

        declared_parameters = _name_table.buildElementSet(declared_index);

        if (!declared_parameters.empty())
        {
            size_t declared_ret = declared_parameters.front();

            assert(declared_ret < _name_table.name_set().size());

            if (_name_table.name_set()[declared_ret].qualification != ret_new_declaration.qualification
             || _name_table.name_set()[declared_ret].type != ret_new_declaration.type)
            {
                _m.reportError(op.getSymbol(),
                               u"Тип функции '" + op.getSymbol().getLexeme() +
                               u"' не соответствует ранее описанному");
                ok = false;
            }
        }
    }
    else if (ret_new_declaration.type == SemanticNameType::Undefined
     && op.getBranchC().front().getSymbol().getLexeme() != u"void")
    {
        _m.reportError(op.getBranchC().front().getSymbol(),
                       u"Недопустимый тип '" + op.getBranchC().front().getSymbol().getLexeme() +
                       u"' функции '" + op.getSymbol().getLexeme() + u"'");
        ok = false;
    }

    auto it_parameters = it;
    ++it;
    auto it_body = it;

    if (it_body != op.getBranchC().end())
    {
        if (declared_index == NOT_FOUND_INDEX)
        {
            fun_new_declaration.definition = op.getSymbol().getLocation();
            fun_new_declaration.type = SemanticNameType::IntFunction;
        }
        else
        {
            const SemanticName & fun = _name_table.name_set()[declared_index];

            if (!fun.definition.file_name.empty())
            {
                _m.reportError(op.getSymbol(),
                               u"Функция '" + op.getSymbol().getLexeme() + u"' уже определена");
                ok = false;
            }
            else
            {
                _name_table.addNameDefinition(declared_index, op.getSymbol().getLocation());
                _name_table.addNameType(declared_index, SemanticNameType::IntFunction);
            }
        }
    }

    size_t function_index = declared_index;

    if (declared_index == NOT_FOUND_INDEX)
    {
        function_index = _name_table.addNameDeclaration(fun_new_declaration);
        ret_new_declaration.owner = function_index;
        _name_table.addNameDeclaration(ret_new_declaration);
    }

    if (it_body != op.getBranchC().end())
    {
        _name_table.openScope(it_body->getSymbol().getLocation());

        if (_functions != nullptr)
        {
            /// \attention АСД-дерево после этого места разрушается (переаллоцируется?)...
            const AstNode & function_body_node = *it_body;
            _functions->insert({op.getSymbol().getLexeme(), &function_body_node});
        }
    }

    // Параметры

    size_t param_count = 1;
    for(const AstNode & p : it_parameters->getBranchC())
    {
        size_t type_index = _name_table.findName(p.getSymbol().getLexeme());

        if (type_index != UNDEFINED_INDEX && _name_table.name_set()[type_index].qualification != SemanticNameQualification::Type)
            type_index = UNDEFINED_INDEX;

        SemanticName param(p.getSymbol().getLocation());

        if (!p.getBranchC().empty())
            param.name = p.getBranchC().front().getSymbol();
        else
            param.name = Token(LexemeType::Id, u""s, p.getSymbol().getLocation());

        u16string param_count_string = !param.name.getLexeme().empty()
                ? (u"'" + param.name.getLexeme() + u"'")
                : convertToU16(to_string(param_count));

        param.qualification = (type_index != UNDEFINED_INDEX) ? SemanticNameQualification::Tuple : SemanticNameQualification::Scalar;
        param.type = getType(p);
        param.owner = function_index;

        if (param.type == SemanticNameType::Undefined && type_index == UNDEFINED_INDEX)
        {
            _m.reportError(p.getSymbol(),
                           u"Параметр " + param_count_string +
                           u" функции '" + op.getSymbol().getLexeme() + u"' имеет недопустимый тип");
            ok = false;
        }
        else if (param_count < declared_parameters.size())
        {
            size_t i = declared_parameters[param_count];
            const std::vector<SemanticName> & name_set = _name_table.name_set();
            const SemanticName & name = name_set[i];

            if(name.qualification != param.qualification || name.type != param.type)
            {
                _m.reportError(p.getSymbol(),
                               u"Тип параметра " + param_count_string +
                               u" функции '" + op.getSymbol().getLexeme() +
                               u"' не соответствует ранее описанному");
                ok = false;
            }
        }

        if (declared_index == NOT_FOUND_INDEX)
        {
            if (type_index != UNDEFINED_INDEX) {
                param.type_index = type_index;
                _name_table.addNameReference(type_index, p.getSymbol().getLocation());
            }

            size_t owner = _name_table.addNameDeclaration(param);

            if (type_index != UNDEFINED_INDEX) {
                // auto start = chrono::high_resolution_clock::now();

                /// @todo Полное рекурсивное копирование параметров приводит к 
                /// чудовищному торможению во время анализа сложных моделей.
                /// Нужно вводить работу со ссылками? -- ссылки тут ни при чём!
                /// Немного ускорил метод fillTupleFromType (примерно в два раза)
                fillTupleFromType(_name_table.name_set(), type_index, owner);
                // size_t count = fillTupleFromType(_name_table.name_set(), type_index, owner);

                // auto elapsed = chrono::duration_cast<chrono::milliseconds>(chrono::high_resolution_clock::now() - start);
                // cerr << count << " за " << elapsed.count() 
                //     << "ms, '" << convertToU8(param.name.getLexeme()) 
                //     << "', file: '" << convertToU8(param.name.getLocation().file_name)
                //     << "', [" << param.name.getLocation().line
                //     << ", " << param.name.getLocation().column
                //     << "]" << endl;
            }
        }

        param_count ++;
    }

    if (declared_index != NOT_FOUND_INDEX && param_count != declared_parameters.size() && ok)
    {
        _m.reportError(op.getSymbol(),
                       u"Количество параметров функции '" + op.getSymbol().getLexeme() +
                       u"' не соответствует ранее описанному");
        ok = false;
    }

    // Определение функции (тело)

    if (it_body != op.getBranchC().end())
    {
        if (!checkInner(*it_body))
            ok = false;
    }

    if (it_body != op.getBranchC().end())
    {
        _name_table.closeScope(*it_body);
    }

    return ok;
}

bool ScriptC_Semantics::checkAssignStatement(const AstNode &op, const AstNode & var_ast, size_t owner, SemanticNameAccess owner_access)
{
    // Проверки
    size_t var_index = _name_table.findName(var_ast.getSymbol().getLexeme(), owner);

    if (var_index == NOT_FOUND_INDEX && owner == UNDEFINED_INDEX)
        for(size_t i=0; i < _using_set.size(); ++i)
        {
            var_index = _name_table.findName(var_ast.getSymbol().getLexeme(), _using_set[i]);

            if (var_index != NOT_FOUND_INDEX)
            {
                owner = _using_set[i];
                const SemanticName & owner_name = _name_table.name_set()[owner];
                owner_access = strongestAccess(owner_access,owner_name.access);
                break;
            }
        }

    if (var_index == NOT_FOUND_INDEX)
    {
        if (owner == UNDEFINED_INDEX)
            _m.reportError(var_ast.getSymbol(), u"Переменная '" + var_ast.getSymbol().getLexeme() + u"' не определена");
        else
            _m.reportError(var_ast.getSymbol(),
                           u"Переменная '" + var_ast.getSymbol().getLexeme() +
                           u"' не является членом кортежа '" + _name_table.name_set()[owner].name.getLexeme() + u"'");
        return false;
    }

    bool ok = true;

    {
        const SemanticName & var = _name_table.name_set()[var_index];
        if (var.qualification == SemanticNameQualification::Type) {
            _m.reportError(op.getSymbol(), 
                        u"Недопустимое использование типа " + var_ast.getSymbol().getLexeme());
            ok = false;
        }
        if (var.owner == UNDEFINED_INDEX && var.name.getLexeme().substr(0,5) == u"__dt_") {
            size_t origin = _name_table.findName(var.name.getLexeme().substr(5));
            if (origin != NOT_FOUND_INDEX) {
                TokenLocation old_definition = _name_table.name_set()[origin].definition;
                _name_table.addNameDefinition(origin, var_ast.getSymbol().getLocation());
                _name_table.forceInputFlag(origin);
                if (!old_definition.file_name.empty())
                    _name_table.addNameReference(origin, old_definition);
            }
        }
        else {
            _name_table.assignChecked(var_index);

            if (var.definition.file_name.empty())
                _name_table.addNameDefinition(var_index, var_ast.getSymbol().getLocation());
            else
                _name_table.addNameReference(var_index, var_ast.getSymbol().getLocation());
        }
    }

    bool is_array = false; // Костыль

    if (!var_ast.getBranchC().empty())
    {
        auto it = var_ast.getBranchC().begin();

        if (var_ast.getBranchC().front().getOperation() == AstOperation::Address_Array)
        {
            is_array = true;

            // Ложное срабатывание для некоторых случаев
            // if (var_name.qualification != SemanticNameQualification::Tuple)
            // {
            //     _m.reportError(var_ast.getSymbol(),
            //                    u"Переменная '" + var_ast.getSymbol().getLexeme() + 
            //                    u"' не является кортежем (" + getSemanticNameQualificationName(var_name.qualification)+ u")");
            //     ok = false;
            // }

            const AstNode & arr_op = var_ast.getBranchC().front();

            if (ok)
            {
                size_t last_stack_count = _type_stack.size();

                if (!checkInner(arr_op))
                    ok = false;

                const SemanticName & var = _name_table.name_set()[var_index];
                if (var.dimensions != 0 && var.dimensions != _type_stack.size() - last_stack_count)
                {
                    _m.reportError(op.getSymbol(),
                                   u"Размерность массива '" + op.getSymbol().getLexeme() + u"' не соответствует заданному индексу");
                    ok = false;
                }

                while(_type_stack.size() > last_stack_count)
                    _type_stack.pop_back();
            }

            ++it;
        }

        if (var_ast.getBranchC().front().getOperation() == AstOperation::Address_Tuple)
        {
            ++it;
            if (it->getOperation() != AstOperation::Push_Id)
            {
                _m.reportError(op.getSymbol(),
                               u"Операция '"+op.getSymbol().getLexeme()+u"' для имени " + it->getSymbol().getLexeme() + u" не поддерживается");
                return false;
            }
            return checkAssignStatement(op, *it, var_index, strongestAccess(owner_access,_name_table.name_set()[var_index].access));
        }
    }

    SemanticNameAccess access_level = strongestAccess(owner_access,_name_table.name_set()[var_index].access);

    if (access_level != SemanticNameAccess::FullAccess)
    {
        _m.reportError(op.getSymbol(),
                       u"Присваивание элементу кортежа со спецификатором доступа " + getSemanticNameAccessName(access_level));
        ok = false;
    }

    // Вычисляем правую часть
    if (!checkInner(op.getBranchC().back()))
        ok = false;

    // assert(!_type_stack.empty());
    if (_type_stack.empty()) {
        throw Exception("ScriptC_Semantics::checkAssignStatement", "Нарушение инварианта !_type_stack.empty()");
    }

    // Присваиваем
    if (var_index != NOT_FOUND_INDEX)
    {
        // если корневой элемент кортеж и он же является аргументом функции, значит он передан по ссылке, т.е. используется
        size_t root_owner_index = var_index;

        while(_name_table.name_set()[root_owner_index].owner != UNDEFINED_INDEX)
        {
            size_t i = _name_table.name_set()[root_owner_index].owner;

            if (_name_table.name_set()[i].qualification == SemanticNameQualification::Function)
                break;

            root_owner_index = i;
        }

        if (root_owner_index != UNDEFINED_INDEX && _name_table.name_set()[root_owner_index].qualification == SemanticNameQualification::Tuple)
            _name_table.addNameReference(root_owner_index, op.getSymbol().getLocation());

        // присваиваем

        const SemanticName & var = _name_table.name_set()[var_index];
        switch(op.getOperation())
        {
        case AstOperation::Statement_Assign:
            if (_type_stack.back() != var.type)
            {
                if (is_array)
                {
                }
                else if (_type_stack.back() == SemanticNameType::Int && var.type == SemanticNameType::Float)
                {
                }
                else if (_type_stack.back() != SemanticNameType::Undefined && var.type != SemanticNameType::Undefined)
                {
                    _m.reportError(op.getSymbol(), u"Тип выражения в операторе присваивания (" +
                                                       getSemanticNameTypeName(_type_stack.back()) + 
                                                       u") не соответствует типу переменной (" +
                                                       getSemanticNameTypeName(var.type) + u")");
                    ok = false;
                }
            }
            break;
        case AstOperation::Statement_Assign_Addition:
        case AstOperation::Statement_Assign_Subtraction:
        case AstOperation::Statement_Assign_Multiplication:
        case AstOperation::Statement_Assign_Division:
        case AstOperation::Statement_Assign_Modulo:
        {
            // Проверки типов
            if (!checkArithmeticOperation(op,var.type,_type_stack.back()))
                ok = false;

            SemanticNameType res = calculateArithmeticOperationType(op, var.type, _type_stack.back());
            if (res != var.type)
            {
                _m.reportError(op.getSymbol(), u"Тип выражения в операторе комбинированного присваивания не соответствует типу переменной");
                ok = false;
            }
            break;
        }
        default:
            assert(false);
            break;
        }
    }

    _type_stack.pop_back();

    return ok;
}

bool ScriptC_Semantics::checkBreakContinue(const AstNode &op)
{
    const AstNode * parent = op.getParentC();

    while(parent != nullptr)
    {
        if (parent->getOperation() == AstOperation::Statement_For ||
                parent->getOperation() == AstOperation::Statement_While ||
                parent->getOperation() == AstOperation::Statement_DoWhile)
            break;
        parent = parent -> getParentC();
    }

    if (parent == nullptr)
    {
        _m.reportError(op.getSymbol(),
                       u"Оператор " + op.getSymbol().getLexeme() + u" не находится внутри тела цикла");
        return false;
    }

    return true;
}

bool ScriptC_Semantics::checkReturn(const AstNode &op)
{
    const AstNode * parent = op.getParentC();

    while(parent != nullptr)
    {
        if (parent->getOperation() == AstOperation::Function_Body)
            break;
        parent = parent -> getParentC();
    }

    if (parent == nullptr)
    {
        _m.reportError(op.getSymbol(),
                       u"Оператор " + op.getSymbol().getLexeme() + u" не находится внутри тела функции");
        return false;
    }

    assert(parent->getParentC() != nullptr);

    const AstNode & func_decl_node = *(parent->getParentC());

    auto it = func_decl_node.getBranchC().begin();

    assert(it != func_decl_node.getBranchC().end());

    SemanticNameType func_decl_ret_val_type = getType(*it);

    if (func_decl_ret_val_type == SemanticNameType::Undefined && !op.getBranchC().empty())
    {
        _m.reportError(op.getBranchC().front().getSymbol(),
                       u"Функция '" + func_decl_node.getSymbol().getLexeme() + u"' не должна возвращать значений");
        return false;
    }

    if (func_decl_ret_val_type != SemanticNameType::Undefined && op.getBranchC().empty())
    {
        _m.reportError(op.getSymbol(),
                       u"Функция '" + func_decl_node.getSymbol().getLexeme() + u"' должна возвращать значение");
        return false;
    }

    if (!op.getBranchC().empty())
    {
        if (!checkInner(op))
            return false;

        assert(!_type_stack.empty());

        if (!checkTypeConversion(_type_stack.back(),func_decl_ret_val_type))
        {
            _m.reportError(op.getBranchC().front().getSymbol(),
                           u"Некорректный тип возвращаемого значения функции '" + func_decl_node.getSymbol().getLexeme() + u"'");
            return false;
        }
    }

    return true;
}

bool ScriptC_Semantics::checkCycleWhile(const AstNode &op)
{
    assert(op.getBranchC().size() == 2);

    bool state = checkInner(op.getBranchC().front());

    assert(!_type_stack.empty());

    if (_type_stack.back() != SemanticNameType::Bool)
    {
        _m.reportError(op.getSymbol(), u"Условие цикла while должно иметь логический тип");
        state = false;
    }

    _type_stack.pop_back();

    if (!checkInner(op.getBranchC().back()))
        state = false;

    return state;
}

bool ScriptC_Semantics::checkCycleDoWhile(const AstNode &op)
{
    assert(op.getBranchC().size() == 2);

    bool state = checkInner(op.getBranchC().front());

    if (!checkInner(op.getBranchC().back()))
        state = false;

    assert(!_type_stack.empty());

    if (_type_stack.back() != SemanticNameType::Bool)
    {
        _m.reportError(op.getBranchC().back().getSymbol(), u"Условие цикла do-while должно иметь логический тип");
        state = false;
    }

    _type_stack.pop_back();

    return state;
}

bool ScriptC_Semantics::checkCycleFor(const AstNode &op)
{
    assert(!op.getBranchC().empty());

    bool state = true;

    _name_table.openScope(op.getSymbol().getLocation());

    auto it = op.getBranchC().begin();

    if (!it->getBranchC().empty())
    {
        // Перый фрагмент цикла for - определение итератора
        auto it_declaration = it;

        if (!checkInner(*it_declaration))
            state = false;
    }

    it++;
    assert(it != op.getBranchC().end());

    // Второй фрагмент цикла for - условие
    auto it_condition = it;

    it++;
    assert(it != op.getBranchC().end());

    // Третий фрагмент цикла for - итерация
    auto it_iteration = it;

    it++;
    assert(it != op.getBranchC().end());

    // Четвёртый фрагмент цикла for - тело
    auto it_body = it;

    // Теперь всё вместе!
    if (!it_condition->getBranchC().empty())
    {
        if (!checkInner(*it_condition))
            state = false;

        assert(!_type_stack.empty());

        if (_type_stack.back() != SemanticNameType::Bool)
        {
            _m.reportError(it_condition->getSymbol(),u"Условие цикла for должно иметь логический тип");
            state = false;
        }

        _type_stack.pop_back();
    }

    if (!it_iteration->getBranchC().empty())
        if (!checkInner(*it_iteration))
            state = false;

    if (!it_body->getBranchC().empty())
        if (!checkInner(*it_body))
            state = false;

    _name_table.closeScope(*it_body);

    return state;
}

bool ScriptC_Semantics::checkDataStructure(const AstNode &op)
{
    if (op.getOperation() == AstOperation::DataStructure_Array)
    {
        checkInner(op);
        // Определяем тип массива по первому элементу (возможна вложенность для многомерного массива)
        const std::list<AstNode> * p_array = & op.getBranchC();
        while(!p_array->empty() && p_array->front().getOperation() == AstOperation::DataStructure_Array)
            p_array = & p_array->front().getBranchC(); 

        SemanticNameType type = (p_array->empty()) ? SemanticNameType::Undefined : getType(p_array->front());

        _type_stack.push_back(type);
    }
    else {
        set<u16string> names;
        set<size_t>    indexes;

        size_t owner_index = UNDEFINED_INDEX;

        if (!_owner_stack.empty()) {
            assert(_owner_stack.back() < _name_table.name_set().size());
            owner_index = _owner_stack.back();
        }

        for(const AstNode & o : op.getBranchC())
            if (!o.getBranchC().empty()) {
                if (names.find(o.getSymbol().getLexeme()) != names.end()) {
                    _m.reportError(o.getSymbol(), u"Недопустимо повторение имени '" + o.getSymbol().getLexeme() + u"' в именованной структуре");
                    // return false;
                }

                names.insert(o.getSymbol().getLexeme());

                size_t  last_stack_size = _type_stack.size();
                bool    ok              = checkInner(o);

                if (owner_index < _name_table.name_set().size()) {
                    const SemanticName & owner     = _name_table.name_set()[owner_index];
                    size_t               var_index = _name_table.findName(o.getSymbol().getLexeme(), owner_index);

                    if (var_index < _name_table.name_set().size()) {
                        _name_table.addNameReference(var_index, o.getSymbol().getLocation());
                        indexes.insert(var_index);
                    }
                    else if (owner.qualification == SemanticNameQualification::None) {
                        SemanticName var(o.getSymbol().getLocation(), o.getSymbol().getLexeme(), 
                                         SemanticNameQualification::Scalar, _type_stack.back());

                        var.owner = owner_index;
                        size_t member_index = _name_table.addNameDeclaration(var);
                        indexes.insert(member_index);
                    }
                }

                while(_type_stack.size() > last_stack_size)
                    _type_stack.pop_back();

                if (!ok)
                    return false;
            }

        if (owner_index < _name_table.name_set().size()) 
            for(size_t i=owner_index+1; i < _name_table.name_set().size(); ++i) {
                const SemanticName & member = _name_table.name_set()[i];
                if (member.owner == owner_index
                 && member.qualification == SemanticNameQualification::Scalar
                 && indexes.find(i) == indexes.end()) {
                    if (member.name.getLexeme().substr(0,5) != u"__dt_"
                     && (member.semantic_flags & SemanticFlag_Input) == SemanticFlag_Input)
                        _m.reportWarning(_name_table.name_set()[owner_index].name, 
                                        u"Входной параметр '" + member.name.getLexeme() + 
                                        u"' структуры '" + _name_table.name_set()[owner_index].name.getLexeme() + 
                                        u"' не указан при инициализации");
                }
            }

        _type_stack.push_back(SemanticNameType::Undefined);
    }

    return true;
}

SemanticNameType ScriptC_Semantics::getType(const AstNode &op) const
{
    const u16string & lexeme = op.getSymbol().getLexeme();

    if (lexeme == u"int")
        return SemanticNameType::Int;

    if (lexeme == u"float")
        return SemanticNameType::Float;

    if (lexeme == u"bool")
        return SemanticNameType::Bool;

    if (lexeme == u"string")
        return SemanticNameType::String;

    return SemanticNameType::Undefined;
}

bool ScriptC_Semantics::checkArithmeticOperation(AstNode op, SemanticNameType t1, SemanticNameType t2) const
{
    if (t1 == SemanticNameType::Undefined || t2 == SemanticNameType::Undefined)
        return true; //????????????????????

    if (t1 == SemanticNameType::String || t2 == SemanticNameType::String)
    {
        if (op.getOperation() != AstOperation::Arithmetic_Addition && op.getOperation() != AstOperation::Statement_Assign_Addition)
        {
            _m.reportError(op.getSymbol(),u"Недопустимая операция со строками");
            return false;
        }
        if (t1 == SemanticNameType::Undefined ||
            t1 == SemanticNameType::ExtFunction ||
            t1 == SemanticNameType::IntFunction ||
            t2 == SemanticNameType::Undefined ||
            t2 == SemanticNameType::ExtFunction ||
            t2 == SemanticNameType::IntFunction)
        {
            _m.reportError(op.getSymbol(),u"Недопустимая комбинация типов операндов арифметической операции");
            return false;
        }
    }
    else if (t1 == SemanticNameType::Undefined ||
             t1 == SemanticNameType::Bool ||
             t1 == SemanticNameType::ExtFunction ||
             t1 == SemanticNameType::IntFunction ||
             t2 == SemanticNameType::Undefined ||
             t2 == SemanticNameType::Bool ||
             t2 == SemanticNameType::ExtFunction ||
             t2 == SemanticNameType::IntFunction)
    {
        _m.reportError(op.getSymbol(),u"Недопустимая комбинация типов операндов арифметической операции");
        return false;
    }

    if ((t1 == SemanticNameType::Float || t2 == SemanticNameType::Float) &&
        (op.getOperation() == AstOperation::Arithmetic_Modulo || op.getOperation() == AstOperation::Statement_Assign_Modulo))
    {
        _m.reportError(op.getSymbol(),u"Недопустимая операция");
        return false;
    }

    return true;
}

SemanticNameType ScriptC_Semantics::calculateArithmeticOperationType(AstNode op, SemanticNameType t1, SemanticNameType t2) const
{
    if (t1 == SemanticNameType::Undefined || t2 == SemanticNameType::Undefined)
        return SemanticNameType::Undefined;

    SemanticNameType res;

    switch(op.getOperation())
    {
    case AstOperation::Arithmetic_Addition:
    case AstOperation::Statement_Assign_Addition:
        if (t1 == SemanticNameType::String ||
            t2 == SemanticNameType::String)
        {
            res = SemanticNameType::String;
        }
        else if (t1 == SemanticNameType::Float ||
                 t2 == SemanticNameType::Float)
        {
            res = SemanticNameType::Float;
        }
        else
        {
            res = SemanticNameType::Int;
        }
        break;
    case AstOperation::Arithmetic_Subtraction:
    case AstOperation::Statement_Assign_Subtraction:
        if (t1 == SemanticNameType::Float ||
            t2 == SemanticNameType::Float)
        {
            res = SemanticNameType::Float;
        }
        else
        {
            res = SemanticNameType::Int;
        }
        break;
    case AstOperation::Arithmetic_Multiplication:
    case AstOperation::Statement_Assign_Multiplication:
        if (t1 == SemanticNameType::Float ||
            t2 == SemanticNameType::Float)
        {
            res = SemanticNameType::Float;
        }
        else
        {
            res = SemanticNameType::Int;
        }
        break;
    case AstOperation::Arithmetic_Division:
    case AstOperation::Statement_Assign_Division:
        if (t1 == SemanticNameType::Float ||
            t2 == SemanticNameType::Float)
        {
            res = SemanticNameType::Float;
        }
        else
        {
            res = SemanticNameType::Int;
        }
        break;
    case AstOperation::Arithmetic_Modulo:
    case AstOperation::Statement_Assign_Modulo:
        {
            res = SemanticNameType::Int;
        }
        break;
    case AstOperation::Arithmetic_Power:
        {
            res = SemanticNameType::Float;
        }
        break;
    default: // default: нужен на случай расширения перечисления, чтобы видеть ошибку
        assert(true);
        res = SemanticNameType::Undefined;
        break;
    }

    return res;
}

void ScriptC_Semantics::fillTupleFromType(const std::vector<SemanticName> &name_set, size_t from_owner, size_t to_owner)
{
    assert(from_owner < name_set.size());
    assert(to_owner < _name_table.name_set().size());

    size_t limit = name_set.size();

    for(size_t i=from_owner+1; i < limit; ++i) {
        const SemanticName & n = name_set[i];

        if (n.owner == from_owner) {
            SemanticName v = n;
            v.owner = to_owner;
            size_t new_owner = _name_table.addNameDeclaration(v);

            if (v.qualification == SemanticNameQualification::Type
             || v.qualification == SemanticNameQualification::Tuple
             || v.qualification == SemanticNameQualification::Function)
                fillTupleFromType(name_set, i, new_owner);
        }
    }
}

bool ScriptC_Semantics::checkTypeConversion(SemanticNameType from, SemanticNameType to) const
{
    switch(to)
    {
    case SemanticNameType::Undefined:
        return false;
    case SemanticNameType::String:
        return true;
    case SemanticNameType::Float:
        if (from == SemanticNameType::Int)
            return true;
        [[fallthrough]];
    default:
        return (from == to);
    }

    return false;
}
