#include "simodo/lsp/LanguageClient.h"
#include "simodo/inout/log/Logger.h"
#include "simodo/inout/token/FileStream.h"
#include "simodo/inout/token/LexicalParameters.h"
#include "simodo/inout/token/Tokenizer.h"
#include "simodo/inout/convert/functions.h"

#include <sstream>

using namespace simodo;
using namespace simodo::inout;

void performOpenCommand(lsp::LanguageClient & client, std::vector<token::Token> command) 
{
    // format: o uri [grammar]

    if (command.size() < 2) {
        std::cout << "Wrong structure of Open command" << std::endl;
        return;
    }

    SIMODO_INOUT_CONVERT_STD_IFSTREAM(in, convert::toU8(command[1].lexeme));
    if (!in) {
        std::cout << "Unable to open file '" << convert::toU8(command[1].lexeme) << "'" << std::endl;
        return;
    }

    token::FileStream in_stream(in);
    std::u16string    string_buffer;

    for(;;) {
        char16_t ch = in_stream.get(); 
        if (ch == std::char_traits<char16_t>::eof())
            break; 
        string_buffer += ch;
    }

    client.exec("textDocument/didOpen", variable::Record {{
                    {u"textDocument", variable::Record {{
                        {u"uri",        command[1].lexeme},
                        {u"languageId", command.size() > 2 ? command[2].lexeme : u"scriptc0"},
                        {u"version",    0},
                        {u"text",       convert::encodeSpecialChars(string_buffer)},
                    }}},
                }});
}

void performCloseCommand(lsp::LanguageClient & client, std::vector<token::Token> command) 
{
    // format: c uri

    if (command.size() != 2) {
        std::cout << "Wrong structure of Close command" << std::endl;
        return;
    }

    client.exec("textDocument/didClose", variable::Record {{
                    {u"textDocument", variable::Record {{
                        {u"uri",        command[1].lexeme},
                    }}},
                }});
}

void performChangeCommand(lsp::LanguageClient & client, std::vector<token::Token> command) 
{
    // format: u uri version text

    if (command.size() != 4) {
        std::cout << "Wrong structure of Change command" << std::endl;
        // for(const token::Token & t : command)
        //     std::cout << "'" << convert::toU8(t.lexeme) << "': " << convert::toU8(token::getLexemeTypeName(t.type)) << std::endl;
        return;
    }

    client.exec("textDocument/didChange", variable::Record {{
                    {u"textDocument", variable::Record {{
                        {u"uri",        command[1].lexeme},
                        {u"version",    int64_t(std::stol(convert::toU8(command[2].lexeme)))},
                    }}},
                    {u"contentChanges", variable::ValueArray {{
                        variable::Record {{{u"text", command[3].lexeme}}},
                    }}},
                }});
}

void performHoverCommand(lsp::LanguageClient & client, std::vector<token::Token> command) 
{
    // format: h uri line character

    if (command.size() < 4) {
        std::cout << "Wrong structure of Hover command" << std::endl;
        return;
    }

    client.exec("textDocument/hover", 
                variable::Record {{
                    {u"textDocument", variable::Record {{
                        {u"uri", command[1].lexeme},
                    }}},
                    {u"position", variable::Record {{
                        {u"line", int64_t(std::stol(convert::toU8(command[2].lexeme)))},
                        {u"character", int64_t(std::stol(convert::toU8(command[3].lexeme)))},
                    }}},
                }}, 
                [](variable::json::Rpc rpc){
                    std::cout << "= textDocument/hover => " << variable::json::toString(rpc.value()) << std::endl;
                });
}

void performDocSymbolsCommand(lsp::LanguageClient & client, std::vector<token::Token> command) 
{
    // format: s uri

    if (command.size() != 2) {
        std::cout << "Wrong structure of 'documentSymbol' command" << std::endl;
        return;
    }

    client.exec("textDocument/documentSymbol", 
                variable::Record {{
                    {u"textDocument", variable::Record {{
                        {u"uri", command[1].lexeme},
                    }}},
                }}, 
                [](variable::json::Rpc rpc){
                    std::cout << "= textDocument/documentSymbol => " << variable::json::toString(rpc.value()) << std::endl;
                });
}

void performSemTokensCommand(lsp::LanguageClient & client, std::vector<token::Token> command) 
{
    // format: t uri

    if (command.size() != 2) {
        std::cout << "Wrong structure of 'semanticTokens' command" << std::endl;
        return;
    }

    client.exec("textDocument/semanticTokens/full", 
                variable::Record {{
                    {u"textDocument", variable::Record {{
                        {u"uri", command[1].lexeme},
                    }}},
                }}, 
                [](variable::json::Rpc rpc){
                    std::cout << "= textDocument/semanticTokens => " << variable::json::toString(rpc.value()) << std::endl;
                });
}

void performWaitCommand(lsp::LanguageClient & , std::vector<token::Token> command) 
{
    // format: w mills

    if (command.size() < 2 || command[1].type != token::LexemeType::Number) {
        std::cout << "Wrong structure of Wait command" << std::endl;
        return;
    }

    int wait_mills = std::stol(convert::toU8(command[1].lexeme));
    std::this_thread::sleep_for(std::chrono::milliseconds(wait_mills));
}

int main(int argc, char * argv[]) 
{
    if (argc < 2) {
        std::cout << "Usage: language-client <path to language server> {<language server args...>}" << std::endl;
        return 1;
    }

    log::Logger log(std::cout, "LSP-client", log::SeverityLevel::Warning);

    std::vector<std::string> args(argv+2,argv+argc);

    lsp::LanguageClient client(argv[1], args, {}, nullptr, log);

    if (!client.running())
        return 1;

    client.registerListener("textDocument/publishDiagnostics", [](variable::json::Rpc rpc){
        std::cout << "= textDocument/publishDiagnostics => " << variable::json::toString(rpc.value()) << std::endl;
    });

    std::string line;
    while(std::getline(std::cin,line) && !line.empty()) {
        std::vector<token::Token>   command;
        std::istringstream          str_in(line);
        token::FileStream           in(str_in);
        token::LexicalParameters    lex;
        lex.masks.push_back({ token::BUILDING_NUMBER, token::LexemeType::Number, 10 });
        
        token::Tokenizer            tokenizer(u"", in, lex, 4);
        token::Token                t = tokenizer.getToken();

        while(t.type != token::LexemeType::Empty) {
            command.push_back(t);
            t = tokenizer.getToken();
        }

        if (command.empty() || command[0].type != token::LexemeType::Id) {
            std::cout << "Wrong command" << std::endl;
            continue;
        }

        if (command[0].lexeme == u"o")
            performOpenCommand(client, command);
        else if (command[0].lexeme == u"c")
            performCloseCommand(client, command);
        else if (command[0].lexeme == u"u")
            performChangeCommand(client, command);
        else if (command[0].lexeme == u"h")
            performHoverCommand(client, command);
        else if (command[0].lexeme == u"w")
            performWaitCommand(client, command);
        else if (command[0].lexeme == u"s")
            performDocSymbolsCommand(client, command);
        else if (command[0].lexeme == u"t")
            performSemTokensCommand(client, command);
        else {
            std::cout << "Command don't recognized" << std::endl;
            continue;
        }
    }

    return client.ok() ? 0 : 1;
}
