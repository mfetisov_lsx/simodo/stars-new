cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

project(simfuze)

add_executable(${PROJECT_NAME} simfuze.cpp)

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS YES
)
target_link_libraries(${PROJECT_NAME} SIMODOdsl)
# target_link_libraries(${PROJECT_NAME} "stdc++fs")

set_target_properties(${PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
