/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <fstream>
#include <iostream>
#include <memory>
#include <algorithm>
#include <chrono>

#if (__GNUC__ > 7) || defined(__clang__)
#include <filesystem>
#else
#include <experimental/filesystem>
using namespace std::experimental;
#endif

#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/GrammarBuilder.h"
#include "simodo/dsl/Parser.h"

#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_Semantics.h"
#include "simodo/dsl/ModulesManagement.h"

#include "simodo/convert.h"
#include "simodo/version.h"

/*! \file Утилита тестирования средств начальной загрузки и интерпретации стартовых (FUZE) грамматик библиотеки SIMODOdsl. Проект SIMODO Stars
*/

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    class COutListener: public AReporter
    {
    public:
        COutListener() : _max_severity(SeverityLevel::Information) {}
        virtual ~COutListener() override;

        virtual void report(const SeverityLevel level,
                            TokenLocation ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge) override;

    private:
        SeverityLevel _max_severity;
    };

    COutListener::~COutListener() {}

    void COutListener::report(const SeverityLevel level,
                              TokenLocation ,
                              const std::u16string & briefly,
                              const std::u16string & atlarge)
    {
        cout << convertToU8(getSeverityLevelName(level) + briefly) << endl;
        if (!atlarge.empty())
            cout << convertToU8(atlarge) << endl;
        _max_severity = max(_max_severity,level);
    }

    string getMnemonic(const Lexeme & lex, bool abstract_lexeme=true)
    {
        string mnemonic;

        switch(lex.getType())
        {
        case LexemeType::Empty:
            mnemonic = "$";
            break;
        case LexemeType::Compound:
            mnemonic = convertToU8(lex.getLexeme());
//            mnemonic = "\\" + convertToU8(lex.getLexeme());
            break;
        case LexemeType::Punctuation:
            mnemonic = "'" + convertToU8(lex.getLexeme()) + "'";
            break;
        case LexemeType::Id:
            mnemonic = abstract_lexeme ? "ID" : convertToU8(lex.getLexeme());
            break;
        case LexemeType::Annotation:
            mnemonic = abstract_lexeme ? "ANNOTATION" : ("\"" + convertToU8(lex.getLexeme()) + "\"");
            break;
        case LexemeType::Number:
            mnemonic = abstract_lexeme ? "NUMBER" : convertToU8(lex.getLexeme());
            break;
        case LexemeType::Comment:
            mnemonic = "COMMENT";
            break;
        case LexemeType::Error:
            mnemonic = "ERROR";
            break;
        default:
            mnemonic = "***";
            break;
        }

        return mnemonic;
    }

    void    printAst(const AstNode & ast, int level=0, int shift=0)
    {
        for(const AstNode & n : ast.getBranchC())
        {
            for(int i=0; i < shift; ++i)
                cout << "\t";

            for(int i=0; i < level; ++i)
                cout << "→\t";

            cout << convertToU8(n.getSymbol().getLexeme()) << " ("
                 << convertToU8(getAstOperationName(n.getOperation())) << ") "
                 << convertToU8(getTokenLocationString(n.getBound(),true)) << endl;

            if (!n.getBranchC().empty())
                printAst(n,level+1,shift);
        }
    }

    void replaceAll(std::string& str, const std::string& from, const std::string& to)
    {
        if(from.empty())
            return;

        size_t start_pos = 0;
        while((start_pos = str.find(from, start_pos)) != std::string::npos)
        {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
        }
    }

    string toHtml(string text)
    {
        replaceAll(text, "&", "&amp;");
        replaceAll(text, "<", "&lt;");
        replaceAll(text, ">", "&gt;");

        return text;
    }

    int parse(const string & path,
              const string & grammar_name,
              const string & file_name,
              TableBuildMethod grammar_builder_method,
              bool need_rebuild_grammar,
              bool need_info,
              bool need_rules,
              const string & dot_file,
              bool need_syntax_only,
              bool need_ast_info,
              bool need_semantics,
              bool need_name_table,
              bool need_scope_struct,
              bool need_execute,
              bool need_time_intervals)
    {
        COutListener  m;
        Grammar       g;
        std::vector<std::string> paths {path};

        GrammarLoader loader(m, paths, grammar_name);

        bool ok = loader.load(need_rebuild_grammar, g, grammar_builder_method);

        if (need_info || need_rules)
        {
            if (!g.handlers.empty())
            {
                if (need_ast_info)
                {
                    cout << "Обработчики:" << endl;
                    for(const auto & [handler_name,handler_ast] : g.handlers)
                    {
                        cout << 't' << convertToU8(handler_name) << ":" << endl;
                        printAst(handler_ast, 0, 2);
                    }
                }
                else
                    cout << "Обработчики присутствуют." << endl;
            }

            if (need_rules)
            {
                cout << "Правила грамматики:" << endl;
                for(size_t i=0; i < g.rules.size(); ++i)
                {
                    char action = (g.rules[i].reduce_action.getBranchC().empty()) ? ' ' : '*';
                    char direction = (g.rules[i].reduce_direction == RuleReduceDirection::Undefined)
                            ? ' ' : ((g.rules[i].reduce_direction == RuleReduceDirection::LeftAssociative)
                                     ? '<' : '>');
                    cout << i << ": " << action << direction << "\t" << convertToU8(g.rules[i].production) << "\t→ ";

                    for(const Lexeme & lex : g.rules[i].pattern)
                        cout << getMnemonic(lex) << " ";

                    cout << endl;

                    if (need_ast_info && !g.rules[i].reduce_action.getBranchC().empty())
                        printAst(g.rules[i].reduce_action, 0, 2);
                }
            }

            if (need_info)
            {
                cout << "Состояния автомата разбора:" << endl;
                for(size_t i=0; i < g.states.size(); ++i)
                {
                    cout << "State [" << i << "]:" << endl;

                    const FsmState_t & state = g.states[i];

                    for(const FsmStatePosition & p : state)
                    {
                        cout << (p.is_main ? "    M:" : "     ") << "\t"
                             << convertToU8(g.rules[p.rule_no].production) << "\t→ ";

                        for(size_t j=0; j < g.rules[p.rule_no].pattern.size(); ++j)
                        {
                            if (p.position == j)
                                cout << "•";
                            cout << getMnemonic(g.rules[p.rule_no].pattern[j]) << " ";
                        }
                        if (p.position == g.rules[p.rule_no].pattern.size())
                            cout << "•";

                        if (!p.lookahead.empty())
                        {
                            cout << "\t|";

                            for(const Lexeme & lex : p.lookahead)
                                cout << " " << getMnemonic(lex);
                        }

                        cout << "\t\t---- " << ((p.next_state_no > 0) ? "S" : "R") << ((p.next_state_no > 0) ? p.next_state_no : p.rule_no) << endl;
                    }
                }

                cout << "Символы грамматики:" << endl;
                for(size_t i=0; i < g.columns.size(); ++i)
                    cout << i << ": " << getMnemonic(g.columns[i]) << endl;

                cout << "Таблица разбора:";
                size_t cl         = 100000;
                size_t table_size = 0;
                for(auto [fsm_key,fsm_value] : g.parse_table)
                {
                    size_t        line     = g.unpackFsmState(fsm_key);
                    size_t        column   = g.unpackFsmColumn(fsm_key);
                    FsmActionType action   = g.unpackFsmAction(fsm_value);
                    size_t        location = g.unpackFsmLocation(fsm_value);

                    if (cl != line)
                        cout << endl << "[" << line << "]:\t";

                    cout << getMnemonic(g.columns[column]) << "(" << convertToU8(getFsmActionChar(action)) << location << ") ";

                    cl = line;
                    table_size ++;
                }
                cout << endl << "Количество элементов = " << table_size << endl;
            }
        }

        if (!dot_file.empty())
        {
            ofstream dot(dot_file);

            if(dot.good())
            {
                dot << "digraph \"" << grammar_name << "\" {" << endl;
                for(size_t i=0; i < g.states.size(); ++i)
                {
                    dot << "\tS" << i << " [shape=none,margin=0,fontsize=12,fontname=Helvetica,labelfloat=false,labelloc=t,labeljust=l,label=<" << endl
                        << "\t\t<TABLE BORDER=\"1\" CELLBORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"2\">" << endl
                        << "\t\t<TR><TD COLSPAN=\"2\"><b>" << i << "</b></TD></TR>" << endl;

                    const FsmState_t & state = g.states[i];

                    for(const FsmStatePosition & p : state)
                    {

                        dot << "\t\t\t<TR><TD align='right'>" << (p.is_main ? "* " : "") << toHtml(convertToU8(g.rules[p.rule_no].production))
                            << " →</TD><TD align='left'>";

                        for(size_t j=0; j < g.rules[p.rule_no].pattern.size(); ++j)
                        {
                            if (p.position == j)
                                dot << "•";
                            dot << toHtml(getMnemonic(g.rules[p.rule_no].pattern[j])) << " ";
                        }
                        if (p.position == g.rules[p.rule_no].pattern.size())
                            dot << "•";

                        if (!p.lookahead.empty())
                        {
                            dot << " |";

                            for(const Lexeme & lex : p.lookahead)
                                dot << " " << toHtml(getMnemonic(lex));
                        }

                        dot << "</TD></TR>" << endl;
                    }
                    dot << "\t\t</TABLE>>];" << endl;
                }
                for(size_t i=0; i < g.states.size(); ++i)
                {
                    const FsmState_t & state = g.states[i];

                    for(size_t ip=0; ip < state.size(); ++ip)
                    {
                        const FsmStatePosition & p = state[ip];

                        if (p.next_state_no > 0)
                        {
                            size_t j = 0;
                            for(; j < ip; ++j)
                                if (state[j].position < g.rules[state[j].rule_no].pattern.size() &&
                                        p.position < g.rules[p.rule_no].pattern.size() &&
                                        g.rules[state[j].rule_no].pattern[state[j].position] == g.rules[p.rule_no].pattern[p.position])
                                    break;

                            if (j < ip)
                                continue;

                            string label;
                            if (p.position < g.rules[p.rule_no].pattern.size())
                                label = toHtml(getMnemonic(g.rules[p.rule_no].pattern[p.position]));
                            dot <<  "\t\tS" << i << " -> " << "S" << p.next_state_no
                                 << " [fontsize=12,fontname=Helvetica,label=<" << label << ">];" << endl;
                        }
                    }
                }
                dot << "}" << endl;

                if (!dot.good())
                    cout << "Не удалось записать в файл '" << dot_file << "'" << endl;
            }
            else
                cout << "Не удалось записать в файл '" << dot_file << "'" << endl;
        }

        if (ok)
            cout << "Грамматика '" << grammar_name << "' собрана (метод " << convertToU8(getGrammarBuilderMethodName(g.build_method)) << ")" << endl;
        else
        {
            cout << "При построении грамматики '" << grammar_name << "' возникли ошибки" << endl;
            return 2;
        }

        if (!file_name.empty())
        {
            simodo::dsl::Parser            p(file_name,m,g);
            unique_ptr<ISyntaxTreeBuilder> ast_builder;
            AstNode                        ast(convertToU16(file_name));

            if (need_syntax_only)
                ast_builder = make_unique<DummyTreeBuilder>();
            else
                ast_builder = make_unique<AstBuilder>(m,ast,g.handlers);

            cout << "Выполняется синтаксический анализ файла '" << file_name << "'..." << endl;

            auto start_of_parsing = chrono::high_resolution_clock::now();

            ok = p.parse(*ast_builder);

            if (ok)
            {
                if (need_time_intervals)
                {
                    auto elapsed = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start_of_parsing);

                    cout << "Разбор выполнен за " << elapsed.count() << " микросекунд" << endl;
                }
                else
                    cout << "Синтаксических ошибок не обнаружено" << endl;
            }
            else
                cout << "Синтаксический анализ выявил ошибки" << endl;

            if (!need_syntax_only && need_ast_info)
            {
                cout << "Абстрактное синтаксическое дерево:" << endl;
                printAst(ast);
            }

            if (!ok)
                return 2;

            ModulesManagement mm(m, m, nullptr, path);

            if (need_semantics)
            {
                cout << "Выполняется семантический анализ '" << file_name << "'..." << endl;

                std::vector<SemanticName>   name_set;
                std::vector<SemanticScope>  scope_set;

                ScriptC_Semantics checker(m, mm, name_set, scope_set);

                ScriptC_NS_Sys   sys(m);
                ScriptC_NS_Math  math;
                ScriptC_NS_Scene scene;

                checker.importNamespace(u"sys", sys.getNamespace());
                checker.importNamespace(u"math", math.getNamespace());
                checker.importNamespace(u"scene", scene.getNamespace());

                bool res = checker.check(ast);

                if (res)
                {
                    cout << "Анализ выполнен успешно" << endl;
                }
                else
                {
                    cout << "Анализ выполнен с ошибками" << endl;
                    return 2;
                }

                if (need_name_table)
                {
                    cout << "Таблица имён:" << endl;
                    for(size_t i=0; i < name_set.size(); ++i)
                    {
                        const SemanticName & n = name_set[i];
                        cout << i
                             << ": '" << convertToU8(n.name.getLexeme())
                             << "'\t-> " << convertToU8(getSemanticNameTypeName(n.type))
                             << ", qua: " << convertToU8(getSemanticNameQualificationName(n.qualification))
                             << ", " << n.name.getLocation().line
                             << "/" << n.lower_scope.line
                             << " [" << n.name.getLocation().begin
                             << ", " << n.lower_scope.end
                             << "], owner " << ((n.owner == UNDEFINED_INDEX) ? "none" : to_string(n.owner))
                             << ", depth " << n.depth
                             << ", file '" << convertToU8(n.name.getLocation().file_name) << "'"
                             << endl;
                    }
                }
                if (need_scope_struct)
                {
                    cout << "Зоны видимости имён:" << endl;
                    for(const SemanticScope & s : scope_set)
                        cout << "'" << convertToU8(s.first.file_name) << "' " << s.first.line << " (" << s.first.begin << ") "
                             << ((s.first.file_name == s.second.file_name) ? "" : "'" + convertToU8(s.second.file_name) + "' ")
                             << s.second.line << " (" << s.second.end << ")"
                             << endl;
                }
            }

            if (need_execute)
            {
                cout << "Выполняется сценарий '" << file_name << "'..." << endl;

                ScriptC_Interpreter machine(m,mm);

                ScriptC_NS_Sys   sys(m);
                ScriptC_NS_Math  math;
                ScriptC_NS_Scene scene(&machine);

                machine.importNamespace(u"sys", sys.getNamespace());
                machine.importNamespace(u"math", math.getNamespace());
                machine.importNamespace(u"scene", scene.getNamespace());

                auto start_of_executing = chrono::high_resolution_clock::now();

                SCI_RunnerCondition res = machine.catchAst(ast);

                if (res == SCI_RunnerCondition::Regular)
                {
                    if (need_time_intervals)
                    {
                        auto elapsed = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start_of_executing);

                        cout << "Сценарий выполнен за " << elapsed.count() << " микросекунд" << endl;
                    }
                    else
                        cout << "Сценарий выполнен успешно" << endl;
                }
                else
                {
                    cout << "Сценарий выполнен с кодом " << convertToU8(getSCI_RunnerConditionName(res)) << endl;
                    return 2;
                }
            }
        }

        return 0;
    }

}

int main(int argc, char *argv[])
{
    vector<std::string> arguments(argv + 1, argv + argc);

    string	file_name           = "";
    string  path_to_grammar     = "";
    string  grammar_name        = "";
    string  dot_file_name       = "";
    string  grammar_builder_method_string = "";
    bool    need_rebuild_grammar= false;
    bool    need_syntax_only    = false;
    bool    need_grammar_info   = false;
    bool    need_grammar_rules  = false;
    bool    need_ast_info       = false;
    bool    need_semantics      = false;
    bool    need_name_table     = false;
    bool    need_scope_struct   = false;
    bool    need_execute        = false;
    bool    need_time_intervals = false;
    bool	error               = false;
    bool	help                = false;
    bool	version             = false;

    for(size_t i=0; i < arguments.size(); ++i)
    {
        const string & arg = arguments[i];

        if (arg[0] == '-')
        {
            if (arg == "--help" || arg == "-h")
                help = true;
            else if (arg == "--version" || arg == "-v")
                version = true;
            else if (arg == "--grammar-path" || arg == "-p")
            {
                if (i == arguments.size()-1 || !path_to_grammar.empty())
                    error = true;
                else
                    path_to_grammar = arguments[++i];
            }
            else if (arg == "--grammar-name" || arg == "-g")
            {
                if (i == arguments.size()-1 || !grammar_name.empty())
                    error = true;
                else
                    grammar_name = arguments[++i];
            }
            else if (arg == "--dot-generation" || arg == "-d")
            {
                if (i == arguments.size()-1 || !dot_file_name.empty())
                    error = true;
                else
                    dot_file_name = arguments[++i];
            }
            else if (arg == "--grammar-builder-method" || arg == "-z")
            {
                if (i == arguments.size()-1 || !grammar_builder_method_string.empty())
                    error = true;
                else
                    grammar_builder_method_string = arguments[++i];
            }
            else if (arg == "--rebuild-grammar" || arg == "-b")
                need_rebuild_grammar = true;
            else if (arg == "--syntax-only" || arg == "-s")
                need_syntax_only = true;
            else if (arg == "--grammar-info" || arg == "-i")
                need_grammar_info = true;
            else if (arg == "--grammar-rules" || arg == "-r")
                need_grammar_rules = true;
            else if (arg == "--ast-info" || arg == "-a")
                need_ast_info = true;
            else if (arg == "--semantics" || arg == "-m")
                need_semantics = true;
            else if (arg == "--name-table" || arg == "-n")
                need_name_table = true;
            else if (arg == "--scope-structure" || arg == "-o")
                need_scope_struct = true;
            else if (arg == "--execute" || arg == "-e")
                need_execute = true;
            else if (arg == "--time-intervals" || arg == "-t")
                need_time_intervals = true;
            else
                error = true;
        }
        else if (file_name.empty())
            file_name = arg;
        else
            error = true;
    }

    TableBuildMethod grammar_builder_method;

    if (grammar_builder_method_string == "lr")
        grammar_builder_method = TableBuildMethod::LR1;
    else if (grammar_builder_method_string == "slr")
        grammar_builder_method = TableBuildMethod::SLR;
    else if (grammar_builder_method_string.empty() || grammar_builder_method_string == "none")
        grammar_builder_method = TableBuildMethod::none;
    else
    {
        grammar_builder_method = TableBuildMethod::none;
        cout << "Задан недопустимый метод формирования таблицы грамматики" << endl;
        error = true;
    }

    if (file_name.empty() && !version && !help)
        error = true;

    if (need_ast_info && need_syntax_only)
    {
        cout << "Заданы несовместимые параметры" << endl;
        error = true;
    }

    if (error)
    {
        cout << "Ошибка в параметрах запуска" << endl;
        help = true;
    }

    const string logo = "Утилита тестирования средств начальной загрузки и интерпретации стартовых (fuze) грамматик. Проект SIMODO stars.";

    if (help)
        cout	<< logo << endl
                << "Формат запуска:" << endl
                << "    simfuze [<параметры>] <файл>" << endl
                << "Параметры:" << endl
                << "    -h | --help                  - отображение подсказки по запуску программы" << endl
                << "    -v | --version               - отображение версии программы" << endl
                << "    -p | --grammar-path <путь>   - путь к файлам грамматики" << endl
                << "    -g | --grammar-name <имя>    - наименование грамматики" << endl
                << "    -b | --rebuild-grammar       - пересобрать указанную грамматику" << endl
                << "    -z | --grammar-builder-method <метод> - метод построения таблицы грамматики (none,slr,lr)" << endl
                << "    -s | --syntax-only           - выполнить только синтаксический анализ" << endl
                << "    -i | --grammar-info          - вывести информацию о грамматике" << endl
                << "    -r | --grammar-rules         - вывести правила грамматики" << endl
                << "    -d | --dot-generation <путь> - cоздать DOT-файл графа переходов состояний грамматики" << endl
                << "    -a | --ast-info              - вывести сгенерированное АСД" << endl
                << "    -m | --semantics             - выполнить семантический анализ" << endl
                << "    -n | --name-table            - вывести таблицу имён" << endl
                << "    -o | --scope-structure       - вывести структуру зон видимостей имён" << endl
                << "    -e | --execute               - интерпретировать заданный файл" << endl
                << "    -t | --time-intervals        - отображать интервалы времени разбора и выполнения сценария" << endl
                ;

    if (error)
        return 1;

    if (version)
        cout << logo << endl
             << "Версия: " << version_major << "." << version_middle << "." << version_minor << endl;

    if (file_name.empty())
        return 0;

    if (grammar_name.empty())
        return parse(path_to_grammar,
                     file_name,
                     "",
                     grammar_builder_method,
                     need_rebuild_grammar,
                     need_grammar_info,
                     need_grammar_rules,
                     dot_file_name,
                     need_syntax_only,
                     need_ast_info,
                     need_semantics,
                     need_name_table,
                     need_scope_struct,
                     need_execute,
                     need_time_intervals);

    return parse(path_to_grammar,
                 grammar_name,
                 file_name,
                 grammar_builder_method,
                 need_rebuild_grammar,
                 need_grammar_info,
                 need_grammar_rules,
                 dot_file_name,
                 need_syntax_only,
                 need_ast_info,
                 need_semantics,
                 need_name_table,
                 need_scope_struct,
                 need_execute,
                 need_time_intervals);
}
