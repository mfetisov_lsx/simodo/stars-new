#include "SemanticTokens.h"
#include "../ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void SemanticTokens::work()
{
    if (_jsonrpc.is_valid()) {
        const variable::Value & params_value = _jsonrpc.params();
        if (params_value.type() == variable::ValueType::Record) {
            std::string uri;
            std::shared_ptr<variable::Record> params_object = params_value.getRecord();
            const variable::Value & textDocument_value  = params_object->find(u"textDocument");
            if (textDocument_value.type() == variable::ValueType::Record) {
                const variable::Value & uri_value = textDocument_value.getRecord()->find(u"uri");
                if (uri_value.type() == variable::ValueType::String)
                    uri = convert::toU8(uri_value.getString());
            }

            if (!uri.empty()) {
                DocumentContext * doc = _server.findDocument(uri);
                if (doc) {
                    variable::Value result = doc->produceSemanticTokens();
                    _server.sending().push(variable::json::Rpc(result, _jsonrpc.id()));
                    return;
                }
                _server.log().error("'textDocument/semanticTokens' command: uri '" + uri + "' don't loaded yet",
                                    variable::json::toString(_jsonrpc.value()));
                _server.sending().push(
                    /// @todo Скорректировать коды (и тексты) ошибок
                    variable::json::Rpc(-1,
                        u"'textDocument/semanticTokens' command: uri '" + convert::toU16(uri) + u"' don't loaded yet",
                        _jsonrpc.id()));
                return;
            }
        }
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/semanticTokens' command", 
                        variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1,u"There are wrong parameter structure of 'textDocument/semanticTokens' command",
                            _jsonrpc.id()));
}

