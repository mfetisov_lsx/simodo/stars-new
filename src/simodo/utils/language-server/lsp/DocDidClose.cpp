#include "DocDidClose.h"
#include "../ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void DocDidClose::work()
{
    if (_jsonrpc.is_valid() && _jsonrpc.params().type() == variable::ValueType::Record) {
        variable::Value & textDocument_value = _jsonrpc.params().getRecord()->find(u"textDocument");
        if (textDocument_value.type() == variable::ValueType::Record) {
            variable::Value & uri_value = textDocument_value.getRecord()->find(u"uri");
            if (uri_value.type() == variable::ValueType::String) {
                if (_server.closeDocument(convert::toU8(uri_value.getString())))
                    return;
            }
        }
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/didClose' notification", variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1,u"There are wrong parameter structure of 'textDocument/didClose' notification",-1));
}

