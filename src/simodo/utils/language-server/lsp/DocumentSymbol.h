#ifndef TaskDocumentSymbol_h
#define TaskDocumentSymbol_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class DocumentSymbol : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

public:
    DocumentSymbol() = delete;
    DocumentSymbol(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;

private:
};

#endif // TaskDocumentSymbol_h