#ifndef TaskDidChange_h
#define TaskDidChange_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class DocDidChange : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

public:
    DocDidChange() = delete;
    DocDidChange(ServerContext & server, std::string jsonrpc_content)
        : _server(server), _jsonrpc(jsonrpc_content)
    {}

    virtual void work() override;
};

#endif // TaskDidChange_h