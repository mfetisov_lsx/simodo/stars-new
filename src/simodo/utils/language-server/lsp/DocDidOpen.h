#ifndef TaskDidOpen_h
#define TaskDidOpen_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class DocDidOpen : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

public:
    DocDidOpen() = delete;
    DocDidOpen(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

#endif // TaskDidOpen_h