#ifndef GotoDefinition_h
#define GotoDefinition_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class GotoDefinition : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

public:
    GotoDefinition() = delete;
    GotoDefinition(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

#endif // GotoDefinition_h