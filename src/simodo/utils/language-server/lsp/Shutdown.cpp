#include "Shutdown.h"
#include "../ServerContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void Shutdown::work()
{
    if (_server.state() == ServerState::Shutdown) {
        _server.log().error("There are wrong parameter structure of 'shutdown' command", variable::json::toString(_jsonrpc.value()));
        /// @todo Скорректировать коды (и тексты) ошибок
        _server.sending().push(variable::json::Rpc(-1, u"Invalid request", _jsonrpc.id()));
        return;
    }

    if (_server.shutdown()) {
        _server.sending().push(variable::json::Rpc(variable::Value {}, _jsonrpc.id()));
        return;
    }

    _server.log().error("There are wrong parameter structure of 'shutdown' command", variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1, u"An exception happens during shutdown request", _jsonrpc.id()));
}

