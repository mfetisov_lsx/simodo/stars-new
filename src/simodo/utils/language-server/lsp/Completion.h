#ifndef TaskCompletion_h
#define TaskCompletion_h

#include "simodo/tp/Task_interface.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/Variable.h"

class ServerContext;

class Completion : public simodo::tp::Task_interface
{
    ServerContext &             _server;
    simodo::variable::json::Rpc _jsonrpc;

public:
    Completion() = delete;
    Completion(ServerContext & server, std::string jsonrpc)
        : _server(server), _jsonrpc(jsonrpc)
    {}

    virtual void work() override;
};

#endif // TaskCompletion_h