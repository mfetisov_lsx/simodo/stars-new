#include "GotoDeclaration.h"
#include "../ServerContext.h"
#include "../DocumentContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/lsp/TextDocumentPositionParams.h"

using namespace simodo;
using namespace simodo::inout;

void GotoDeclaration::work()
{
    if (_jsonrpc.is_valid()) {
        lsp::TextDocumentPositionParams doc_position;
        if (lsp::parseTextDocumentPositionParams(_jsonrpc.params(), doc_position)) {
            DocumentContext * doc = _server.findDocument(doc_position.textDocument.uri);
            if (doc) {
                variable::Value result = doc->produceGotoDeclarationResult(doc_position.position);
                _server.sending().push(variable::json::Rpc(result, _jsonrpc.id()));
                return;
            }

            _server.log().error("'textDocument/declaration' command: uri '" 
                                + doc_position.textDocument.uri + "' don't loaded yet",
                                variable::json::toString(_jsonrpc.value()));
            _server.sending().push(
                /// @todo Скорректировать коды (и тексты) ошибок
                variable::json::Rpc(-1,
                    u"'textDocument/declaration' command: uri '" 
                    + convert::toU16(doc_position.textDocument.uri) + u"' don't loaded yet",
                    _jsonrpc.id()));
            return;
        }
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/declaration' command", 
                        variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1,u"There are wrong parameter structure of 'textDocument/declaration' command",
                            _jsonrpc.id()));
}

