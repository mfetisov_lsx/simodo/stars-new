#include "Completion.h"
#include "../ServerContext.h"
#include "../DocumentContext.h"

#include "simodo/variable/json/Rpc.h"
#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/lsp/CompletionParams.h"

using namespace simodo;
using namespace simodo::inout;

void Completion::work()
{
    if (_jsonrpc.is_valid()) {
        lsp::CompletionParams completionParams;
        if (lsp::parseTextDocumentPositionParams(_jsonrpc.params(), completionParams)
         && lsp::parseCompletionParams(_jsonrpc.params(), completionParams)) {
            DocumentContext * doc = _server.findDocument(completionParams.textDocument.uri);
            if (doc) {
                variable::Value result = doc->produceCompletionResult(completionParams);
                _server.sending().push(variable::json::Rpc(result, _jsonrpc.id()));
                return;
            }

            _server.log().error("'textDocument/completion' command: uri '" 
                                + completionParams.textDocument.uri + "' don't loaded yet",
                                variable::json::toString(_jsonrpc.value()));
            _server.sending().push(
                /// @todo Скорректировать коды (и тексты) ошибок
                variable::json::Rpc(-1,
                    u"'textDocument/completion' command: uri '" 
                    + convert::toU16(completionParams.textDocument.uri) + u"' don't loaded yet",
                    _jsonrpc.id()));
            return;
        }
    }
    _server.log().error("There are wrong parameter structure of 'textDocument/completion' command", 
                        variable::json::toString(_jsonrpc.value()));
    _server.sending().push(
        /// @todo Скорректировать коды (и тексты) ошибок
        variable::json::Rpc(-1,u"There are wrong parameter structure of 'textDocument/completion' command",
                            _jsonrpc.id()));
}

