#ifndef CommonData_h
#define CommonData_h

#include "DocumentContext.h"
#include "ReportCombiner.h"

#include "simodo/dsl/GrammarManagement.h"

#include "simodo/lsp/ClientParams.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/tp/ThreadPool.h"
#include "simodo/inout/log/Logger_interface.h"

#include <map>
#include <memory>

class ServerContext;

enum class ServerState
{
    None,
    Work,
    Shutdown
};

class ServerContext
{
    std::string                     _path_to_grammar;
    simodo::inout::log::Logger_interface & 
                                    _log;
    std::istream &                  _is;
    std::ostream &                  _os;
    simodo::tp::ThreadsafeQueue<simodo::variable::json::Rpc>  
                                    _queue_for_sending;
    simodo::tp::ThreadPool          _loom;
    std::thread                     _response_thread;
    bool                            _done = false;
    ReportCombiner                  _m;
    simodo::dsl::GrammarManagement  _gm;
    std::map<std::string,std::unique_ptr<DocumentContext>>
                                    _documents;
    std::mutex                      _documents_mutex;
    ServerState                     _state = ServerState::None;
    simodo::lsp::ClientParams       _client_params;
    bool                            _save_mode = false;

public:
    ServerContext() = delete;
    ServerContext(std::string path_to_grammar, simodo::inout::log::Logger_interface & log, std::istream & is, std::ostream & os, bool save_mode=false);
    ~ServerContext();

public:
    const std::string &             path_to_grammar() const { return _path_to_grammar; }
    simodo::dsl::GrammarManagement &gm() { return _gm; }
    simodo::tp::ThreadsafeQueue<simodo::variable::json::Rpc> &
                                    sending() { return _queue_for_sending; }
    simodo::tp::ThreadPool &        loom() { return _loom; }
    ServerState                     state() const { return _state; }
    bool                            save_mode() const { return _save_mode; }

    bool openDocument(const simodo::variable::Record & doc_params);
    bool changeDocument(const simodo::variable::Record & doc_params);
    bool closeDocument(const std::string & uri);
    DocumentContext * findDocument(const std::string & uri);

    bool findAndCopy(const std::string & uri, std::u16string & content);

public:
    simodo::inout::log::Logger_interface & log() const { return _log; }
    bool                        is_done() const { return _done; }
    bool                        run();
    simodo::variable::json::Rpc initialize(simodo::lsp::ClientParams params, int id);
    void                        initialized();
    bool                        shutdown();
    void                        exit();

private:
    void sendResponse_loop();
};

#endif // TaskDistribute_h