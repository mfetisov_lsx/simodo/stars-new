/*
MIT License

Copyright (c) 2021 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo
*/

/*! \file Утилита тестирования средств работы с JSON библиотеки SIMODO core. Проект SIMODO.
*/

#include "ServerContext.h"

#include "simodo/inout/log/Logger.h"
#include "simodo/inout/token/FileStream.h"
#include "simodo/inout/convert/functions.h"
#include "simodo/variable/json/Serialization.h"

#include <iostream>
#include <memory>

using namespace simodo;
using namespace simodo::inout;
using namespace simodo::variable;

int main(int argc, char * argv[])
{
    std::string                   path_to_grammar = "~/Simodo/SimodoEdit/data/grammar";
    log::SeverityLevel            log_level = log::SeverityLevel::Info;
    bool                          save_mode = false;
    std::unique_ptr<std::ostream> log_stream;
    std::unique_ptr<std::istream> in;

    if (argc > 1)
        path_to_grammar = argv[1];

    if (argc > 2) 
        log_stream = std::make_unique<std::ofstream>(argv[2]);
    
    if (argc > 3) {
        if (std::string(argv[3]) == "Debug")
            log_level = log::SeverityLevel::Debug;
        else if (std::string(argv[3]) == "Info")
            log_level = log::SeverityLevel::Info;
        else if (std::string(argv[3]) == "Warning")
            log_level = log::SeverityLevel::Warning;
        else if (std::string(argv[3]) == "Error")
            log_level = log::SeverityLevel::Error;
        else if (std::string(argv[3]) == "Critical")
            log_level = log::SeverityLevel::Critical;
    }

    if (argc > 4)
        save_mode = std::string(argv[4]) == "save";

    if (argc > 5)
#ifdef CROSS_WIN
        in = std::make_unique<std::ifstream>(simodo::inout::convert::fromUtf8CharToWChar(argv[5]).c_str());
#else
        in = std::make_unique<std::ifstream>(argv[5]);
#endif
    log::Logger log(log_stream ? *log_stream.get() : std::cerr, 
                    "SIMODO-LSP-server", 
                    log_level);

    ServerContext server(path_to_grammar, log,
                         in ? *in.get() : std::cin,
                         std::cout,
                         save_mode);

    server.run();

    return 0;
}
