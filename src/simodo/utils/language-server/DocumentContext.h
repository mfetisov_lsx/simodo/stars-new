#ifndef DocumentContext_h
#define DocumentContext_h

#include "MessageFullContent.h"

#include "simodo/dsl/SemanticBase.h"
#include "simodo/dsl/SemanticNameTable.h"
#include "simodo/dsl/Stream.h"

#include "simodo/variable/Variable.h"
#include "simodo/lsp/CompletionParams.h"

#include <mutex>
#include <set>

struct ProtectedState
{
    mutable std::mutex mutex;
    /// @attention Строка ниже может быть полезна, если случится попытка импорта модуля с тем же именем рекурсивно
    /// В этом случае сервер может зависнуть, если использовать простой мьютекс. Протестить бы.
    /// @code {.cpp}
    /// std::recursive_mutex _parser_mutex; 
    /// @endcode
};

struct ParserState : public ProtectedState
{
    std::u16string                  text;
    std::string                     uri;
    int64_t                         version;
    std::string                     grammar_name;
    std::vector<simodo::dsl::Token> tokens;
    std::multimap<size_t,size_t>    line_to_token_index;
};

struct CheckerState : public ProtectedState
{
    std::u16string                  path_to_file;
    std::set<std::string>           dependencies;      ///< Зависимости от других модулей
    std::vector<MessageFullContent> message_set;       ///< Сообщения после парсинга и анализа
    std::vector<simodo::dsl::SemanticName> name_set;  ///< Таблица имён с семантической информацией
    size_t                          doc_names_begin_index=0;///< Индекс начала символов документа    
    std::vector<simodo::dsl::SemanticScope> scope_set; ///< Структура модуля (без учёта комментариев)
    /// @brief <строка с 0, позиция имени в name_set>
    std::multimap<size_t,size_t>    line_to_name_index;///< Индекс используемых имён по строкам
};

class ServerContext;

class DocumentContext
{
    ServerContext &     _server;
    /// @todo Сделать Atomic, тк есть вероятность перепутывания значения в конструкторе
    bool                _valid;   
    ParserState         _parser;
    CheckerState        _checker;
    bool                _is_opened = false; 

public:
    DocumentContext() = delete;
    // DocumentContext(DocumentContext && dc) = default;
    DocumentContext(ServerContext & server, const simodo::variable::Record & textDocument_object);

    bool valid()  const { return _valid; }
    bool is_opened()  const { return _is_opened; }

public:
    bool open(const simodo::variable::Record & textDocument_object);
    bool change(const simodo::variable::Record & doc_params);
    bool close();
    bool analyze();
    void copyContent(std::u16string & content);
    bool checkDependency(const std::string & uri);

public:
    simodo::variable::Value produceHoverInfo(const simodo::lsp::Position & pos);
    simodo::variable::Value produceGotoDeclarationResult(const simodo::lsp::Position & pos);
    simodo::variable::Value produceGotoDefinitionResult(const simodo::lsp::Position & pos);
    simodo::variable::Value produceCompletionResult(const simodo::lsp::CompletionParams & completionParams);
    simodo::variable::Value produceSemanticTokens();
    simodo::variable::Value produceDocumentSymbols();

private:
    std::u16string          makeName(size_t index) const;
    std::u16string          makeType_plaintext(size_t index, const std::u16string & on_None_type_name) const;
    std::u16string          makeDeclaration_plaintext(size_t index, bool include_owners=false) const;
    std::u16string          makeHover_plaintext(size_t index) const;
    simodo::variable::Value makeDiagnosticParams() const;
    int64_t                 makeCompletionItemKind(size_t index) const;
    simodo::variable::Value makeCompletionItem(const std::u16string & name, 
                                               const std::u16string & detail, 
                                               const std::u16string & description,
                                               int64_t kind) const;
    simodo::variable::Value makeCompletionItem(size_t index) const;

    size_t                  findSymbol(const simodo::lsp::Position & pos) const;

    static std::shared_ptr<simodo::variable::Record> 
                            makeRange(const simodo::dsl::TokenLocation & loc);
    static std::shared_ptr<simodo::variable::Record> 
                            makeRange(std::pair<int64_t, int64_t> start, std::pair<int64_t, int64_t> end);
    static std::shared_ptr<simodo::variable::Record> 
                            makePosition(int64_t line, int64_t character);
    static int64_t          makeSeverity(simodo::dsl::SeverityLevel level);
    static std::u16string   makeMessage(const MessageFullContent & message);
    static int64_t          makeSymbolKind(const simodo::dsl::SemanticName & name);

    simodo::dsl::SemanticFlags_t bringFlags(const simodo::dsl::SemanticName & name) const;

    std::string             convertUriToPath(const std::string & uri);
};


#endif // DocumentContext_h