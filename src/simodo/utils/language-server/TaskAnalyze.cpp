#include "TaskAnalyze.h"
#include "DocumentContext.h"

TaskAnalyze::TaskAnalyze(DocumentContext & doc) 
    : _doc(doc)
{
}

void TaskAnalyze::work()
{
    _doc.analyze();
}

