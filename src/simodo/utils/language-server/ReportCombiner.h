#ifndef ReportCombiner_h
#define ReportCombiner_h

#include "MessageFullContent.h"

#include <vector>

class ReportCombiner: public simodo::dsl::AReporter
{
    std::vector<MessageFullContent> _messages;

public:
    virtual void report(const simodo::dsl::SeverityLevel level,
                        simodo::dsl::TokenLocation location,
                        const std::u16string & briefly,
                        const std::u16string & atlarge) override;

    std::vector<MessageFullContent> & messages() { return _messages; }
};


#endif // ReportCombiner