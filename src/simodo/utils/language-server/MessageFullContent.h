#ifndef MessageFullContent_h
#define MessageFullContent_h

#include "simodo/dsl/AReporter.h"

struct MessageFullContent
{
    simodo::dsl::SeverityLevel  level;
    simodo::dsl::TokenLocation  location;
    std::u16string              briefly;
    std::u16string              atlarge;
};

#endif // MessageFullContent_h