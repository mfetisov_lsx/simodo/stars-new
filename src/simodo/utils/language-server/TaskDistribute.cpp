#include "TaskDistribute.h"
#include "ServerContext.h"
#include "lsp/Initialize.h"
#include "lsp/Initialized.h"
#include "lsp/Shutdown.h"
#include "lsp/DocDidOpen.h"
#include "lsp/DocDidChange.h"
#include "lsp/DocDidClose.h"
#include "lsp/DocHover.h"
#include "lsp/DocumentSymbol.h"
#include "lsp/SemanticTokens.h"
#include "lsp/GotoDeclaration.h"
#include "lsp/GotoDefinition.h"
#include "lsp/Completion.h"
#include "lsp/SegmentationFault.h"

#include "simodo/variable/json/Serialization.h"
#include "simodo/variable/json/Rpc.h"
#include "simodo/inout/convert/functions.h"

using namespace simodo;
using namespace simodo::inout;

void TaskDistribute::work()
{
    variable::json::Rpc json_rpc(_jsonrpc_content);

    /// @note Метод может быть пустым только когда сделаем отправку запросов с сервера на клиент
    if (!json_rpc.is_valid() || json_rpc.method().empty()) {
        _server.log().error("Unrecognized JSON-RPC structure", _jsonrpc_content);
        _server.sending().push(
            /// @todo Скорректировать коды (и тексты) ошибок
            simodo::variable::json::Rpc(-1, u"Unrecognized JSON-RPC structure", json_rpc.id()));
    }

    if (json_rpc.method() == u"initialize")
        _server.loom().submit(new Initialize(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"initialized")
        _server.loom().submit(new Initialized(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"shutdown")
        _server.loom().submit(new Shutdown(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/didOpen")
        _server.loom().submit(new DocDidOpen(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/didChange")
        _server.loom().submit(new DocDidChange(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/didClose")
        _server.loom().submit(new DocDidClose(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/hover")
        _server.loom().submit(new DocHover(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/documentSymbol")
        _server.loom().submit(new DocumentSymbol(_server,_jsonrpc_content));
    else if (json_rpc.method().starts_with(u"textDocument/semanticTokens"))
        _server.loom().submit(new SemanticTokens(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/declaration")
        _server.loom().submit(new GotoDeclaration(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/definition")
        _server.loom().submit(new GotoDefinition(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"textDocument/completion")
        _server.loom().submit(new Completion(_server,_jsonrpc_content));
    else if (json_rpc.method() == u"SegmentationFault")
        _server.loom().submit(new SegmentationFault(_server,_jsonrpc_content));
    else {
        _server.log().warning("Unsupported method '" + convert::toU8(json_rpc.method()) + "'", _jsonrpc_content);
        _server.sending().push(
            /// @todo Скорректировать коды (и тексты) ошибок
            simodo::variable::json::Rpc(-1, u"Unsupported method '" + json_rpc.method() + u"'", json_rpc.id()));
    }
}
