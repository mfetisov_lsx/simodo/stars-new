#include "ServerContext.h"
#include "TaskDistribute.h"
#include "TaskAnalyze.h"

#include "simodo/version.h"

#include "simodo/variable/json/Serialization.h"
#include "simodo/inout/convert/functions.h"

#include <thread>
#include <iostream>
#include <chrono>
#include <iomanip>
#include <filesystem>

using namespace simodo;
using namespace simodo::inout;

inline const std::string content_length_const = "Content-Length: ";

namespace 
{
std::pair<int, std::string> readHeader(std::istream & is)
{
    /// @todo Необходимо заменить на работу с boost::asio, чтобы не было клинча в ожиданиях.
    /// Это когда клиент ждёт, когда сервер завершиться после нотификации о завершении, а 
    /// сервер в ожидании очередной команды. Или придумать что-то ещё.
    std::string header;
    std::string line;
    int         content_size = 0;

    while(std::getline(is,line) && !line.empty()) {
        if (line.find(content_length_const) == 0)
            content_size = std::stol(line.substr(content_length_const.size()));
        header += line + "\n";
    }

    return {content_size, header};
}

std::string readContent(std::istream & is, int content_length)
{
    std::string content;

    for(int i=0; i < content_length && is.good(); ++i)
        content += is.get();

    return content;
}

bool verifyJson(const std::string & str)
{
    return !str.empty() && str[0] == '{' && str[str.size()-1] == '}';
}

bool verifyExit(const std::string & json_str)
{
    const std::string exit_request_string = "\"method\":\"exit\"";

    if (json_str.length() > 4*exit_request_string.length())
        return false;

    return json_str.find(exit_request_string) != std::string::npos;
}

}

ServerContext::ServerContext(std::string path_to_grammar, 
                             inout::log::Logger_interface & log, 
                             std::istream & is, std::ostream & os, 
                             bool save_mode)
    : _path_to_grammar(path_to_grammar)
    , _log(log)
    , _is(is)
    , _os(os)
    , _queue_for_sending()
    , _loom(save_mode ? 0 : 4)
    , _response_thread(&ServerContext::sendResponse_loop, this)
    , _m()
    , _gm(_m, _path_to_grammar)
    , _save_mode(save_mode)
{
}

ServerContext::~ServerContext()
{
    _done = true;
    _loom.stop();
    _response_thread.join();
}

bool ServerContext::openDocument(const simodo::variable::Record & doc_params)
{
    const variable::Value & textDocument_value = doc_params.find(u"textDocument");
    if (textDocument_value.type() != variable::ValueType::Record)
        return false;

    const variable::Value & uri_value = textDocument_value.getRecord()->find(u"uri");
    if (uri_value.type() != variable::ValueType::String)
        return false;

    std::string         uri   = convert::toU8(uri_value.getString());
    std::unique_lock    documents_locker(_documents_mutex);
    auto                it    = _documents.find(uri);

    if (it != _documents.end()) {
        if (it->second->is_opened())
            return false;

        if (!it->second->open(*textDocument_value.getRecord()))
            return false;
    }
    else {
        std::unique_ptr<DocumentContext> doc = std::make_unique<DocumentContext>(*this,*textDocument_value.getRecord());

        if (!doc->valid())
            return false;

        auto [it_doc,ok] = _documents.insert({uri,std::move(doc)});
        it = it_doc;
    }

    documents_locker.unlock();

    /// @attention Попавшие в контейнер документы нельзя оттуда удалять!
    loom().submit(new TaskAnalyze(*it->second));

    return true;
}

bool ServerContext::changeDocument(const simodo::variable::Record & doc_params)
{
    const variable::Value & textDocument_value = doc_params.find(u"textDocument");
    if (textDocument_value.type() != variable::ValueType::Record)
        return false;

    const variable::Value & uri_value = textDocument_value.getRecord()->find(u"uri");
    if (uri_value.type() != variable::ValueType::String)
        return false;

    std::string     uri   = convert::toU8(uri_value.getString());
    std::lock_guard documents_locker(_documents_mutex);
    auto            it    = _documents.find(uri);
    if (it == _documents.end()) 
        return false;

    if (!it->second->change(doc_params))
        return false;

    /// @attention Попавшие в контейнер документы нельзя оттуда удалять!
    loom().submit(new TaskAnalyze(*it->second));

    for(auto & [doc_uri,p_doc] : _documents)
        if (doc_uri != uri && p_doc->is_opened() && p_doc->checkDependency(uri))
            loom().submit(new TaskAnalyze(*p_doc));

    return true;
}

bool ServerContext::closeDocument(const std::string & uri)
{
    std::lock_guard locker(_documents_mutex);
    auto it = _documents.find(uri);

    if (it == _documents.end())
        return false;

    return it->second->close();
}

DocumentContext * ServerContext::findDocument(const std::string & uri) 
{
    std::lock_guard locker(_documents_mutex);

    auto it = _documents.find(uri);
    
    return it != _documents.end() ? it->second.get() : nullptr;
}

bool ServerContext::findAndCopy(const std::string & uri, std::u16string & content)
{
    std::lock_guard locker(_documents_mutex);
    auto it = _documents.find(uri);

    if (it == _documents.end())
        return false;

    it->second->copyContent(content);
    return true;
}

variable::json::Rpc ServerContext::initialize(lsp::ClientParams params, int id)
{
    _client_params = params;

    using namespace variable;

    if (_client_params.rootPath.empty())
        _client_params.rootPath = std::filesystem::current_path().string();
    else {
        std::error_code ec;
        std::filesystem::path work_dir = _client_params.rootPath;
        std::filesystem::current_path(work_dir, ec);
        if (ec)
            /// @todo Скорректировать коды (и тексты) ошибок
            return json::Rpc(-1, u"Unable to set work dir '" + convert::toU16(_client_params.rootPath) + u"'. " 
                                + convert::toU16(ec.message()), id);
    }

    return json::Rpc(Value { Record {{
        {u"capabilities", Record {{
            // The position encoding the server picked from the encodings offered by the 
            // client via the client capability `general.positionEncodings`.
            {u"positionEncoding",       u"utf-16"},
            // Documents are synced by always sending the full content of the document.
            {u"textDocumentSync",       static_cast<int64_t>(1)},
            // The server provides completion support.
            {u"completionProvider",     Record {{
                {u"triggerCharacters",      ValueArray {{{u"."}}}},
                // The server provides support to resolve additional information for a completion item.
                {u"resolveProvider",        false},
                // The server supports the following `CompletionItem` specific capabilities.
                {u"completionItem",         Record {{
                    // The server has support for completion item label details (see also 
                    // `CompletionItemLabelDetails`) when receiving a completion item in a
                    // resolve call.
                    {u"labelDetailsSupport",    false},
                }}},
            }}},
            // The server provides hover support.
            {u"hoverProvider",          true},
            // The server provides go to declaration support.
            {u"declarationProvider",    true},
            // The server provides goto definition support.
            {u"definitionProvider",     true},
            // The server provides document symbol support.
            {u"documentSymbolProvider", true},
            // The server provides semantic tokens support.
            {u"semanticTokensProvider", Record {{
                {u"legend", Record {{
                    {u"tokenTypes",         std::vector<Value>{{
                        u"type", u"struct", u"variable", u"property", u"function", u"method"
                    }}},
                    {u"tokenModifiers",     std::vector<Value>{{
                        u"declaration", u"definition", u"readonly", u"array",
                        u"parameter", u"input", u"output", u"anchor"
                    }}},
                }}},
                // Server supports providing semantic tokens for a full document.
                {u"full",   true},
            }}},
        }}},
        {u"serverInfo", Record {{
            {u"name",                   u"SIMODO/stars"},
            {u"version",                convert::toU16(std::to_string(version_major)+"."+
                                                       std::to_string(version_middle)+"."+
                                                       std::to_string(version_minor))},
        }}},
    }}}, id);
}

void ServerContext::initialized()
{
    _state = ServerState::Work;
}

bool ServerContext::shutdown()
{
    _state = ServerState::Shutdown;
    /// @todo Выполнить необходимые действия для сохранения состояния, если это нужно
    return true;
}

void ServerContext::exit()
{
    _done = true;
}

bool ServerContext::run()
{
    bool ok = true;

    while(!is_done()) {
        auto [content_length, header] = readHeader(_is);
        if (header.empty() || content_length <= 0) {
            /// @todo Скорректировать коды (и тексты) ошибок
            _queue_for_sending.push(simodo::variable::json::Rpc(-1, u"Wrong header structure", -1));
            _log.critical("Wrong header structure", header);
            ok = false;
            break;
        }
        {
            std::ostringstream so;
            so  << std::endl 
                << "RECEIVE FROM CLIENT " << std::endl
                << "'" << header << "'" << std::endl;
            _log.debug(so.str());
        }

        std::string content = readContent(_is, content_length);
        if (content.empty()) {
            /// @todo Скорректировать коды (и тексты) ошибок
            _queue_for_sending.push(simodo::variable::json::Rpc(-1, u"Wrong content length", -1));
            _log.critical("Wrong content structure", content);
            ok = false;
            break;
        }
        {
            std::ostringstream so;
            so << "'" << content << "'" << std::endl;
            _log.debug("Content of JSON-RPC", so.str());
        }

        if (!verifyJson(content)) {
            /// @todo Скорректировать коды (и тексты) ошибок
            _queue_for_sending.push(simodo::variable::json::Rpc(-1, u"Wrong JSON structure", -1));
            _log.critical("Wrong JSON structure", content);
            ok = false;
            break;
        }

        /// @attention Завершающую нотификацию сделал сразу при входном анализе запросов клиента, 
        /// чтобы корректно завершить работу
        if (verifyExit(content)) {
            _log.info("EXIT");
            exit();
            break;
        }

        loom().submit(new TaskDistribute(*this, content));

        if (save_mode())
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    
    /// @todo Дожидаться отправки всех сообщений? Вроде бы не нужно, т.к. протокол
    /// исключает такую необходимость.
    return ok;
}

void ServerContext::sendResponse_loop()
{
    /// @attention Очереди входящих запросов, как и исходящих ответов 
    /// при завершение работы могут оставаться не пустыми.
    while(!_done) {
        variable::json::Rpc response;
        if (_queue_for_sending.pop_or_timeout(response, 100)) {
            std::string json_string = variable::json::toString(response.value(), true, true);
            std::ostringstream log_str;
            log_str << std::endl 
                    << "SEND TO CLIENT " << std::endl
                    << content_length_const << json_string.length() << std::endl << std::endl 
                    << json_string;
            _log.debug(log_str.str());
            _os     << content_length_const << json_string.length() << std::endl << std::endl 
                    << json_string;
            _os.flush();
        }
    }
}
