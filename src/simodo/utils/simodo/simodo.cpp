/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <fstream>
#include <iostream>
#include <memory>
#include <algorithm>
#include <chrono>

#if (__GNUC__ > 7) || defined(__clang__)
#include <filesystem>
#else
#include <experimental/filesystem>
using namespace std::experimental;
#endif

#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/GrammarBuilder.h"
#include "simodo/dsl/Parser.h"

#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_Interpreter.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_NS_TableFunction.h"
#include "simodo/dsl/ScriptC_Semantics.h"
#include "simodo/dsl/ModulesManagement.h"

#include "simodo/dsl/Remote_NS_Chart.h"
#include "simodo/dsl/Remote_NS_Cockpit.h"
#include "simodo/dsl/Remote_NS_Graph3D.h"

#include "simodo/convert.h"
#include "simodo/version.h"

/*! \file Утилита тестирования средств начальной загрузки и интерпретации стартовых (FUZE) грамматик библиотеки SIMODOdsl. Проект SIMODO Stars
*/

using namespace std;
using namespace simodo;
using namespace simodo::dsl;

namespace
{
    class COutListener: public AReporter
    {
    public:
        COutListener() {}
        virtual ~COutListener() override;

        virtual void report(const SeverityLevel level,
                            TokenLocation ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge) override;
    };

    COutListener::~COutListener() {}

    void COutListener::report(const SeverityLevel level,
                              TokenLocation ,
                              const std::u16string & briefly,
                              const std::u16string & atlarge)
    {
        cout << convertToU8(getSeverityLevelName(level) + briefly) << endl;
        if (!atlarge.empty())
            cout << convertToU8(atlarge) << endl;
    }

    int parse(const string & path,
              const string & grammar_name,
              const string & file_name,
              bool need_analyze_only)
    {
        if (file_name.empty())
            return 1;

        COutListener  m;
        Grammar       g;
        std::vector<std::string> paths {path};

        GrammarLoader loader(m, paths, grammar_name);

        bool ok = loader.load(false, g, TableBuildMethod::none);

        if (!ok)
            return 2;

        simodo::dsl::Parser p(file_name,m,g);
        AstNode             ast(convertToU16(file_name));
        AstBuilder          ast_builder(m,ast,g.handlers);

        ok = p.parse(ast_builder);

        if (!ok)
            return 2;

        ScriptC_NS_Sys           sys(m);
        ScriptC_NS_Math          math;
        ScriptC_NS_TableFunction tf;
        Remote_NS_Chart          chart(m);
        Remote_NS_Cockpit        cockpit(m);
        Remote_NS_Graph3D        graph3d(m);

        std::vector<std::pair<std::u16string,IScriptC_Namespace*>> additional_namespaces {
            {u"chart", &chart},
            {u"cockpit", &cockpit},
            {u"graph3d", &graph3d}
        };

        ModulesManagement mm(m, m, nullptr, path, nullptr, additional_namespaces);

        if (need_analyze_only)
        {
            std::vector<SemanticName>   name_set;
            std::vector<SemanticScope>  scope_set;

            ScriptC_Semantics checker(m, mm, name_set, scope_set);

            ScriptC_NS_Scene  scene;

            checker.importNamespace(u"sys",     sys.getNamespace());
            checker.importNamespace(u"math",    math.getNamespace());
            checker.importNamespace(u"tf",      tf.getNamespace());
            checker.importNamespace(u"scene",   scene.getNamespace());
            checker.importNamespace(u"chart",   chart.getNamespace());
            checker.importNamespace(u"cockpit", cockpit.getNamespace());
            checker.importNamespace(u"graph3d", graph3d.getNamespace());

            bool res = checker.check(ast);

            if (!res)
                return 2;

            return 0;
        }

        ScriptC_Interpreter machine(m,mm);
        ScriptC_NS_Scene    scene(&machine,true);

        machine.importNamespace(u"sys",     sys.getNamespace());
        machine.importNamespace(u"math",    math.getNamespace());
        machine.importNamespace(u"tf",      tf.getNamespace());
        machine.importNamespace(u"scene",   scene.getNamespace());
        machine.importNamespace(u"chart",   chart.getNamespace());
        machine.importNamespace(u"cockpit", cockpit.getNamespace());
        machine.importNamespace(u"graph3d", graph3d.getNamespace());

        SCI_RunnerCondition res = machine.catchAst(ast);

        if (res != SCI_RunnerCondition::Regular)
            return 2;

        return 0;
    }

}

int main(int argc, char *argv[])
{
    vector<std::string> arguments(argv + 1, argv + argc);

    string	file_name           = "";
    string  path_to_grammar     = "data/grammar";
    string  grammar_name        = "scriptc0";
    bool    need_analyze_only   = false;
    bool	error               = false;
    bool	help                = false;
    bool	version             = false;

    for(size_t i=0; i < arguments.size(); ++i)
    {
        const string & arg = arguments[i];

        if (arg[0] == '-')
        {
            if (arg == "--help" || arg == "-h")
                help = true;
            else if (arg == "--version" || arg == "-v")
                version = true;
            else if (arg == "--grammar-path" || arg == "-p")
            {
                if (i == arguments.size()-1)
                    error = true;
                else
                    path_to_grammar = arguments[++i];
            }
            else if (arg == "--grammar-name" || arg == "-g")
            {
                if (i == arguments.size()-1)
                    error = true;
                else
                    grammar_name = arguments[++i];
            }
            else if (arg == "--analyze-only" || arg == "-a")
                need_analyze_only = true;
            else
                error = true;
        }
        else if (file_name.empty())
            file_name = arg;
        else
            error = true;
    }

    if (file_name.empty() && !version && !help)
        error = true;

    if (error)
    {
        cout << "Ошибка в параметрах запуска" << endl;
        help = true;
    }

    const string logo = "Проект SIMODO stars. Интерпретатор";

    if (help)
        cout	<< logo << endl
                << "Формат запуска:" << endl
                << "    simodo [<параметры>] <файл>" << endl
                << "Параметры:" << endl
                << "    -h | --help                  - отображение подсказки по запуску программы" << endl
                << "    -v | --version               - отображение версии программы" << endl
                << "    -p | --grammar-path <путь>   - путь к файлам грамматики" << endl
                << "    -g | --grammar-name <имя>    - наименование грамматики" << endl
                << "    -a | --analyze-only          - выполнить только анализ заданного файла, не выполняя его" << endl
                ;

    if (error)
        return 1;

    if (version)
        cout << logo << endl
             << "Версия: " << version_major << "." << version_middle << "." << version_minor << endl;

    if (file_name.empty())
        return 0;

    return parse(path_to_grammar,
                 grammar_name,
                 file_name,
                 need_analyze_only
                );
}
