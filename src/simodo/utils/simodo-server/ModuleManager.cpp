/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#include <cassert>

#include "ModuleManager.h"

#include "simodo/convert.h"

#include "simodo/dsl/ScriptC_Semantics.h"
#include "simodo/dsl/GrammarManagement.h"
#include "simodo/dsl/GrammarLoader.h"
#include "simodo/dsl/Parser.h"
#include "simodo/dsl/AstBuilder.h"
#include "simodo/dsl/ScriptC_NS_Sys.h"
#include "simodo/dsl/ScriptC_NS_Math.h"
#include "simodo/dsl/ScriptC_NS_Scene.h"
#include "simodo/dsl/ScriptC_NS_TableFunction.h"

#include <filesystem>
namespace fs = std::filesystem;

using namespace simodo::dsl::server;
using namespace simodo::dsl;
using namespace std;

namespace
{
static ModuleImportResults dummy_result {};

}

ModuleManager::ModuleManager( ModuleSupplier module_supplier
                            , Initializer init
                            )
    : ModulesManagement( init.m
                       , init.rtm
                       , init.gm
                       , init.path_to_grammars
                       , nullptr
                       )
    , _module_supplier(module_supplier)
{
}

const ModuleImportResults &ModuleManager::loadModule( const Token & module_file_token
                                                    , const Token & grammar_name_token
                                                    )
{
    // if (_mm != nullptr)
    //     return _mm->loadModule(op, id_name);

    if (_gm == nullptr && _path_to_grammars.empty()) {
        _m.reportError(module_file_token, u"Контекст использования не предусматривает работу с модулями");
        return dummy_result;
    }

    fs::path module_path = module_file_token.getLocation().file_name;
    if (module_path.empty()) {
        _m.reportError(module_file_token, u"Невозможно определить контекст подключения модуля");
        return dummy_result;
    }

    module_path.replace_filename(module_file_token.getLexeme());

    auto module_script = _module_supplier(module_path.string());
    if (!module_script.has_value()) {
        _m.reportError(module_file_token, u"Файл '" + module_file_token.getLexeme() + u"' не найден");
        return dummy_result;
    }

    if (auto it = _module_set.find(module_path.string()); it != _module_set.end())
    {
        if (!(it->second.ok))
            _m.reportError(module_file_token, u"Ошибки в модуле '" + module_file_token.getLexeme() + u"'");
        return it->second;
    }

    u16string grammar_name = grammar_name_token.getLexeme();
    if (grammar_name.empty())
        grammar_name = module_path.extension().u16string().substr(1);

    auto [it_module,it_ok] = _module_set.insert({
        module_path.string(),
        {AstNode(module_path.u16string()), {}, {}, true}});

    if (!it_ok) {
        _m.reportError(module_file_token, u"Сбой при добавлении модуля '" + module_file_token.getLexeme() + u"'");
        return dummy_result;
    }

    bool                  ok     = it_ok;
    ModuleImportResults & module = it_module->second;

    Grammar               g;
    const Grammar *       p_g;

    if (_gm == nullptr) {
        std::vector<std::string> paths {_path_to_grammars};
        GrammarLoader            loader(_m, paths, convertToU8(grammar_name));

        if (!loader.load(false, g))
        {
            _m.reportError(grammar_name_token.getLexeme().empty() ? module_file_token : grammar_name_token, 
                           u"Невозможно загрузить грамматику '" + grammar_name + u"'");
            return dummy_result;
        }
        p_g = &g;
    }
    else {
        if (!_gm->loadGrammar(false,convertToU8(grammar_name))) {
            _m.reportError(grammar_name_token.getLexeme().empty() ? module_file_token : grammar_name_token, 
                           u"Невозможно загрузить грамматику '" + grammar_name + u"'");
            return dummy_result;
        }
        p_g = & _gm->getGrammar(convertToU8(grammar_name));
    }

    Parser      p(module_path.string(), _m, *p_g);
    AstBuilder  builder(_m, module.ast, p_g->handlers);

    stringstream ss;
    ss.str(*module_script);
    FileStream in(ss);

    ok = p.parse(in, builder);

    if (!ok) {
        _m.reportError(module_file_token, u"Ошибки синтаксиса в модуле '" + module_file_token.getLexeme() + u"'");
        module.ok = false;
        return module;
    }

    std::vector<SemanticScope> scope_set;

    ScriptC_Semantics checker(_m, *this, module.names, scope_set, &module.functions);

    ScriptC_NS_Sys           sys(_rtm);
    ScriptC_NS_Math          math;
    ScriptC_NS_Scene         scene;
    ScriptC_NS_TableFunction tf;

    checker.importNamespace(u"sys", sys.getNamespace());
    checker.importNamespace(u"math", math.getNamespace());
    checker.importNamespace(u"scene", scene.getNamespace());
    checker.importNamespace(u"tf", tf.getNamespace());

    if (!checker.check(module.ast)) {
        _m.reportError(module_file_token, u"Ошибки семантики в модуле '" + module_file_token.getLexeme() + u"'");
        module.ok = false;
        return module;
    }

    return module;
}
