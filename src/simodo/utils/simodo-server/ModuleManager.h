/*
MIT License

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов,

https://bmstu.codes/lsx/simodo/stars
*/

#ifndef SIMODO_DSL_SERVER_ModuleManager
#define SIMODO_DSL_SERVER_ModuleManager

/*! \file ModulesManagement.h
    \brief Управление модулями.
*/

#include "simodo/dsl/ModulesManagement.h"

#include <map>
#include <functional>
#include <optional>

namespace simodo::dsl
{
    class AReporter;
    class GrammarManagement;
}

namespace simodo::dsl::server
{
    class ModuleManager : public ModulesManagement
    {
    public:
        using ModuleSupplier
            = std::function<std::optional<std::string>(std::string)>;

        struct Initializer
        {
            AReporter & m;
            AReporter & rtm;
            GrammarManagement * gm;
            // ModulesManagement * mm = nullptr;
            std::string path_to_grammars = "";
        };

        ModuleManager() = delete;

        ModuleManager(ModuleSupplier module_supplier, Initializer init);

        const ModuleImportResults & loadModule( const Token & module_file_token
                                              , const Token & grammar_name_token
                                              ) override;
    
    protected:
        ModuleSupplier _module_supplier;
    };

}

#endif // SIMODO_DSL_SERVER_ModuleManager
