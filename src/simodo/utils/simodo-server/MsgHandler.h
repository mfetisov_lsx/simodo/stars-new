#ifndef SIMODO_DSL_SERVER_MsgHandler
#define SIMODO_DSL_SERVER_MsgHandler

#include <QtCore>

#include <optional>

namespace simodo::dsl::server
{
    class MsgHandler
    {
    public:
        virtual ~MsgHandler() = default;

        virtual std::optional<int> handle(QJsonObject msg) = 0;
    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_MsgHandler
