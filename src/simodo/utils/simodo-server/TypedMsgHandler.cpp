#include "TypedMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::dsl::server;

TypedMsgHandler::TypedMsgHandler(QString type)
    : _type(type)
{}

std::optional<int> TypedMsgHandler::handle(QJsonObject msg)
{
    if (JsonUtility::findString(msg, "type") != _type)
    {
        return {};
    }

    return 0;
}
