#include "StartMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::dsl::server;

StartMsgHandler::StartMsgHandler( InterpreterIsRunningCallback is_running_callback
                                , StartInterpreterCallback start_callback
                                )
    : TypedMsgHandler("start")
    , _is_running_callback(is_running_callback)
    , _start_callback(start_callback)
{}

std::optional<int> StartMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return {};
    }

    if (_is_running_callback())
    {
        return {3};
    }

    auto file_path_o = JsonUtility::findString(msg, "path");
    if (!file_path_o)
    {
        return {5};
    }

    auto script_o = JsonUtility::findString(msg, "script");
    if (!script_o)
    {
        return {4};
    }

    auto file_path = (*file_path_o).toStdString();
    auto script = (*script_o).toStdString();
    _start_callback(file_path, script);

    return {0};
}