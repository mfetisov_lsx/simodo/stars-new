#ifndef SIMODO_DSL_SERVER_TypedMsgHandler
#define SIMODO_DSL_SERVER_TypedMsgHandler

#include "MsgHandler.h"

namespace simodo::dsl::server
{
    class TypedMsgHandler : public MsgHandler
    {
    public:
        TypedMsgHandler(QString type);

        virtual ~TypedMsgHandler() = default;

        std::optional<int> handle(QJsonObject msg) override;

    protected:
        QString _type;
    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_TypedMsgHandler
