#ifndef SIMODO_DSL_SERVER_StdIoService
#define SIMODO_DSL_SERVER_StdIoService

#include "IoService.h"

#include <boost/asio.hpp>

#include <atomic>
#include <memory>

namespace simodo::dsl::server
{

class StdIoService : public IoService
{
public:
    StdIoService(boost::asio::io_service & io);

    void asyncRead( ReadCallback read_callback
                      = DEFAULT_READ_CALLBACK
                  ) override;
    void asyncWrite( std::string str
                   , WriteCallback write_callback
                       = DEFAULT_WRITE_CALLBACK
                   ) override;

private:
    boost::asio::io_service & _io;
    boost::asio::io_service::strand _write_strand;
    ReadCallback _read_callback;

    void _postAsyncRead();
};

} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_StdIoService
