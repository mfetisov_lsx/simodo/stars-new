#include "ModuleMsgHandler.h"

#include "JsonUtility.h"

using namespace simodo::dsl::server;

ModuleMsgHandler::ModuleMsgHandler( InterpreterIsRunningCallback is_running_callback
                                  , StoreModuleCallback start_callback
                                  )
    : TypedMsgHandler("module")
    , _is_running_callback(is_running_callback)
    , _store_module_callback(start_callback)
{}

std::optional<int> ModuleMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return {};
    }

    if (!_is_running_callback())
    {
        return {8};
    }

    auto file_path_o = JsonUtility::findString(msg, "path");
    if (!file_path_o)
    {
        return {9};
    }

    auto script_o = JsonUtility::findString(msg, "script");
    auto error_o = JsonUtility::findString(msg, "error");
    if (!script_o && !error_o)
    {
        return {10};
    }

    auto file_path = (*file_path_o).toStdString();
    auto script = script_o.value_or("").toStdString();
    auto error = error_o.value_or("").toStdString();

    _store_module_callback({file_path, script}, error);
    return {0};
}