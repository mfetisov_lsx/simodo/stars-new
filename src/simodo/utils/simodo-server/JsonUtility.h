#ifndef SIMODO_DSL_JsonUtility
#define SIMODO_DSL_JsonUtility

#include <QtCore>

#include <optional>

namespace simodo::dsl::server::JsonUtility
{
    std::optional<double> findNumber(QJsonObject & o, QString key);
    std::optional<QString> findString(QJsonObject & o, QString key);
} // simodo::dsl::server::JsonUtility

#endif // SIMODO_DSL_JsonUtiltiy
