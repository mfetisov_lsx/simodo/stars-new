#include "COutListener.h"

#include <QtCore>

#include <iostream>

using namespace simodo::dsl;
using namespace simodo::dsl::server;

COutListener::COutListener(IoService & io)
    : _io(io)
{}

void COutListener::report(const SeverityLevel level,
                            TokenLocation ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge)
{
    if (!briefly.empty() && briefly.at(0) == '#')
    {
        QJsonObject jo;
        jo.insert("type", "record");
        jo.insert("value", QString::fromStdU16String(briefly));

        auto jt = QJsonDocument(jo).toJson(QJsonDocument::Compact).toStdString();
        _io.asyncWrite(jt);
        return;
    }

    QString severity_level =
        level == SeverityLevel::Information
            ? "info"
            : level == SeverityLevel::Warning
            ? "warning"
            : level == SeverityLevel::Error
            ? "error"
            : level == SeverityLevel::Fatal
            ? "fatal"
            : "unknown";
    
    QJsonObject jo;
    jo.insert("type", "message");
    jo.insert("level", severity_level);
    jo.insert("briefly", QString::fromStdU16String(briefly));
    jo.insert("atlarge", QString::fromStdU16String(atlarge));

    auto jt = QJsonDocument(jo).toJson(QJsonDocument::Compact).toStdString();
    _io.asyncWrite(jt);
}
