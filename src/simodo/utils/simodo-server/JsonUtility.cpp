#include "JsonUtility.h"

using namespace simodo::dsl::server;

std::optional<double> JsonUtility::findNumber(QJsonObject & o, QString key)
{
    auto num_it = o.find(key);
    if (num_it == o.end() || !(*num_it).isDouble())
    {
        return {};
    }
    return (*num_it).toDouble();
}

std::optional<QString> JsonUtility::findString(QJsonObject & o, QString key)
{
    auto str_it = o.find(key);
    if (str_it == o.end() || !(*str_it).isString())
    {
        return {};
    }
    return (*str_it).toString();
}
