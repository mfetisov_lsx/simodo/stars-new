#include "CallbackTypedMsgHandler.h"

using namespace simodo::dsl::server;

CallbackTypedMsgHandler::CallbackTypedMsgHandler(QString type, CallbackTypedMsgHandler::Callback callback)
            : TypedMsgHandler(type)
            , _callback(callback)
{}

std::optional<int> CallbackTypedMsgHandler::handle(QJsonObject msg)
{
    if (!TypedMsgHandler::handle(msg))
    {
        return {};
    }

    return _callback(msg);
}
