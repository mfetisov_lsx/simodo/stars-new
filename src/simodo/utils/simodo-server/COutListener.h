#ifndef SIMODO_DSL_SERVER_COutListener
#define SIMODO_DSL_SERVER_COutListener

#include "IoService.h"

#include "simodo/dsl/AReporter.h"

namespace simodo::dsl::server
{
    class COutListener: public simodo::dsl::AReporter
    {
    public:
        COutListener(IoService & io);
        virtual ~COutListener() override = default;

        virtual void report(const simodo::dsl::SeverityLevel level,
                            simodo::dsl::TokenLocation ,
                            const std::u16string & briefly,
                            const std::u16string & atlarge) override;

    private:
        IoService & _io;
    };
}

#endif // SIMODO_DSL_SERVER_COutListener
