#ifndef SIMODO_DSL_SERVER_ModuleMsgHandler
#define SIMODO_DSL_SERVER_ModuleMsgHandler

#include "TypedMsgHandler.h"

#include "InterpreterWrapper.h"

namespace simodo::dsl::server
{
    class ModuleMsgHandler : public TypedMsgHandler
    {
    public:
        using InterpreterIsRunningCallback = std::function<bool()>;
        using StoreModuleCallback = std::function<void(Module, std::string)>;

        ModuleMsgHandler( InterpreterIsRunningCallback is_running_callback
                        , StoreModuleCallback start_callback
                        );

        virtual ~ModuleMsgHandler() = default;

        std::optional<int> handle(QJsonObject msg) override;

    private:
        InterpreterIsRunningCallback _is_running_callback;
        StoreModuleCallback _store_module_callback;

    };
} // simodo::dsl::server

#endif // SIMODO_DSL_SERVER_ModuleMsgHandler
