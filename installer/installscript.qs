function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    component.createOperations();

    if (installer.value("os") == "win") 
    {
        var editPath = "@TargetDir@/bin/simodoedit.exe";
        var iconName = "Simodo-simodoedit.ico";

        component.addOperation( "CreateShortcut"
                              , "@TargetDir@/bin/simodoedit.exe"
                              , "@StartMenuDir@/SimodoEdit.lnk",
                              );

        component.addOperation( "CreateShortcut"
                              , "@TargetDir@/maintenancetool.exe"
                              , "@StartMenuDir@/UninstallSimodoEdit.lnk"
                              );

        var sc0FileType = "sc0";

        //component.addOperation("RegisterFileType",
        //    sc0FileType,
        //    editPath + " '%1'",
        //    "ScriptC stage 0 file type",
        //    "text/plain",
        //    "@TargetDir@/icons/" + iconName,
        //    "ProgId=BMSTU.SIMODOedit." + sc0FileType);
    }

    if (installer.value("os") == "x11") 
    {
        //component.addOperation( "Mkdir", "@HomeDir@/lib" );
        //component.addOperation( "CopyDirectory", "@TargetDir@/lib", "@HomeDir@/lib" );
        component.addOperation( "InstallIcons", "@TargetDir@/icons" );

        component.addOperation( "CreateDesktopEntry"
                              , "codes.bmstu.simodo.simodoedit.desktop"
                              , "Type=Application\nExec=@TargetDir@/bin/simodoedit\nName=SIMODO edit\nGenericName=The IDE of SIMODO modeling system.\nGenericName[ru]=Интегрированная среда разработки SIMODO.\nIcon=Simodo-simodoedit\nTerminal=false\nCategories=Development;IDE;SIMODO;\n\n"
                              );
    }
}
